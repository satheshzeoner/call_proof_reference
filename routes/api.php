<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
/*
|----------------------------------------------------------------------
|    Call Proof API
|----------------------------------------------------------------------
|
 */

/* Prefix For All API */

Route::prefix('api')->group(function () {

  /*
  |----------------------------------------------------------------------
  |    LOGIN AND REGISTRATION
  |----------------------------------------------------------------------
  |
 */

  /* User Credentials Checking */
  Route::post('login', 'API\AuthController@login')
    ->name('login');

  /* Signup New User in CallProof */
  Route::post('register', 'API\Users\RegisterController@userRegister')
    ->name('register');

  /* Login With Google signup/Login */
  Route::post('google-login', 'API\Users\GoogleController@googleLogin')
    ->name('google-login');

  /* Login With Microsoft signup/Login */
  Route::post('microsoft-login', 'API\Users\MicrosoftController@microsoftLogin')
    ->name('microsoft-login');

  /* Login With Apple signup/Login */
  Route::post('apple-login', 'API\Users\AppleController@appleLogin')
    ->name('apple-login');

  /* 
  |----------------------------------------------------------------------
  |    AUTHORIZED API
  |----------------------------------------------------------------------
  |
 */

  Route::group(['middleware' => ['auth:api']], function () {

    /* =============================DASHBOARD============================= */

    /* Dashboard Details Graph,Calls,Appointments */
    Route::post('dashboard-details', 'API\Dashboard\DashboardController@dashboardDetails');

    /* Dashboard Widgets Get Details */
    Route::get('widgets', 'API\Dashboard\DashboardController@getWidgetOrdering');

    /* Dashboard Widgets Update Details */
    Route::put('widgets', 'API\Dashboard\DashboardController@widgetOrdering');

    /* =============================DASHBOARD-END============================= */

    /* =============================CONTACT============================= */

    /* Retrieve all Contact List Details */
    Route::get('contact-list', 'API\Contact\ContactController@contactList');

    /* Retrieve the Contact List Based on ContactId */
    Route::get('contacts/{id}', 'API\Contact\ContactController@contactData');

    /* Update the Contact */
    Route::put('contacts/{id}', 'API\Contact\ContactController@editContact');

    /* Retrieve the Contact Menu Details */
    Route::post('contact-menu', 'API\Contact\ContactController@contactMenu');

    /* Retrieve the Contact Details Related Opportunities Based on ContactId */
    Route::get('contacts/{id}/opportunities', 'API\Contact\ContactController@getContactOpportunitiesList');

    /* Retrieve the Opportunity Based on ContactId */
    Route::get('contacts/{id}/opportunities/{opportunity_id}', 'API\Contact\ContactController@getContactOpportunity');

    /* Create a New Opportunities */
    Route::post('contacts/{id}/opportunities', 'API\Contact\ContactController@addContactOpportunities');

    /* Update the ContactId Based Opportunities */
    Route::put('contacts/{id}/opportunities/{opportunity_id}', 'API\Contact\ContactController@editContactOpportunities');

    /* Delete the ContactId Based Opportunities */
    Route::delete('contacts/{id}/opportunities/{opportunity_id}', 'API\Contact\ContactController@removeContactOpportunity');

    /* Get Current user Contact Type */
    Route::get('contact-type', 'API\Contact\ContactController@getcontactType');

    /* Create a New Contact */
    Route::post('contacts', 'API\Contact\ContactController@addContact');

    /* Get Reverse Geocode */
    Route::get('contact/get/address/latitude/{latitude}/longitude/{longitude}', 'API\Contact\ContactController@getReverseGeocode');

    /* Start Appointment For Contact */
    Route::post('contact/start-appointment', 'API\Contact\ContactController@contactStartAppointment');

    /* Get Current Appointment for Current user */
    Route::get('current-appointment', 'API\Contact\ContactController@contactCurrentAppointment');

    /* Get EndAppointment Details for Current user */
    Route::get('end-appointment', 'API\Contact\ContactController@getContactEndAppointment');

    /* Update the End Appointment Details */
    Route::post('end-appointment', 'API\Contact\ContactController@saveContactEndAppointment');

    /* Get Current User Company People Role */
    Route::get('contact/people-role', 'API\Contact\ContactController@getPeopleRole');

    /* Update the Contact Company Details */
    Route::put('contacts/{id}/company-details', 'API\Contact\ContactController@storeCompanyDetails');

    /* Retrieve the Contact Personnel Based on ContactId */
    Route::get('contacts/{id}/contact-personnel', 'API\Contact\ContactController@getContactPersonnel');

    /* Retrieve the Contact Personnel Based on ContactId */
    Route::get('contacts/{id}/contact-personnel/{personnel_id}', 'API\Contact\ContactController@getSpecificContactPersonnel');

    /* Create a New Contact Personnel */
    Route::post('contacts/{id}/contact-personnel', 'API\Contact\ContactController@addContactPersonnel');

    /* Update the Contact Personnel Details */
    Route::put('contacts/{id}/contact-personnel/{personnel_id}', 'API\Contact\ContactController@updateContactPersonnel');

    /* Delete the Contact Personnel Detail*/
    Route::delete('contacts/{id}/contact-personnel/{personnel_id}', 'API\Contact\ContactController@removeContactPersonnel');

    /* Move the Personnel to Another Contact */
    Route::put('contact/move-personnel', 'API\Contact\ContactController@moveContactPersonnel');

    /* Retrieve the Contact Phone Based on ContactId */
    Route::get('contacts/{id}/phone', 'API\Contact\ContactController@getContactPhone');

    /* Add the Contact Phone Based on ContactId */
    Route::post('contacts/{id}/phone', 'API\Contact\ContactController@addContactPhone');

    /* Update the Contact Phone Based on ContactId */
    Route::put('contacts/{id}/phone/{phone_id}', 'API\Contact\ContactController@updateContactPhone');

    /* Delete the Contact Phone Based on ContactId */
    Route::delete('contacts/{id}/phone/{phone_id}', 'API\Contact\ContactController@removeContactPhone');

    /* Retrieve the Contact Info Email Based on ContactId */
    Route::get('contacts/{id}/info-email', 'API\Contact\ContactController@getContactInfoEmail');

    /* Add New Contact Info Email */
    Route::post('contacts/{id}/info-email', 'API\Contact\ContactController@addContactInfoEmail');

    /* Update the Contact Info Email */
    Route::put('contacts/{id}/info-email/{email_id}', 'API\Contact\ContactController@updateContactInfoEmail');

    /* Delete the Contact Info Email */
    Route::delete('contacts/{id}/info-email/{email_id}', 'API\Contact\ContactController@removeContactInfoEmail');

    /* Update the Contact Selected Users */
    Route::put('contacts/{id}/selected-users', 'API\Contact\ContactController@updateSelectedUsers');

    /* Update the Contact Selected Personnel */
    Route::put('contacts/{id}/selected-contact-personnel', 'API\Contact\ContactController@updateSelectedcontactPersonnel');

    /* Get Notes For Specific Contact */
    Route::get('contact/{id}/contact-note', 'API\Contact\ContactController@getContactNote');

    /* Add New Notes For Specific Contact */
    Route::post('contact/{id}/contact-note', 'API\Contact\ContactController@addContactNote');

    /* Get Images List For Specific Contact */
    Route::get('contact/{id}/images', 'API\Contact\ContactImageController@getImages');

    /* Update Image For Specific Contact */
    Route::post('contact/{id}/image', 'API\Contact\ContactImageController@uploadImage');

    /* Show Specific Contact Image */
    Route::get('contact/{id}/image/{image_id}', 'API\Contact\ContactImageController@showImage');

    /* Delete Contact Image */
    Route::delete('contact/{id}/image/{image_id}', 'API\Contact\ContactImageController@removeContactImage');

    /* Get Label List For Specific Contact */
    Route::get('contact/{id}/labels', 'API\Contact\ContactLabelController@getContactLabels');

    /* Add New labels For Specific Contact */
    Route::post('contact/{id}/label', 'API\Contact\ContactLabelController@addContactLabel');

    /* Delete Contact label For Specific Contact */
    Route::delete('contact/{id}/label/{label_id}', 'API\Contact\ContactLabelController@removeContactLabel');

    /* Get Contact Custom Fields Detail */
    Route::get('contact/custom-fields', 'API\Contact\ContactController@getContactFilterCustomFields');

    /* Get Contact Parent Company Detail  */
    Route::get('contact/parent-company', 'API\Contact\ContactController@getContactParentCompany');

    /* =============================CONTACT-END============================= */

    /* =============================EVENTFORM============================= */

    /* EventForm Previous List With Searching */
    Route::get('contact/{contact_id}/eventform', 'API\Eventform\EventFormController@eventPreviousList');

    /* EventForm Select Before the Add EventForm */
    Route::get('contact/{contact_id}/eventformselect/{event_form_id?}', 'API\Eventform\EventFormController@eventSelectList');

    /* EventForm With People */
    Route::get('contact/{contact_id}/eventformpeople', 'API\Eventform\EventFormController@eventFormPeopleList');

    /* EventForm Add Process Get Details */
    Route::get('contact/{contact_id}/eventform/{event_form_id}', 'API\Eventform\EventFormController@eventFormAddProcessDetails');

    /* EventForm Add Process Enabled Checkbox for SchedultTask Details */
    Route::get('contact/eventformscheduletask', 'API\Eventform\EventFormController@eventFormScheduleDetails');

    /* EventForm Store Process  */
    Route::post('contact/{contact_id}/eventformstore/{event_form_id}', 'API\Eventform\EventFormController@eventForminsertProcessDetails');

    /* EventForm Update Process Details */
    Route::get('contact/{contact_id}/eventformedit/{event_form_id}/contacteventform/{contact_event_form_id}', 'API\Eventform\EventFormController@eventFormEditProcessDetails');

    /* EventForm Put Process Details */
    Route::post('contact/{contact_id}/eventformedit/{event_form_id}/contacteventform/{contact_event_form_id}', 'API\Eventform\EventFormController@eventFormPutProcessDetails');

    /* EventForm Remove Process */
    Route::delete('contacteventform/{contact_event_form_id}', 'API\Eventform\EventFormController@eventFormDestroy');

    /* =============================EVENTFORM-END============================= */

    /* =============================TASK============================= */

    /* Get Task Details For Specific Task Id */
    Route::get('contact/{contact_id}/task/{task_id}', 'API\Task\TaskController@getTaskDetails');

    /* Task / Shedule With / FollowupTask / Taskassignment */
    Route::post('contact/{contact_id}/task', 'API\Task\TaskController@taskAddProcess');

    /* Task Details Updation Based on Task Id */
    Route::put('contact/{contact_id}/task/{task_id}', 'API\Task\TaskController@taskUpdateProcess');

    /* Task Type */
    Route::get('task-type', 'API\Task\TaskController@taskType');

    /* Task */
    Route::get('task-completion', 'API\Task\TaskController@taskDetails');

    /* Task Updation */
    Route::put('task-completion/{task_id}', 'API\Task\TaskController@taskCompletionUpdate');

    /* =============================TASK END============================= */

    /* =============================ROUTES============================= */

    /* Store Routes */
    Route::post('routes', 'API\Route\RouteController@storeCurrentRoute');

    /* Update Routes */
    Route::put('routes/{route_id}', 'API\Route\RouteController@updateCurrentRoute');

    /* Current Routes */
    Route::get('current-route/{route_id}', 'API\Route\RouteController@getCurrentRoute');

    /* My Routes */
    Route::get('my-routes', 'API\Route\RouteController@getMyRoute');

    /* All Routes */
    Route::get('all-routes', 'API\Route\RouteController@getAllRoute');

    /* Delete Specific Routes */
    Route::delete('routes/{route_id}', 'API\Route\RouteController@removeRoute');

    /* =============================ROUTES-END============================= */

    /* =============================SETTINGS============================= */

    /* Settings Details */
    Route::get('app-settings', 'API\Settings\SettingsController@settings');

    /* Settings to Put Current user Preference Configuration */
    Route::put('app-settings', 'API\Settings\SettingsController@updateSettings');

    /* Get Settings-info */
    Route::get('user-settings', 'API\Settings\SettingsController@getSettingsInfo');

    /* =============================SETTINGS-END============================= */

    /* =============================PLACES================================= */

    /* Get Places Category */
    Route::get('place-category', 'API\Places\PlacesController@placeCategoryList');

    /* Get Nearest Places Details */
    Route::get('places/latitude/{latitude}/longitude/{longitude}', 'API\Places\PlacesController@getNearestPlacesDetails');

    /* Nearest Places Add to CallProof Contacts */
    Route::post('place-contact', 'API\Places\PlacesController@storePlaceContact');

    /* =============================PLACES-END============================= */

    /* =============================EVENTS================================= */

    /* Get Event-Type Details */
    Route::get('event-type', 'API\Event\EventController@eventTypeList');

    /* Get Events History FrontPage Details */
    Route::get('events', 'API\Event\EventController@eventsDetails');

    /* =============================EVENTS-END============================= */

    /* State Details */
    Route::get('state', 'HomeController@state');

    /* Country Details */
    Route::get('country', 'HomeController@country');

    /* Phone-Type Details */
    Route::get('phone-type', 'HomeController@phoneType');

    /* TwilioCountry Details */
    Route::get('twilio-country', 'HomeController@twilioCountry');

    /* Opportunity Details */
    Route::get('opportunities', 'API\Opportunity\OpportunityController@opportunities');

    /* Update the Opportunity Details */
    Route::put('opportunities/{id}', 'API\Opportunity\OpportunityController@updateOpportunities');

    /* Delete Specific Opportunity */
    Route::delete('opportunities/{id}', 'API\Opportunity\OpportunityController@removeOpportunity');

    /* Retrieve the Opportunity Type Details */
    Route::get('opportunities-type', 'API\Opportunity\OpportunityController@opportunitiesType');

    /* Retrieve the Opportunity Stage Details */
    Route::get('opportunities-stage', 'API\Opportunity\OpportunityController@opportunitiesStage');

    /* Sales Users Details */
    Route::get('users', 'HomeController@getUsers');

    /* Team List */
    Route::get('team', 'API\Team\TeamController@getTeam');

    /* Label List */
    Route::get('labels', 'API\Label\LabelController@getLabels');

    /* Forgot Password */
    Route::post('forgot-password', 'API\Users\ForgotPasswordController@forgotPassword');

    /* User Profile Update */
    Route::put('user-profile', 'API\Users\UserProfileController@profileUpdate');

    /* Logout */
    Route::get('logout', 'API\Users\LogoutController@logoutUser');
  });

  /* MiddleWare Access for Check user Role */
  Route::group(['middleware' => ['auth:api', 'userAccessible']], function () {

    /* Opportunity Details Filter */
    Route::get('opportunities/user/{user_id}', 'API\Opportunity\OpportunityController@opportunities');
  });

  /* RefreshToken Based Access Token */
  Route::get('access-token', 'API\AuthController@getAccessToken');

  /* If there is no Url Available in Routes Response */
  Route::fallback(function () {
    return response()->json([
      'message' => 'API Not Found'
    ], 404);
  });
});
