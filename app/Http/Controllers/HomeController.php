<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Auth;

/* Service */
use App\Services\State\StateService;
use App\Services\Country\CountryService;
use App\Services\PhoneType\PhoneTypeService;
use App\Services\TwilioCountry\TwilioCountryService;
use App\Services\User\UserService;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->StateService = new StateService();
        $this->CountryService = new CountryService();
        $this->PhoneTypeService = new PhoneTypeService();
        $this->TwilioCountryService = new TwilioCountryService();
        $this->UserService = new UserService();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/state",
     * tags={"State"},
     * summary="Get Current user State",
     * operationId="State Api",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    public function state(Request $request)
    {
        $userId = Auth::user()->id;
        $stateResponse = $this->StateService->getState($request, $userId);
        if ($stateResponse['status'] == true) {
            return Helper::customResponse(200, $stateResponse['response'], $request->all(), config('constants.ServiceName.State'));
        } else {
            return Helper::customError(400, $stateResponse['message'], $request->all(), config('constants.ServiceName.State'));
        }
    }
    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/country",
     * tags={"Country"},
     * summary="Get Current user Country",
     * operationId="Country Api",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    public function country(Request $request)
    {
        $userId = Auth::user()->id;
        $countryResponse = $this->CountryService->getCountry($request, $userId);
        if ($countryResponse['status'] == true) {
            return Helper::customResponse(200, $countryResponse['response'], $request->all(), config('constants.ServiceName.Country'));
        } else {
            return Helper::customError(400, $countryResponse['message'], $request->all(), config('constants.ServiceName.Country'));
        }
    }
    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/phone-type",
     * tags={"phoneType"},
     * summary="Get Current user phoneType",
     * operationId="phoneType Api",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    public function phoneType(Request $request)
    {
        $userId = Auth::user()->id;
        $phoneTypeResponse = $this->PhoneTypeService->getPhoneType($request, $userId);
        if ($phoneTypeResponse['status'] == true) {
            return Helper::customResponse(200, $phoneTypeResponse['response'], $request->all(), config('constants.ServiceName.PhoneType'));
        } else {
            return Helper::customError(400, $phoneTypeResponse['message'], $request->all(), config('constants.ServiceName.PhoneType'));
        }
    }
    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/twilio-country",
     * tags={"TwilioCountry"},
     * summary="Get Current user TwilioCountry",
     * operationId="TwilioCountry Api",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    public function twilioCountry(Request $request)
    {
        $userId = Auth::user()->id;
        $twilioCountryResponse = $this->TwilioCountryService->getTwilioCountry($request, $userId);
        if ($twilioCountryResponse['status'] == true) {
            return Helper::customResponse(200, $twilioCountryResponse['response'], $request->all(), config('constants.ServiceName.TwilioCountry'));
        } else {
            return Helper::customError(400, $twilioCountryResponse['message'], $request->all(), config('constants.ServiceName.TwilioCountry'));
        }
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/users",
     * tags={"Users"},
     * summary="Get Users Based on Current User Company",
     * operationId="Users API",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    public function getUsers(Request $request)
    {
        $userId = Auth::user()->id;
        $usersResponse = $this->UserService->getUsers($request, $userId);
        if ($usersResponse['status'] == true) {
            return Helper::customResponse(200, $usersResponse['response'], $request->all(), config('constants.ServiceName.User'));
        } else {
            return Helper::customError(400, $usersResponse['message'], $request->all(), config('constants.ServiceName.User'));
        }
    }
}
