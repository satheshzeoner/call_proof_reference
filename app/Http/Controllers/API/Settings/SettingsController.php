<?php

namespace App\Http\Controllers\API\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Validator;

/* Models */
use App\Models\Auth\AuthUser;

/* Service */
use App\Services\Setting\SettingServices;

/* Repositories */

class SettingsController extends Controller
{

    public function __construct()
    {
        $this->SettingServices = new SettingServices();
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/app-settings",
     * tags={"Settings"},
     * summary="Settings to Get Current user Preference Configuration",
     * operationId="Settings Api",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =              Settings                     =
    # =============================================


    public function settings(Request $request)
    {
        $userId = Auth::user()->id;
        $settingsResponse = $this->SettingServices->getSettings($request, $userId);
        if ($settingsResponse['status'] == true) {
            return Helper::customResponse(200, $settingsResponse['response'], $request->all(), config('constants.ServiceName.Settings'));
        } else {
            return Helper::customError(400, $settingsResponse['message'], $request->all(), config('constants.ServiceName.Settings'));
        }
    }

    /**
     *  @SWG\Put( 
     *    security = { { "Bearer": {} } },
     *    path="/app-settings",
     *    tags={"Settings"},
     *    summary="Settings to Put Current user Preference Configuration",
     *    operationId="Settings API",
     * @SWG\Parameter(
     *     name="sync_task_on_device_calender",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="hide_mobile_appointment_popup",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     * @SWG\Parameter(
     *     name="task_popup_setting",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     * @SWG\Parameter(
     *     name="is_contact_page_as_homepage",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     * @SWG\Parameter(
     *     name="task_notification_time_id",
     *     in="formData",
     *     required=false, 
     *     type="integer", 
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =              Settings                     =
    # =============================================


    public function updateSettings(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sync_task_on_device_calender' => 'required|boolean',
            'hide_mobile_appointment_popup' => 'required|boolean',
            'task_popup_setting' => 'required|boolean',
            'is_contact_page_as_homepage' => 'required|boolean',
            'task_notification_time_id' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.ContactMenu'));
            return response()->json($result, 200);
        }
        $userId = Auth::user()->id;
        $settingsResponse = $this->SettingServices->updateSettings($request, $userId);
        if ($settingsResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $settingsResponse['message'], $request->all(), config('constants.ServiceName.Settings'));
        } else {
            return Helper::customError(400, $settingsResponse['message'], $request->all(), config('constants.ServiceName.Settings'));
        }
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/user-settings",
     * tags={"Settings"},
     * summary="User Settings to Get Users,Company Settings Configuration",
     * operationId="Settings-info Api",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    # =============================================
    # =              Settings-info                =
    # =============================================

    public function getSettingsInfo(Request $request)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        $settingsInfo = $this->SettingServices->getSettingsInfo($request, $userId, $companyId);

        if ($settingsInfo['status'] == true) {
            return Helper::customResponse(200, $settingsInfo['response'], $request->all(), config('constants.ServiceName.Settings'));
        } else {
            return Helper::customError(400, $settingsInfo['message'], $request->all(), config('constants.ServiceName.Settings'));
        }
    }
}
