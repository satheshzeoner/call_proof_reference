<?php

namespace App\Http\Controllers\API\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Helpers\Helper;
use Carbon\Carbon;

/* Service */
use App\Services\Dashboard\DashboardService;
use App\Services\Dashboard\DashboardWidgetsService;

/* Repositories */
use App\Repositories\Dashboard\DashboardWidgetsRepositories;

/* Models */
use App\Models\Widget\Widget;

/** 
 * @SWG\Post(
 * security = { { "Bearer": {} } },
 * path="/dashboard-details",
 * tags={"Dashboard"},
 * summary="Return high-level stats that should appear on the primary dashboard",
 * operationId="Dasboard Api",
 *     consumes={"application/x-www-form-urlencoded"},
 *      @SWG\Parameter(
 *      name="timeframe",
 *      in="formData",
 *      required=true,
 *      type="string",
 *      description="1w/4w/1y/mtd/qtd/ytd/all",
 *      ),
 * @SWG\Response(response=200, description="Successful request, often a GET"),
 * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
 * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
 * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
 * )
 *
 */

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->DashboardService             = new DashboardService();
        $this->DashboardWidgets             = new DashboardWidgetsService();
        $this->Widget                       = new Widget();
        $this->DashboardWidgetsRepositories = new DashboardWidgetsRepositories();

        $this->chartCapacitySize            = 5;
        $this->widgetOrder                  = "Appointments,Calls,Emails";
        $this->widgetOrderList              = ['Appointments', 'Calls', 'Emails'];
    }

    # =============================================
    # =       Dashboard detail                    =
    # =============================================

    public function dashboardDetails(Request $request)
    {
        /* BearerToken UserId */
        $userId         = Auth::user()->id;

        /* TimeFram Validation */
        $paramDetails = request()->all();

        /* 
            Year-to-date (YTD)
            quarter-to-date (QTD)
            Month-to-date (QTD)        
        */
        $timeFrameSet = [
            '1w'  => 7,
            '4w'  => 28,
            '1y'  => 365,
            'mtd' => Carbon::parse(Carbon::now()->firstOfMonth())->diffInDays(now()),
            'qtd' => Carbon::parse(Carbon::now()->firstOfQuarter())->diffInDays(now()),
            'ytd' => Carbon::parse(Carbon::now()->startOfYear())->diffInDays(now()),
            'all' => 0,
        ];

        /* ByDefault TimeFrame is Set to One Week */
        if (!isset($paramDetails['timeframe'])) {
            $paramDetails['timeframe'] = '1w';
        }

        if (isset($timeFrameSet[$paramDetails['timeframe']])) {
            $timeFrames = $timeFrameSet[$paramDetails['timeframe']];
        } else {
            $result = Helper::customError(401, config('constants.Errors.timeFrame'), $request->all(), config('constants.ServiceName.Dashboard'));
            return response()->json($result, 200);
        }

        /* if TimeFrame is zero Then get the date of the User created  Date as From and To date Still Todays Date */
        if ($timeFrames == 0) {
            $timeFrames =  $this->getBeginingDateForThisUser($userId);
        }

        /* TimeFram */
        $dates = $this->doTimeFramProgress($timeFrames);

        /* WidgetOrdering Details */
        $activeOrder   = $this->DashboardService->storeWidgetOrdering($userId)->pluck('name');
        $inactiveOrder = $this->DashboardWidgetsRepositories->getInActiveWidgetsOrder($userId)->pluck('name');

        /* Verify the Given Active Details has Valid */
        foreach ($activeOrder as $activeOrdervalue) {
            if (!in_array($activeOrdervalue, $this->widgetOrderList)) {
                $result = Helper::customError(401, config('constants.Errors.widgetOrderIncluded'), $request->all(), config('constants.ServiceName.Dashboard'));
                return response()->json($result, 200);
            }
        }

        /* Verify the Given In-Active Details has Valid */
        foreach ($inactiveOrder as $inactiveOrdervalue) {
            if ($inactiveOrdervalue != '') {
                if (!in_array($inactiveOrdervalue, $this->widgetOrderList)) {
                    $result = Helper::customError(401, config('constants.Errors.widgetOrderMore'), $request->all(), config('constants.ServiceName.Dashboard'));
                    return response()->json($result, 200);
                }
            }
        }

        /* Response Parent Structure */
        $result['events']        = $this->DashboardService->getEventdetails($userId);
        $result['widgets_order'] = $this->DashboardService->getWidgetsOrder($activeOrder);
        $result['widgets']       = $this->DashboardService->getWidgets($userId, $dates);

        $result = Helper::customResponse(200, $result, $request->all(), config('constants.ServiceName.Dashboard'), 1600);
        return response()->json($result, 200);
    }

    public function doTimeFramProgress($durationFram)
    {
        $days    = $durationFram; /* Total Days of TimeFram  */
        $predays =  $durationFram * 2; /* Total Days of Calculate Previous TimeFram Dates */

        /* From Starting and Ending Progress Date */
        $framDate['progressFromDate'] = Carbon::now()->subDays($days)->format('Y-m-d');
        $framDate['progressToDate']   = Carbon::now()->format('Y-m-d');

        /* From Starting and Ending Previous Date */
        $framDate['previousFromDate'] = Carbon::now()->subDays($predays)->format('Y-m-d');
        $framDate['previousToDate']   = Carbon::now()->subDays($days)->format('Y-m-d');

        /* 
        For Calulate DurationTimefram Hours 
        TotalDays * 24 Hours (Perday)
        We Equally Divide to ChartSize
        */
        $framDate['hours']   = ($durationFram * 24) / $this->chartCapacitySize;

        return $framDate;
    }

    /* Get From Date From User Joining Create Date */
    public function getBeginingDateForThisUser($userId)
    {
        $beginDate    = Auth::user()->date_joined;
        return Carbon::parse(Carbon::parse($beginDate))->diffInDays(now());
    }


    # =============================================
    # =       Widget detail                       =
    # =============================================

    /**
     * @SWG\GET(
     * security = { { "Bearer": {} } },
     * path="/widgets",
     * tags={"Widgets"},
     * summary="Retrive the widgets Order Details For Widgets and WidgetsAvailable  ",
     * operationId="widgetsApi",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    /* Widgets GET METHOD */
    public function getWidgetOrdering()
    {
        /* BearerToken UserId */
        $userId         = Auth::user()->id;

        /* Validate Weather User Has WidgetsOrder Details */
        if (Widget::whereUser_id($userId)->exists() == false) {
            $result = Helper::customError(401, config('constants.Errors.unAvailWidgetOrdering'), null, config('constants.ServiceName.Dashboard'));
            return response()->json($result, 200);
        }

        /* widgets */
        $widgetResult['widgets']           = $this->DashboardWidgets->getWidgetsOrderingActive($userId);
        /* widgets Available */
        $widgetResult['widgets_available'] = $this->DashboardWidgets->getWidgetsOrderingInActive($userId);

        $result = Helper::customResponse(200, $widgetResult, null, config('constants.ServiceName.Dashboard'), 1600);
        return response()->json($result, 200);
    }


    /**
     * @SWG\Put(
     * security = { { "Bearer": {} } },
     * path="/widgets",
     * tags={"Widgets"},
     * summary="Update the Widgets Order and Position",
     * operationId="widgets Post Api",
     *     consumes={"application/x-www-form-urlencoded"},
     *      @SWG\Parameter(
     *      name="widgets",
     *      in="formData",
     *      required=true,
     *      type="string",
     *      ),
     *      @SWG\Parameter(
     *      name="widgets_available",
     *      in="formData",
     *      required=true,
     *      type="string",
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    /* Widgets PUT METHOD */
    public function widgetOrdering(Request $request)
    {
        /* BearerToken UserId */
        $userId         = Auth::user()->id;

        /* All Request */
        $paramRequestDetails = $request->all();
        Log::error($request->all());

        $widgets           = collect(json_decode($paramRequestDetails['widgets']));
        $widgets_available = collect(json_decode($paramRequestDetails['widgets_available']));

        $paramDetails['widgets_id']           = $widgets->pluck('id')->toArray();
        $paramDetails['widgets_available_id'] = $widgets_available->pluck('id')->toArray();
        $paramDetails['position']             = $widgets->pluck('position')->toArray();

        /* WidgetOrdering */
        $activeOrder   = ($paramDetails['widgets_id']) ? $paramDetails['widgets_id'] : '';
        $inactiveOrder = ($paramDetails['widgets_available_id']) ? $paramDetails['widgets_available_id'] : '';
        $position      = ($paramDetails['position']) ? $paramDetails['position'] : '';

        /* Validations Widgets */
        $widgetValidationOrdering = $this->DashboardWidgets->doValidationWidgets($activeOrder, $inactiveOrder, $position);
        if ($widgetValidationOrdering['status'] == false) {
            $result = Helper::customError(401, $widgetValidationOrdering['message'], $paramDetails, config('constants.ServiceName.Dashboard'));
            return response()->json($result, 200);
        }
        /* Updating WidgetOrder */
        $widgetOrdering = $this->DashboardWidgets->doStoreWidgetPosition($userId, $activeOrder, $inactiveOrder, $position);

        $result = Helper::customUpdatedResponse(200, config('constants.Success.Widgets'), null, config('constants.ServiceName.Dashboard'), 1600);
        return response()->json($result, 200);
    }
}
