<?php

namespace App\Http\Controllers\API\Contact;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Helper;
use Illuminate\Support\Carbon;


/* Models */
use App\Models\Contact\Contact;
use App\Models\Auth\AuthUser;
use App\Models\Appointment\Appointment;

/* Service */
use App\Services\Contact\ContactListServices;
use App\Services\Contact\ContactMenuServices;
use App\Services\Contact\ContactViewServices;
use App\Services\Opportunity\OpportunityService;
use App\Services\Contact\AddContactService;
use App\Services\Contact\EditContactService;
use App\Services\Contact\ContactService;
use App\Services\Contact\ContactAppointmentService;
use App\Services\Contact\ContactOpportunitiesService;
use App\Services\CustomField\CustomFieldService;
use App\Services\Contact\ContactPersonnelService;




class ContactController extends Controller
{

    public function __construct()
    {
        $this->Contact             = new Contact();
        $this->ContactListServices = new ContactListServices();
        $this->ContactMenuServices = new ContactMenuServices();
        $this->ContactViewServices = new ContactViewServices();
        $this->OpportunityService = new OpportunityService();
        $this->AddContactService = new AddContactService();
        $this->EditContactService = new EditContactService();
        $this->ContactService = new ContactService();
        $this->ContactAppointmentService = new ContactAppointmentService();
        $this->ContactOpportunitiesService = new ContactOpportunitiesService();
        $this->CustomFieldService = new CustomFieldService();
        $this->ContactPersonnelService = new ContactPersonnelService();
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/contact-list",
     * tags={"Contacts"},
     * summary="Retrieve all Contact List Details",
     * operationId="ContactApi",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/contacts/{id}",
     * tags={"Contacts"},
     * summary="Retrieve the Contact List Based on ContactId",
     * operationId="ContactIdApi",
     *  	@SWG\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =           Account AND Contact List      =
    # =============================================

    public function contactList(Request $request)
    {
        $userId = Auth::user()->id;
        $contactResponse = $this->ContactListServices->getContactList($request, $userId);
        if ($contactResponse['status'] == true) {
            return Helper::customResponsePagination(200,  $contactResponse['response']['data'], $contactResponse['paginationDetails'], $request, config('constants.ServiceName.ContactList'));
        } else {
            return Helper::customError(400, $contactResponse['message'], $request->all(), config('constants.ServiceName.ContactList'));
        }
    }

    # =============================================
    # =              Contact View                 =
    # =============================================

    public function contactData(Request $request, $contactId)
    {
        $userId = Auth::user()->id;
        $contactDataResponse = $this->ContactViewServices->getContactData($request, $userId, $contactId);
        if ($contactDataResponse['status'] == true) {
            return Helper::customResponse(200, $contactDataResponse['response'], $request->all(), config('constants.ServiceName.ContactView'));
        } else {
            return Helper::customError(400, $contactDataResponse['message'], $request->all(), config('constants.ServiceName.ContactView'));
        }
        return $contactDataResponse;
    }

    /**
     * @SWG\Post(
     * security = { { "Bearer": {} } },
     * path="/contact-menu",
     * tags={"Contacts"},
     * summary="Retrieve the Contact Menu Details",
     * operationId="ContactMenuApi",
     *      	@SWG\Parameter(
     *          name="contact_id",
     *          in="formData",
     *          required=true, 
     *          type="string" 
     *      ),
     * 	 		@SWG\Parameter(
     *          name="menu_id",
     *          in="formData",
     *          required=true, 
     *          type="string" 
     *      ),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =           Account AND Contact Menu        =
    # =============================================
    public function contactMenu(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'contact_id' => 'required',
        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.ContactMenu'));
            return response()->json($result, 200);
        }
        $userId = Auth::user()->id;
        $contactMenuResponse = $this->ContactMenuServices->getContactMenu($request, $userId);
        if ($contactMenuResponse['status'] == true) {
            return Helper::customResponse(200, $contactMenuResponse['response'], $request->all(), config('constants.ServiceName.ContactMenu'));
        } else {
            return Helper::customError(400, $contactMenuResponse['message'], $request->all(), config('constants.ServiceName.ContactMenu'));
        }
        return $contactMenuResponse;
    }


    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/contacts/{id}/opportunities",
     * tags={"Contact Opportunities"},
     * summary="Retrieve the Contact Details Related Opportunities Based on ContactId",
     * operationId="ContactOpportunityApi",
     * 		@SWG\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */



    /*Contact Opportunities*/
    public function getContactOpportunitiesList(Request $request, $contactId)
    {
        $userId = Auth::user()->id;
        $contactOpportunitiesResponse = $this->OpportunityService->getContactOpportunities($request, $userId, $contactId);
        if ($contactOpportunitiesResponse['status'] == true) {
            return Helper::customResponse(200, $contactOpportunitiesResponse['response'], $request->all(), config('constants.ServiceName.ContactOpportunity'));
        } else {
            return Helper::customError(400, $contactOpportunitiesResponse['message'], $request->all(), config('constants.ServiceName.ContactOpportunity'));
        }
    }


    /**
     * @SWG\Post(
     * security = { { "Bearer": {} } },
     * path="/contacts/{id}/opportunities",
     *    tags={"Contact Opportunities"},
     *    summary="Create a New Opportunities",
     *    operationId="addContactOpportunities API",
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     required=true, 
     *     type="integer", 
     *      ),
     * @SWG\Parameter(
     *     name="opportunity_name",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="linked_accounts",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="linked_contacts",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="opportunity_stage_id",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="opportunity_type_id",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="owner_id",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="value",
     *     in="formData",
     *     required=false,
     *     type="integer", 
     *      ),
     *  @SWG\Parameter(
     *     name="probability",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="close_date",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="notes",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *   @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     *   @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     *   @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function addContactOpportunities(Request $request, $contactId)
    {
        $validator = Validator::make($request->all(), [
            'opportunity_stage_id' => 'required|exists:cp_oppstage,id',
            'opportunity_type_id' => 'required|exists:cp_opptype,id',
            'owner_id' => 'required|exists:auth_user,id',
            'close_date' => 'date',
            'value' => 'numeric',
            'probability' => 'numeric',

        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.ContactMenu'));
            return response()->json($result, 200);
        }
        $userId = Auth::user()->id;
        $addContactOpportunitiesResponse = $this->ContactOpportunitiesService->addContactOpportunities($request, $userId, $contactId);
        if ($addContactOpportunitiesResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $addContactOpportunitiesResponse['message'], $request->all(), config('constants.ServiceName.ContactOpportunity'));
        } else {
            return Helper::customError(400, $addContactOpportunitiesResponse['message'], $request->all(), config('constants.ServiceName.ContactOpportunity'));
        }
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/contacts/{id}/opportunities/{opportunity_id}",
     * tags={"Contact Opportunities"},
     * summary="Retrieve the Opportunity Based on ContactId",
     * operationId="getContactOpportunity API",
     * 		@SWG\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     *      @SWG\Parameter(
     *          name="opportunity_id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    public function getContactOpportunity(Request $request, $contactId, $opportunityId)
    {
        $userId = Auth::user()->id;
        $getContactOpportunityResponse = $this->ContactOpportunitiesService->getContactOpportunity($request, $userId, $contactId, $opportunityId);
        if ($getContactOpportunityResponse['status'] == true) {
            return Helper::customResponse(200, $getContactOpportunityResponse['response'], $request->all(), config('constants.ServiceName.ContactOpportunity'));
        } else {
            return Helper::customError(400, $getContactOpportunityResponse['message'], $request->all(), config('constants.ServiceName.ContactOpportunity'));
        }
    }

    /**
     * @SWG\Put(
     * security = { { "Bearer": {} } },
     * path="/contacts/{id}/opportunities/{opportunity_id}",
     *    tags={"Contact Opportunities"},
     *    summary="Update the opportunities",
     *    operationId="editContactOpportunities API",
     * 	@SWG\Parameter(
     *     name="id",
     *     in="path",
     *     required=true, 
     *     type="integer", 
     *      ),
     * @SWG\Parameter(
     *     name="opportunity_name",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     * @SWG\Parameter(
     *     name="opportunity_id",
     *     in="path",
     *     required=true, 
     *     type="integer", 
     *      ),
     *  @SWG\Parameter(
     *     name="linked_accounts",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="linked_contacts",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="opportunity_stage_id",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="opportunity_type_id",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="owner_id",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="value",
     *     in="formData",
     *     required=false,
     *     type="integer", 
     *      ),
     *  @SWG\Parameter(
     *     name="probability",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="close_date",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="notes",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *   @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     *   @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     *   @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function editContactOpportunities(Request $request, $contactId, $opportunityId)
    {
        $validator = Validator::make($request->all(), [
            'opportunity_stage_id' => 'required|exists:cp_oppstage,id',
            'opportunity_type_id' => 'required|exists:cp_opptype,id',
            'owner_id' => 'required|exists:auth_user,id',
            'close_date' => 'date',
            'value' => 'numeric',
            'probability' => 'numeric',

        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.ContactMenu'));
            return response()->json($result, 200);
        }
        $userId = Auth::user()->id;
        $editContactOpportunitiesResponse = $this->ContactOpportunitiesService->editContactOpportunities($request, $userId, $contactId, $opportunityId);
        if ($editContactOpportunitiesResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $editContactOpportunitiesResponse['message'], $request->all(), config('constants.ServiceName.ContactOpportunity'));
        } else {
            return Helper::customError(400, $editContactOpportunitiesResponse['message'], $request->all(), config('constants.ServiceName.ContactOpportunity'));
        }
    }

    /**
     * @SWG\Delete(
     * security = { { "Bearer": {} } },
     * path="/contacts/{id}/opportunities/{opportunity_id}",
     * tags={"Contact Opportunities"},
     * summary="Remove Contact Opportunity",
     * operationId="RemoveContactOpportunityAPI",
     * @SWG\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Parameter(
     *      name="opportunity_id",
     *      in="path",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    public function removeContactOpportunity(Request $request, $contactId, $opportunityId)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        $opportunityResponse = $this->ContactOpportunitiesService->destroyContactOpportunity($request, $userId, $companyId, $contactId, $opportunityId);

        if ($opportunityResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $opportunityResponse['response'], $request->all(), config('constants.ServiceName.Opportunity'));
        } else {
            return Helper::customError(400, $opportunityResponse['message'], $request->all(), config('constants.ServiceName.Opportunity'));
        }
    }




    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/contact-type",
     * tags={"Contacts"},
     * summary="Get Current user Contact Type",
     * operationId="Contact-Type Api",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function getContactType(Request $request)
    {
        $userId = Auth::user()->id;
        $contactTypeResponse = $this->ContactService->getContactType($request, $userId);
        if ($contactTypeResponse['status'] == true) {
            return Helper::customResponse(200, $contactTypeResponse['response'], $request->all(), config('constants.ServiceName.Contact'));
        } else {
            return Helper::customError(400, $contactTypeResponse['message'], $request->all(), config('constants.ServiceName.Contact'));
        }
    }

    /**
     * @SWG\Post(
     * security = { { "Bearer": {} } },
     * path="/contacts",
     *    tags={"Contacts"},
     *    summary="Create a New Contact",
     *    operationId="AddContact",
     * 	@SWG\Parameter(
     *     name="account_type_id",
     *     in="formData",
     *     required=true, 
     *     type="integer", 
     *      ),
     *  @SWG\Parameter(
     *     name="account_number",
     *     in="formData",
     *     required=false, 
     *     type="number", 
     *     format="number" 
     *      ),
     *  @SWG\Parameter(
     *     name="company_name",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="parent_company",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="address",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="address2",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="city",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="state_id",
     *     in="formData",
     *     required=false,
     *     type="integer", 
     *      ),
     *  @SWG\Parameter(
     *     name="zipcode",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="country_id",
     *     in="formData",
     *     required=false,
     *     type="integer", 
     *      ),
     *  @SWG\Parameter(
     *     name="company_email",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="phone",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="phone_type_id",
     *     in="formData",
     *     required=false,
     *     type="integer", 
     *      ),
     *  @SWG\Parameter(
     *     name="country_code",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="website",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *   @SWG\Parameter(
     *     name="notes",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *    @SWG\Parameter(
     *     name="contact_company_id",
     *     in="formData",
     *     required=false,
     *     type="integer", 
     *      ),
     *   @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     *   @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     *   @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function addContact(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_name' => 'required',
            'phone' => 'regex:/[0-9]/|not_regex:/[a-z]/|nullable|min:10',
            'account_type_id' => 'required',
            'company_email' => 'email|nullable',

        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.AddContact'));
            return response()->json($result, 200);
        }
        $userId = Auth::user()->id;
        $addContactResponse = $this->AddContactService->addContactDetails($request, $userId);
        if ($addContactResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $addContactResponse['message'], $request->all(), config('constants.ServiceName.AddContact'));
        } else {
            return Helper::customError(400, $addContactResponse['message'], $request->all(), config('constants.ServiceName.AddContact'));
        }
        return $addContactResponse;
    }

    /**
     * @SWG\Put(
     * security = { { "Bearer": {} } },
     * path="/contacts/{id}",
     *    tags={"Contacts"},
     *    summary="Update the Contact",
     *    operationId="UpdateContact",
     *  @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     required=true, 
     *     type="integer", 
     *      ),
     * 	@SWG\Parameter(
     *     name="account_type_id",
     *     in="formData",
     *     required=true, 
     *     type="integer", 
     *      ),
     *  @SWG\Parameter(
     *     name="account_number",
     *     in="formData",
     *     required=false, 
     *     type="number", 
     *     format="number" 
     *      ),
     *  @SWG\Parameter(
     *     name="company_name",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="parent_company",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="address",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="address2",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="city",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="state_id",
     *     in="formData",
     *     required=false,
     *     type="integer", 
     *      ),
     *  @SWG\Parameter(
     *     name="zipcode",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="country_id",
     *     in="formData",
     *     required=false,
     *     type="integer", 
     *      ),
     *  @SWG\Parameter(
     *     name="company_email",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="phone",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="phone_type_id",
     *     in="formData",
     *     required=false,
     *     type="integer", 
     *      ),
     *  @SWG\Parameter(
     *     name="country_code",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="website",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *   @SWG\Parameter(
     *     name="notes",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *    @SWG\Parameter(
     *     name="contact_company_id",
     *     in="formData",
     *     required=false,
     *     type="integer", 
     *      ),
     *   @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     *   @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     *   @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    public function editContact(Request $request, $contactId)
    {
        $userId = Auth::user()->id;
        $userCompanyPeopletab = AuthUser::find($userId)->userProfile->userCompany->people_tab;
        if ($userCompanyPeopletab) {
            $validator = Validator::make($request->all(), [
                'company_name' => 'required',
                'phone' => 'regex:/[0-9]/|not_regex:/[a-z]/|nullable|min:10',
                'account_type_id' => 'required',
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'contact_company_id' => 'required',
                'phone' => 'regex:/[0-9]/|not_regex:/[a-z]/|nullable|min:10',
                'account_type_id' => 'required',
            ]);
        }
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.EditContact'));
            return response()->json($result, 200);
        }
        $editContactResponse = $this->EditContactService->editContactDetails($request, $userId, $contactId);
        if ($editContactResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $editContactResponse['message'], $request->all(), config('constants.ServiceName.EditContact'));
        } else {
            return Helper::customError(400, $editContactResponse['message'], $request->all(), config('constants.ServiceName.EditContact'));
        }
        return $editContactResponse;
    }

    /**
     *  @SWG\Post( 
     *    security = { { "Bearer": {} } },
     *    path="/contact/start-appointment",
     *    tags={"Contact Appointment"},
     *    summary="Start Appointment For Contact",
     *    operationId="StartAppointment",
     * 	@SWG\Parameter(
     *     name="contact_id",
     *     in="formData",
     *     required=true, 
     *     type="number", 
     *     format="number" 
     *      ),
     *  @SWG\Parameter(
     *     name="event_form_id",
     *     in="formData",
     *     required=false, 
     *     type="number", 
     *     format="number" 
     *      ),
     *  @SWG\Parameter(
     *     name="start",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *     format="date" 
     *      ),
     *  @SWG\Parameter(
     *     name="latitude",
     *     in="formData",
     *     required=false, 
     *     type="number", 
     *     format="double"
     *      ),
     *  @SWG\Parameter(
     *     name="longitude",
     *     in="formData",
     *     type="number", 
     *     format="double" 
     *      ),
     *   @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     *   @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     *   @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    public function contactStartAppointment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'contact_id' => 'required',
            'latitude'   => 'regex:/[0-9]/|not_regex:/[a-z]/|min:3',
            'longitude'  => 'regex:/[0-9]/|not_regex:/[a-z]/|min:3',
            'start'      => 'date',
        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.contactAppointment'));
            return response()->json($result, 200);
        }
        $userId = Auth::user()->id;
        $contactAppointmentResponse = $this->ContactAppointmentService->doContactStartAppointmentStore($request, $userId);
        if ($contactAppointmentResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $contactAppointmentResponse['message'], $request->all(), config('constants.ServiceName.contactAppointment'));
        } else {
            return Helper::customError(400, $contactAppointmentResponse['message'], $request->all(), config('constants.ServiceName.contactAppointment'));
        }
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/current-appointment",
     * tags={"Contact Appointment"},
     * summary="Get Current Appointment for Current user",
     * operationId="Current-appointment Api",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function contactCurrentAppointment(Request $request)
    {
        $userId = Auth::user()->id;
        $contactCurrentAppointmentResponse = $this->ContactAppointmentService->getCurrentAppointment($request, $userId);
        if ($contactCurrentAppointmentResponse['status'] == true) {
            return Helper::customResponse(200, $contactCurrentAppointmentResponse['response'], $request->all(), config('constants.ServiceName.contactAppointment'));
        } else {
            return Helper::customError(400, $contactCurrentAppointmentResponse['message'], $request->all(), config('constants.ServiceName.contactAppointment'));
        }
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/end-appointment",
     * tags={"Contact Appointment"},
     * summary="Get EndAppointment Details for Current user",
     * operationId="GetEndAppointment Api",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function getContactEndAppointment(Request $request)
    {
        $userId = Auth::user()->id;
        $contactEndAppointmentResponse = $this->ContactAppointmentService->doGetContactEndAppointment($request, $userId);
        if ($contactEndAppointmentResponse['status'] == true) {
            return Helper::customResponse(200, $contactEndAppointmentResponse['response'], $request->all(), config('constants.ServiceName.contactAppointment'));
        } else {
            return Helper::customError(400, $contactEndAppointmentResponse['message'], $request->all(), config('constants.ServiceName.contactAppointment'));
        }
    }

    /**
     *  @SWG\Post( 
     *    security = { { "Bearer": {} } },
     *    path="/end-appointment",
     *    tags={"Contact Appointment"},
     *    summary="Add the End Appointment Details",
     *    operationId="saveContactEndAppointment API",
     * 	@SWG\Parameter(
     *     name="appointment_id",
     *     in="formData",
     *     required=true, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="contact_id",
     *     in="formData",
     *     required=true, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="treat_event_form_as_appointment",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     * @SWG\Parameter(
     *     name="event_from_id",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="notes",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     * @SWG\Parameter(
     *     name="selected_people",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="add_task_or_follow_up",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *   @SWG\Response(response=200, description="Successful request, often a PUT"),
     *   @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     *   @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     *   @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function saveContactEndAppointment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'appointment_id' => 'required|integer|exists:cp_appointment,id',
            'contact_id' => 'required|integer|exists:cp_contact,id',
        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.contactAppointment'));
            return response()->json($result, 200);
        }
        $userId = Auth::user()->id;
        $contactEndAppointmentResponse = $this->ContactAppointmentService->doContactEndAppointmentStore($request, $userId);
        if ($contactEndAppointmentResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $contactEndAppointmentResponse['message'], $request->all(), config('constants.ServiceName.contactAppointment'));
        } else {
            return Helper::customError(400, $contactEndAppointmentResponse['message'], $request->all(), config('constants.ServiceName.contactAppointment'));
        }
    }



    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/contact/get/address/latitude/{latitude_id}/longitude/{longitude_id}",
     * tags={"Contacts"},
     * summary="Get Reverse Geocode",
     * operationId="ReverseGeocode Api",
     *      @SWG\Parameter(
     *          name="latitude_id",
     *          in="path",
     *          required=true, 
     *          type="string" 
     *      ),
     *      @SWG\Parameter(
     *          name="longitude_id",
     *          in="path",
     *          required=true, 
     *          type="string" 
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    public function getReverseGeocode(Request $request, $latitude, $longitude)
    {
        $userId = Auth::user()->id;
        $contactEndAppointmentResponse = $this->ContactService->getReverseGeocode($request, $userId, $latitude, $longitude);
        if ($contactEndAppointmentResponse['status'] == true) {
            return Helper::customResponse(200, $contactEndAppointmentResponse['response'], $request->all(), config('constants.ServiceName.contactAppointment'));
        } else {
            return Helper::customError(400, $contactEndAppointmentResponse['message'], $request->all(), config('constants.ServiceName.contactAppointment'));
        }
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/contact/people-role",
     * tags={"Contacts"},
     * summary="Get Current User Company People Role",
     * operationId="Contact-People-Api",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function getPeopleRole(Request $request)
    {
        $userId = Auth::user()->id;
        $peopleRoleResponse = $this->ContactService->getPeopleRole($request, $userId);
        if ($peopleRoleResponse['status'] == true) {
            return Helper::customResponse(200, $peopleRoleResponse['response'], $request->all(), config('constants.ServiceName.Contact'));
        } else {
            return Helper::customError(400, $peopleRoleResponse['message'], $request->all(), config('constants.ServiceName.Contact'));
        }
    }

    # =============================================
    # =   Contact Personnel for End Appointment   =
    # =============================================

    /**
     *  @SWG\Post( 
     *    security = { { "Bearer": {} } },
     *    path="/contacts/{id}/contact-personnel",
     *    tags={"Contact Personnel"},
     *    summary="Create a New Contact Personnel",
     *    operationId="AddContactPersonnel API",
     * 	@SWG\Parameter(
     *     name="id",
     *     in="path",
     *     required=true, 
     *     type="integer", 
     *      ),
     * @SWG\Parameter(
     *     name="people_role_id",
     *     in="formData",
     *     required=false, 
     *     type="integer", 
     *      ),
     * @SWG\Parameter(
     *     name="first_name",
     *     in="formData",
     *     required=true, 
     *     type="string", 
     *      ),
     * @SWG\Parameter(
     *     name="last_name",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ), 
     *  @SWG\Parameter(
     *     name="phones",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="title",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     * @SWG\Parameter(
     *     name="contact_notes",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *   @SWG\Response(response=200, description="Successful request, often a POST"),
     *   @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     *   @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     *   @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function addContactPersonnel(Request $request, $contactId)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'email|nullable',
            'first_name' => 'required',
        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.ContactPersonnel'));
            return response()->json($result, 200);
        }
        $userId = Auth::user()->id;
        $contactPersonnelResponse = $this->ContactPersonnelService->doAddContactPersonnel($request, $userId, $contactId);
        if ($contactPersonnelResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $contactPersonnelResponse['message'], $request->all(), config('constants.ServiceName.ContactPersonnel'));
        } else {
            return Helper::customError(400, $contactPersonnelResponse['message'], $request->all(), config('constants.ServiceName.ContactPersonnel'));
        }
    }

    /**
     *  @SWG\Put( 
     *    security = { { "Bearer": {} } },
     *    path="/contacts/{id}/contact-personnel/{personnel_id}",
     *    tags={"Contact Personnel"},
     *    summary="Update the Contact Personnel",
     *    operationId="UpdateContactPersonnel API",
     * 	@SWG\Parameter(
     *     name="id",
     *     in="path",
     *     required=true, 
     *     type="integer", 
     *      ),
     * @SWG\Parameter(
     *     name="personnel_id",
     *     in="path",
     *     required=true, 
     *     type="integer", 
     *      ),
     * @SWG\Parameter(
     *     name="people_role_id",
     *     in="formData",
     *     required=false, 
     *     type="integer", 
     *      ),
     * @SWG\Parameter(
     *     name="first_name",
     *     in="formData",
     *     required=true, 
     *     type="string", 
     *      ),
     * @SWG\Parameter(
     *     name="last_name",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ), 
     *  @SWG\Parameter(
     *     name="phones",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="title",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     * @SWG\Parameter(
     *     name="contact_notes",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *   @SWG\Response(response=200, description="Successful request, often a POST"),
     *   @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     *   @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     *   @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function updateContactPersonnel(Request $request, $contactId, $personnelId)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'email|nullable',
            'first_name' => 'required',
        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.ContactPersonnel'));
            return response()->json($result, 200);
        }
        $userId = Auth::user()->id;
        $contactPersonnelResponse = $this->ContactPersonnelService->doUpdateContactPersonnel($request, $userId, $contactId, $personnelId);
        if ($contactPersonnelResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $contactPersonnelResponse['message'], $request->all(), config('constants.ServiceName.ContactPersonnel'));
        } else {
            return Helper::customError(400, $contactPersonnelResponse['message'], $request->all(), config('constants.ServiceName.ContactPersonnel'));
        }
    }

    /**
     * @SWG\Delete(
     * security = { { "Bearer": {} } },
     * path="/contacts/{id}/contact-personnel/{personnel_id}",
     * tags={"Contact Personnel"},
     * summary="Remove the Contact Personnel",
     * operationId="RemoveContactPersonnelAPI",
     * @SWG\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Parameter(
     *      name="personnel_id",
     *      in="path",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function removeContactPersonnel(Request $request, $contactId, $personnelId)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        $contactPersonnelResponse = $this->ContactPersonnelService->destroyContactPersonnel($request, $userId, $companyId, $contactId, $personnelId);

        if ($contactPersonnelResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $contactPersonnelResponse['response'], $request->all(), config('constants.ServiceName.Contact'));
        } else {
            return Helper::customError(400, $contactPersonnelResponse['message'], $request->all(), config('constants.ServiceName.Contact'));
        }
    }


    /**
     * @SWG\Put(
     * security = { { "Bearer": {} } },
     * path="/contact/move-personnel",
     * tags={"Contact Personnel"},
     * summary="Move the Personnel to Another Contact",
     * operationId="MoveContactPersonnelAPI",
     * @SWG\Parameter(
     *      name="personnel_id",
     *      in="formData",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Parameter(
     *      name="contact_id",
     *      in="formData",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function moveContactPersonnel(Request $request)
    {
        $userId      = Auth::user()->id;
        $userCompanyId   = AuthUser::find($userId)->userProfile->company_id;

        $contactPersonnelResponse = $this->ContactPersonnelService->doMoveContactPersonnel($request, $userId, $userCompanyId);

        if ($contactPersonnelResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $contactPersonnelResponse['message'], $request->all(), config('constants.ServiceName.ContactPersonnel'));
        } else {
            return Helper::customError(400, $contactPersonnelResponse['message'], $request->all(), config('constants.ServiceName.ContactPersonnel'));
        }
    }



    /**
     *  @SWG\Put( 
     *    security = { { "Bearer": {} } },
     *    path="/contacts/{id}/company-details",
     *    tags={"Contacts"},
     *    summary="Update the Contact Company Details",
     *    operationId="storeCompanyDetails API",
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     required=false, 
     *     type="string", 
     *      ),
     * 	@SWG\Parameter(
     *     name="parent_company_name",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="company_details",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *   @SWG\Response(response=200, description="Successful request, often a PUT"),
     *   @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     *   @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     *   @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function storeCompanyDetails(Request $request, $contactId)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;
        $requestDetails = $request->all();

        /* Requested POST as Converted to Array */
        $formDetails     = collect(json_decode($requestDetails['company_details']));
        $formDetails      = json_decode(json_encode($formDetails), true);

        /* CompanyDetails Update Processing */
        $contactParentCompanyResponse = $this->EditContactService->doUpdateContactParentCompany($request, $userId, $contactId, $companyId);
        $customFieldsResponse = $this->CustomFieldService->doUpdateCustomFieldsProcessing($request, $userId, $companyId, $contactId, $formDetails);

        if ($customFieldsResponse['status'] == true) {
            $result = Helper::customUpdatedResponse(200, config('constants.Success.ContactCompanyDetails'), $request->all(), config('constants.ServiceName.CustomField'), 1600);
            return response()->json($result, 200);
        } else {
            return Helper::customError(400, config('constants.Errors.ContactCompanyDetails'), $request->all(), config('constants.ServiceName.CustomField'));
        }
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/contacts/{id}/contact-personnel",
     * tags={"Contact Personnel"},
     * summary="Retrieve the Contact Personnel Based on ContactId",
     * operationId="getContactPersonnel API",
     * 		@SWG\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function getContactPersonnel(Request $request, $contactId)
    {
        $userId = Auth::user()->id;
        $contactPersonnelResponse = $this->ContactPersonnelService->getContactPersonnel($request, $userId, $contactId);
        if ($contactPersonnelResponse['status'] == true) {
            return Helper::customResponse(200, $contactPersonnelResponse['response'], $request->all(), config('constants.ServiceName.ContactPersonnel'));
        } else {
            return Helper::customError(400, $contactPersonnelResponse['message'], $request->all(), config('constants.ServiceName.ContactPersonnel'));
        }
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/contacts/{id}/contact-personnel/{personnel_id}",
     * tags={"Contact Personnel"},
     * summary="Retrieve the Contact Personnel Based on ContactId",
     * operationId="getSpecificContactPersonnel API",
     * 		@SWG\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     *      @SWG\Parameter(
     *          name="personnel_id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function getSpecificContactPersonnel(Request $request, $contactId, $personnelId)
    {
        $userId = Auth::user()->id;
        $contactPersonnelResponse = $this->ContactPersonnelService->getSpecificContactPersonnel($request, $userId, $contactId, $personnelId);
        if ($contactPersonnelResponse['status'] == true) {
            return Helper::customResponse(200, $contactPersonnelResponse['response'], $request->all(), config('constants.ServiceName.ContactPersonnel'));
        } else {
            return Helper::customError(400, $contactPersonnelResponse['message'], $request->all(), config('constants.ServiceName.ContactPersonnel'));
        }
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/contacts/{id}/phone",
     * tags={"Contact Phone"},
     * summary="Retrieve the Contact Phone Based on ContactId",
     * operationId="getContactPhone API",
     * 		@SWG\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function getContactPhone(Request $request, $contactId)
    {
        $userId = Auth::user()->id;
        $contactPhoneResponse = $this->ContactService->getContactPhone($request, $userId, $contactId);
        if ($contactPhoneResponse['status'] == true) {
            return Helper::customResponse(200, $contactPhoneResponse['response'], $request->all(), config('constants.ServiceName.Contact'));
        } else {
            return Helper::customError(400, $contactPhoneResponse['message'], $request->all(), config('constants.ServiceName.Contact'));
        }
    }

    /**
     *  @SWG\Post( 
     *    security = { { "Bearer": {} } },
     *    path="/contacts/{id}/phone",
     *    tags={"Contact Phone"},
     *    summary="Create a New Contact Phone",
     *    operationId="AddContactPhone API",
     * 	@SWG\Parameter(
     *     name="id",
     *     in="path",
     *     required=true, 
     *     type="integer", 
     *      ),
     *  @SWG\Parameter(
     *     name="phone_type_id",
     *     in="formData",
     *     required=false, 
     *     type="integer", 
     *      ),
     * @SWG\Parameter(
     *     name="phone_number",
     *     in="formData",
     *     required=false, 
     *     type="integer", 
     *      ),
     * @SWG\Parameter(
     *     name="extension",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ), 
     * @SWG\Parameter(
     *     name="country_code",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="associated_contacts",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ), 
     *   @SWG\Response(response=200, description="Successful request, often a POST"),
     *   @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     *   @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     *   @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function addContactPhone(Request $request, $contactId)
    {
        $validator = Validator::make($request->all(), [
            'phone_type_id' => 'integer',
            'phone_number' => 'regex:/[0-9]/|not_regex:/[a-z]/|nullable|min:10',
        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.Contact'));
            return response()->json($result, 200);
        }
        $userId = Auth::user()->id;
        $contactPhoneResponse = $this->ContactService->doAddContactPhone($request, $userId, $contactId);
        if ($contactPhoneResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $contactPhoneResponse['message'], $request->all(), config('constants.ServiceName.Contact'));
        } else {
            return Helper::customError(400, $contactPhoneResponse['message'], $request->all(), config('constants.ServiceName.Contact'));
        }
    }

    /**
     *  @SWG\Put( 
     *    security = { { "Bearer": {} } },
     *    path="/contacts/{id}/phone/{phone_id}",
     *    tags={"Contact Phone"},
     *    summary="Update the Contact Phone",
     *    operationId="UpdateContactPhone API",
     * 	@SWG\Parameter(
     *     name="id",
     *     in="path",
     *     required=true, 
     *     type="integer", 
     *      ),
     * 	@SWG\Parameter(
     *     name="phone_id",
     *     in="path",
     *     required=true, 
     *     type="integer", 
     *      ),
     *  @SWG\Parameter(
     *     name="phone_type_id",
     *     in="formData",
     *     required=false, 
     *     type="integer", 
     *      ),
     * @SWG\Parameter(
     *     name="phone_number",
     *     in="formData",
     *     required=false, 
     *     type="integer", 
     *      ),
     * @SWG\Parameter(
     *     name="extension",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ), 
     * @SWG\Parameter(
     *     name="country_code",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="associated_contacts",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ), 
     *   @SWG\Response(response=200, description="Successful request, often a POST"),
     *   @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     *   @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     *   @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function updateContactPhone(Request $request, $contactId, $contactPhoneId)
    {
        $validator = Validator::make($request->all(), [
            'phone_type_id' => 'integer',
            'phone_number' => 'regex:/[0-9]/|not_regex:/[a-z]/|nullable|min:10',
        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.Contact'));
            return response()->json($result, 200);
        }
        $userId = Auth::user()->id;
        $contactPhoneResponse = $this->ContactService->doUpdateContactPhone($request, $userId, $contactId, $contactPhoneId);
        if ($contactPhoneResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $contactPhoneResponse['message'], $request->all(), config('constants.ServiceName.Contact'));
        } else {
            return Helper::customError(400, $contactPhoneResponse['message'], $request->all(), config('constants.ServiceName.Contact'));
        }
    }

    /**
     * @SWG\Delete(
     * security = { { "Bearer": {} } },
     * path="/contacts/{id}/phone/{phone_id}",
     * tags={"Contact Phone"},
     * summary="Remove Contact Phone",
     * operationId="RemoveContactPhoneAPI",
     * @SWG\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Parameter(
     *      name="phone_id",
     *      in="path",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    public function removeContactPhone(Request $request, $contactId, $contactPhoneId)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        $contactPhoneResponse = $this->ContactService->destroyContactPhone($request, $userId, $companyId, $contactId, $contactPhoneId);

        if ($contactPhoneResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $contactPhoneResponse['response'], $request->all(), config('constants.ServiceName.Opportunity'));
        } else {
            return Helper::customError(400, $contactPhoneResponse['message'], $request->all(), config('constants.ServiceName.Opportunity'));
        }
    }


    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/contacts/{id}/info-email",
     * tags={"Contact Info-Email"},
     * summary="Retrieve the Contact Info Email Based on ContactId",
     * operationId="getContactInfoEmail API",
     * 		@SWG\Parameter(
     *          name="id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function getContactInfoEmail(Request $request, $contactId)
    {
        $userId = Auth::user()->id;
        $contactInfoEmailResponse = $this->ContactService->doGetContactInfoEmail($request, $userId, $contactId);
        if ($contactInfoEmailResponse['status'] == true) {
            return Helper::customResponse(200, $contactInfoEmailResponse['response'], $request->all(), config('constants.ServiceName.Contact'));
        } else {
            return Helper::customError(400, $contactInfoEmailResponse['message'], $request->all(), config('constants.ServiceName.Contact'));
        }
    }

    /**
     *  @SWG\Post( 
     *    security = { { "Bearer": {} } },
     *    path="/contacts/{id}/info-email",
     *    tags={"Contact Info-Email"},
     *    summary="Add New Contact Info Email",
     *    operationId="addContactInfoEmail API",
     * 	@SWG\Parameter(
     *     name="id",
     *     in="path",
     *     required=true, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *   @SWG\Response(response=200, description="Successful request, often a PUT"),
     *   @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     *   @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     *   @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function addContactInfoEmail(Request $request, $contactId)
    {
        $userId = Auth::user()->id;
        $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:cp_contactinfoemail,email',
        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.Contact'));
            return response()->json($result, 200);
        }
        $userId = Auth::user()->id;
        $contactInfoEmailResponse = $this->ContactService->doAddContactInfoEmail($request, $userId, $contactId);
        if ($contactInfoEmailResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $contactInfoEmailResponse['message'], $request->all(), config('constants.ServiceName.Contact'));
        } else {
            return Helper::customError(400, $contactInfoEmailResponse['message'], $request->all(), config('constants.ServiceName.Contact'));
        }
    }

    /**
     *  @SWG\Put( 
     *    security = { { "Bearer": {} } },
     *    path="/contacts/{id}/info-email/{email_id}",
     *    tags={"Contact Info-Email"},
     *    summary="Update the Contact Info Email",
     *    operationId="updateContactInfoEmail API",
     * 	@SWG\Parameter(
     *     name="id",
     *     in="path",
     *     required=true, 
     *     type="string", 
     *      ),
     * 	@SWG\Parameter(
     *     name="email_id",
     *     in="path",
     *     required=true, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="email",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *   @SWG\Response(response=200, description="Successful request, often a PUT"),
     *   @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     *   @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     *   @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function updateContactInfoEmail(Request $request, $contactId, $contactInfoEmailId)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:cp_contactinfoemail',
        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.Contact'));
            return response()->json($result, 200);
        }
        $userId = Auth::user()->id;
        $contactInfoEmailResponse = $this->ContactService->doUpdateContactInfoEmail($request, $userId, $contactId, $contactInfoEmailId);
        if ($contactInfoEmailResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $contactInfoEmailResponse['message'], $request->all(), config('constants.ServiceName.Contact'));
        } else {
            return Helper::customError(400, $contactInfoEmailResponse['message'], $request->all(), config('constants.ServiceName.Contact'));
        }
    }

    /**
     * @SWG\Delete(
     * security = { { "Bearer": {} } },
     * path="/contacts/{id}/info-email/{email_id}",
     * tags={"Contact Info-Email"},
     * summary="Remove Contact Info Email",
     * operationId="RemoveContactInfoEmailAPI",
     * @SWG\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Parameter(
     *      name="email_id",
     *      in="path",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    public function removeContactInfoEmail(Request $request, $contactId, $contactInfoEmailId)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        $contactInfoEmailResponse = $this->ContactService->doRemoveContactInfoEmail($request, $userId, $companyId, $contactId, $contactInfoEmailId);

        if ($contactInfoEmailResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $contactInfoEmailResponse['response'], $request->all(), config('constants.ServiceName.Contact'));
        } else {
            return Helper::customError(400, $contactInfoEmailResponse['message'], $request->all(), config('constants.ServiceName.Contact'));
        }
    }

    /**
     *  @SWG\Put( 
     *    security = { { "Bearer": {} } },
     *    path="/contacts/{id}/selected-users",
     *    tags={"Contacts"},
     *    summary="Update the Contact Selected Users",
     *    operationId="updateContactSelected Users API",
     * 	@SWG\Parameter(
     *     name="id",
     *     in="path",
     *     required=true, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="user_id",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *   @SWG\Response(response=200, description="Successful request, often a PUT"),
     *   @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     *   @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     *   @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function updateSelectedUsers(Request $request, $contactId)
    {
        $userId = Auth::user()->id;
        $updateSelectedUsersResponse = $this->ContactService->doUpdateSelectedUsers($request, $userId, $contactId);
        if ($updateSelectedUsersResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $updateSelectedUsersResponse['message'], $request->all(), config('constants.ServiceName.Contact'));
        } else {
            return Helper::customError(400, $updateSelectedUsersResponse['message'], $request->all(), config('constants.ServiceName.Contact'));
        }
    }


    /**
     *  @SWG\Get( 
     *    security = { { "Bearer": {} } },
     *    path="/contact/{contact_id}/contact-note",
     *    tags={"Contacts"},
     *    summary=" Retrieve the Contact Note Details",
     *    operationId="GetContactNoteDetails",
     * 	@SWG\Parameter(
     *     name="contact_id",
     *     in="path",
     *     required=true, 
     *     type="string", 
     *      ),
     *   @SWG\Response(response=200, description="Successful request, often a PUT"),
     *   @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     *   @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     *   @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function getContactNote(Request $request, $contactId)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;
        $contactNotesResponse = $this->ContactListServices->getContactNoteDetails($userId, $companyId, $contactId);
        if ($contactNotesResponse['status'] == true) {
            return Helper::customResponse(200, $contactNotesResponse['response'], $request->all(), config('constants.ServiceName.Contact'));
        } else {
            return Helper::customError(400, $contactNotesResponse['message'], $request->all(), config('constants.ServiceName.Contact'));
        }
    }

    /**
     * @SWG\Post(
     * security = { { "Bearer": {} } },
     * path="/contact/{contact_id}/contact-note",
     * tags={"Contacts"},
     * summary="Create New Contact Note Based on ContactId",
     * operationId="CreateContactNoteAPI",
     * 		@SWG\Parameter(
     *          name="contact_id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     *     @SWG\Parameter(
     *          name="note",
     *          in="formData",
     *          required=true, 
     *          type="string", 
     *      ),
     *     @SWG\Parameter(
     *          name="source_type",
     *          in="formData",
     *          required=false, 
     *          type="integer", 
     *      ),
     *     @SWG\Parameter(
     *          name="source_id",
     *          in="formData",
     *          required=false, 
     *          type="integer", 
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function addContactNote(Request $request, $contactId)
    {
        /* Param Validations */
        $validator = Validator::make($request->all(), [
            'note'        => 'required',
            'source_type' => 'regex:/[0-9]/|not_regex:/[a-z]/|nullable',
            'source_id'   => 'regex:/[0-9]/|not_regex:/[a-z]/|nullable',
        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.Contact'));
            return response()->json($result, 200);
        }

        $userId = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;
        $contactNotesResponse = $this->AddContactService->addContactNote($request, $userId, $companyId, $contactId);
        if ($contactNotesResponse['status'] == true) {
            return Helper::customResponse(200, $contactNotesResponse['response'], $request->all(), config('constants.ServiceName.Contact'));
        } else {
            return Helper::customError(400, $contactNotesResponse['message'], $request->all(), config('constants.ServiceName.Contact'));
        }
    }


    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/contact/custom-fields",
     * tags={"Contacts"},
     * summary="Get Contact Custom Fields Detail",
     * operationId="getCustomFieldsAPI",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function getContactFilterCustomFields(Request $request)
    {
        $userId = Auth::user()->id;
        $contactFilterCustomFieldsResponse = $this->ContactService->doGetContactFilterCustomFields($request, $userId);
        if ($contactFilterCustomFieldsResponse['status'] == true) {
            return Helper::customResponse(200, $contactFilterCustomFieldsResponse['response'], $request->all(), config('constants.ServiceName.Contact'));
        } else {
            return Helper::customError(400, $contactFilterCustomFieldsResponse['message'], $request->all(), config('constants.ServiceName.Contact'));
        }
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/contact/parent-company",
     * tags={"Contacts"},
     * summary="Get Contact Parent Company Detail",
     * operationId="getParentCompanyAPI",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function getContactParentCompany(Request $request)
    {
        $userId = Auth::user()->id;
        $contactParentCompanyResponse = $this->ContactService->doGetContactParentCompany($request, $userId);
        if ($contactParentCompanyResponse['status'] == true) {
            return Helper::customResponse(200, $contactParentCompanyResponse['response'], $request->all(), config('constants.ServiceName.Contact'));
        } else {
            return Helper::customError(400, $contactParentCompanyResponse['message'], $request->all(), config('constants.ServiceName.Contact'));
        }
    }
}
