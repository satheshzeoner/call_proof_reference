<?php

namespace App\Http\Controllers\API\Contact;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Validator;

/* Models */
use App\Models\Auth\AuthUser;

/* Service */
use App\Services\Contact\ContactImageService;

/* Repositories */

class ContactImageController extends Controller
{

    public function __construct()
    {
        $this->ContactImageService = new ContactImageService();
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/contact/{id}/images",
     * tags={"Contact Image"},
     * summary="Get Image Details for Specific Contact",
     * operationId="ImagesApi",
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     required=true, 
     *     type="string", 
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =              IMAGES                       =
    # =============================================


    public function getImages(Request $request, $contactId)
    {
        $userId = Auth::user()->id;
        $contactImageResponse = $this->ContactImageService->doGetImages($request, $userId, $contactId);
        if ($contactImageResponse['status'] == true) {
            return Helper::customResponse(200, $contactImageResponse['response'], $request->all(), config('constants.ServiceName.ContactImage'));
        } else {
            return Helper::customError(400, $contactImageResponse['message'], $request->all(), config('constants.ServiceName.ContactImage'));
        }
    }

    /**
     *  @SWG\Post( 
     *    security = { { "Bearer": {} } },
     *    path="/contact/{id}/image",
     *    tags={"Contact Image"},
     *    summary="Upload a New Image",
     *    operationId="UploadContactImageAPI",
     * 	@SWG\Parameter(
     *     name="id",
     *     in="path",
     *     required=true, 
     *     type="integer", 
     *      ),
     *  @SWG\Parameter(
     *     name="contact_image",
     *     in="formData",
     *     required=true, 
     *     type="file", 
     *      ),
     *   @SWG\Response(response=200, description="Successful request, often a POST"),
     *   @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     *   @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     *   @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function uploadImage(Request $request, $contactId)
    {
        $validator = Validator::make($request->all(), [
            'contact_image' => 'required|image',
        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.ContactImage'));
            return response()->json($result, 200);
        }
        $userId = Auth::user()->id;
        $contactImageResponse = $this->ContactImageService->doUploadImage($request, $userId, $contactId);
        if ($contactImageResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $contactImageResponse['message'], $request->all(), config('constants.ServiceName.ContactImage'));
        } else {
            return Helper::customError(400, $contactImageResponse['message'], $request->all(), config('constants.ServiceName.ContactImage'));
        }
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/contact/{id}/image/{image_id}",
     * tags={"Contact Image"},
     * summary="View Specific Image",
     * operationId="SpecificImageApi",
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     required=true, 
     *     type="string", 
     *      ),
     * @SWG\Parameter(
     *     name="image_id",
     *     in="path",
     *     required=true, 
     *     type="string", 
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function showImage(Request $request, $contactId, $imageId)
    {
        $userId = Auth::user()->id;
        $contactImageResponse = $this->ContactImageService->doShowImage($request, $userId, $contactId, $imageId);
        if ($contactImageResponse['status'] == true) {
            return Helper::customResponse(200, $contactImageResponse['response'], $request->all(), config('constants.ServiceName.ContactImage'));
        } else {
            return Helper::customError(400, $contactImageResponse['message'], $request->all(), config('constants.ServiceName.ContactImage'));
        }
    }

    /**
     * @SWG\Delete(
     * security = { { "Bearer": {} } },
     * path="/contact/{id}/image/{image_id}",
     * tags={"Contact Image"},
     * summary="Remove Contact Image",
     * operationId="RemoveContactImageAPI",
     * @SWG\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Parameter(
     *      name="image_id",
     *      in="path",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    public function removeContactImage(Request $request, $contactId, $contactImageId)
    {

        $userId      = Auth::user()->id;
        $userCompanyId   = AuthUser::find($userId)->userProfile->company_id;

        $contactImageResponse = $this->ContactImageService->doRemoveContactImage($request, $userId, $userCompanyId, $contactId, $contactImageId);

        if ($contactImageResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $contactImageResponse['response'], $request->all(), config('constants.ServiceName.ContactImage'));
        } else {
            return Helper::customError(400, $contactImageResponse['message'], $request->all(), config('constants.ServiceName.ContactImage'));
        }
    }
}
