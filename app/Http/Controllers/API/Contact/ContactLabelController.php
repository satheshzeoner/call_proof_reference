<?php

namespace App\Http\Controllers\API\Contact;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Validator;

/* Service */
use App\Services\Contact\ContactLabelService;

/* Models */
use App\Models\Auth\AuthUser;

class ContactLabelController extends Controller
{
    public function __construct()
    {
        $this->ContactLabelService = new ContactLabelService();
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/contact/{id}/labels",
     * tags={"Contact Label"},
     * summary="Get Label List For Specific Contact",
     * operationId="LabelApi",
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     required=true, 
     *     type="string", 
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    public function getContactLabels(Request $request, $contactId)
    {
        $userId = Auth::user()->id;
        $contactLabelResponse = $this->ContactLabelService->doGetLabels($request, $userId, $contactId);
        if ($contactLabelResponse['status'] == true) {
            return Helper::customResponse(200, $contactLabelResponse['response'], $request->all(), config('constants.ServiceName.ContactLabel'));
        } else {
            return Helper::customError(400, $contactLabelResponse['message'], $request->all(), config('constants.ServiceName.ContactLabel'));
        }
    }

    /**
     *  @SWG\Post( 
     *    security = { { "Bearer": {} } },
     *    path="/contact/{id}/label",
     *    tags={"Contact Label"},
     *    summary="Add New Contact Label",
     *    operationId="addContactLabelAPI",
     * 	@SWG\Parameter(
     *     name="id",
     *     in="path",
     *     required=true, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="label",
     *     in="formData",
     *     required=true, 
     *     type="string", 
     *      ),
     *   @SWG\Response(response=200, description="Successful request, often a PUT"),
     *   @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     *   @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     *   @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function addContactLabel(Request $request, $contactId)
    {
        $userId = Auth::user()->id;
        $validator = Validator::make($request->all(), [
            'label' => 'required|max:100',
        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.ContactLabel'));
            return response()->json($result, 200);
        }
        $userId = Auth::user()->id;
        $contactLabelResponse = $this->ContactLabelService->doAddContactLabel($request, $userId, $contactId);
        if ($contactLabelResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $contactLabelResponse['message'], $request->all(), config('constants.ServiceName.ContactLabel'));
        } else {
            return Helper::customError(400, $contactLabelResponse['message'], $request->all(), config('constants.ServiceName.ContactLabel'));
        }
    }

    /**
     * @SWG\Delete(
     * security = { { "Bearer": {} } },
     * path="/contact/{id}/label/{label_id}",
     * tags={"Contact Label"},
     * summary="Remove Contact Label",
     * operationId="RemoveContactLabelAPI",
     * @SWG\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Parameter(
     *      name="label_id",
     *      in="path",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    public function removeContactLabel(Request $request, $contactId, $labelId)
    {

        $userId      = Auth::user()->id;
        $userCompanyId   = AuthUser::find($userId)->userProfile->company_id;

        $contactLabelResponse = $this->ContactLabelService->doRemoveContactLabel($request, $userId, $userCompanyId, $contactId, $labelId);

        if ($contactLabelResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $contactLabelResponse['response'], $request->all(), config('constants.ServiceName.ContactLabel'));
        } else {
            return Helper::customError(400, $contactLabelResponse['message'], $request->all(), config('constants.ServiceName.ContactLabel'));
        }
    }
}
