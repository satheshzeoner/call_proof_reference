<?php

namespace App\Http\Controllers\API\Route;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Validator;

/* Models */
use App\Models\Auth\AuthUser;

/* Service */
use App\Services\Route\RouteServices;
use App\Services\Route\RouteAddServices;
use App\Services\Route\RouteEditServices;
use App\Services\Route\RouteDestroyServices;


class RouteController extends Controller
{

    public function __construct()
    {
        $this->RouteServices    = new RouteServices();
        $this->RouteAddServices = new RouteAddServices();
        $this->RouteEditServices = new RouteEditServices();
        $this->RouteDestroyServices = new RouteDestroyServices();
    }

    /**
     * @SWG\Post(
     * security = { { "Bearer": {} } },
     * path="/routes",
     * tags={"Routes"},
     * summary="Create New Routes",
     * operationId="CreateNewRouteAPI",
     *  @SWG\Parameter(
     *      name="route_name",
     *      in="formData",
     *      required=true,
     *      type="string",
     *      ),
     * @SWG\Parameter(
     *      name="contact_ids",
     *      in="formData",
     *      required=true,
     *      type="string",
     *      ),
     * @SWG\Parameter(
     *      name="assigned_by",
     *      in="formData",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =         ADD ROUTES                        =
    # =============================================


    public function storeCurrentRoute(Request $request)
    {

        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        $currentRouteResponse = $this->RouteAddServices->storeCurrentRouteDetails($request, $userId, $companyId);

        if ($currentRouteResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $currentRouteResponse['response'], $request->all(), config('constants.ServiceName.Routes'));
        } else {
            return Helper::customError(400, $currentRouteResponse['message'], $request->all(), config('constants.ServiceName.Routes'));
        }
    }

    /**
     * @SWG\Put(
     * security = { { "Bearer": {} } },
     * path="/routes/{route_id}",
     * tags={"Routes"},
     * summary="Update New Routes",
     * operationId="UpdateNewRouteAPI",
     *  @SWG\Parameter(
     *      name="route_id",
     *      in="path",
     *      required=true,
     *      type="string",
     *      ),
     *  @SWG\Parameter(
     *      name="route_name",
     *      in="formData",
     *      required=true,
     *      type="string",
     *      ),
     * @SWG\Parameter(
     *      name="contact_ids",
     *      in="formData",
     *      required=true,
     *      type="string",
     *      ),
     * @SWG\Parameter(
     *      name="assigned_by",
     *      in="formData",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =         UPDATE ROUTES                     =
    # =============================================


    public function updateCurrentRoute(Request $request, $routeId)
    {

        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        $currentRouteResponse = $this->RouteEditServices->storeCurrentRouteDetails($request, $userId, $companyId, $routeId);

        if ($currentRouteResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $currentRouteResponse['response'], $request->all(), config('constants.ServiceName.Routes'));
        } else {
            return Helper::customError(400, $currentRouteResponse['message'], $request->all(), config('constants.ServiceName.Routes'));
        }
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/current-route/{route_id}",
     * tags={"Routes"},
     * summary="Retrieve Current Routes",
     * operationId="GetRoutesAPI",
     *  @SWG\Parameter(
     *      name="route_id",
     *      in="path",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =         GET CURRENT-ROUTES                =
    # =============================================


    public function getCurrentRoute(Request $request, $routeId)
    {

        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        $currentRouteResponse = $this->RouteServices->getCurrentRouteDetails($request, $userId, $companyId, $routeId);

        if ($currentRouteResponse['status'] == true) {
            return Helper::customResponse(200, $currentRouteResponse['response'], $request->all(), config('constants.ServiceName.Routes'));
        } else {
            return Helper::customError(400, $currentRouteResponse['message'], $request->all(), config('constants.ServiceName.Routes'));
        }
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/my-routes",
     * tags={"Routes"},
     * summary="Retrieve My Routes",
     * operationId="GetMyRoutesAPI",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =         GET MY-ROUTES                     =
    # =============================================
    public function getMyRoute(Request $request)
    {

        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        $myRouteResponse = $this->RouteServices->getMyRouteDetails($request, $userId, $companyId);

        if ($myRouteResponse['status'] == true) {
            return Helper::customResponse(200, $myRouteResponse['response'], $request->all(), config('constants.ServiceName.Routes'));
        } else {
            return Helper::customError(400, $myRouteResponse['message'], $request->all(), config('constants.ServiceName.Routes'));
        }
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/all-routes",
     * tags={"Routes"},
     * summary="Retrieve All Routes",
     * operationId="GetAllRoutesAPI",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =         GET All-ROUTES                     =
    # =============================================
    public function getAllRoute(Request $request)
    {

        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        $myRouteResponse = $this->RouteServices->getAllRouteDetails($request, $userId, $companyId);

        if ($myRouteResponse['status'] == true) {
            return Helper::customResponse(200, $myRouteResponse['response'], $request->all(), config('constants.ServiceName.Routes'));
        } else {
            return Helper::customError(400, $myRouteResponse['message'], $request->all(), config('constants.ServiceName.Routes'));
        }
    }

    /**
     * @SWG\Delete(
     * security = { { "Bearer": {} } },
     * path="/routes/{route_id}",
     * tags={"Routes"},
     * summary="Remove All Routes",
     * operationId="RemoveAllRoutesAPI",
     * @SWG\Parameter(
     *      name="route_id",
     *      in="path",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    # =============================================
    # =         REMOVE-ROUTES                     =
    # =============================================
    public function removeRoute(Request $request, $routeId)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        $myRouteResponse = $this->RouteDestroyServices->destroyRoute($request, $userId, $companyId, $routeId);

        if ($myRouteResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $myRouteResponse['response'], $request->all(), config('constants.ServiceName.Routes'));
        } else {
            return Helper::customError(400, $myRouteResponse['message'], $request->all(), config('constants.ServiceName.Routes'));
        }
    }
}
