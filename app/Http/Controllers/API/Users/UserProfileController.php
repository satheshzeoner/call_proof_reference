<?php

namespace App\Http\Controllers\API\Users;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

/* Models */
use App\Models\Auth\AuthUser;

/* Service */
use App\Services\User\UserProfileService;

class UserProfileController extends Controller
{

    public function __construct()
    {
        $this->UserProfileService   = new UserProfileService();
    }

    # =============================================
    # =          User Profile Process             =
    # =============================================

    public function profileUpdate(Request $request)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        $paramDetails = $request->all();

        $userProfileUpdate = $this->UserProfileService->updateUserProfile($paramDetails, $userId, $companyId);

        if ($userProfileUpdate['status'] == true) {
            return Helper::customUpdatedResponse(200, $userProfileUpdate['response'], $request->all(), config('constants.ServiceName.UserProfile'));
        } else {
            return Helper::customError(400, $userProfileUpdate['message'], $request->all(), config('constants.ServiceName.UserProfile'));
        }
    }
}
