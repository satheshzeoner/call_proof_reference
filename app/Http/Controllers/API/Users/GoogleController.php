<?php

namespace App\Http\Controllers\API\Users;

use App\Helpers\Helper;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Passport\Client as OClient;
use Illuminate\Support\Facades\Auth;

/* Models */
use App\Models\Auth\AuthUser;
use App\Models\Company\Company;

/* Service */
use App\Services\Signup\SignupServices;


class GoogleController extends Controller
{

    public function __construct()
    {
        $this->SignupStoreService         = new SignupServices();
        $this->company                    = new Company();
        $this->authUser                   = new AuthUser();
    }


    # =============================================
    # =           Get User Google Login           =
    # =============================================

    public function googleLogin(Request $request)
    {

        $token      = $request['token'];
        $deviceType = $request['device_type'];


        /* Field Token Validations */
        if ($token == null ||  $deviceType == null) {
            $result = Helper::customError(401, config('constants.Errors.Token'), $request->all(), config('constants.ServiceName.Google'));
            return response()->json($result, 200);
        }

        /* Validate Devicetype */
        $deviceTypeValidations = $this->deviceTypeValidations($deviceType);
        $clientId              = $deviceTypeValidations['clientToken'];
        if ($deviceTypeValidations['status'] ==  false) {
            $result = Helper::customError(401, $deviceTypeValidations['message'], $request->all(), config('constants.ServiceName.Google'));
            return response()->json($result, 200);
        }

        /* Validate Token */
        $tokenResult = $this->GoogleTokenValidation($token, $clientId);

        if ($tokenResult['status'] ==  false) {
            $result = Helper::customError(401, $tokenResult['message'], $request->all(), config('constants.ServiceName.Google'));
            return response()->json($result, 200);
        }

        $googleUserdata = $this->TokenDetails($token);

        $password       = config('constants.default.Password');
        $email          = $googleUserdata->email;
        $contact        = $googleUserdata->name;
        $company        = $googleUserdata->given_name;
        $phone          = config('constants.default.DefaultPhoneNumber');
        

        if (!empty($googleUserdata->error)) {  /* If Token is Not Valid */
            return Helper::unauthenticatedAction($request->all(), config('constants.ServiceName.Google'));
        } else {

            $userDetails = AuthUser::where('email', $email)->first();

            $companyDetails = Company::where('name', $company)->first();

            if (!empty($companyDetails)) {
                $company = $company . '' . rand(4, 10000);
            }

            if ($userDetails == null) { /* Check Already Have This Email in DB */

                $userData['company']  = $company;
                $userData['contact']  = $contact;
                $userData['email']    = $email;
                $userData['phone']    = $phone;
                $userData['password'] = $password;

                /* ========= Insertion Processing ========= */
                $userResult = $this->SignupStoreService->doStore($userData);

                if ($userResult == true) {
                    $oClient = OClient::where('password_client', 1)->first();
                    return Helper::getTokenAndRefreshToken($oClient, $email, $password);
                } else {
                    return Helper::unauthenticatedAction($request->all(), config('constants.ServiceName.Google'));
                }
            } else { /* Otherwise It Login To Normal Access Token Generation */
                $loginUser = AuthUser::where('email', $email)->first();
                if ($loginUser) {
                    $oClient = OClient::where('password_client', 1)->first();
                    return Helper::getTokenAndRefreshToken($oClient, $email, $password);
                } else {
                    return Helper::unauthenticatedAction($request->all(), config('constants.ServiceName.Google'));
                }
            }
        }
    }

    public function deviceTypeValidations($devicetype)
    {
        if ($devicetype == config('constants.Device.ios')) {
            $clientId = config('constants.Google.iosClientId');
            $result['clientToken']  = $clientId;
            $result['status']  = True;
            return $result;
        } else if ($devicetype == config('constants.Device.android')) {
            $clientId = config('constants.Google.andriodClientId');
            $result['clientToken']  = $clientId;
            $result['status']  = True;
            return $result;
        } else {
            $result['clientToken']  = '';
            $result['message'] = config('constants.Errors.DeviceType');
            $result['status']  = false;
            return $result;
        }
    }

    public function GoogleTokenValidation($token, $clientId)
    {
        $idToken = explode('.', $token);

        if (empty($idToken[0])) {
            $result['message'] = config('constants.Errors.Token');
            $result['status'] = false;
            return $result;
        }
        if (empty($idToken[1])) {
            $result['message'] = config('constants.Errors.Token');
            $result['status'] = false;
            return $result;
        }
        if (empty($idToken[2])) {
            $result['message'] = config('constants.Errors.Token');
            $result['status'] = false;
            return $result;
        }

        $client = new \Google_Client(['client_id' => $clientId]);
        $payload = $client->verifyIdToken($token);
        if ($payload) {
            $result['status']  = true;
            return $result;
        } else {
            $result['message'] = config('constants.Errors.Token');
            $result['status']  = false;
            return $result;
        }
    }

    public function TokenDetails($token)
    {
        $googleUrl = config('constants.default.GoogleUserDetailsTokenUrl');
        $response  = Http::get($googleUrl . '?id_token=' . $token);
        return json_decode($response);
    }
}
