<?php

namespace App\Http\Controllers\API\Users;

use App\Helpers\Helper;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Passport\Client as OClient;

/* Models */
use App\Models\Auth\AuthUser;
use App\Models\Company\Company;

/* Service */
use App\Services\Signup\SignupServices;

/**
 * @SWG\Post( 
 *   path="/register",
 *   tags={"Users"},
 *   summary="Create a New User Account",
 *   operationId="Signup",
 * 	 		@SWG\Parameter(
 *          name="company",
 *          in="formData",
 *          required=true, 
 *          type="string" 
 *      ),
 * 	 		@SWG\Parameter(
 *          name="contact",
 *          in="formData",
 *          required=true, 
 *          type="string" 
 *      ),
 * 	 		@SWG\Parameter(
 *          name="email",
 *          in="formData",
 *          required=true, 
 *          type="string" 
 *      ),
 *  	    @SWG\Parameter(
 *          name="phone",
 *          in="formData",
 *          required=true, 
 *          type="string" 
 *      ),
 * 	        @SWG\Parameter(
 *          name="password",
 *          in="formData",
 *          required=true, 
 *          type="string" 
 *      ),
 *   @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
 *   @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
 *   @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
 * )
 *
 */

class RegisterController extends Controller
{

    public function __construct()
    {
        $this->SignupStoreService         = new SignupServices();
        $this->company                    = new Company();
        $this->authUser                   = new AuthUser();
    }

    # =============================================
    # =           User Registration               =
    # =============================================

    public function userRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company'  => 'required',
            'contact'  => 'required',
            'email'    => 'required|email',
            'phone'    => 'required|min:10|max:15',
            'password' => 'required|min:6',
        ]);

        if ($validator->fails()) {
            $validatorList = $validator->errors();
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.Signup'));
            return response()->json($result, 200);
        }

        /* Validation Check */
        if (!empty(Company::where('name', $request['company'])->count())) {
            $result = Helper::customError(401, config('constants.Errors.companyExist'), $request->all(), config('constants.ServiceName.Signup'));
            return response()->json($result, 200);
        }

        if ($request['contact'] == trim($request['contact']) && strpos($request['contact'], ' ') !== false) {
        } else {
            $result = Helper::customError(401, config('constants.Errors.contactSplit'), $request->all(), config('constants.ServiceName.Signup'));
            return response()->json($result, 200);
        }

        if (!empty(AuthUser::where('email', $request['email'])->count())) {
            $result = Helper::customError(401, config('constants.Errors.email'), $request->all(), config('constants.ServiceName.Signup'));
            return response()->json($result, 200);
        }

        if (strlen($request['phone']) < 10) {
            $result = Helper::customError(401, config('constants.Errors.phone'), $request->all(), config('constants.ServiceName.Signup'));
            return response()->json($result, 200);
        }

        /* ========= Insertion Processing ========= */
        $userResult = $this->SignupStoreService->doStore($request);

        if ($userResult == true) {
            $oClient = OClient::where('password_client', 1)->first();
            return Helper::getTokenAndRefreshToken($oClient, $request->email, $request->password);
        } else {
            return Helper::unauthenticatedAction($request->all(), config('constants.ServiceName.Signup'));
        }
    }
}
