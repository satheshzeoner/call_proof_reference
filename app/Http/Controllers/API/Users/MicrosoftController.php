<?php

namespace App\Http\Controllers\API\Users;

use App\Helpers\Helper;
use Http;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Passport\Client as OClient;
use Illuminate\Support\Facades\Auth;

/* Models */
use App\Models\Auth\AuthUser;
use App\Models\Company\Company;

/* Service */
use App\Services\Signup\SignupServices;

class MicrosoftController extends Controller
{

    public function __construct()
    {
        $this->SignupStoreService         = new SignupServices();
        $this->company                    = new Company();
        $this->authUser                   = new AuthUser();
    }

    # =============================================
    # =           Get User Microsoft Login           =
    # =============================================

    public function microsoftLogin(Request $request)
    {

        $token      = $request['token'];
        $deviceType = $request['device_type'];

        /* Field Token Validations */
        if ($token == null ||  $deviceType == null) {
            $result = Helper::customError(401, config('constants.Errors.Token'), $request->all(), config('constants.ServiceName.Microsoft'));
            return response()->json($result, 200);
        }

        /* Validate Devicetype */
        $deviceTypeValidations = $this->deviceTypeValidations($deviceType);
        if ($deviceTypeValidations['status'] ==  false) {
            $result = Helper::customError(401, $deviceTypeValidations['message'], $request->all(), config('constants.ServiceName.Microsoft'));
            return response()->json($result, 200);
        }

        /* Validate Token */
        $tokenResult = $this->MicrosoftTokenValidation($token);
        if ($tokenResult['status'] == false) {
            $result = Helper::customError(401, config('constants.Errors.Token'), $request->all(), config('constants.ServiceName.Microsoft'));
            return response()->json($result, 200);
        }


        $password          = config('constants.default.Password');
        $email             = $tokenResult['message']['preferred_username'];
        $contact           = $tokenResult['message']['name'];
        $company           = $tokenResult['message']['company'];
        $phone             = config('constants.default.DefaultPhoneNumber');

        if ($tokenResult['status'] == false) { /* If Token is Not Valid */
            return Helper::unauthenticatedAction();
        } else {

            $userDetails = AuthUser::where('email', $email)->first();

            $companyDetails = Company::where('name', $company)->first();

            if (!empty($companyDetails)) {
                $company = $company . '' . rand(4, 10000);
            }

            if (empty($userDetails)) { /* Check Already Have This Email in DB */

                $userData['company']  = $company;
                $userData['contact']  = $contact;
                $userData['email']    = $email;
                $userData['phone']    = $phone;
                $userData['password'] = $password;

                /* ========= Insertion Processing ========= */
                $userResult = $this->SignupStoreService->doStore($userData);

                if ($userResult == true) {
                    $oClient = OClient::where('password_client', 1)->first();
                    return Helper::getTokenAndRefreshToken($oClient, $email, $password);
                } else {
                    return Helper::unauthenticatedAction($request->all(), config('constants.ServiceName.Microsoft'));
                }
            } else { /* Otherwise It Login To Normal Access Token Generation */
                $loginUser = AuthUser::where('email', $email)->first();
                if ($loginUser) {
                    $oClient = OClient::where('password_client', 1)->first();
                    return Helper::getTokenAndRefreshToken($oClient, $email, $password);
                } else {
                    return Helper::unauthenticatedAction($request->all(), config('constants.ServiceName.Microsoft'));
                }
            }
        }
    }

    public function deviceTypeValidations($devicetype)
    {
        if ($devicetype == config('constants.Device.ios')) {
            $result['status']  = True;
            return $result;
        } else if ($devicetype == config('constants.Device.android')) {
            $result['status']  = True;
            return $result;
        } else {
            $result['message'] = config('constants.Errors.DeviceType');
            $result['status']  = false;
            return $result;
        }
    }

    public function MicrosoftTokenValidation($token)
    {
        $idToken = explode('.', $token);

        if (empty($idToken[0])) {
            $result['status'] = false;
            return $result;
        }
        if (empty($idToken[1])) {
            $result['status'] = false;
            return $result;
        }
        if (empty($idToken[2])) {
            $result['status'] = false;
            return $result;
        }

        $segmentOne   = base64_decode(strtr($idToken[0], '-_', '+/'));
        $segmentTwo   = base64_decode(strtr($idToken[1], '-_', '+/'));
        $segmentThree = base64_decode(strtr($idToken[2], '-_', '+/'));

        $headerDetails = json_decode($segmentOne, true);
        $bodyDetails   = json_decode($segmentTwo, true);

        if (!isset($bodyDetails['preferred_username'])) {
            $result['status'] = false;
            return $result;
        }

        $bodyDetails['company']   = explode(' ', $bodyDetails['preferred_username'])[0];

        /* Check Is Name is Email to Split */
        if (filter_var($bodyDetails['company'], FILTER_VALIDATE_EMAIL)) {
            $bodyDetails['company']   = explode('@', $bodyDetails['preferred_username'])[0];
        }

        /* Check Is Name Key Exist Otherwise Use Preferred_name */
        if (!array_key_exists("name", $bodyDetails)) {
            $profileName            = explode('@', $bodyDetails['preferred_username']);
            $profileName            = reset($profileName);
            $bodyDetails['company'] = $profileName;
            $bodyDetails['name']    = $profileName . ' ' . $profileName;
        }

        /* Validate the Split Name Conventions in Name */
        if ($bodyDetails['name'] == trim($bodyDetails['name']) && strpos($bodyDetails['name'], ' ') !== false) {
        } else {
            $bodyDetails['name']    = $bodyDetails['name'] . ' ' . $bodyDetails['name'];
        }

        $publicKeyURL       = config('constants.Microsoft.openidConfig');

        $publicKeyURLResult = json_decode(file_get_contents($publicKeyURL), true);

        $jwksUriResult    = json_decode(file_get_contents($publicKeyURLResult['jwks_uri']), true);

        $listOfKid = [];

        foreach ($jwksUriResult['keys'] as $key => $value) {
            array_push($listOfKid, $value['kid']);
        }

        if (in_array($headerDetails['kid'], $listOfKid)) {
            $result['message'] = $bodyDetails;
            $result['status'] = true;
            return $result;
        } else {
            $result['status'] = false;
            return $result;
        }
    }
}
