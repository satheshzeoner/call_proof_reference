<?php

namespace App\Http\Controllers\API\Users;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

/* Models */
use App\Models\Auth\AuthUser;

/* Service */
use App\Services\User\UserLogoutService;

class LogoutController extends Controller
{

    public function __construct()
    {
        $this->UserLogoutService   = new UserLogoutService();
    }

    # =============================================
    # =          Logout Process                   =
    # =============================================

    public function logoutUser(Request $request)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        $userLogoutResponse = $this->UserLogoutService->dologout($request, $userId, $companyId);

        if ($userLogoutResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $userLogoutResponse['response'], $request->all(), config('constants.ServiceName.Routes'));
        } else {
            return Helper::customError(400, $userLogoutResponse['message'], $request->all(), config('constants.ServiceName.Routes'));
        }
    }
}
