<?php

namespace App\Http\Controllers\API\Users;

use App\Helpers\Helper;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Http\Controllers\Controller;
use Laravel\Passport\Client as OClient;
use Carbon\Carbon;

/* Models */
use App\Models\Auth\AuthUser;
use App\Models\Company\Company;
use App\Models\AppleSigninTos\AppleSigninTos;

/* Service */
use App\Services\Signup\SignupServices;

/* Apple ClientSecretGeneration */
use Jose\Component\Core\AlgorithmManager;
use Jose\Component\KeyManagement\JWKFactory;
use Jose\Component\Signature\Algorithm\ES256;
use Jose\Component\Signature\JWSBuilder;
use Jose\Component\Signature\Serializer\CompactSerializer;

class AppleController extends Controller
{

    public function __construct()
    {
        $this->SignupStoreService         = new SignupServices();
        $this->company                    = new Company();
        $this->authUser                   = new AuthUser();
    }

    # =============================================
    # =           Get User Apple Login            =
    # =============================================

    public function appleLogin(Request $request)
    {
        /* Param AuthToken */
        $token      = $request['token'];

        /* Field Token Validations */
        if ($token == null) {
            $result = Helper::customError(401, config('constants.Errors.Token'), $request->all(), config('constants.ServiceName.Apple'));
            return response()->json($result, 200);
        }

        /* ClientSecret Generation */
        $clientSecretId = $this->clientSecretGeneration();

        /* Validate Token */
        $tokenResult = $this->doAppleTokenValidation($token, $clientSecretId, $request);

        if ($tokenResult['status'] == false) {
            $result = Helper::customError(401, $tokenResult['message'], $request->all(), config('constants.ServiceName.Apple'));
            return response()->json($result, 200);
        }

        $userDetails    = $tokenResult['userDetails'];
        $applesignintos = $tokenResult['applesignintos'];

        /* Process For NewSignup or Login To CallProof */
        return $this->doAppleSignup($userDetails, $applesignintos, $request);
    }

    public function doAppleSignup($userDetails, $applesignintos, $request)
    {
        $password          = $userDetails['password'];
        $email             = $userDetails['email'];
        $contact           = $userDetails['contact'];
        $company           = $userDetails['company'];
        $phone             = $userDetails['phone'];

        /* Check This Mail in PrivateMailid */
        $privateMail = AppleSigninTos::where('private_email', $email)->first();
        if (isset($privateMail)) {
            $email = $privateMail->email;
        }
        /* Check Email already exist in DB */
        $userEmailDetails = AuthUser::where('email', $email)->first();

        $companyDetails = Company::where('name', $company)->first();

        /* Incase of Already Present Company Name been Added Random Number For Unique Company */
        if (!empty($companyDetails)) {
            $company = $company . '' . rand(4, 10000);
        }

        if (empty($userEmailDetails)) { /* Check Already Have This Email in DB */

            $userData['company']  = $company;
            $userData['contact']  = $contact;
            $userData['email']    = $email;
            $userData['phone']    = $phone;
            $userData['password'] = $password;

            /* ========= Insertion Processing ========= */
            $userResult = $this->SignupStoreService->doStore($userData, $applesignintos);


            if ($userResult == true) {
                $oClient = OClient::where('password_client', 1)->first();
                return Helper::getTokenAndRefreshToken($oClient, $email, $password);
            } else {
                return Helper::unauthenticatedAction($request->all(), config('constants.ServiceName.Apple'));
            }
        } else { /* Otherwise It Login To Normal Access Token Generation */
            $loginUser = AuthUser::where('email', $email)->first();
            if ($loginUser) {
                $oClient = OClient::where('password_client', 1)->first();
                return Helper::getTokenAndRefreshToken($oClient, $email, $password);
            } else {
                return Helper::unauthenticatedAction($request->all(), config('constants.ServiceName.Apple'));
            }
        }
    }

    public function clientSecretGeneration()
    {
        # Save your private key from Apple in a file called `key value` config('constants.apple.privateKey')
        $keyFile = config('constants.Apple.privateKey');

        # Your 10-character Team ID
        $teamId   = config('constants.Apple.teamId');

        # Your Services ID, e.g. com.aaronparecki.services
        $clientId = config('constants.Apple.clientId');

        # Find the 10-char Key ID value from the portal
        $keyId    = config('constants.Apple.keyId');

        $algorithmManager = new AlgorithmManager([new ES256()]);
        $jwsBuilder = new JWSBuilder($algorithmManager);

        /* JWS as JSON WEBTOKEN */
        $jws = $jwsBuilder
            ->create()
            ->withPayload(json_encode([
                'iat' => time(),
                'exp' => time() + 86400 * 180,
                'iss' => $teamId,
                'aud' => config('constants.Apple.audLink'),
                'sub' => $clientId
            ]))
            ->addSignature(JWKFactory::createFromKey($keyFile), [
                'alg' => config('constants.Apple.alg'),
                'kid' => $keyId
            ])
            ->build();

        $serializer = new CompactSerializer();
        $token = $serializer->serialize($jws, 0);

        return $token;
    }

    public function doAppleTokenValidation($token, $clientSecretId, $request)
    {
        /* Given AuthToken as Verrification By AppleServer and Response the AccessToken and idToken */
        $response = Http::asForm()->post(config('constants.Apple.authTokenLink'), [
            'grant_type'    => 'authorization_code',
            'code'          => $token,
            'client_id'     => config('constants.Apple.clientId'),
            'client_secret' => $clientSecretId,
        ]);

        $appleTokenIdDetails = json_decode((string) $response->getBody(), true);

        /* Validation For AuthToken */
        if (isset($appleTokenIdDetails['error'])) {
            $result['message'] = $appleTokenIdDetails['error'] . ' ' . config('constants.Errors.basicToken');
            $result['status']  = false;
            return $result;
        }

        /* Validation For IDToken */
        if (!isset($appleTokenIdDetails['id_token'])) {
            $result['message'] = config('constants.Errors.idToken');
            $result['status']  = false;
            return $result;
        }

        /* idToken Has Splitted To . Dot Segments / Totally 3 Segments Will Present */
        $idToken = explode('.', $appleTokenIdDetails['id_token']);

        if (empty($idToken[0])) {
            $result['status'] = false;
            return $result;
        }
        if (empty($idToken[1])) {
            $result['status'] = false;
            return $result;
        }
        if (empty($idToken[2])) {
            $result['status'] = false;
            return $result;
        }

        /* idToken To PlainTextConvertion */
        $segmentTwo  = base64_decode(strtr($idToken[1], '-_', '+/'));
        $bodyDetails = json_decode($segmentTwo, true);

        /* UserEmail Validations */
        if (isset($bodyDetails['email'])) {

            $paramDetails = $request->all();

            $privateEmail = $bodyDetails['email'];

            if (isset($bodyDetails['is_private_email'])) {
                if ($bodyDetails['is_private_email'] == true) {
                    if (isset($paramDetails['email'])) {
                        $privateEmail         = $bodyDetails['email'];
                        $bodyDetails['email'] = $paramDetails['email'];
                    } else {
                        $privateEmail         = $bodyDetails['email'];
                    }
                }
            } else {
                $privateEmail         = $bodyDetails['email'];
                $bodyDetails['email'] = $bodyDetails['email'];
            }

            // /* Version */
            $version = Carbon::now()->format('Y-m-d');
            if (isset($paramDetails['version'])) {
                $version = $paramDetails['version'];
            }

            // /* Details for applesignintos */
            $applesignintos['email']         = $bodyDetails['email'];
            $applesignintos['private_email'] = $privateEmail;
            $applesignintos['version']       = $version;

            $userName                = explode('@', $bodyDetails['email'])[0];
            $userDetails['company']  = $bodyDetails['email'];
            $userDetails['contact']  = $userName . ' ' . $userName;
            $userDetails['email']    = $bodyDetails['email'];
            $userDetails['phone']    = config('constants.default.DefaultPhoneNumber');
            $userDetails['password'] = config('constants.default.Password');

            $result['status']         = true;
            $result['userDetails']    = $userDetails;
            $result['applesignintos'] = $applesignintos;
            return $result;
        } else { /* If There is No Email in UserDetails Trigger Error For New AuthToken */
            $result['message'] = config('constants.Errors.idToken');
            $result['status'] = false;
            return $result;
        }
    }
}
