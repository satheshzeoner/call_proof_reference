<?php

namespace App\Http\Controllers\API\Users;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

/* Service */
use App\Services\User\ForgotPasswordService;

class ForgotPasswordController extends Controller
{

    public function __construct()
    {
        $this->ForgotPasswordService   = new ForgotPasswordService();
    }

    # =============================================
    # =          Forgot Password Process                   =
    # =============================================

    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.ForgotPassword'));
            return response()->json($result, 200);
        }
        $forgotPasswordResponse = $this->ForgotPasswordService->doForgotPassword($request);

        if ($forgotPasswordResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $forgotPasswordResponse['response'], $request->all(), config('constants.ServiceName.ForgotPassword'));
        } else {
            return Helper::customError(400, $forgotPasswordResponse['message'], $request->all(), config('constants.ServiceName.ForgotPassword'));
        }
    }
}
