<?php

namespace App\Http\Controllers\API\Task;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Validator;

/* Models */
use App\Models\Auth\AuthUser;

/* Service */
use App\Services\Task\TaskServices;
use App\Services\Task\TaskCompletionServices;



class TaskController extends Controller
{

    public function __construct()
    {
        $this->TaskServices = new TaskServices();
        $this->TaskCompletionServices = new TaskCompletionServices();
    }


    /** 
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/contact/{contact_id}/task/{task_id}",
     * tags={"Task"},
     * summary="Retrieve the Task Details",
     * operationId="Task-get",
     *     consumes={"application/x-www-form-urlencoded"},
     *      @SWG\Parameter(
     *      name="contact_id",
     *      in="path",
     *      required=false,
     *      type="integer",
     *      ),
     *      @SWG\Parameter(
     *      name="task_id",
     *      in="path",
     *      required=false,
     *      type="integer",
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =              Task Get Details             =
    # =============================================

    public function getTaskDetails(Request $request, $contactId, $taskId)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;
        $taskResponse = $this->TaskServices->getSpecificTaskDetails($contactId, $taskId, $request);
        if ($taskResponse['status'] == true) {
            return Helper::customResponse(200, $taskResponse['response'], $request->all(), config('constants.ServiceName.Task'));
        } else {
            return Helper::customError(400, $taskResponse['message'], $request->all(), config('constants.ServiceName.Task'));
        }
    }

    /**
     * @SWG\Post(
     * security = { { "Bearer": {} } },
     * path="/contact/{contact_id}/task",
     * tags={"Task"},
     * summary="Create New Schedule Task",
     * operationId="Task-add-Schedule",
     * consumes={"application/x-www-form-urlencoded"},
     *      @SWG\Parameter(
     *      name="contact_id",
     *      in="path",
     *      required=true,
     *      type="integer",
     *      ),
     *      @SWG\Parameter(
     *      name="people",
     *      in="formData",
     *      required=true,
     *      type="string",
     *      ),
     *      @SWG\Parameter(
     *      name="task_assignment",
     *      in="formData",
     *      required=true,
     *      type="string",
     *      ),
     *      @SWG\Parameter(
     *      name="date",
     *      in="formData",
     *      required=true,
     *      type="string",
     *      ),
     *      @SWG\Parameter(
     *      name="time",
     *      in="formData",
     *      required=true,
     *      type="string",
     *      ),
     *      @SWG\Parameter(
     *      name="duration",
     *      in="formData",
     *      required=true,
     *      type="string",
     *      ),
     *      @SWG\Parameter(
     *      name="notes",
     *      in="formData",
     *      required=false,
     *      type="string",
     *      ),
     *      @SWG\Parameter(
     *      name="google_calender",
     *      in="formData",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =              Task ADD                     =
    # =============================================


    public function taskAddProcess(Request $request, $contactId)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;
        $taskResponse = $this->TaskServices->addTaskDetails($userId, $companyId, $contactId, $request);
        if ($taskResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $taskResponse['response'], $request->all(), config('constants.ServiceName.Task'));
        } else {
            return Helper::customError(400, $taskResponse['message'], $request->all(), config('constants.ServiceName.Task'));
        }
    }


    /**
     * @SWG\Put(
     * security = { { "Bearer": {} } },
     * path="/contact/{contact_id}/task/{task_id}",
     * tags={"Task"},
     * summary="Task to Put Schedule Task",
     * operationId="Task-update-Schedule",
     * consumes={"application/x-www-form-urlencoded"},
     *      @SWG\Parameter(
     *      name="contact_id",
     *      in="path",
     *      required=false,
     *      type="integer",
     *      ),
     *      @SWG\Parameter(
     *      name="task_id",
     *      in="path",
     *      required=true,
     *      type="integer",
     *      ),
     *      @SWG\Parameter(
     *      name="task_assignment",
     *      in="formData",
     *      required=true,
     *      type="string",
     *      ),
     *      @SWG\Parameter(
     *      name="date",
     *      in="formData",
     *      required=true,
     *      type="string",
     *      ),
     *      @SWG\Parameter(
     *      name="time",
     *      in="formData",
     *      required=true,
     *      type="string",
     *      ),
     *      @SWG\Parameter(
     *      name="duration",
     *      in="formData",
     *      required=true,
     *      type="string",
     *      ),
     *      @SWG\Parameter(
     *      name="notes",
     *      in="formData",
     *      required=false,
     *      type="string",
     *      ),
     *      @SWG\Parameter(
     *      name="google_calender",
     *      in="formData",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =              Task Update                  =
    # =============================================


    public function taskUpdateProcess(Request $request, $contactId, $taskId)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;
        $taskResponse = $this->TaskServices->updateTaskDetails($userId, $companyId, $contactId, $taskId, $request);
        if ($taskResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $taskResponse['response'], $request->all(), config('constants.ServiceName.Task'));
        } else {
            return Helper::customError(400, $taskResponse['message'], $request->all(), config('constants.ServiceName.Task'));
        }
    }

    /** 
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/task-type",
     * tags={"Task"},
     * summary="Retrieve the Task Details",
     * operationId="Task-type",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =              Task Type                    =
    # =============================================


    public function taskType(Request $request)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;
        $taskResponse = $this->TaskServices->getTaskTypeDetails($userId, $companyId);
        if ($taskResponse['status'] == true) {
            return Helper::customResponse(200, $taskResponse['response'], $request->all(), config('constants.ServiceName.Task'));
        } else {
            return Helper::customError(400, $taskResponse['message'], $request->all(), config('constants.ServiceName.Task'));
        }
    }

    /** 
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/task-completion",
     * tags={"Task"},
     * summary="Retrieve the Task Completion Details",
     * operationId="Task-completion",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =       Task Completion Details             =
    # =============================================


    public function taskDetails(Request $request)
    {

        /* Validations for taskId */
        $validator = Validator::make($request->all(), [
            'task_type'   => 'numeric|nullable',
            'task_status' => 'numeric|nullable',
        ]);

        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.Contact'));
            return response()->json($result, 200);
        }

        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        $paramDetails = $request->all();

        $taskResponse = $this->TaskCompletionServices->getTaskDetails($userId, $companyId, $paramDetails);
        if ($taskResponse['status'] == true) {
            return Helper::customResponsePagination(200,  $taskResponse['response']['task_completion'], $taskResponse['response']['paginationDetails'], $request, config('constants.ServiceName.Task'));
        } else {
            return Helper::customError(400, $taskResponse['message'], $request->all(), config('constants.ServiceName.Task'));
        }
    }

    /** 
     * @SWG\Put(
     * security = { { "Bearer": {} } },
     * path="/task-completion/{task_id}",
     * tags={"Task"},
     * summary="Update  the Task Completion Details",
     * operationId="Task-update",
     * @SWG\Parameter(
     *      name="task_id",
     *      in="path",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Parameter(
     *      name="task_status",
     *      in="formData",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =       Task Completion Updates             =
    # =============================================

    public function taskCompletionUpdate(Request $request, $taskId)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        $paramDetails = $request->all();

        /* Validations for taskId */
        $validator = Validator::make($request->all(), [
            'task_status' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.Contact'));
            return response()->json($result, 200);
        }

        /* TaskCompletion updates */
        $taskResponse = $this->TaskCompletionServices->taskCompletionStatusUpdates($userId, $companyId, $paramDetails, $taskId);
        if ($taskResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $taskResponse['response'], $request->all(), config('constants.ServiceName.Task'));
        } else {
            return Helper::customError(400, $taskResponse['message'], $request->all(), config('constants.ServiceName.Task'));
        }
    }
}
