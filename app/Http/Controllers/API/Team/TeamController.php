<?php

namespace App\Http\Controllers\API\Team;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Validator;

/* Models */
use App\Models\Auth\AuthUser;

/* Service */
use App\Services\Team\TeamService;

/* Repositories */

class TeamController extends Controller
{

    public function __construct()
    {
        $this->TeamService = new TeamService();
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/team",
     * tags={"Team"},
     * summary="Get Team Details",
     * operationId="TeamApi",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    public function getTeam(Request $request)
    {
        $userId = Auth::user()->id;
        $teamResponse = $this->TeamService->doGetTeam($request, $userId);
        if ($teamResponse['status'] == true) {
            return Helper::customResponse(200, $teamResponse['response'], $request->all(), config('constants.ServiceName.Team'));
        } else {
            return Helper::customError(400, $teamResponse['message'], $request->all(), config('constants.ServiceName.Team'));
        }
    }
}
