<?php

namespace App\Http\Controllers\API\Places;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Validator;

/* Service */
use App\Services\Places\PlacesServices;
use App\Services\Places\PlacesContactAddServices;


/* Models */
use App\Models\Auth\AuthUser;

class PlacesController extends Controller
{

    public function __construct()
    {
        $this->PlacesServices           = new PlacesServices();
        $this->PlacesContactAddServices = new PlacesContactAddServices();
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/place-category",
     * tags={"Places"},
     * summary="Retrieve the Places Category Details",
     * operationId="PlacesCategoryList",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =              Places                       =
    # =============================================
    public function placeCategoryList(Request $request)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        $placesCategoryResponse = $this->PlacesServices->getPlacesCategoryList($request, $userId, $companyId);
        if ($placesCategoryResponse['status'] == true) {
            return Helper::customResponse(200, $placesCategoryResponse['response'], $request->all(), config('constants.ServiceName.Places'));
        } else {
            return Helper::customError(400, $placesCategoryResponse['message'], $request->all(), config('constants.ServiceName.Places'));
        }
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/places/latitude/{latitude}/longitude/{longitude}",
     * tags={"Places"},
     * summary="Retrieve the Places Contact Details",
     * operationId="PlacesContactList",
     * @SWG\Parameter(
     *      name="latitude",
     *      in="path",
     *      required=true,
     *      type="string",
     *      ),
     * @SWG\Parameter(
     *      name="longitude",
     *      in="path",
     *      required=true,
     *      type="string",
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =              Places Contact               =
    # =============================================

    public function getNearestPlacesDetails(Request $request, $latitude, $longitude)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        $placesCategoryResponse = $this->PlacesServices->getNearestPlacesContactList($request, $userId, $companyId, $latitude, $longitude);
        if ($placesCategoryResponse['status'] == true) {
            return Helper::customResponsePagination(200,  $placesCategoryResponse['response']['places_contacts'], $placesCategoryResponse['response']['paginationDetails'], $request, config('constants.ServiceName.Places'));
        } else {
            return Helper::customError(400, $placesCategoryResponse['message'], $request->all(), config('constants.ServiceName.Places'));
        }
    }

    /**
     * @SWG\Post(
     * security = { { "Bearer": {} } },
     * path="/place-contact",
     * tags={"Places"},
     * summary="Create New PlaceContact in Callproof Contacts",
     * operationId="CreateNewPlaceContacts",
     *  @SWG\Parameter(
     *      name="place_id",
     *      in="formData",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Parameter(
     *      name="name",
     *      in="formData",
     *      required=false,
     *      type="string",
     *      ),
     * @SWG\Parameter(
     *      name="latitude",
     *      in="formData",
     *      required=true,
     *      type="string",
     *      ),
     * @SWG\Parameter(
     *      name="longitude",
     *      in="formData",
     *      required=true,
     *      type="string",
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # = Places Contact Add to CallProof Contacts  =
    # =============================================

    public function storePlaceContact(Request $request)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        /* Place Contact Parm validation */
        $validator = Validator::make($request->all(), [
            'place_id'  => 'required|regex:/[0-9]/',
            'name'      => 'required',
            'latitude'  => 'required',
            'longitude' => 'required',
        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.Contact'));
            return response()->json($result, 200);
        }

        /* ParamRequest */
        $paramDetails = $request->all();

        $placesCategoryResponse = $this->PlacesContactAddServices->doStorePlaceContact($paramDetails, $userId, $companyId);
        if ($placesCategoryResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $placesCategoryResponse['response'], $request->all(), config('constants.ServiceName.Places'));
        } else {
            return Helper::customError(400, $placesCategoryResponse['message'], $request->all(), config('constants.ServiceName.Places'));
        }
    }
}
