<?php

namespace App\Http\Controllers\API;

use Exception;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Client as OClient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    public function __construct()
    {
        $this->user_model = new User();
    }

    /**
     * @SWG\Post(
     *   path="/login",
     *   tags={"Users"},
     *   summary="Retrieve the authenticated Access For User",
     *   operationId="Login",
     * 	 		@SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          required=true,
     *          type="string"
     *      ),
     * 	 		@SWG\Parameter(
     *          name="password",
     *          in="formData",
     *          required=true,
     *          type="string"
     *      ),
     *   @SWG\Response(response=200, description="Successful request, often a GET"),
     *   @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     *   @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     *   @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =           Get User Login                  =
    # =============================================

    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $this->user_model->LoginUserEvent(request('email'), request('password'));
            $oClient = OClient::where('password_client', 1)->first();
            return Helper::getTokenAndRefreshToken($oClient, request('email'), request('password'));
        } else {
            $result =  Helper::customError(401, config('constants.Errors.loginError'), $user = ['email' => request('email'), 'password' => request('password')], config('constants.ServiceName.Login'));
            return response()->json($result, 200);
        }
    }

    # =============================================
    # =           Get AccessToken                 =
    # =============================================

    public function getAccessToken(Request $request)
    {
        $paramDetails = $request->all();

        $validator = Validator::make($request->all(), [
            'refresh_token' => 'required'
        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.Login'));
            return response()->json($result, 200);
        }

        $oClient = OClient::where('password_client', 1)->first();
        try {
            return Helper::getRefreshTokenbasedAccess($oClient, $paramDetails['refresh_token']);
        } catch (Exception $e) {
            $result =  Helper::customError(401, config('constants.Errors.GetAccessToken'), config('constants.ServiceName.Login'));
            return response()->json($result, 200);
        }
    }
}
