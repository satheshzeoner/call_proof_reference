<?php

namespace App\Http\Controllers\API\Event;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;

/* Models */
use App\Models\Auth\AuthUser;

/* Service */
use App\Services\Event\EventTypeListService;
use App\Services\Event\EventListService;


class EventController extends Controller
{

    public function __construct()
    {
        $this->EventTypeListService        = new EventTypeListService();
        $this->EventListService            = new EventListService();
    }

    # =============================================
    # =       EventType List                      =
    # =============================================


    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/event-type",
     * tags={"Event"},
     * summary="Retrieve the EventType List Details",
     * operationId="EventType",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function eventTypeList(Request $request)
    {
        $userId    = Auth::user()->id;
        $companyId = AuthUser::find($userId)->userProfile->company_id;

        $eventResponse = $this->EventTypeListService->getEventTypeList($userId, $companyId);

        if ($eventResponse['status'] == true) {
            $result = Helper::customResponse(200, $eventResponse['response'], $request->all(), config('constants.ServiceName.Event'), 1600);
            return response()->json($result, 200);
        } else {
            return Helper::customError(400, $eventResponse['message'], $request->all(), config('constants.ServiceName.Event'));
        }
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/events",
     * tags={"Event"},
     * summary="Retrieve the Events Details",
     * operationId="EventsList",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =       EventType List                      =
    # =============================================

    public function eventsDetails(Request $request)
    {
        $userId    = Auth::user()->id;
        $companyId = AuthUser::find($userId)->userProfile->company_id;

        $eventResponse = $this->EventListService->getEventsList($userId, $companyId, $request);

        if ($eventResponse['status'] == true) {
            return Helper::customResponsePagination(200,  $eventResponse['response']['events'], $eventResponse['response']['paginationDetails'], $request, config('constants.ServiceName.Event'));
        } else {
            return Helper::customError(400, $eventResponse['message'], $request->all(), config('constants.ServiceName.Event'));
        }
    }
}
