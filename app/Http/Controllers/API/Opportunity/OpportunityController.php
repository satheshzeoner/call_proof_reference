<?php

namespace App\Http\Controllers\API\Opportunity;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Validator;
/* Service */
use App\Services\Opportunity\OpportunityService;
/* Models */
use App\Models\Auth\AuthUser;

class OpportunityController extends Controller
{

    public function __construct()
    {
        $this->OpportunityService = new OpportunityService();
    }

    # =============================================
    # =              opportunities                =
    # =============================================

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/opportunities",
     * tags={"Opportunities"},
     * summary="Retrieve the Opportunity Details",
     * operationId="OpportunityApi",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    public function opportunities(Request $request, $oppUserId = NULL)
    {
        $userId = Auth::user()->id;
        $opportunitiesResponse = $this->OpportunityService->getOpportunities($request, $userId, $oppUserId);
        if ($opportunitiesResponse['status'] == true) {
            return Helper::customResponse(200, $opportunitiesResponse['response'], $request->all(), config('constants.ServiceName.Opportunity'));
        } else {
            return Helper::customError(400, $opportunitiesResponse['message'], $request->all(), config('constants.ServiceName.Opportunity'));
        }
    }

    /**
     * @SWG\Put(
     * security = { { "Bearer": {} } },
     * path="/opportunities/{id}",
     *    tags={"Opportunities"},
     *    summary="Update the opportunities",
     *    operationId="updateOpportunities API",
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     required=true, 
     *     type="integer", 
     *      ),
     *  @SWG\Parameter(
     *     name="opportunity_name",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="linked_accounts",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="linked_contacts",
     *     in="formData",
     *     required=false, 
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="opportunity_stage_id",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="opportunity_type_id",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="owner_id",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="value",
     *     in="formData",
     *     required=false,
     *     type="integer", 
     *      ),
     *  @SWG\Parameter(
     *     name="probability",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="close_date",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *  @SWG\Parameter(
     *     name="notes",
     *     in="formData",
     *     required=false,
     *     type="string", 
     *      ),
     *   @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     *   @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     *   @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function updateOpportunities(Request $request, $opportunityId)
    {
        $validator = Validator::make($request->all(), [
            'opportunity_stage_id' => 'required|exists:cp_oppstage,id',
            'opportunity_type_id' => 'required|exists:cp_opptype,id',
            'owner_id' => 'required|exists:auth_user,id',
            'close_date' => 'date',
            'value' => 'numeric',
            'probability' => 'numeric',

        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.Opportunity'));
            return response()->json($result, 200);
        }
        $userId = Auth::user()->id;
        $updateOpportunitiesResponse = $this->OpportunityService->updateOpportunities($request, $userId, $opportunityId);
        if ($updateOpportunitiesResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $updateOpportunitiesResponse['message'], $request->all(), config('constants.ServiceName.Opportunity'));
        } else {
            return Helper::customError(500, $updateOpportunitiesResponse['message'], $request->all(), config('constants.ServiceName.Opportunity'));
        }
    }

    /**
     * @SWG\Delete(
     * security = { { "Bearer": {} } },
     * path="/opportunities/{id}",
     * tags={"Opportunities"},
     * summary="Remove Opportunity",
     * operationId="RemoveOpportunityAPI",
     * @SWG\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    public function removeOpportunity(Request $request, $opportunityId)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        $opportunityResponse = $this->OpportunityService->destroyOpportunity($request, $userId, $companyId, $opportunityId);

        if ($opportunityResponse['status'] == true) {
            return Helper::customUpdatedResponse(200, $opportunityResponse['response'], $request->all(), config('constants.ServiceName.Opportunity'));
        } else {
            return Helper::customError(400, $opportunityResponse['message'], $request->all(), config('constants.ServiceName.Opportunity'));
        }
    }


    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/opportunities-type",
     * tags={"Opportunities"},
     * summary="Retrieve the Opportunity Type Details",
     * operationId="OpportunityTypeApi",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    public function opportunitiesType(Request $request)
    {
        $userId = Auth::user()->id;
        $opportunitiesTypeResponse = $this->OpportunityService->getOpportunitiesType($request, $userId);
        if ($opportunitiesTypeResponse['status'] == true) {
            return Helper::customResponse(200, $opportunitiesTypeResponse['response'], $request->all(), config('constants.ServiceName.Opportunity'));
        } else {
            return Helper::customError(400, $opportunitiesTypeResponse['message'], $request->all(), config('constants.ServiceName.Opportunity'));
        }
    }

    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/opportunities-stage",
     * tags={"Opportunities"},
     * summary="Retrieve the Opportunity Stage Details",
     * operationId="OpportunityStageAPI",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
    public function opportunitiesStage(Request $request)
    {
        $userId = Auth::user()->id;
        $opportunitiesStageResponse = $this->OpportunityService->getopportunitiesStage($request, $userId);
        if ($opportunitiesStageResponse['status'] == true) {
            return Helper::customResponse(200, $opportunitiesStageResponse['response'], $request->all(), config('constants.ServiceName.Opportunity'));
        } else {
            return Helper::customError(400, $opportunitiesStageResponse['message'], $request->all(), config('constants.ServiceName.Opportunity'));
        }
    }



    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/opportunities/user/{user_id}",
     * tags={"Opportunities"},
     * summary="Retrieve the Opportunity Detail Based On  UserId ",
     * operationId="OpportunitysApi",
     * 		@SWG\Parameter(
     *          name="user_id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */
}
