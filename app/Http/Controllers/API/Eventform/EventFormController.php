<?php

namespace App\Http\Controllers\API\Eventform;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Helper;

/* Models */
use App\Models\Auth\AuthUser;

/* Service */
use App\Services\EventForm\EventFormListService;
use App\Services\EventForm\EventFormScheduleTaskService;
use App\Services\EventForm\EventFormAddService;
use App\Services\EventForm\EventFormEditService;
use App\Services\EventForm\EventFormDestroyService;


class EventFormController extends Controller
{

    public function __construct()
    {
        $this->EventListServices        = new EventFormListService();
        $this->EventScheduleTaskService = new EventFormScheduleTaskService();
        $this->EventAddService          = new EventFormAddService();
        $this->EventEditService         = new EventFormEditService();
        $this->EventDestroyService      = new EventFormDestroyService();
    }

    # =============================================
    # =       EventForm Previous List             =
    # =============================================

    /**
     * @SWG\GET(
     * security = { { "Bearer": {} } },
     * path="/contact/{contact_id}/eventform",
     * tags={"EventForm"},
     * summary="Retrieve the EventForm previous List Details",
     * operationId="EventFormPreviousList",
     *      @SWG\Parameter(
     *          name="contact_id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    public function eventPreviousList(Request $request, $contactId)
    {
        $userId    = Auth::user()->id;
        $companyId = AuthUser::find($userId)->userProfile->company_id;

        $searchEvent = ($request->has('search')) ? $request['search'] : '';

        $eventResponse = $this->EventListServices->getPreviousEventList($userId, $companyId, $contactId, $searchEvent);

        if ($eventResponse['status'] == true) {
            return Helper::customResponsePagination(200,  $eventResponse['response']['eventform'], $eventResponse['response']['paginationDetails'], $request, config('constants.ServiceName.Event'));
        } else {
            return Helper::customError(400, $eventResponse['message'], $request->all(), config('constants.ServiceName.Event'));
        }
    }

    /**
     * @SWG\GET(
     * security = { { "Bearer": {} } },
     * path="/contact/{contact_id}/eventformselect",
     * tags={"EventForm"},
     * summary="Retrieve the EventFormSelect previous List Details",
     * operationId="EventFormSelectList",
     *      @SWG\Parameter(
     *          name="contact_id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =       EventForm Select List               =
    # =============================================

    /* Get EventForm Select List Details */
    public function eventSelectList(Request $request, $contactId, $eventFormId = NULL)
    {
        $userId    = Auth::user()->id;
        $companyId = AuthUser::find($userId)->userProfile->company_id;

        $eventSelectResponse = $this->EventListServices->getSelectEventFormDetails($userId, $companyId, $contactId, $eventFormId);

        if ($eventSelectResponse['status'] == true) {
            $result = Helper::customResponse(200, $eventSelectResponse['response'], $request->all(), config('constants.ServiceName.Event'), 1600);
            return response()->json($result, 200);
        } else {
            return Helper::customError(400, $eventSelectResponse['message'], $request->all(), config('constants.ServiceName.Event'));
        }
    }

    /**
     * @SWG\GET(
     * security = { { "Bearer": {} } },
     * path="/contact/{contact_id}/eventformpeople",
     * tags={"EventForm"},
     * summary="Retrieve the EventFormPeople List Details",
     * operationId="EventFormPeopleList",
     *      @SWG\Parameter(
     *          name="contact_id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =       EventForm People List               =
    # =============================================

    public function eventFormPeopleList(Request $request, $contactId)
    {
        $userId    = Auth::user()->id;
        $companyId = AuthUser::find($userId)->userProfile->company_id;

        $eventSelectResponse = $this->EventListServices->getpeopleEventFormDetails($userId, $companyId, $contactId);

        if ($eventSelectResponse['status'] == true) {
            $result = Helper::customResponse(200, $eventSelectResponse['response'], $request->all(), config('constants.ServiceName.Event'), 1600);
            return response()->json($result, 200);
        } else {
            return Helper::customError(400, $eventSelectResponse['message'], $request->all(), config('constants.ServiceName.Event'));
        }
    }

    /**
     * @SWG\GET(
     * security = { { "Bearer": {} } },
     * path="/contact/{contact_id}/eventform/{event_form_id}",
     * tags={"EventForm"},
     * summary="Retrieve the EventForm Add Form Details",
     * operationId="EventFormAddDetails",
     *      @SWG\Parameter(
     *          name="contact_id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     *      @SWG\Parameter(
     *          name="event_form_id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =       EventForm Add Process Details       =
    # =============================================

    public function eventFormAddProcessDetails(Request $request, $contactId, $eventFormId)
    {
        $userId    = Auth::user()->id;
        $companyId = AuthUser::find($userId)->userProfile->company_id;

        $eventSelectResponse = $this->EventListServices->getEventFormAddDetails($userId, $companyId, $contactId, $eventFormId);

        if ($eventSelectResponse['status'] == true) {
            $result = Helper::customResponse(200, $eventSelectResponse['response'], $request->all(), config('constants.ServiceName.Event'), 1600);
            return response()->json($result, 200);
        } else {
            return Helper::customError(400, $eventSelectResponse['message'], $request->all(), config('constants.ServiceName.Event'));
        }
    }

    /**
     * @SWG\GET(
     * security = { { "Bearer": {} } },
     * path="/contact/eventformscheduletask",
     * tags={"EventForm"},
     * summary="Retrieve the ScheduleTask Form Details",
     * operationId="EventFormScheduleTask",
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =       EventForm Add Process Details       =
    # =============================================

    /* EventForm ScheduleTask Details */
    public function eventFormScheduleDetails(Request $request)
    {
        $userId    = Auth::user()->id;
        $companyId = AuthUser::find($userId)->userProfile->company_id;

        $eventScheduleResponse = $this->EventScheduleTaskService->getEventScheduleTaskDetails($userId, $companyId);

        if ($eventScheduleResponse['status'] == true) {
            $result = Helper::customResponse(200, $eventScheduleResponse['response'], $request->all(), config('constants.ServiceName.Event'), 1600);
            return response()->json($result, 200);
        } else {
            return Helper::customError(400, $eventScheduleResponse['message'], $request->all(), config('constants.ServiceName.Event'));
        }
    }


    /**
     * @SWG\Post(
     * security = { { "Bearer": {} } },
     * path="/contact/{contact_id}/eventformstore/{event_form_id}",
     * tags={"EventForm"},
     * summary="Create New ContactEventForm",
     * operationId="CreateEventForm",
     *      @SWG\Parameter(
     *          name="contact_id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     *      @SWG\Parameter(
     *          name="event_form_id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     *      @SWG\Parameter(
     *          name="people",
     *          in="formData",
     *          required=true, 
     *          type="string" 
     *      ),
     *      @SWG\Parameter(
     *          name="form_field",
     *          in="formData",
     *          required=true, 
     *          type="string" 
     *      ),
     *      @SWG\Parameter(
     *          name="schedule_task",
     *          in="formData",
     *          required=false, 
     *          type="string" 
     *      ),  
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =       EventForm Store Process Details     =
    # =============================================

    public function eventForminsertProcessDetails(Request $request, $contactId, $eventFormId)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        $validator = Validator::make($request->all(), [
            'form_field' => 'required',

        ]);
        if ($validator->fails()) {
            $result = Helper::customError(401, Helper::getArrayFirstElement($validator->errors()), $request->all(), config('constants.ServiceName.ContactMenu'));
            return response()->json($result, 200);
        }

        /* EventForm new Store Processing */
        $eventSelectResponse = $this->EventAddService->eventFormAddProcess($userId, $companyId, $contactId, $eventFormId, $request);

        if ($eventSelectResponse['status'] == true) {
            $result = Helper::customUpdatedResponse(200, $eventSelectResponse['response'], $request->all(), config('constants.ServiceName.Event'), 1600);
            return response()->json($result, 200);
        } else {
            return Helper::customError(400, $eventSelectResponse['message'], $request->all(), config('constants.ServiceName.Event'));
        }
    }


    /**
     * @SWG\Get(
     * security = { { "Bearer": {} } },
     * path="/contact/{contact_id}/eventformedit/{event_form_id}/contacteventform/{contact_event_form_id}",
     * tags={"EventForm"},
     * summary="Edit ContactEventForm Details",
     * operationId="EditDetailsEventForm",
     *      @SWG\Parameter(
     *          name="contact_id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     *      @SWG\Parameter(
     *          name="event_form_id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     *      @SWG\Parameter(
     *          name="contact_event_form_id",
     *          in="path",
     *          required=true, 
     *          type="string" 
     *      ), 
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =       EventForm Edit Process Details     =
    # =============================================

    public function eventFormEditProcessDetails(Request $request, $contactId, $EventFormId, $contactEventFormId)
    {

        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        /* EventForm new Store Processing */
        $eventEditResponse = $this->EventListServices->getEditContactEventFormDetails($userId, $companyId, $contactId, $EventFormId, $contactEventFormId);

        if ($eventEditResponse['status'] == true) {
            $result = Helper::customResponse(200, $eventEditResponse['response'], $request->all(), config('constants.ServiceName.Event'), 1600);
            return response()->json($result, 200);
        } else {
            return Helper::customError(400, $eventEditResponse['message'], $request->all(), config('constants.ServiceName.Event'));
        }
    }

    /**
     * @SWG\Post(
     * security = { { "Bearer": {} } },
     * path="/contact/{contact_id}/eventformedit/{event_form_id}/contacteventform/{contact_event_form_id}",
     * tags={"EventForm"},
     * summary="Update ContactEventForm",
     * operationId="UpdateCreateEventForm",
     *      @SWG\Parameter(
     *          name="contact_id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     *      @SWG\Parameter(
     *          name="event_form_id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     *      @SWG\Parameter(
     *          name="contact_event_form_id",
     *          in="path",
     *          required=true, 
     *          type="integer" 
     *      ),
     *      @SWG\Parameter(
     *          name="people",
     *          in="formData",
     *          required=true, 
     *          type="string" 
     *      ),
     *      @SWG\Parameter(
     *          name="form_field",
     *          in="formData",
     *          required=true, 
     *          type="string" 
     *      ),
     *      @SWG\Parameter(
     *          name="schedule_task",
     *          in="formData",
     *          required=false, 
     *          type="string" 
     *      ),  
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */


    # =============================================
    # =       EventForm Put Process Details     =
    # =============================================

    public function eventFormPutProcessDetails(Request $request, $contactId, $EventFormId, $contactEventFormId)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        /* EventForm Update Processing */
        $eventEditResponse = $this->EventEditService->eventFormUpdateProcess($userId, $companyId, $contactId, $EventFormId, $contactEventFormId, $request);

        if ($eventEditResponse['status'] == true) {
            $result = Helper::customUpdatedResponse(200, $eventEditResponse['response'], $request->all(), config('constants.ServiceName.Event'), 1600);
            return response()->json($result, 200);
        } else {
            return Helper::customError(400, $eventEditResponse['message'], $request->all(), config('constants.ServiceName.Event'));
        }
    }

    /**
     * @SWG\Delete(
     * security = { { "Bearer": {} } },
     * path="/contacteventform/{contact_event_form_id}",
     * tags={"EventForm"},
     * summary="Remove Contact EventForm Details",
     * operationId="RemoveContactEventformAPI",
     * @SWG\Parameter(
     *      name="contact_event_form_id",
     *      in="path",
     *      required=true,
     *      type="integer",
     *      ),
     * @SWG\Response(response=200, description="Successful request, often a GET"),
     * @SWG\Response(response=201, description="Successful request after a create, usually a POST"),
     * @SWG\Response(response=204, description="Successful request with no content returned, usually a PUT or PATCH"),
     * @SWG\Response(response=301, description="Permanently redirect to another endpoint"),
     * )
     *
     */

    # =============================================
    # =       EventForm Remove                    =
    # =============================================

    public function eventFormDestroy(Request $request, $contactEventFormId)
    {
        $userId      = Auth::user()->id;
        $companyId   = AuthUser::find($userId)->userProfile->company_id;

        /* EventForm Remove Processing */
        $eventRemoveResponse = $this->EventDestroyService->contactEventFormRemoveProcess($userId, $companyId, $contactEventFormId);

        if ($eventRemoveResponse['status'] == true) {
            $result = Helper::customUpdatedResponse(200, $eventRemoveResponse['response'], $request->all(), config('constants.ServiceName.Event'), 1600);
            return response()->json($result, 200);
        } else {
            return Helper::customError(400, $eventRemoveResponse['message'], $request->all(), config('constants.ServiceName.Event'));
        }
    }
}
