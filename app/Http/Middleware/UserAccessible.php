<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\Auth\AuthUser;
use App\Helpers\Helper;

class UserAccessible
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userId = Auth::user()->id;
        $isMarketManager = isset(AuthUser::find($userId)->userProfile->market_manager) ? (AuthUser::find($userId)->userProfile->market_manager) : null;
        $isManager = isset(AuthUser::find($userId)->userProfile->manager) ? (AuthUser::find($userId)->userProfile->manager) : null;
        if (!$isMarketManager | !$isManager) {
            return Helper::unauthenticatedAction($request->all(), '');
        }
        return $next($request);
    }
}
