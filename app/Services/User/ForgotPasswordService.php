<?php

namespace App\Services\User;

use Exception;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgotPasswordMail;
/* EventService */
use App\Services\Event\EventAddService;

/* Repositories */
use App\Repositories\User\ForgotPasswordRepositories;

class ForgotPasswordService
{
    public function __construct()
    {
        $this->ForgotPasswordRepositories = new ForgotPasswordRepositories();
    }

    public function doForgotPassword($request)
    {
        try {
            $email = strtolower($request->email);
            $user = $this->ForgotPasswordRepositories->getUserDetail($request, $email);
            if (!$user) {
                return $this->doErrorFormatDetails(config('constants.Errors.InvalidEmail'));
            }
            $userProfile = $this->ForgotPasswordRepositories->getUserProfileDetail($request, $user);
            $randomGeneratedSecretCode = $this->ForgotPasswordRepositories->generateRandomSecretCode($request);
            if ($userProfile) {
                $updatedUserProfile = $this->ForgotPasswordRepositories->updateUserProfile($request, $userProfile, $randomGeneratedSecretCode);
            }
            $dynamicURL = ([config('constants.ForgotPassword.domain_name'), $randomGeneratedSecretCode]);
            $dynamicEmailBodyKey = config('constants.DynamicForgotPasswordKey');
            $emailBody = str_replace($dynamicEmailBodyKey, $dynamicURL, config('constants.ForgotPassword.email_body'));
            $passwordResetLinkMail = $this->sendPasswordResetLinkMail($request, $user, $emailBody);
            return $this->doSuccessFormatDetails(config('constants.Success.ForgotPasswordResetLink'));
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function sendPasswordResetLinkMail($request, $user, $emailBody)
    {
        $detail['body'] = $emailBody;
        Mail::to($user->email)->send(new ForgotPasswordMail($detail));
        return true;
    }

    public function doSuccessFormatDetails($data)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response'] = $data;

        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails;
        $result['response'] = '';

        return $result;
    }
}
