<?php

namespace App\Services\User;

use Exception;

/* Models */
use App\Models\Auth\AuthUser;

/* Repositories */
use App\Repositories\User\UserRepositories;

class UserService
{
    public function __construct()
    {
        $this->UserRepositories = new UserRepositories();
    }

    public function getUsers($request, $userId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $salesUsers = $this->UserRepositories->getUsersDetails($request, $userId, $userCompanyId);
            return $this->doSuccessFormatDetails($salesUsers);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e);
        }
    }

    public function doSuccessFormatDetails($data)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response']['users'] = $data;

        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails->getMessage();
        $result['response'] = '';

        return $result;
    }
}
