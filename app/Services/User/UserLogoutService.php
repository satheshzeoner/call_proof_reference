<?php

namespace App\Services\User;

use Exception;

/* EventService */
use App\Services\Event\EventAddService;

/* Repositories */
use App\Repositories\User\UserLogoutRepositories;

class UserLogoutService
{
    public function __construct()
    {
        $this->UserLogoutRepositories = new UserLogoutRepositories();
    }

    public function dologout($request, $userId, $companyId)
    {
        try {
            $this->UserLogoutRepositories->doRemoveUserLogout($userId, $companyId);

            /* Event Store Process */
            $this->doEventAdd($userId, $companyId, config('constants.Event_name.mobile_logout'));

            return $this->doSuccessFormatDetails('logout Successully');
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function doSuccessFormatDetails($data)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response'] = $data;

        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails;
        $result['response'] = '';

        return $result;
    }
    /* ===============================EVENT========================== */
    public function doEventAdd($userId, $companyId, $eventName)
    {
        /* Event Creation */
        $EventAddService        = new EventAddService();
        $EventAddService->addEvent(
            $eventName,
            $eventCompanyId          = $companyId,
            $eventUserId             = $userId,
            $eventContactId          = null,
            $eventOtherUserId        = null,
            $eventCallId             = null,
            $eventStartTime          = null,
            $eventDuration           = null,
            $eventBadgeId            = null,
            $eventGooglePlace        = null,
            $eventTwilioCallId       = null,
            $eventContactEventFormId = null,
            $eventAppointmentId      = null,
            $eventFollowupId         = null,
            $eventMessage            = null
        );

        return true;
    }
    /* ===============================EVENT-END====================== */
}
