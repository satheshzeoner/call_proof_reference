<?php

namespace App\Services\User;

use Exception;

/* Models */
use App\Models\Auth\AuthUser;

/* Repositories */
use App\Repositories\User\UserProfileRepositories;

class UserProfileService
{
    public function __construct()
    {
        $this->UserProfileRepositories = new UserProfileRepositories();
    }

    public function updateUserProfile($request, $userId, $companyId)
    {
        try {
            $userProfileDetails = $this->UserProfileRepositories->updateUserProfileDetails($request, $userId, $companyId);

            return $this->doSuccessFormatDetails($userProfileDetails);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e);
        }
    }

    public function doSuccessFormatDetails($data)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response'] = config('constants.Success.UserProfile');

        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails->getMessage();
        $result['response'] = '';

        return $result;
    }
}
