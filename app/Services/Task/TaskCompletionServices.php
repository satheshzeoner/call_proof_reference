<?php

namespace App\Services\Task;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;

/* Repositories */
use App\Repositories\Task\TaskCompletionRepositories;

/* Models */
use App\Models\Followup\Followup;

/* EventService */
use App\Services\Event\EventAddService;

class TaskCompletionServices
{
    public function __construct()
    {
        $this->TaskCompletionRepositories    = new TaskCompletionRepositories();

        /* Initialze For LogSytem */
        $this->companyId                     = '';
        $this->userId                        = '';
    }

    /* Get task-FollowUp Details */
    public function getTaskDetails($userId, $companyId, $request)
    {
        /* Get Specific Task Details  */
        $taskDetails              = $this->TaskCompletionRepositories->getTaskCompletionDetails($userId, $companyId, $request);

        $followupFormattedDetails = $this->doFormattingGetTaskDetails($taskDetails);
        $paginationDetails        = Helper::customPagination($taskDetails);

        try {
            return $this->doSuccessFormat($followupFormattedDetails, $paginationDetails);
        } catch (Exception $e) {
            DB::rollback();
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    /* Update Task-Updates */
    public function taskCompletionStatusUpdates($userId, $companyId, $paramDetails, $taskId)
    {
        $this->companyId                     = $companyId;
        $this->userId                        = $userId;

        $taskResult['status']   = true;
        $taskResult['response'] = config('constants.Success.UpdateTask');
        $taskValidationResult   = $this->taskValidations($paramDetails, $taskId);

        if ($taskValidationResult['status'] == false) {
            $taskResult['status']   = false;
            $taskResult['message']  = $taskValidationResult['message'];
            return $taskResult;
        }
        DB::beginTransaction();
        try {
            $taskUpdateDetails['completed'] = $paramDetails['task_status'];
            $taskCompletionStatus = $this->TaskCompletionRepositories->taskCompletionStatusUpdate($userId, $companyId, $taskUpdateDetails, $taskId);
            $this->logEvents(config('constants.Success.UpdateTask') . ' ' . $taskId);
            DB::commit();

            /* Event Store Process */
            $this->doEventAdd($userId, $companyId, $paramDetails, $taskId, config('constants.Event_name.task_completed'));
        } catch (Exception $e) {
            DB::rollback();
            $this->logEventsError($e->getMessage());
            return $this->doErrorFormatDetails(config('constants.Errors.TaskUpdate'));
        }

        return $taskResult;
    }

    /* Task Validations */
    public function taskValidations($paramDetails, $taskId)
    {
        $taskValidationResult['status']  = true;
        $taskValidationResult['message'] = '';

        /* TaskId Validations */
        if (Followup::whereId($taskId)->exists() == false) {

            $taskValidationResult['status']  = false;
            $taskValidationResult['message'] = config('constants.Errors.TaskId');

            return $taskValidationResult;
        }

        /* Task Status Code Validations Check */
        if ($paramDetails['task_status'] == 0 || $paramDetails['task_status'] == 1) {
            $taskValidationResult['status']  = true;
            $taskValidationResult['message'] = '';
        } else {
            $taskValidationResult['status']  = false;
            $taskValidationResult['message'] = config('constants.Errors.TaskStatusCode');

            return $taskValidationResult;
        }

        return $taskValidationResult;
    }

    public function doFormattingGetTaskDetails($taskAllDetails)
    {

        $taskDetailsResult = [];
        foreach ($taskAllDetails['data'] as $taskKey => $taskDetailsValue) {

            if (isset($taskDetailsValue['contact_follow']['id'])) {



                $taskDetailsValue = $this->TaskCompletionRepositories->getContactPhoneDetails($taskDetailsValue);

                /* Task Schedule Time Set */
                $day      = Carbon::parse($taskDetailsValue['start'])->format('l');
                $month    = Carbon::parse($taskDetailsValue['start'])->format('M');
                $dateInt  = Carbon::parse($taskDetailsValue['start'])->format('d');
                $year     = Carbon::parse($taskDetailsValue['start'])->format('Y');
                $fullTime = Carbon::parse($taskDetailsValue['start'])->format('H:i:s A');

                $scheduleTimeSet = $day . ',' . $month . ' ' . $dateInt . ',' . $year . ',' . $fullTime;

                /* task Details Formatting */
                $taskDetails['task_id']              = $taskDetailsValue['id'];
                $taskDetails['task_type_id']         = $taskDetailsValue['task_type']['id'];
                $taskDetails['task_type_name']       = $taskDetailsValue['task_type']['name'];
                $taskDetails['task_date']            = Carbon::parse($taskDetailsValue['start'])->format('d/m/Y');
                $taskDetails['task_time']            = Carbon::parse($taskDetailsValue['start'])->format('H:i:s A');
                $taskDetails['task_duration']        = $taskDetailsValue['duration'];
                $taskDetails['task_notes']           = $taskDetailsValue['notes'];
                $taskDetails['task_schedule_time']   = $scheduleTimeSet;
                $taskDetails['task_completed']       = $taskDetailsValue['completed'];
                $taskDetails['contact_id']           = $taskDetailsValue['contact_follow']['id'];
                $taskDetails['contact_name']         = $taskDetailsValue['contact_follow']['first_name'] . ' ' . $taskDetailsValue['contact_follow']['last_name'];
                $taskDetails['contact_phone']        = $this->getPhoneFormattedAttribute($taskDetailsValue['contact_follow']['phone_number']);
                $taskDetails['contact_people']       = $taskDetailsValue['contact_follow']['people'];
                $taskDetails['contact_sales_rep']    = $taskDetailsValue['contact_follow']['rep'];
                $taskDetails['contact_contacted']    = $taskDetailsValue['contact_follow']['last_contacted'];

                array_push($taskDetailsResult, $taskDetails);
            }
        }

        return $taskDetailsResult;
    }


    public function getPhoneFormattedAttribute($phone)
    {
        if ($phone) {
            $phone = preg_replace("/[^0-9]/", "", $phone); //Remove all non-numers
            return substr($phone, 0, 3) . "-" . substr($phone, 3, 3) . "-" . substr($phone, 6);
        }
        return '';
    }

    public function doSuccessFormat($responseResult, $paginationDetails)
    {
        $result['status']                = true;
        $result['message']               = 'Success';
        $result['response']['task_completion']              = $responseResult;
        $result['response']['paginationDetails'] = $paginationDetails;
        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails;
        $result['response'] = '';

        return $result;
    }

    public function logEvents($messageDetails = null)
    {
        $taskCompletionLog['User_id']      = $this->userId;
        $taskCompletionLog['Company_id']   = $this->companyId;
        $taskCompletionLog['Message']      = $messageDetails;

        Log::notice($taskCompletionLog);

        return true;
    }

    public function logEventsError($messageDetails = null)
    {
        $taskCompletionLog['User_id']      = $this->userId;
        $taskCompletionLog['Company_id']   = $this->companyId;
        $taskCompletionLog['Message']      = $messageDetails;

        Log::error($taskCompletionLog);

        return true;
    }

    /* ===============================EVENT========================== */
    public function doEventAdd($userId, $companyId, $paramDetails, $followupId, $eventName)
    {
        if ($paramDetails['task_status'] == 1) {
            /* Event Creation */
            $eventAddService        = new EventAddService();
            $eventAddService->addEvent(
                $eventName,
                $eventCompanyId          = $companyId,
                $eventUserId             = $userId,
                $eventContactId          = null,
                $eventOtherUserId        = null,
                $eventCallId             = null,
                $eventStartTime          = null,
                $eventDuration           = null,
                $eventBadgeId            = null,
                $eventGooglePlace        = null,
                $eventTwilioCallId       = null,
                $eventContactEventFormId = null,
                $eventAppointmentId      = null,
                $eventFollowupId         = $followupId,
                $eventMessage            = null
            );
        }

        return true;
    }
    /* ===============================EVENT-END====================== */
}
