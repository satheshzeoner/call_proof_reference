<?php

namespace App\Services\Task;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

/* Repositories */
use App\Repositories\Task\TaskRepositories;

/* EventService */
use App\Services\Event\EventAddService;

class TaskServices
{
    public function __construct()
    {
        $this->TaskRepositories        = new TaskRepositories();

        /* Initialze For LogSytem */
        $this->companyId                     = '';
        $this->userId                        = '';
        $this->contactId                     = '';
    }

    public function getSpecificTaskDetails($contactId, $taskId, $request)
    {
        /* Get Specific Task Details  */
        $followupDetails = $this->TaskRepositories->getFollowupDetails($contactId, $taskId);
        $followupFormattedDetails = $this->doFormattingGetTaskDetails($followupDetails);
        try {
            return $this->doSuccessFormat($followupFormattedDetails);
        } catch (Exception $e) {
            DB::rollback();
            return $this->doErrorFormatDetails($e);
        }
    }


    /* Add Task List*/
    public function addTaskDetails($userId, $companyId, $contactId, $request)
    {

        $this->companyId                     = $companyId;
        $this->userId                        = $userId;
        $this->contactId                     = $contactId;

        $formDetails = $request->all();

        $peopleId               = collect(json_decode($formDetails['people']));
        $taskAssignmentPeopleId = collect(json_decode($formDetails['task_assignment']));

        /* DateTime Formatting */
        $dateTimeDetails = $formDetails['date'] . ' ' . $formDetails['time'];

        /* Strcturing DateTime Format */
        $dateTimeDetails = Carbon::parse($dateTimeDetails)->format('Y-m-d H:i:s');

        DB::beginTransaction();

        foreach ($peopleId as $peopleIdvalue) {

            $followupDetails['user_id']         = $peopleIdvalue;
            $followupDetails['contact_id']      = $contactId;
            $followupDetails['start']           = $dateTimeDetails;
            $followupDetails['duration']        = $formDetails['duration'];
            $followupDetails['notes']           = $formDetails['notes'];
            $followupDetails['send_google_cal'] = $formDetails['google_calender'];
            $followupDetails['created']         = now();

            try {

                $followupId = $this->TaskRepositories->storeFollowupValue($followupDetails);
                $this->logEvents(config('constants.Success.Followup') . ' Created Id :' . $followupId);

                /* Event Store Process */
                $this->doEventAdd($userId, $companyId, $followupId, config('constants.Event_name.task_scheduled'));
            } catch (Exception $e) {
                $this->logEventsError($e->getMessage());
                $scheduleResult['status']  = false;
                $scheduleResult['message'] = config('constants.Errors.Task');
                return $scheduleResult;
            }

            if ($taskAssignmentPeopleId != '') {
                foreach ($taskAssignmentPeopleId as $key => $taskAssignmentPeoplevalue) {
                    $followupPersonalDetails['followup_id']  = $followupId;
                    $followupPersonalDetails['personnel_id'] = $taskAssignmentPeoplevalue;

                    try {
                        $followupPersonalId = $this->TaskRepositories->storeFollowupPersonalValue($followupPersonalDetails);
                        $this->logEvents(config('constants.Success.FollowupPersonnel') . ' Created Id :' . $followupPersonalId);
                    } catch (Exception $e) {
                        $this->logEventsError($e->getMessage());
                        $scheduleResult['status']  = false;
                        $scheduleResult['message'] = config('constants.Errors.Task');
                        return $scheduleResult;
                    }
                }
            }
        }

        DB::commit();

        try {
            return $this->doSuccessFormat(config('constants.Success.addTask'));
        } catch (Exception $e) {
            DB::rollback();
            return $this->doErrorFormatDetails($e);
        }
    }


    /* Updation Process for Task/Schedule */
    public function updateTaskDetails($userId, $companyId, $contactId, $taskId, $request)
    {
        $this->companyId                     = $companyId;
        $this->userId                        = $userId;
        $this->contactId                     = $contactId;

        $formDetails = $request->all();

        $taskAssignmentPeopleId = collect(json_decode($formDetails['task_assignment']));

        /* DateTime Formatting */
        $dateTimeDetails = $formDetails['date'] . ' ' . $formDetails['time'];

        /* Strcturing DateTime Format */
        $dateTimeDetails = Carbon::parse($dateTimeDetails)->format('Y-m-d H:i:s');

        DB::beginTransaction();

        $followupDetails['contact_id']      = $contactId;
        $followupDetails['start']           = $dateTimeDetails;
        $followupDetails['duration']        = $formDetails['duration'];
        $followupDetails['notes']           = $formDetails['notes'];
        $followupDetails['send_google_cal'] = $formDetails['google_calender'];
        $followupDetails['created']         = now();

        try {
            /* Update Process */
            $this->TaskRepositories->updateFollowupValue($followupDetails, $taskId);
            $this->logEvents(config('constants.Success.Followup') . ' Updated Id :' . $taskId);
        } catch (Exception $e) {
            $this->logEventsError($e->getMessage());
            $scheduleResult['status']  = false;
            $scheduleResult['message'] = config('constants.Errors.Task');
            return $scheduleResult;
        }

        if ($taskAssignmentPeopleId != '') {

            $followupPersonalBaseId     = $this->TaskRepositories->removeSpecificFollowupId($taskId);

            foreach ($taskAssignmentPeopleId as $taskAssignmentPeoplevalue) {
                $followupPersonalDetails['followup_id']  = $taskId;
                $followupPersonalDetails['personnel_id'] = $taskAssignmentPeoplevalue;

                try {
                    $followupPersonalId = $this->TaskRepositories->storeFollowupPersonalValue($followupPersonalDetails);
                    $this->logEvents(config('constants.Success.FollowupPersonnel') . ' Created Id :' . $followupPersonalId);
                } catch (Exception $e) {
                    $this->logEventsError($e->getMessage());
                    $scheduleResult['status']  = false;
                    $scheduleResult['message'] = config('constants.Errors.Task');
                    return $scheduleResult;
                }
            }
        }

        DB::commit();

        try {
            return $this->doSuccessFormat(config('constants.Success.editTask'));
        } catch (Exception $e) {
            DB::rollback();
            return $this->doErrorFormatDetails($e);
        }
    }

    public function getTaskTypeDetails($userId, $companyId)
    {
        $TaskTypeDetails = $this->TaskRepositories->getTaskTypeListDetails($companyId);
        return $this->doSuccessGetFormat($TaskTypeDetails);
    }


    public function doFormattingGetTaskDetails($taskDetails)
    {
        $taskDetailsResult = [];
        foreach ($taskDetails as $taskDetailsValue) {
            $taskDetailsResult['task_id']              = $taskDetailsValue['id'];
            $taskDetailsResult['task_date']            = Carbon::parse($taskDetailsValue['start'])->format('d/m/Y');
            $taskDetailsResult['task_time']            = Carbon::parse($taskDetailsValue['start'])->format('H:i:s A');
            $taskDetailsResult['task_duration']        = $taskDetailsValue['duration'];
            $taskDetailsResult['task_notes']           = $taskDetailsValue['notes'];
            $taskDetailsResult['task_completed']       = $taskDetailsValue['completed'];
            $taskDetailsResult['task_send_office_cal'] = $taskDetailsValue['send_office_cal'];
            $taskDetailsResult['task_send_google_cal'] = $taskDetailsValue['send_google_cal'];
            $taskDetailsResult['task_people']          = $taskDetailsValue['follow_people'];
        }

        return $taskDetailsResult;
    }


    public function doSuccessFormat($responseResult)
    {
        $result['status']   = true;
        $result['message']  = 'Success';
        $result['response'] = $responseResult;
        return $result;
    }

    public function doSuccessGetFormat($responseResult)
    {
        $result['status']   = true;
        $result['message']  = 'Success';
        $result['response']['task-type'] = $responseResult;
        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails->getMessage();
        $result['response'] = '';

        return $result;
    }

    public function logEvents($messageDetails = null)
    {
        $taskLog['User_id']      = $this->userId;
        $taskLog['Company_id']   = $this->companyId;
        $taskLog['Contact_id']   = $this->contactId;
        $taskLog['Message']      = $messageDetails;

        Log::notice($taskLog);

        return true;
    }

    public function logEventsError($messageDetails = null)
    {
        $taskLog['User_id']      = $this->userId;
        $taskLog['Company_id']   = $this->companyId;
        $taskLog['Contact_id']   = $this->contactId;
        $taskLog['Message']      = $messageDetails;

        Log::error($taskLog);

        return true;
    }

    /* ===============================EVENT========================== */
    public function doEventAdd($userId, $companyId, $followupId, $eventName)
    {
        /* Event Creation */
        $eventAddService        = new EventAddService();
        $eventAddService->addEvent(
            $eventName,
            $eventCompanyId          = $companyId,
            $eventUserId             = $userId,
            $eventContactId          = null,
            $eventOtherUserId        = null,
            $eventCallId             = null,
            $eventStartTime          = null,
            $eventDuration           = null,
            $eventBadgeId            = null,
            $eventGooglePlace        = null,
            $eventTwilioCallId       = null,
            $eventContactEventFormId = null,
            $eventAppointmentId      = null,
            $eventFollowupId         = $followupId,
            $eventMessage            = null
        );

        return true;
    }
    /* ===============================EVENT-END====================== */
}
