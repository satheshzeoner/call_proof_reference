<?php

namespace App\Services\Route;

use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

/* Models */
use App\Models\Route\Route;
use App\Models\Route\RouteContact;

/* Repositories */
use App\Repositories\Route\RouteRepositories;

class RouteDestroyServices
{
    public function __construct()
    {
        $this->RouteRepositories        = new RouteRepositories();

        /* Initialze For LogSytem */
        $this->companyId                     = '';
        $this->userId                        = '';
        $this->contactId                     = '';
    }

    /* Routes Get List*/
    public function destroyRoute($request, $userId, $companyId, $routeId)
    {
        $this->companyId                     = $companyId;
        $this->userId                        = $userId;

        /* Validations */
        $validationResult = $this->routeIdVaidation($routeId);
        if ($validationResult['status'] == false) {
            $this->logEventsError($validationResult['message']);
            return $this->doErrorFormatDetails($validationResult['message']);
        }

        DB::beginTransaction();
        try {
            /* Route Details */
            $this->RouteRepositories->removeRoutes($userId, $companyId, $routeId);
            $this->logEvents(config('constants.Success.RemoveRoutes') . ' ' . $routeId);

            /* RouteContact Details */
            $this->RouteRepositories->removeRoutesContact($userId, $companyId, $routeId);
            $this->logEvents(config('constants.Success.RemoveRoutes') . ' ' . $routeId);

            DB::commit();
            return $this->doSuccessFormatDetails(config('constants.Success.DeleteRoutes'));
        } catch (Exception $e) {
            DB::rollback();
            $this->logEventsError($e->getMessage());
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    /* Validation for Id Has present or Not */
    public function routeIdVaidation($routeId)
    {
        $routeDetails['status'] = true;
        $routeDetails['message'] = '';
        $routeDetailsResult = Route::whereId($routeId)->exists();
        if ($routeDetailsResult == false) {
            $this->logEventsError(config('constants.Errors.RouteId'));
            $routeDetails['status'] = false;
            $routeDetails['message'] = config('constants.Errors.RouteId');
            return $routeDetails;
        }
        $routeContactDetailsResult = RouteContact::whereRoute_id($routeId)->exists();
        if ($routeContactDetailsResult == false) {
            $this->logEventsError(config('constants.Errors.RouteId'));
            $routeDetails['status'] = false;
            $routeDetails['message'] = config('constants.Errors.RouteId');
            return $routeDetails;
        }

        return $routeDetails;
    }

    public function doSuccessFormatDetails($routeDetails)
    {
        $result['status']                      = true;
        $result['message']                     = 'Success';
        $result['response']                    = $routeDetails;

        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails;
        $result['response'] = '';

        return $result;
    }

    public function logEvents($messageDetails = null)
    {
        $routeDestroyLog['User_id']      = $this->userId;
        $routeDestroyLog['Company_id']   = $this->companyId;
        $routeDestroyLog['Message']      = $messageDetails;

        Log::notice($routeDestroyLog);

        return true;
    }

    public function logEventsError($messageDetails = null)
    {
        $routeDestroyLog['User_id']      = $this->userId;
        $routeDestroyLog['Company_id']   = $this->companyId;
        $routeDestroyLog['Message']      = $messageDetails;

        Log::error($routeDestroyLog);

        return true;
    }
}
