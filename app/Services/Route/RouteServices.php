<?php

namespace App\Services\Route;

use Exception;

/* Models */


/* Repositories */
use App\Repositories\Route\RouteRepositories;

class RouteServices
{
    public function __construct()
    {
        $this->RouteRepositories = new RouteRepositories();
    }

    /* Routes Get List*/
    public function getCurrentRouteDetails($request, $userId, $companyId, $routeId)
    {

        try {
            /* Get Specific Route Details */
            $routeDetails = $this->RouteRepositories->getCurrentRouteList($userId, $companyId, $routeId);
            $routeResultDetails['route_details']         = $this->doFormattingRouteDetails($routeDetails);
            $routeResultDetails['route_current_details'] = $this->getCurrentRouteList($routeDetails);

            return $this->doSuccessFormatDetails($routeResultDetails);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e);
        }
    }

    /* Get MyRoutes */
    public function getMyRouteDetails($request, $userId, $companyId)
    {

        try {
            /* Get My Route Details */
            $routeDetails = $this->RouteRepositories->getMyRoutes($userId, $companyId);

            $routeResultDetails['route_details']         = $this->doFormattingMyRouteDetails($routeDetails);

            return $this->doSuccessFormatDetails($routeResultDetails);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e);
        }
    }

    /* Get AllRoutes */
    public function getAllRouteDetails($request, $userId, $companyId)
    {

        try {
            /* Get My Route Details */
            $routeDetails = $this->RouteRepositories->getAllRoutes($companyId);

            $routeResultDetails['route_details']         = $this->doFormattingMyRouteDetails($routeDetails);

            return $this->doSuccessFormatDetails($routeResultDetails);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e);
        }
    }

    /* Transformation Current Formatting */
    public function doFormattingRouteDetails($routeDetails)
    {
        $routeCollect     = collect($routeDetails)->first();
        $routeCollectDetails['Route_id'] = $routeCollect['id'];
        $routeCollectDetails['Route_name'] = $routeCollect['name'];
        return $routeCollectDetails;
    }

    /* Transformation My Routes */
    public function doFormattingMyRouteDetails($routeDetails)
    {
        $routeDetailsResult = [];
        foreach ($routeDetails as $routeDetailsKey => $routeDetailsValue) {

            /* Get Assigned by Details */
            if ($routeDetailsValue['route_contact']['assigned_by_id'] != '') {
                $assignedBy = $this->RouteRepositories->assignedByDetails($routeDetailsValue['route_contact']['assigned_by_id']);
                $assignedById = $routeDetailsValue['route_contact']['assigned_by_id'];
            } else {
                $assignedBy = '';
                $assignedById = '';
            }

            $routeDetailsResult[$routeDetailsKey]['route_id'] = $routeDetailsValue['id'];
            $routeDetailsResult[$routeDetailsKey]['route_name'] = $routeDetailsValue['name'];
            $routeDetailsResult[$routeDetailsKey]['route_assigned_by_id'] = $assignedById;
            $routeDetailsResult[$routeDetailsKey]['route_assigned_by_name'] = $assignedBy;
            $routeDetailsResult[$routeDetailsKey]['created'] = $routeDetailsValue['created'];
        }

        return $routeDetailsResult;
    }

    /* Get Currrent Route Details */
    public function getCurrentRouteList($routeDetails)
    {
        $routeCollect     = collect($routeDetails)->first();
        $routeContactList = collect(json_decode($routeCollect['route_contact']['contact_ids']));

        return $this->RouteRepositories->getRouteContactDetails($routeContactList);
    }

    public function doSuccessFormatDetails($routeDetails)
    {
        $result['status']                      = true;
        $result['message']                     = 'Success';
        $result['response']                    = $routeDetails;

        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails->getMessage();
        $result['response'] = '';

        return $result;
    }
}
