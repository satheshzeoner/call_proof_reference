<?php

namespace App\Services\Route;

use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

/* Models */
use App\Models\Route\Route;

/* Repositories */
use App\Repositories\Route\RouteRepositories;

class RouteAddServices
{
    public function __construct()
    {
        $this->RouteRepositories        = new RouteRepositories();

        /* Initialze For LogSytem */
        $this->companyId                     = '';
        $this->userId                        = '';
        $this->contactId                     = '';
    }

    /* Routes Get List*/
    public function storeCurrentRouteDetails($request, $userId, $companyId)
    {
        $this->companyId                     = $companyId;
        $this->userId                        = $userId;

        /* Receive All Request Param */
        $paramDetails = $request->all();

        /* Validations */
        $validationResult = $this->routeValidations($paramDetails, $userId);
        if ($validationResult['status'] == false) {
            return $this->doErrorFormatDetails($validationResult['message']);
        }

        DB::beginTransaction();
        try {

            /* Store Route */
            $routeDetails = $this->setFormatRouteDetails($paramDetails, $userId, $companyId);
            $routeId = $this->RouteRepositories->storeRouteDetails($routeDetails);
            $this->logEvents(config('constants.Success.CreateRoutes') . ' ' . $routeId);

            /* Store RouteContact */
            $routeContactDetails = $this->setFormatRouteContactDetails($paramDetails, $userId, $companyId, $routeId);
            $routeContactId = $this->RouteRepositories->storeRouteContactDetails($routeContactDetails);
            $this->logEvents(config('constants.Success.CreateRoutes') . ' ' . $routeContactId);

            DB::commit();
            return $this->doSuccessFormatDetails(config('constants.Success.AddRoutes'));
        } catch (Exception $e) {
            DB::rollback();
            $this->logEventsError($e->getMessage());
            return $this->doErrorFormatDetails(config('constants.Errors.AddRoute'));
        }
    }

    public function routeValidations($paramDetails, $userId)
    {
        $routeName = Route::whereName($paramDetails['route_name'])->whereUser_id($userId)->exists();
        if ($routeName == true) {
            $validationResult['status'] = false;
            $validationResult['message'] = config('constants.Errors.RouteName');
            return $validationResult;
        }

        if ($paramDetails['contact_ids'] == '') {
            $validationResult['status'] = false;
            $validationResult['message'] = config('constants.Errors.RouteContacts');
            return $validationResult;
        }

        $validationResult['status'] = true;
        $validationResult['message'] = '';

        return $validationResult;
    }

    public function setFormatRouteDetails($paramDetails, $userId, $companyId)
    {
        $routeDetails['company_id'] = $companyId;
        $routeDetails['name'] = $paramDetails['route_name'];
        $routeDetails['user_id'] = $userId;

        return $routeDetails;
    }

    public function setFormatRouteContactDetails($paramDetails, $userId, $companyId, $routeId)
    {
        $routeContactDetails['company_id']     = $companyId;
        $routeContactDetails['user_id']        = $userId;
        $routeContactDetails['route_id']       = $routeId;
        $routeContactDetails['contact_ids']    = $paramDetails['contact_ids'];
        $routeContactDetails['assigned_by_id'] = (isset($paramDetails['assigned_by'])) ? $paramDetails['assigned_by'] : '';

        return $routeContactDetails;
    }


    public function doSuccessFormatDetails($routeDetails)
    {
        $result['status']                      = true;
        $result['message']                     = 'Success';
        $result['response']                    = $routeDetails;

        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails;
        $result['response'] = '';

        return $result;
    }


    public function logEvents($messageDetails = null)
    {
        $routeLog['User_id']      = $this->userId;
        $routeLog['Company_id']   = $this->companyId;
        $routeLog['Message']      = $messageDetails;

        Log::notice($routeLog);

        return true;
    }

    public function logEventsError($messageDetails = null)
    {
        $routeLog['User_id']      = $this->userId;
        $routeLog['Company_id']   = $this->companyId;
        $routeLog['Message']      = $messageDetails;

        Log::error($routeLog);

        return true;
    }
}
