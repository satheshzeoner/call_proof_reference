<?php

namespace App\Services\Contact;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

/* Models */
use App\Models\Auth\AuthUser;
use App\Models\Contact\ContactImage;

/* Repositories */
use App\Repositories\Contact\ContactImageRepositories;

class ContactImageService
{
    public function __construct()
    {
        $this->ContactImageRepositories = new ContactImageRepositories();
    }

    /* Images List*/
    public function doGetImages($request, $userId, $contactId)
    {
        try {
            $userCompanyId   = AuthUser::find($userId)->userProfile->company_id;
            $userCompanyPeopletab = AuthUser::find($userId)->userProfile->userCompany->people_tab;
            $contact = $this->ContactImageRepositories->getContact(
                $request,
                $userId,
                $userCompanyId,
                $contactId
            );
            $contactCompany = isset($contact->contactCompany) ? $contact->contactCompany : '';
            $contactImage = $this->ContactImageRepositories->getContactImageDetail(
                $request,
                $userId,
                $userCompanyId,
                $contactId
            );
            $contactImageDetail['contact_images'] = $this->doContactImageFormat($contact, $contactImage, $userCompanyPeopletab, $contactCompany);
            return $this->doSuccessFormatDetails($contactImageDetail);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function doUploadImage($request, $userId, $contactId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $requestContactImage = $request->has('contact_image') ? $request->file('contact_image') : [];
            $fileFullName = $requestContactImage->getClientOriginalName();

            $filename  = pathinfo($fileFullName, PATHINFO_FILENAME);
            $extension = pathinfo($fileFullName, PATHINFO_EXTENSION);

            $directory = 'storage/' . config('constants.ContactImage.image_directory');
            $fileFormatName = $filename . "_" . now()->format('YmdHis') . "." . $extension;
            $filePath = $request->file('contact_image')->move(storage_path(config('constants.ContactImage.image_directory')), $fileFormatName);
            $contactImagePath = url($directory, $fileFormatName);
            $uploadImageDetail = $this->ContactImageRepositories->doUploadImageDetail(
                $request,
                $userId,
                $userCompanyId,
                $contactId,
                $contactImagePath
            );
            return $this->doUpdatedResultFormatDetails($uploadImageDetail['status'], $uploadImageDetail['message']);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    /* Show Image*/
    public function doShowImage($request, $userId, $contactId, $imageId)
    {
        try {
            $userCompanyId   = AuthUser::find($userId)->userProfile->company_id;
            $contactImageDetail['contact_image'] = $this->ContactImageRepositories->showImageDetail(
                $request,
                $userId,
                $userCompanyId,
                $contactId,
                $imageId
            );
            return $this->doSuccessFormatDetails($contactImageDetail);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    /* Delete Contact Image*/
    public function doRemoveContactImage($request, $userId, $userCompanyId, $contactId, $contactImageId)
    {
        $this->companyId                     = $userCompanyId;
        $this->userId                        = $userId;

        /* Validations */
        $validationResult = $this->contactImageIdVaidation($contactImageId);
        if ($validationResult['status'] == false) {
            $this->logContactImageError($validationResult['message']);
            return $this->doErrorFormatDetails($validationResult['message']);
        }

        DB::beginTransaction();
        try {

            /* Remove Contact Image */
            $this->ContactImageRepositories->removeContactImage($request, $contactImageId);
            $this->logContactImage(config('constants.Success.RemoveContactImage') . ' ' . $contactImageId);

            DB::commit();
            return $this->doSuccessFormatDetails(config('constants.Success.DeleteContactImage'));
        } catch (Exception $e) {
            DB::rollback();
            $this->logContactImageError($e->getMessage());
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    /* Validation for Id Has present or Not */
    public function contactImageIdVaidation($contactImageId)
    {
        $contactImageDetail['status'] = true;
        $contactImageDetail['message'] = '';
        $contactImage = ContactImage::whereId($contactImageId)->exists();
        if ($contactImage == false) {
            $this->logContactImageError(config('constants.Errors.ContactImageId'));
            $contactImageDetail['status'] = false;
            $contactImageDetail['message'] = config('constants.Errors.ContactImageId');
            return $contactImageDetail;
        }
        return $contactImageDetail;
    }

    public function doContactImageFormat($contact, $contactImageDetail, $userCompanyPeopletab, $contactCompany)
    {
        $contactImageData = [];
        foreach ($contactImageDetail as $key => $contactImage) {
            $contactImageData[$key]['image_id'] = $contactImage->id;
            if (!$userCompanyPeopletab && ($contact->first_name)) {
                $contactImageData[$key]['contact_name'] = $contact->first_name;
            } else {
                $contactImageData[$key]['contact_name'] = isset($contactCompany->name) ? ($contactCompany->name) : '';
            }
            $contactImageData[$key]['image_path'] = isset($contactImage->image) ? ($contactImage->image) : '';
            $contactImageData[$key]['image_height'] = config('constants.ContactImage.default_image_height');
            $contactImageData[$key]['image_width'] = config('constants.ContactImage.default_image_width');
            $contactImageData[$key]['event_form'] = isset($contactImage->contactEventForm->eventForm->name) ? $contactImage->contactEventForm->eventForm->name : '';
            $contactImageData[$key]['custom_field'] = isset($contactImage->customField->name) ? $contactImage->customField->name : '';
            $contactImageData[$key]['created'] = isset($contactImage->created) ? $contactImage->created : '';
        }
        return $contactImageData;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails;
        $result['response'] = '';

        return $result;
    }
    public function doSuccessFormatDetails($data)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response'] = $data;

        return $result;
    }
    public function doUpdatedResultFormatDetails($status, $message)
    {
        $result['status']  = $status;
        $result['message'] = $message;
        $result['response'] = '';
        return $result;
    }

    public function logContactImage($messageDetails = null)
    {
        $contactImageLog['User_id']      = $this->userId;
        $contactImageLog['Company_id']   = $this->companyId;
        $contactImageLog['Message']      = $messageDetails;

        Log::notice($contactImageLog);

        return true;
    }

    public function logContactImageError($messageDetails = null)
    {
        $contactImageLog['User_id']      = $this->userId;
        $contactImageLog['Company_id']   = $this->companyId;
        $contactImageLog['Message']      = $messageDetails;

        Log::error($contactImageLog);

        return true;
    }
}
