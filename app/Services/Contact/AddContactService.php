<?php

namespace App\Services\Contact;

use App\Helpers\Helper;
use Exception;
use Illuminate\Support\Facades\Auth;

/* Models */
use App\Models\Auth\AuthUser;

/* Repository */
use App\Repositories\Contact\AddContactRepositories;

/* EventService */
use App\Services\Event\EventAddService;

class AddContactService
{
    public function __construct()
    {
        $this->AddContactRepositories  = new AddContactRepositories();
    }

    public function addContactDetails($request, $userId)
    {
        $companyId = AuthUser::find($userId)->userProfile->company_id;
        try {
            $contactData = $this->AddContactRepositories->doAddContact(
                $request,
                $userId
            );
            $status = true;
            if ($contactData) {
                $contactPhoneResult = $this->AddContactRepositories->doContactPhoneStore(
                    $request,
                    $contactData
                );
                $contactNoteResult = $this->AddContactRepositories->doContactNoteStore(
                    $request,
                    $contactData
                );
                if (!$contactPhoneResult  | !$contactNoteResult) {
                    $status = false;
                    $message = config('constants.Errors.AddContact');
                }
                /* Event Store Process */
                $contactId = isset($contactData['contact']->id) ? $contactData['contact']->id : null;
                $this->doEventAdd($userId, $companyId, $contactId, config('constants.Event_name.mobile_add_contact'));
                $message = str_replace(config('constants.DynamicSuccessKey.AddContact'), $contactData['requestData']['company_name'], config('constants.Success.AddContact'));
            } else {
                $status = false;
                $message = config('constants.Errors.AddContact');
            }

            return $this->doUpdatedResultFormatDetails($status, $message);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e);
        }
    }


    public function addContactNote($request, $userId, $companyId, $contactId)
    {
        /* Get All Param Details */
        $paramDetails = $request->all();

        /* Source Id Validation */
        $sourceTypeId = (isset($paramDetails['source_type'])) ? $paramDetails['source_type'] : '';
        $sourceId     = (isset($paramDetails['source_id'])) ? $paramDetails['source_id'] : '';

        try {

            $contactNote['contact_id']    = $contactId;
            $contactNote['user_id']       = $userId;
            $contactNote['note']          = $paramDetails['note'];
            $contactNote['created']       = now();
            $contactNote['company_id']    = $companyId;
            if (isset($contactNote['sourcetype_id'])) {
                $contactNote['sourcetype_id'] = $sourceTypeId;
            }
            if (isset($contactNote['source_id'])) {
                $contactNote['source_id'] = $sourceTypeId;
            }

            /* Store Contact Note */
            $contactNoteId = $this->AddContactRepositories->doStoreContactNote($request, $contactNote);

            /* Event Store Process */
            $this->doEventAdd($userId, $companyId, $contactId, config('constants.Event_name.mobile_add_contact_note'));

            return $this->doSuccessFormatDetails(config('constants.Success.ContactNote'));
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e);
        }
    }

    public function doSuccessFormatDetails($contactDetails)
    {
        $result['status']  = true;
        $result['message'] = 'Success';
        $result['response']['contact'] = $contactDetails;

        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails->getMessage();
        $result['response'] = '';

        return $result;
    }
    public function doUpdatedResultFormatDetails($status, $message)
    {
        $result['status']   = $status;
        $result['message']  = $message;
        $result['response'] = '';
        return $result;
    }

    /* ===============================EVENT========================== */
    public function doEventAdd($userId, $userCompanyId, $contactId, $eventName)
    {
        /* Event Creation */
        $eventAddService        = new EventAddService();
        $eventAddService->addEvent(
            $eventName,
            $eventCompanyId          = $userCompanyId,
            $eventUserId             = $userId,
            $eventContactId          = $contactId,
            $eventOtherUserId        = null,
            $eventCallId             = null,
            $eventStartTime          = null,
            $eventDuration           = null,
            $eventBadgeId            = null,
            $eventGooglePlace        = null,
            $eventTwilioCallId       = null,
            $eventContactEventFormId = null,
            $eventAppointmentId      = null,
            $eventFollowupId         = null,
            $eventMessage            = null
        );

        return true;
    }
    /* ===============================EVENT-END====================== */
}
