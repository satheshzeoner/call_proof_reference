<?php

namespace App\Services\Contact;

use App\Helpers\Helper;
use Exception;
use Illuminate\Support\Facades\Auth;

/* Models */
use App\Models\Auth\AuthUser;

/* Repository */
use App\Repositories\Contact\EditContactRepositories;

/* EventService */
use App\Services\Event\EventAddService;

class EditContactService
{
    public function __construct()
    {
        $this->EditContactRepositories  = new EditContactRepositories();
    }

    public function editContactDetails($request, $userId, $contactId)
    {
        try {
            $companyId = AuthUser::find($userId)->userProfile->company_id;
            $contactData = $this->EditContactRepositories->doEditContact(
                $request,
                $userId,
                $contactId
            );
            $status = true;
            $message = config('constants.Success.EditContact');
            if ($contactData) {
                /* Event Store Process */
                $contactId = isset($contactData['contact']->id) ? $contactData['contact']->id : null;
                $this->doEventAdd($userId, $companyId, $contactId, config('constants.Event_name.mobile_update_contact'));
                $contactPhoneResult = $this->EditContactRepositories->doContactPhoneStore(
                    $request,
                    $contactData
                );
                $contactNoteResult = $this->EditContactRepositories->doContactNoteStore(
                    $request,
                    $contactData
                );
                if (!$contactPhoneResult  | !$contactNoteResult) {
                    $status = false;
                    $message = config('constants.Errors.EditContact');
                }
            } else {
                $status = false;
                $message = config('constants.Errors.EditContact');
            }
            return $this->doUpdatedResultFormatDetails($status, $message);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e);
        }
    }

    public function doUpdateContactParentCompany($request, $userId, $contactId, $companyId)
    {
        try {
            $parentCompany = $request->parent_company_name;
            $contactParentCompanyResponse = $this->EditContactRepositories->doUpdateContactParentCompany(
                $request,
                $userId,
                $companyId,
                $contactId,
                $parentCompany
            );
            return $contactParentCompanyResponse;
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e);
        }
    }


    public function doSuccessFormatDetails($ContactDetails)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response']['contact'] = $ContactDetails;
        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails->getMessage();
        $result['response'] = '';
        return $result;
    }
    public function doUpdatedResultFormatDetails($status, $message)
    {
        $result['status']  = $status;
        $result['message'] = $message;
        $result['response'] = '';
        return $result;
    }

    /* ===============================EVENT========================== */
    public function doEventAdd($userId, $userCompanyId, $contactId, $eventName)
    {
        /* Event Creation */
        $eventAddService        = new EventAddService();
        $eventAddService->addEvent(
            $eventName,
            $eventCompanyId          = $userCompanyId,
            $eventUserId             = $userId,
            $eventContactId          = $contactId,
            $eventOtherUserId        = null,
            $eventCallId             = null,
            $eventStartTime          = null,
            $eventDuration           = null,
            $eventBadgeId            = null,
            $eventGooglePlace        = null,
            $eventTwilioCallId       = null,
            $eventContactEventFormId = null,
            $eventAppointmentId      = null,
            $eventFollowupId         = null,
            $eventMessage            = null
        );

        return true;
    }
}
