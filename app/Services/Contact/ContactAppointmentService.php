<?php

namespace App\Services\Contact;

use App\Helpers\Helper;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

/* Models */
use App\Models\Auth\AuthUser;
use App\Models\Contact\Contact;
use App\Models\Cfield\Cfield;
use App\Models\Eventform\Eventform;

/* Service */
use App\Services\Contact\ContactViewServices;
use App\Services\EventForm\EventFormListService;
use App\Services\EventForm\EventFormScheduleTaskService;
use App\Services\Opportunity\OpportunityService;
use App\Services\CustomField\CustomFieldService;
use App\Services\Event\EventAddService;

/* Repository */
use App\Repositories\Contact\ContactAppointmentRepositories;
use App\Repositories\Contact\ContactViewRepositories;
use App\Repositories\EventForm\EventFormAddRepositories;
use App\Repositories\Task\TaskRepositories;

/* FieldType Details */
use App\Services\EventForm\EventFormFieldTypeService;

class ContactAppointmentService
{
    public function __construct()
    {
        $this->ContactAppointmentRepositories   = new ContactAppointmentRepositories();
        $this->ContactViewRepositories          = new ContactViewRepositories();
        $this->ContactViewServices              = new ContactViewServices();
        $this->EventListService                 = new EventFormListService();
        $this->EventScheduleTaskService         = new EventFormScheduleTaskService();
        $this->OpportunityService               = new OpportunityService();
        $this->TaskRepositories                 = new TaskRepositories();
        $this->CustomFieldService               = new CustomFieldService();
        $this->EventFormAddRepositories         = new EventFormAddRepositories();
        $this->EventFieldTypeService            = new EventFormFieldTypeService();
    }

    public function doContactStartAppointmentStore($request, $userId)
    {
        $contactId = $request->has('contact_id') ? $request->get('contact_id') : null;
        $eventFormId = $request->has('event_form_id') ? $request->get('event_form_id') : null;
        //TODO temporarily commented , need to be removed after finalizing
        //$personnelIdList = $request->has('personnel_id') ? collect(json_decode($request->get('personnel_id'))) : [];
        $latitude = $request->has('latitude') ? $request->get('latitude') : null;
        $longitude = $request->has('longitude') ? $request->get('longitude') : null;
        $start = $request->has('start') ? Carbon::parse($request->get('start')) : now();
        $salesUserSingleMode = AuthUser::find($userId)->userProfile->userCompany->sales_rep_single_mode;

        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $previousAppointments = $this->ContactAppointmentRepositories->getPreviousAppointment($request, $userId);
            $autoEndPreviousAppointment = $this->ContactAppointmentRepositories->doAutoEndPreviousAppointment($request, $userId, $previousAppointments);

            $contact = $this->ContactAppointmentRepositories->getContact(
                $request,
                $contactId,
                $userCompanyId
            );
            if ($contact) {
                $appointment = $this->ContactAppointmentRepositories->getAppointment(
                    $request,
                    $userId
                );
                if (!$appointment) {
                    $appointmentResponse = $this->ContactAppointmentRepositories->doAppointmentStore(
                        $request,
                        $userCompanyId,
                        $userId,
                        $contact,
                        $start,
                        $latitude,
                        $longitude,
                        $eventFormId
                    );
                    if ($appointmentResponse) {
                        $startAppointment = config('constants.ContactAppointment.startAppointment');
                        $locationLog = $this->ContactAppointmentRepositories->doLocationLogStore(
                            $request,
                            $userId,
                            $latitude,
                            $longitude,
                            $startAppointment,
                            $appointmentResponse
                        );
                        $this->ContactAppointmentRepositories->doReverseGeocode(
                            $request,
                            $contact,
                            $start,
                            $locationLog,
                            $latitude,
                            $longitude
                        );
                        //TODO temporarily commented , need to be removed after finalizing
                        //$this->ContactAppointmentRepositories->doContactPersonnelStore(
                        //$request,
                        //$appointmentResponse,
                        //$personnelIdList
                        //);
                        $this->ContactAppointmentRepositories->isSalesUserSingleModeContactUserStore(
                            $request,
                            $contact,
                            $userId,
                            $salesUserSingleMode
                        );
                    }
                } else {
                    return $this->doErrorFormatDetails(config('constants.Errors.AppointmentAlreadyExist'));
                }
            } else {
                return $this->doErrorFormatDetails(config('constants.Errors.ContactNotFound'));
            }
            /* Event Store Process */
            $eventName = config('constants.Event_name.mobile_appointment_checkin');
            $appointment = isset($appointmentResponse) ? $appointmentResponse : null;
            $this->doEventAdd($request, $userId, $userCompanyId, $contactId, $appointment, $eventName);
            return $this->doUpdatedResultFormatDetails(true, config('constants.Success.StartAppointment'));
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function getCurrentAppointment($request, $userId)
    {
        $appointment = $this->ContactAppointmentRepositories->getCurrentAppointmentDetails($request, $userId);
        if ($appointment) {
            $appointmentData = $this->doStartAppointmentFormat($appointment);
            return $this->doSuccessFormatDetails($appointmentData);
        } else {
            $appointmentStatus['is_appointment_active'] = config('constants.ContactAppointment.current_appointment_default_status');
            return $this->doSuccessFormatDetails($appointmentStatus);
        }
    }
    public function doGetContactEndAppointment($request, $userId)
    {
        try {
            //TODO temporarily commented , need to be removed after finalizing
            //$customFieldTable = config('constants.ContactAppointment.custom_field_table');
            //$showHidden = config('constants.ContactAppointment.cfield_show_hidden');
            //$showImgFields = config('constants.ContactAppointment.cfield_show_img_fields');
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;

            $appointment = $this->ContactAppointmentRepositories->getAppointment(
                $request,
                $userId
            );
            if ($appointment) {
                $contact = isset($appointment->contact) ? $appointment->contact : null;
                //TODO temporarily commented , need to be removed after finalizing
                //$appointmentId = $appointment->id;
                $appointmentNote = $appointment->notes;

                if ($contact) {
                    $contactId =  $contact->id;
                    //TODO temporarily commented , need to be removed after finalizing
                    //$contactTypeId =   $contact->contact_type_id;

                    $contactDetail = $this->ContactAppointmentRepositories->getContactDetail(
                        $request,
                        $userId,
                        $userCompanyId,
                        $contactId
                    );

                    $contactDataDetail = $this->doContactFormat($contactDetail, $userId);
                    //TODO temporarily commented , need to be removed after finalizing
                    //$customFields = $this->ContactAppointmentRepositories->getCustomField(
                    //$request,
                    //$userId,
                    //$userCompanyId,
                    //$customFieldTable,
                    //$showHidden,
                    //$showImgFields,
                    //$contactId
                    //);
                    //TODO temporarily commented , need to be removed after finalizing
                    //return $this->doEndAppointmentSuccessFormatDetails($contactId, $appointment, $contactTypeId, $appointmentNote, $customFields, $contactDataDetail);
                    return $this->doEndAppointmentSuccessFormatDetails($contactId, $appointment, $appointmentNote, $contactDataDetail);
                } else {
                    $result['status'] = false;
                    $result['response'] = [];
                    Helper::customError(400, config('constants.Errors.ContactNotFound'), $request->all(), config('constants.ServiceName.contactAppointment'));
                    return $result;
                }
            } else {
                $result['status'] = true;
                $result['response'] = [];
                Helper::customError(400, config('constants.Errors.AppointmentNotStarted'), $request->all(), config('constants.ServiceName.contactAppointment'));
                return $result;
            }
        } catch (Exception $e) {
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contactAppointment'));
            return false;
        }
    }
    public function doContactEndAppointmentStore($request, $userId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $appointmentId = $request->appointment_id;
            $contactId = $request->contact_id;

            //TODO temporarily commented , need to be removed after finalizing
            // Requested POST as Converted to Array //
            // $selectedContacts = $request->has('selected_contacts') ? collect(json_decode($request->get('selected_contacts'))) : [];
            //$requestUpdateAccountInfo = $request->has('update_account_info') ? collect(json_decode($request->get('update_account_info'))) : [];;
            //$accountInfoDetail = isset($requestUpdateAccountInfo['account_info']) ? $requestUpdateAccountInfo['account_info'] : [];
            //$requestCustomField = isset($requestUpdateAccountInfo['customfields']) ? $requestUpdateAccountInfo['customfields'] : [];
            //$selectedOpportunities = $request->has('opportunities') ? collect(json_decode($request->get('opportunities'))) : [];

            $selectedPeople = $request->has('selected_people') ? collect(json_decode($request->get('selected_people'))) : [];
            $requestTaskOrFollowUp = $request->has('add_task_or_follow_up') ? collect(json_decode($request->add_task_or_follow_up))->first() : [];
            $requestTreatEventForm = $request->has('treat_event_form_as_appointment') ? collect(json_decode($request->get('treat_event_form_as_appointment'))) : [];
            $eventFormId =  $request->event_form_id;

            // Nested Object to Array Convertion //
            //TODO temporarily commented , need to be removed after finalizing
            //$customFieldDetail      = json_decode(json_encode($requestCustomField), true);
            //$userCompanyPeopletab = AuthUser::find($userId)->userProfile->userCompany->people_tab;

            $notes = $request->notes;
            $appointment = $this->ContactAppointmentRepositories->doContactEndAppointmentStoreDetails($request, $userId, $userCompanyId, $appointmentId, $contactId, $notes, $selectedPeople);

            //TODO temporarily commented , need to be removed after finalizing
            //$appointment = $this->ContactAppointmentRepositories->doContactEndAppointmentStoreDetails($request, $userId, $userCompanyId, $appointmentId, $contactId, $notes, $selectedContacts);
            //if ($accountInfoDetail) {
            //$updateContactResponse = $this->ContactAppointmentRepositories->doUpdateAccountInfoDetail($request, $userId, $userCompanyId, $contactId, $accountInfoDetail, $userCompanyPeopletab);
            //}
            //if ($customFieldDetail) {
            //$customFieldsResponse = $this->CustomFieldService->doUpdateCustomFieldsProcessing($request, $userId, $userCompanyId, $contactId, $customFieldDetail);
            //}
            //if ($selectedOpportunities) {
            //$selectedOpportunitiesResponse = $this->ContactAppointmentRepositories->doContactSelectedOpportunitiesStoreDetails($request, $userId, $userCompanyId, $appointmentId, $contactId, $selectedOpportunities);
            //}

            if (!empty($requestTaskOrFollowUp)) {
                $taskOrFollowUpResponse = $this->addTaskDetails($requestTaskOrFollowUp, $userId, $userCompanyId, $contactId, $appointmentId);
                if ($taskOrFollowUpResponse['status'] == false) {
                    return $this->doUpdatedResultFormatDetails(false, $taskOrFollowUpResponse['message']);
                }
            }
            if (!empty($requestTreatEventForm) && $eventFormId) {
                $treatAsEventFormResponse = $this->eventFormAddProcess($request, $userId, $userCompanyId, $contactId, $eventFormId, $appointmentId);
                if ($treatAsEventFormResponse['status'] == false) {
                    return $this->doUpdatedResultFormatDetails(false, $treatAsEventFormResponse['message']);
                }
            }
            if ($appointment) {
                /* Event Store Process */
                $eventName = config('constants.Event_name.web_appointment_checkout');
                $this->doEventAdd($request, $userId, $userCompanyId, $contactId, $appointment, $eventName);
                return $this->doUpdatedResultFormatDetails(true, config('constants.Success.EndAppointment'));
            }
            return $this->doUpdatedResultFormatDetails(false, config('constants.Errors.EndAppointment'));
        } catch (Exception $e) {
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contactAppointment'));
            return $this->doUpdatedResultFormatDetails(false, config('constants.Errors.EndAppointment'));
        }
    }


    /* Add Task List*/
    public function addTaskDetails($requestTaskOrFollowUp, $userId, $companyId, $contactId, $appointmentId)
    {

        $this->companyId                     = $companyId;
        $this->userId                        = $userId;
        $this->contactId                     = $contactId;

        $scheduleDetails  = json_decode(json_encode($requestTaskOrFollowUp), true);

        /* Initial Response Set */
        $scheduleResult['status']  = true;
        $scheduleResult['message'] = '';

        /* check the Schedule Task request Details */
        if ($scheduleDetails == '') {
            return $scheduleResult;
        }

        /* DateTime Formatting */
        $dateTimeDetails = $scheduleDetails['task_date'] . ' ' . $scheduleDetails['task_hour'] . ':' . $scheduleDetails['task_min'] . ':00 ' . $scheduleDetails['task_meridiem'];

        /* Strcturing DateTime Format */
        $dateTimeDetails = Carbon::parse($dateTimeDetails)->format('Y-m-d H:i:s');

        /* TaskAssignment as User Assigned for FollowUp */
        if ($scheduleDetails['task_assignment'] == '') {
            $scheduleResult['status']  = false;
            $scheduleResult['message'] = config('constants.Errors.TaskAssignmentId');
        }

        $taskAssignmentUserId   = explode(',', $scheduleDetails['task_assignment']);

        /* People List Check */
        if ($scheduleDetails['task_people'] == '') {
            $taskAssignmentPeopleId = '';
        } else {
            $taskAssignmentPeopleId = explode(',', $scheduleDetails['task_people']);
        }

        foreach ($taskAssignmentUserId as $taskAssignmentkey => $taskAssignmentvalue) {

            $followupDetails['user_id']         = $taskAssignmentvalue;
            $followupDetails['contact_id']      = $contactId;
            $followupDetails['start']           = $dateTimeDetails;
            $followupDetails['duration']        = $scheduleDetails['duration'];
            $followupDetails['send_google_cal'] = $scheduleDetails['send_google_cal'];
            $followupDetails['created']         = now();
            $followupDetails['appointmentId']   = $appointmentId;

            try {
                $followupId = $this->EventFormAddRepositories->storeFollowupValue($followupDetails);
                $this->logTaskEvents(config('constants.Success.Followup') . ' Created Id :' . $followupId);
            } catch (Exception $e) {
                $this->logTaskEventsError($e->getMessage());
                $scheduleResult['status']  = false;
                $scheduleResult['message'] = config('constants.Errors.AddEventForm');
                return $scheduleResult;
            }

            if ($taskAssignmentPeopleId != '') {
                foreach ($taskAssignmentPeopleId as $key => $taskAssignmentPeoplevalue) {
                    $followupPersonalDetails['followup_id']  = $followupId;
                    $followupPersonalDetails['personnel_id'] = $taskAssignmentPeoplevalue;

                    try {
                        $followupPersonalId = $this->EventFormAddRepositories->storeFollowupPersonalValue($followupPersonalDetails);
                        $this->logTaskEvents(config('constants.Success.FollowupPersonnel') . ' Created Id :' . $followupPersonalId);
                    } catch (Exception $e) {
                        $this->logTaskEventsError($e->getMessage());
                        $scheduleResult['status']  = false;
                        $scheduleResult['message'] = config('constants.Errors.AddEventForm');
                        return $scheduleResult;
                    }
                }
            }
        }
        return $scheduleResult;
    }



    /* Event ADD Form Service */
    public function eventFormAddProcess($request, $userId, $companyId, $contactId, $eventFormId, $appointmentId)
    {
        $requestDetails = $request->all();
        /* Requested POST as Converted to Array */
        $formDetails = $request->has('treat_event_form_as_appointment') ? collect(json_decode($request->get('treat_event_form_as_appointment'))) : [];
        //TODO temporarily commented , need to be removed after finalizing
        //$peopleDetails = $request->has('selected_contacts') ? collect(json_decode($request->get('selected_contacts'))) : [];
        $peopleDetails = $request->has('selected_people') ? collect(json_decode($request->get('selected_people'))) : [];
        if (isset($requestDetails['add_task_or_follow_up'])) {
            $scheduleDetails = collect(json_decode($requestDetails['add_task_or_follow_up']))->first();
        } else {
            $scheduleDetails = '';
        }
        /* Nested Object to Array Convertion */
        $formDetails      = json_decode(json_encode($formDetails), true);
        $peopleDetails    = json_decode(json_encode($peopleDetails), true);
        $scheduleDetails  = json_decode(json_encode($scheduleDetails), true);

        $this->companyId   = $companyId;
        $this->userId      = $userId;
        $this->contactId   = $contactId;
        $this->EventFormId = $eventFormId;

        try {
            /* Validations */
            $validationResult = $this->doEventFormAddValidations($userId, $companyId, $contactId, $eventFormId, $formDetails);

            if ($validationResult['status'] == false) {
                return $this->doErrorFormat($validationResult['message']);
            }
            /* doStore EventForm Insertion */

            DB::beginTransaction();

            $storeResult = $this->doStoreEventFormProcessing($request, $userId, $companyId, $contactId, $eventFormId, $formDetails, $peopleDetails, $scheduleDetails, $appointmentId);

            DB::commit();

            if ($storeResult['status'] == false) {

                DB::rollback();
                return $this->doErrorFormat($storeResult['message']);
            }

            return $this->doSuccessFormat(config('constants.Success.EventForm'));

            /* doStore EventForm Insertion END */
        } catch (Exception $e) {
            return $this->doErrorFormat($e->getMessage());
        }
    }

    public function doStoreEventFormProcessing($request, $userId, $companyId, $contactId, $eventFormId, $formDetails, $peopleDetails, $scheduleDetails, $appointmentId)
    {
        /* initial Status  */
        $eventFormResult['status'] = true;
        $eventFormResult['message'] = '';

        /* Contact EventForm Store Process */
        $contactEventFormId = $this->doStoreContactEventform($userId, $companyId, $contactId, $eventFormId, $appointmentId);

        /* MultiSelect People contacteventformpersonnel Store  */
        $contactEventFormPersonnelId = $this->doStorePeople($contactEventFormId, $peopleDetails);

        /* customField Values Store */
        $customFieldResult = $this->storeCustomFieldValues($userId, $contactId, $companyId, $contactEventFormId, $formDetails, $request);
        if ($customFieldResult['status'] == false) {
            return $customFieldResult;
        }

        /* ByCheked { Schedule Task Now } Store process {followup} */
        if (!empty($scheduleDetails)) {
            $followUpResult = $this->storeSchedultTask($userId, $contactId, $scheduleDetails, $request);
            if ($followUpResult['status'] == false) {
                return $followUpResult;
            }
        }
        return $eventFormResult;
    }

    public function doStoreContactEventform($userId, $companyId, $contactId, $eventFormId, $appointmentId)
    {
        /* Get Cordinnation */
        $contactDetails = $this->EventFormAddRepositories->getContactsCoordination($contactId);

        $contactEventFormDetails['company_id']     = $companyId;
        $contactEventFormDetails['user_id']        = $userId;
        $contactEventFormDetails['contact_id']     = $contactId;
        $contactEventFormDetails['event_form_id']  = $eventFormId;
        $contactEventFormDetails['latitude']       = ($contactDetails->latitude) ? $contactDetails->latitude  : null;
        $contactEventFormDetails['longitude']      = ($contactDetails->longitude) ? $contactDetails->longitude : null;
        $contactEventFormDetails['points']         = 0;
        $contactEventFormDetails['created']        = now();
        $contactEventFormDetails['appointment_id'] = $appointmentId;

        $contactEventFormId = $this->EventFormAddRepositories->storeContactEventForm($contactEventFormDetails);

        /* Log For ContactEventCreation */
        $this->logEvents('ContactEventForm Created id =' . $contactEventFormId);
        return $contactEventFormId;
    }

    /* MultiSelect People Store in ContactEvent Personal Table */
    public function doStorePeople($contactEventFormId, $peopleDetails)
    {
        if ($peopleDetails == null) {
            return true;
        }

        $contactPersonalDetails = [];
        foreach ($peopleDetails as $Personalkey => $peopleDetailsValue) {
            $contactPersonalDetails['contacteventform_id'] = $contactEventFormId;
            $contactPersonalDetails['personnel_id']        = $peopleDetailsValue;
            $contactPersonalId = $this->EventFormAddRepositories->storeContactEventFormPersonnel($contactPersonalDetails);
            $this->logEvents(config('constants.Success.ContactEventFormPersonal') . ' = ' . $contactPersonalId);
        }
        return true;
    }

    /* Store Dynamic CustomField with Value */
    public function storeCustomFieldValues($userId, $contactId, $companyId, $contactEventFormId, $formDetails, $request)
    {
        foreach ($formDetails as $formDetailsValue) {

            /* TextField */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.text')) {
                $fieldResult = $this->EventFieldTypeService->textField($formDetailsValue);
            }
            /* SelectField */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.select')) {
                $fieldResult = $this->EventFieldTypeService->selectField($formDetailsValue);
            }
            /* RadioField */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.radio')) {
                $fieldResult = $this->EventFieldTypeService->RadioField($formDetailsValue);
            }
            /* checkbox */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.checkbox')) {
                $fieldResult = $this->EventFieldTypeService->checkboxField($formDetailsValue);
            }
            /* textarea */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.textarea')) {
                $fieldResult = $this->EventFieldTypeService->textareaField($formDetailsValue);
            }
            /* date */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.date')) {
                $fieldResult = $this->EventFieldTypeService->dateField($formDetailsValue);
            }
            /* time */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.time')) {
                $fieldResult = $this->EventFieldTypeService->timeField($formDetailsValue);
            }
            /* datetime */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.datetime')) {
                $fieldResult = $this->EventFieldTypeService->datetimeField($formDetailsValue);
            }
            /* integer */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.integer')) {
                $fieldResult = $this->EventFieldTypeService->integerField($formDetailsValue);
            }
            /* decimal */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.decimal')) {
                $fieldResult = $this->EventFieldTypeService->decimalField($formDetailsValue);
            }
            /* auto_integer */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.autoInteger')) {
                $fieldResult = $this->EventFieldTypeService->autoIntegerField($formDetailsValue);
            }
            /* image */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.image')) {
                $fieldResult = $this->EventFieldTypeService->imageField($formDetailsValue, $request);

                /* File Path Store in ContactImage */
                $contactImageDetails['user_id']               = $userId;
                $contactImageDetails['company_id']            = $companyId;
                $contactImageDetails['contact_id']            = $contactId;
                $contactImageDetails['cfield_id']             = $formDetailsValue['custom_field_id'];
                $contactImageDetails['contact_event_form_id'] = $contactEventFormId;
                $contactImageDetails['image']                 = $fieldResult['custom_field_value'];
                $contactImageDetails['created']               = now();

                $contactImage = $this->EventFormAddRepositories->storeCustomFieldFilePath($contactImageDetails);
            }
            /* multiple_select */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.multipleSelect')) {
                $fieldResult = $this->EventFieldTypeService->multipleSelectField($formDetailsValue);
            }

            /* Store in customField Details */
            if ($fieldResult['status'] == true) {

                $customField['cfield_id'] = $formDetailsValue['custom_field_id'];
                $customField['entity_id'] = $contactEventFormId;
                $customField['cf_value']  = $fieldResult['custom_field_value'];
                $customField['updated']   = now();

                /* store CustomFieldValues */
                $contactPersonalId = $this->EventFormAddRepositories->storeCustomFieldValue($customField);
            }

            /* End of Section */
            if ($fieldResult['status'] == false) {
                return $fieldResult;
                break;
            }
        }

        $finalCustomFieldResult['status'] = true;
        $finalCustomFieldResult['message'] = '';
        return $finalCustomFieldResult;
    }

    /* ScheduleTask Store Process */
    public function storeSchedultTask($userId, $contactId, $scheduleDetails)
    {
        /* Initial Response Set */
        $scheduleResult['status']  = true;
        $scheduleResult['message'] = '';

        /* check the Schedule Task request Details */
        if ($scheduleDetails == '') {
            return $scheduleResult;
        }

        /* DateTime Formatting */
        $dateTimeDetails = $scheduleDetails['task_date'] . ' ' . $scheduleDetails['task_hour'] . ':' . $scheduleDetails['task_min'] . ':00 ' . $scheduleDetails['task_meridiem'];

        /* Strcturing DateTime Format */
        $dateTimeDetails = Carbon::parse($dateTimeDetails)->format('Y-m-d H:i:s');

        /* TaskAssignment as User Assigned for FollowUp */
        if ($scheduleDetails['task_assignment'] == '') {
            $scheduleResult['status']  = false;
            $scheduleResult['message'] = config('constants.Errors.TaskAssignmentId');
        }

        $taskAssignmentUserId   = explode(',', $scheduleDetails['task_assignment']);

        /* People List Check */
        if ($scheduleDetails['task_people'] == '') {
            $taskAssignmentPeopleId = '';
        } else {
            $taskAssignmentPeopleId = explode(',', $scheduleDetails['task_people']);
        }

        foreach ($taskAssignmentUserId as $taskAssignmentkey => $taskAssignmentvalue) {

            $followupDetails['user_id']         = $taskAssignmentvalue;
            $followupDetails['contact_id']      = $contactId;
            $followupDetails['start']           = $dateTimeDetails;
            $followupDetails['duration']        = $scheduleDetails['duration'];
            $followupDetails['send_google_cal'] =  $scheduleDetails['send_google_cal'];
            $followupDetails['created']         = now();

            try {
                $followupId = $this->EventFormAddRepositories->storeFollowupValue($followupDetails);
                $this->logEvents(config('constants.Success.Followup') . ' Created Id :' . $followupId);
            } catch (Exception $e) {
                $this->logEventsError($e->getMessage());
                $scheduleResult['status']  = false;
                $scheduleResult['message'] = config('constants.Errors.AddEventForm');
                return $scheduleResult;
            }

            if ($taskAssignmentPeopleId != '') {
                foreach ($taskAssignmentPeopleId as $key => $taskAssignmentPeoplevalue) {
                    $followupPersonalDetails['followup_id']  = $followupId;
                    $followupPersonalDetails['personnel_id'] = $taskAssignmentPeoplevalue;

                    try {
                        $followupPersonalId = $this->EventFormAddRepositories->storeFollowupPersonalValue($followupPersonalDetails);
                        $this->logEvents(config('constants.Success.FollowupPersonnel') . ' Created Id :' . $followupPersonalId);
                    } catch (Exception $e) {
                        $this->logEventsError($e->getMessage());
                        $scheduleResult['status']  = false;
                        $scheduleResult['message'] = config('constants.Errors.AddEventForm');
                        return $scheduleResult;
                    }
                }
            }
        }
        return $scheduleResult;
    }


    /* Each Field Validation Process */
    public function doEventFormAddValidations($userId, $companyId, $contactId, $eventFormId, $formDetails)
    {
        $validationResult['status'] = true;
        $validationResult['message'] = '';
        /* ContactId Validation */
        if (Contact::whereCompany_id($companyId)->whereId($contactId)->exists() == false) {
            $validationResult['status']  = false;
            $validationResult['message'] = config('constants.Errors.ContactId');
            return $validationResult;
        }
        /* EventId Validation */
        if (Eventform::whereCompany_id($companyId)->whereId($eventFormId)->exists() == false) {
            $validationResult['status']  = false;
            $validationResult['message'] = config('constants.Errors.EventFormId');
            return $validationResult;
        }


        foreach ($formDetails as $formDetailskey => $formDetailsvalue) {

            /* Required Validations */
            if ($formDetailsvalue['required'] == true) {
                if ($formDetailsvalue['value'] == '' && $formDetailsvalue['custom_field_option_default_id'] == '') {
                    $validationResult['status']  = false;
                    $validationResult['message'] = $formDetailsvalue['label'] . ' Required';
                    return $validationResult;
                }
            }

            /* Custom Field Type Validation */
            if ($formDetailsvalue['custom_field_type_id'] != null && $formDetailsvalue['custom_field_type_id'] != 1) {
                if (Cfield::whereId($formDetailsvalue['custom_field_id'])->whereCfield_type_id($formDetailsvalue['custom_field_type_id'])->exists() == false) {
                    $validationResult['status']  = false;
                    $validationResult['message'] = $formDetailsvalue['label'] . ' custom Field id/Field Type Id Invalid';
                    return $validationResult;
                }
            }
        }
        return $validationResult;
    }

    public function doSuccessFormat($responseResult)
    {
        $result['status']   = true;
        $result['message']  = 'Success';
        $result['response'] = $responseResult;
        return $result;
    }

    public function doErrorFormat($responseResult)
    {
        Helper::logError($responseResult, 401, $requestdetails = null, config('constants.ServiceName.Event'));
        $result['status']   = false;
        $result['message']  = $responseResult;
        $result['response'] = '';
        return $result;
    }

    public function logEvents($messageDetails = null)
    {
        $contactEventLog['User_id']      = $this->userId;
        $contactEventLog['Company_id']   = $this->companyId;
        $contactEventLog['Contact_id']   = $this->contactId;
        $contactEventLog['EventForm_id'] = $this->EventFormId;
        $contactEventLog['Message']      = $messageDetails;

        Log::notice($contactEventLog);

        return true;
    }

    public function logEventsError($messageDetails = null)
    {
        $ContactEventLog['User_id']      = $this->userId;
        $ContactEventLog['Company_id']   = $this->companyId;
        $ContactEventLog['Contact_id']   = $this->contactId;
        $ContactEventLog['EventForm_id'] = $this->EventFormId;
        $ContactEventLog['Message']      = $messageDetails;

        Log::error($ContactEventLog);

        return true;
    }

    public function logTaskEvents($messageDetails = null)
    {
        $ContactEventLog['User_id']      = $this->userId;
        $ContactEventLog['Company_id']   = $this->companyId;
        $ContactEventLog['Contact_id']   = $this->contactId;
        $ContactEventLog['Message']      = $messageDetails;

        Log::notice($ContactEventLog);

        return true;
    }

    public function logTaskEventsError($messageDetails = null)
    {
        $ContactEventLog['User_id']      = $this->userId;
        $ContactEventLog['Company_id']   = $this->companyId;
        $ContactEventLog['Contact_id']   = $this->contactId;
        $ContactEventLog['Message']      = $messageDetails;

        Log::error($ContactEventLog);

        return true;
    }

    public function doStartAppointmentFormat($data)
    {
        $formattedData['appointment_id'] = $data['id'];
        $formattedData['contact_id'] = $data['contact_id'] ? $data['contact_id'] : 0;
        $formattedData['created'] = $data['created'] ? (Carbon::parse($data['created'])->format('Y-m-d h:i:s')) : '';
        $formattedData['duration'] = $data['duration'] ? $data['duration'] : 0;
        $formattedData['notes'] = (isset($data['notes'])) ? $data['notes'] : '';
        $formattedData['updated_in_seconds'] = $data['updated'] ? strtotime($data['updated']) : '';
        $formattedData['event_form_id'] = $data['event_form_id'] ? $data['event_form_id'] : 0;
        $formattedData['is_appointment_active'] = config('constants.ContactAppointment.current_appointment_active_status');

        return $formattedData;
    }
    //TODO temporarily commented , need to be removed after finalizing
    //public function doEndAppointmentSuccessFormatDetails($contactId, $appointment, $contactTypeId, $appointmentNote, $customFields, $contactDetail)
    public function doEndAppointmentSuccessFormatDetails($contactId, $appointment, $appointmentNote, $contactDetail)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response']['appointment_id'] = $appointment->id;
        $result['response']['contact_id'] = $contactId;
        $result['response']['event_form_id'] = $appointment->event_form_id;
        $result['response']['event_form_name'] = ($appointment->event_form_id > 0) ? $appointment->eventForm->name : config('constants.ContactAppointment.default_appointment');
        //TODO temporarily commented , need to be removed after finalizing
        //$result['response']['account_type_id'] = $contactTypeId;
        $result['response']['contact_view'] = $contactDetail;
        $result['response']['notes'] = ($appointmentNote) ? $appointmentNote : '';
        $result['response']['treat_event_form_as_appointment'] = [];
        //TODO temporarily commented , need to be removed after finalizing
        //$result['response']['selected_contacts'] = [];
        //$result['response']['update_account_info'] = [];
        //$result['response']['opportunities'] = [];
        //$result['response']['event_forms'] = [];
        $result['response']['selected_people'] = [];
        $result['response']['add_task_or_follow_up'] = [];

        return $result;
    }

    public function doContactFormat($contactDetail, $userId)
    {
        $city = isset($contactDetail['contact']->city) ? $contactDetail['contact']->city : '';
        $state = isset($contactDetail['state']->abbr) ? $contactDetail['state']->abbr : '';
        $zipcode = isset($contactDetail['contact']->zip) ? $contactDetail['contact']->zip : '';
        $userLatitude  = (AuthUser::find($userId)->userProfile->latitude) ? AuthUser::find($userId)->userProfile->latitude : null;
        $userLongitude = (AuthUser::find($userId)->userProfile->longitude) ? AuthUser::find($userId)->userProfile->longitude : null;
        $contactLatitude = isset($contactDetail['contact']->latitude) ? $contactDetail['contact']->latitude : null;
        $contactLongitude = isset($contactDetail['contact']->longitude) ? $contactDetail['contact']->longitude : null;
        $lastContacted = isset($contactDetail['contact']->last_contacted) ? $contactDetail['contact']->last_contacted : null;
        $contactData['contact_id'] = isset($contactDetail['contact']->id) ? $contactDetail['contact']->id : 0;
        $contactData['company_name'] = isset($contactDetail['contactCompany']->name) ? $contactDetail['contactCompany']->name : '';
        $contactData['address1'] = isset($contactDetail['contact']->address) ? $contactDetail['contact']->address : '';
        $contactData['address2'] =   isset($contactDetail['contact']->address2) ? ($contactDetail['contact']->address2) : '';
        $contactData['city'] = $city;
        $contactData['state'] = $state;
        $contactData['zipcode'] = $zipcode;
        $contactData['country'] = isset($contactDetail['country']->name) ? $contactDetail['country']->name : '';
        $contactData['last_contacted'] = $lastContacted ? (Carbon::parse($lastContacted)->format('M d,Y g:i A')) : 'Never';
        $contactData['distance'] = $this->getDistance($userLatitude, $userLongitude, $contactLatitude, $contactLongitude, config('constants.Contact.distance_unit'));
        return $contactData;
    }

    function getDistance($userLatitude, $userLongitude, $contactLatitude, $contactLongitude, $unit)
    {
        if ($userLatitude && $userLongitude && $contactLatitude && $contactLongitude) {
            if (($userLatitude == $userLongitude) && ($contactLatitude == $contactLongitude)) {
                return '0';
            } else {
                $theta = $userLatitude - $userLongitude;
                $distance = sin(deg2rad($userLatitude)) * sin(deg2rad($userLongitude)) +  cos(deg2rad($userLatitude)) * cos(deg2rad($userLongitude)) * cos(deg2rad($theta));
                $distance = acos($distance);
                $distance = rad2deg($distance);
                $miles = $distance * 60 * 1.1515;
                $unit = strtoupper($unit);

                if ($unit == config('constants.Contact.distance_unit_kilometers')) {
                    return ($miles * 1.609344);
                } else if ($unit == config('constants.Contact.distance_unit_nautical_miles')) {
                    return ($miles * 0.8684);
                } else {
                    return round($miles, 2) . config('constants.Contact.distance_unit');
                }
            }
        } else {
            return '0';
        }
    }
    public function doSuccessFormatDetails($data)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response'] = $data;

        return $result;
    }

    public function doUpdatedResultFormatDetails($status, $message)
    {
        $result['status']  = $status;
        $result['message'] = $message;
        $result['response'] = '';
        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails;
        $result['response'] = '';
        return $result;
    }

    /* ===============================EVENT========================== */
    public function doEventAdd($request, $userId, $userCompanyId, $contactId, $appointment, $eventName)
    {
        /* Event Creation */
        $eventAddService        = new EventAddService();
        $eventAddService->addEvent(
            $eventName,
            $eventCompanyId          = $userCompanyId,
            $eventUserId             = $userId,
            $eventContactId          = $contactId,
            $eventOtherUserId        = null,
            $eventCallId             = null,
            $eventStartTime          = isset($appointment->start) ? $appointment->start : null,
            $eventDuration           = isset($appointment->duration) ? $appointment->duration : null,
            $eventBadgeId            = null,
            $eventGooglePlace        = null,
            $eventTwilioCallId       = null,
            $eventContactEventFormId = null,
            $eventAppointmentId      = isset($appointment->id) ? $appointment->id : null,
            $eventFollowupId         = null,
            $eventMessage            = null
        );
        return true;
    }
    /* ===============================EVENT-END====================== */
}
