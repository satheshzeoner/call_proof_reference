<?php

namespace App\Services\Contact;

use Carbon\Carbon;
use Exception;
use App\Helpers\Helper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/* Models */
use App\Models\Auth\AuthUser;
use App\Models\Contact\ContactPersonnel;
use App\Models\Contact\Contact;

/* Repositories */
use App\Repositories\Contact\ContactPersonnelRepositories;

class ContactPersonnelService
{
    public function __construct()
    {
        $this->ContactPersonnelRepositories = new ContactPersonnelRepositories();
    }

    public function getContactPersonnel($request, $userId, $contactId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $contactPersonnelDetail = $this->ContactPersonnelRepositories->getContactPersonnelDetails(
                $request,

                $userCompanyId,
                $contactId
            );
            $result['contact_personnel'] = $this->doContactPersonnelFormatDetails($contactPersonnelDetail);
            return $this->doSuccessFormatDetails($result);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function getSpecificContactPersonnel($request, $userId, $contactId, $personnelId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $contactPersonnelDetail = $this->ContactPersonnelRepositories->getSpecificContactPersonnelDetails(
                $request,
                $userCompanyId,
                $contactId,
                $personnelId
            );
            $result = $this->doSpecificContactPersonnelFormatDetails($contactPersonnelDetail);
            return $this->doSuccessFormatDetails($result);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function doAddContactPersonnel($request, $userId, $contactId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $contact = $this->ContactPersonnelRepositories->isContactExist($request, $contactId);
            if (!$contact) {
                Helper::customError(400, config('constants.Errors.ContactNotFound'), $request->all(), config('constants.ServiceName.ContactPersonnel'));
                return $this->doErrorFormatDetails(config('constants.Errors.ContactNotFound'));
            }
            $phones = $request->has('phones') ? collect(json_decode($request->phones)) : [];
            # validate phone number already exist #
            foreach ($phones as $key => $phone) {
                $phoneNumber = $phone->phone_number;
                $isContactPhoneExist = $this->ContactPersonnelRepositories->isContactPhoneExist($request, $contactId, $phoneId = NULL, $phoneNumber);
                if ($isContactPhoneExist) {
                    return $this->doErrorFormatDetails($phoneNumber . ' - ' . config('constants.Errors.ContactPhoneExist'));
                }
            }
            $contactPersonnelDetail = $this->ContactPersonnelRepositories->doAddContactPersonnelDetail(
                $request,
                $userCompanyId,
                $contact,
                $phones
            );

            if ($contactPersonnelDetail) {
                $status = true;
                $message = config('constants.Success.AddContactPersonnel');
                return $this->doUpdatedResultFormatDetails($status, $message);
            } else {
                $status = false;
                $message = config('constants.Errors.AddContactPersonnel');
                return $this->doUpdatedResultFormatDetails($status, $message);
            }
        } catch (Exception $e) {
            return $this->doErrorFormatDetails(config('constants.Errors.AddContactPersonnel'));
        }
    }

    public function doUpdateContactPersonnel($request, $userId, $contactId, $personnelId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $contact = $this->ContactPersonnelRepositories->isContactExist($request, $contactId);
            if (!$contact) {
                Helper::customError(400, config('constants.Errors.ContactNotFound'), $request->all(), config('constants.ServiceName.ContactPersonnel'));
                return $this->doErrorFormatDetails(config('constants.Errors.ContactNotFound'));
            }
            $phones = $request->has('phones') ? collect(json_decode($request->phones)) : [];
            # validate phone number already exists #
            foreach ($phones as $key => $phone) {
                $phoneId = $phone->id;
                $phoneNumber = $phone->phone_number;
                $isContactPhoneExist = $this->ContactPersonnelRepositories->isContactPhoneExist($request, $contactId, $phoneId, $phoneNumber);
                if ($isContactPhoneExist) {
                    Helper::customError(400, $phoneNumber . ' - ' . config('constants.Errors.ContactPhoneExist'), $request->all(), config('constants.ServiceName.ContactPersonnel'));
                    return $this->doErrorFormatDetails($phoneNumber . ' - ' . config('constants.Errors.ContactPhoneExist'));
                }
            }
            # validate Contact Personnel exists #
            $isContactPersonnelExist = $this->ContactPersonnelRepositories->isContactPersonnelExist($request, $personnelId, $contactId, $userCompanyId);
            if (!$isContactPersonnelExist) {
                Helper::customError(400, config('constants.Errors.ContactPersonnelNotFound'), $request->all(), config('constants.ServiceName.ContactPersonnel'));
                return $this->doErrorFormatDetails(config('constants.Errors.ContactPersonnelNotFound'));
            }

            $contactPersonnelDetail = $this->ContactPersonnelRepositories->doUpdateContactPersonnelAndPhoneDetails(
                $request,
                $userCompanyId,
                $contact,
                $phones,
                $personnelId
            );

            if ($contactPersonnelDetail) {
                $status = true;
                $message = config('constants.Success.UpdateContactPersonnel');
                return $this->doUpdatedResultFormatDetails($status, $message);
            } else {
                $status = false;
                $message = config('constants.Errors.UpdateContactPersonnel');
                return $this->doUpdatedResultFormatDetails($status, $message);
            }
        } catch (Exception $e) {
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.ContactPersonnel'));
            return $this->doErrorFormatDetails(config('constants.Errors.UpdateContactPersonnel'));
        }
    }

    public function doMoveContactPersonnel($request, $userId, $userCompanyId)
    {
        try {
            DB::beginTransaction();
            $personnelId   = $request->personnel_id;
            $contactId     = $request->contact_id;

            $result = $this->ContactRepositories->doUpdateContactPersonnelDetails(
                $request,
                $userCompanyId,
                $personnelId,
                $contactId
            );
            if (!$result['status']) {
                return $this->doUpdatedResultFormatDetails($result['status'], $result['message']);
            }
            $result = $this->ContactRepositories->doUpdateContactDetails(
                $request,
                $userCompanyId,
                $personnelId,
                $contactId
            );
            if (!$result['status']) {
                return $this->doUpdatedResultFormatDetails($result['status'], $result['message']);
            }
            $phoneType = $this->ContactRepositories->getPhoneTypeDetail(
                $request
            );
            $contactPhonePersonnel = $this->ContactRepositories->getContactPhonePersonnel(
                $request,
                $personnelId
            );
            $result = $this->ContactRepositories->updatePhonePersonnel(
                $request,
                $userCompanyId,
                $contactId,
                $personnelId,
                $contactPhonePersonnel,
                $phoneType
            );
            if (!$result['status']) {
                return $this->doUpdatedResultFormatDetails($result['status'], $result['message']);
            }
            $result = $this->ContactRepositories->updateFollowupCall(
                $request,
                $contactId
            );
            if (!$result['status']) {
                return $this->doUpdatedResultFormatDetails($result['status'], $result['message']);
            }
            $result = $this->ContactRepositories->updateFollowup(
                $request,
                $contactId
            );
            if (!$result['status']) {
                return $this->doUpdatedResultFormatDetails($result['status'], $result['message']);
            }
            $result = $this->ContactRepositories->updateTwilioSms(
                $request,
                $contactId
            );
            if (!$result['status']) {
                return $this->doUpdatedResultFormatDetails($result['status'], $result['message']);
            }
            $result = $this->ContactRepositories->updateTwilioCall(
                $request,
                $contactId
            );
            if (!$result['status']) {
                return $this->doUpdatedResultFormatDetails($result['status'], $result['message']);
            }
            if ($result['status']) {
                DB::commit();
            }
            return $this->doUpdatedResultFormatDetails($result['status'], $result['message']);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function destroyContactPersonnel($request, $userId, $companyId, $contactId, $personnelId)
    {
        $this->companyId                     = $companyId;
        $this->userId                        = $userId;

        /* Validations */
        $validationResult = $this->contactIdVaidation($contactId);
        if ($validationResult['status'] == false) {
            $this->logEventsError($validationResult['message']);
            return $this->doErrorFormatDetails($validationResult['message']);
        }

        /* Validations */
        $isDeletePersonnel   = AuthUser::find($userId)->userProfile->is_delete_personnel;
        if (!$isDeletePersonnel) {
            $this->logEventsError(config('constants.Success.DeletePersonnelPermission'));
            return $this->doErrorFormatDetails(config('constants.Errors.DeletePersonnelPermission'));
        }

        /* Validations */
        $validationResult = $this->contactPersonnelIdVaidation($personnelId);
        if ($validationResult['status'] == false) {
            $this->logEventsError($validationResult['message']);
            return $this->doErrorFormatDetails($validationResult['message']);
        }

        DB::beginTransaction();
        try {
            /* Remove Contact Personnel */
            $this->ContactRepositories->removeContactPersonnel($userId, $companyId, $contactId, $personnelId);
            $this->logEvents(config('constants.Success.RemoveContactPersonnel') . ' ' . $personnelId);

            DB::commit();
            return $this->doSuccessFormatDetails(config('constants.Success.DeleteContactPersonnel'));
        } catch (Exception $e) {
            DB::rollback();
            $this->logEventsError($e->getMessage());
            return $this->doErrorFormatDetails(config('constants.Errors.UnableDeleteContactPersonnel'));
        }
    }

    public function contactIdVaidation($contactId)
    {
        $contactDetail['status'] = true;
        $contactDetail['message'] = '';
        $contact = Contact::whereId($contactId)->exists();
        if ($contact == false) {
            $this->logEventsError(config('constants.Errors.ContactId'));
            $contactDetail['status'] = false;
            $contactDetail['message'] = config('constants.Errors.ContactId');
            return $contactDetail;
        }
        return $contactDetail;
    }

    /* Validation for Id Has present or Not */
    public function contactPersonnelIdVaidation($personnelId)
    {
        $contactPersonnelDetail['status'] = true;
        $contactPersonnelDetail['message'] = '';
        $contactPersonnel = ContactPersonnel::whereId($personnelId)->exists();
        if ($contactPersonnel == false) {
            $this->logEventsError(config('constants.Errors.ContactPersonnelId'));
            $contactPersonnelDetail['status'] = false;
            $contactPersonnelDetail['message'] = config('constants.Errors.ContactPersonnelId');
            return $contactPersonnelDetail;
        }
        return $contactPersonnelDetail;
    }

    public function doContactPersonnelFormatDetails($contactPersonnelDetail)
    {
        $contactPersonnelData = [];
        foreach ($contactPersonnelDetail as $key => $contactPersonnel) {
            $contactPersonnelData[$key]['id'] = $contactPersonnel->id;
            $contactPersonnelData[$key]['name'] = $contactPersonnel->first_name . ' ' . $contactPersonnel->last_name;
            // $contactPersonnelData[$key]['email'] =  ($contactPersonnel->email) ? $contactPersonnel->email : '';
            // $contactPersonnelData[$key]['title'] =  ($contactPersonnel->title) ? $contactPersonnel->title : '';
            $contactPersonnelData[$key]['role_id'] =  isset($contactPersonnel->PeopleRole->id) ? $contactPersonnel->PeopleRole->id : config('constants.default.DefaultId');
            $contactPersonnelData[$key]['role'] =  isset($contactPersonnel->PeopleRole->name) ? $contactPersonnel->PeopleRole->name : '';
            // $contactPersonnelData[$key]['last_contacted'] =  ($contactPersonnel->last_contacted) ? $contactPersonnel->last_contacted : '';
            $contactPhoneList = $contactPersonnel->contactPhone;
            $contactPhoneData = [];
            foreach ($contactPhoneList as $contactPhonekey => $contactPhone) {
                $contactPhoneData[$contactPhonekey]['id'] = isset($contactPhone->id) ? $contactPhone->id : 0;
                $contactPhoneData[$contactPhonekey]['phone_number'] = isset($contactPhone->phone_number) ? $contactPhone->phone_number : '';
                $contactPhoneData[$contactPhonekey]['phone_type_id'] = isset($contactPhone->phoneType->id) ? $contactPhone->phoneType->id : '';
                $contactPhoneData[$contactPhonekey]['phone_type'] = isset($contactPhone->phoneType->name) ? $contactPhone->phoneType->name : '';
                $contactPhoneData[$contactPhonekey]['extension'] = isset($contactPhone->ext) ? $contactPhone->ext : '';
                $contactPhoneData[$contactPhonekey]['country_code'] = isset($contactPhone->country_code) ? $contactPhone->country_code : '';
            }
            $contactPersonnelData[$key]['phones']  = $contactPhoneData;
            // $contactPersonnelData[$key]['contact_notes'] =  ($contactPersonnel->personnel_notes) ? $contactPersonnel->personnel_notes : '';
        }
        return $contactPersonnelData;
    }

    public function doSpecificContactPersonnelFormatDetails($contactPersonnelDetail)
    {
        $contactPersonnelData = [];
        foreach ($contactPersonnelDetail as $key => $contactPersonnel) {
            $contactPersonnelData[$key]['id'] = $contactPersonnel->id;
            $contactPersonnelData[$key]['name'] = $contactPersonnel->first_name . ' ' . $contactPersonnel->last_name;
            $contactPersonnelData[$key]['first_name'] = $contactPersonnel->first_name;
            $contactPersonnelData[$key]['last_name'] = $contactPersonnel->last_name;
            $contactPersonnelData[$key]['email'] =  ($contactPersonnel->email) ? $contactPersonnel->email : '';
            $contactPersonnelData[$key]['title'] =  ($contactPersonnel->title) ? $contactPersonnel->title : '';
            $contactPersonnelData[$key]['role_id'] =  isset($contactPersonnel->PeopleRole->id) ? $contactPersonnel->PeopleRole->id : config('constants.default.DefaultId');
            $contactPersonnelData[$key]['role'] =  isset($contactPersonnel->PeopleRole->name) ? $contactPersonnel->PeopleRole->name : '';
            $contactPersonnelData[$key]['last_contacted'] =  ($contactPersonnel->last_contacted) ? $contactPersonnel->last_contacted : '';
            $contactPhoneList = $contactPersonnel->contactPhone;
            $contactPhoneData = [];
            foreach ($contactPhoneList as $contactPhonekey => $contactPhone) {
                $contactPhoneData[$contactPhonekey]['id'] = isset($contactPhone->id) ? $contactPhone->id : 0;
                $contactPhoneData[$contactPhonekey]['phone_number'] = isset($contactPhone->phone_number) ? $contactPhone->phone_number : '';
                $contactPhoneData[$contactPhonekey]['phone_type_id'] = isset($contactPhone->phoneType->id) ? $contactPhone->phoneType->id : '';
                $contactPhoneData[$contactPhonekey]['phone_type'] = isset($contactPhone->phoneType->name) ? $contactPhone->phoneType->name : '';
                $contactPhoneData[$contactPhonekey]['extension'] = isset($contactPhone->ext) ? $contactPhone->ext : '';
                $contactPhoneData[$contactPhonekey]['country_code'] = isset($contactPhone->country_code) ? $contactPhone->country_code : '';
            }
            $contactPersonnelData[$key]['phones']  = $contactPhoneData;
            $contactPersonnelData[$key]['contact_notes'] =  ($contactPersonnel->personnel_notes) ? $contactPersonnel->personnel_notes : '';
        }
        return $contactPersonnelData;
    }

    public function getPhoneFormattedAttribute($phone)
    {
        if ($phone) {
            $phone = preg_replace("/[^0-9]/", "", $phone); //Remove all non-numers
            return substr($phone, 0, 3) . "-" . substr($phone, 3, 3) . "-" . substr($phone, 6);
        }
        return '';
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails;
        $result['response'] = '';

        return $result;
    }
    public function doSuccessFormatDetails($data)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response'] = $data;

        return $result;
    }
    public function doUpdatedResultFormatDetails($status, $message)
    {
        $result['status']  = $status;
        $result['message'] = $message;
        $result['response'] = '';
        return $result;
    }

    public function logEvents($messageDetails = null)
    {
        $contactEventLog['User_id']      = $this->userId;
        $contactEventLog['Company_id']   = $this->companyId;
        $contactEventLog['Message']      = $messageDetails;

        Log::notice($contactEventLog);

        return true;
    }

    public function logEventsError($messageDetails = null)
    {
        $contactEventLog['User_id']      = $this->userId;
        $contactEventLog['Company_id']   = $this->companyId;
        $contactEventLog['Message']      = $messageDetails;

        Log::error($contactEventLog);

        return true;
    }
}
