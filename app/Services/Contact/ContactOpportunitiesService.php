<?php

namespace App\Services\Contact;

use App\Helpers\Helper;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

/* EventService */
use App\Services\Event\EventAddService;

/* Models */
use App\Models\Auth\AuthUser;
use App\Models\Opportunity\Opportunity;
use App\Models\Opportunity\OppContact;

/* Repository */
use App\Repositories\Contact\ContactOpportunitiesRepositories;

class ContactOpportunitiesService
{
    public function __construct()
    {
        $this->ContactOpportunitiesRepositories  = new ContactOpportunitiesRepositories();
    }


    public function addContactOpportunities($request, $userId, $contactId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $addContactOpportunities = $this->ContactOpportunitiesRepositories->addContactOpportunitiesDetails(
                $request,
                $userCompanyId,
                $userId,
                $contactId
            );
            if ($addContactOpportunities) {
                $status  = true;
                $message = config('constants.Success.AddOpportunities');
                /* Event Store Process */
                $eventName = config('constants.Event_name.opportunity_created');
                $this->doEventAdd($request, $userId, $userCompanyId, $contactId, $eventName);
                return $this->doUpdatedResultFormatDetails($status, $message);
            } else {
                $status = false;
                $message = config('constants.Errors.AddOpportunities');
                return $this->doUpdatedResultFormatDetails($status, $message);
            }
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function getContactOpportunity($request, $userId, $contactId, $opportunityId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $getContactOpportunity = $this->ContactOpportunitiesRepositories->getContactOpportunityDetails(
                $request,
                $userCompanyId,
                $userId,
                $contactId,
                $opportunityId
            );
            if ($getContactOpportunity) {
                $contactOpportunity['opportunities'] = $this->doFormat($getContactOpportunity);
            } else {
                $contactOpportunity['opportunities'] = [];
            }
            return $this->doSuccessFormatDetails($contactOpportunity);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function editContactOpportunities($request, $userId, $contactId, $opportunityId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $requestCloseDate = $request->close_date;
            $editContactOpportunities = $this->ContactOpportunitiesRepositories->editContactOpportunitiesDetails(
                $request,
                $userCompanyId,
                $contactId,
                $opportunityId
            );

            if ($editContactOpportunities) {
                $status = true;
                $message = config('constants.Success.EditOpportunities');
                /* Event Store Process */
                $eventName = config('constants.Event_name.opportunity_updated');
                $this->doEventAdd($request, $userId, $userCompanyId, $contactId, $eventName);
                return $this->doUpdatedResultFormatDetails($status, $message);
            } else {
                $status = false;
                $message = config('constants.Errors.EditOpportunities');
                return $this->doUpdatedResultFormatDetails($status, $message);
            }
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    /* Delete Opportunity*/
    public function destroyContactOpportunity($request, $userId, $companyId, $contactId, $opportunityId)
    {
        $this->companyId                     = $companyId;
        $this->userId                        = $userId;

        /* Validations */
        $validationResult = $this->opportunityIdVaidation($opportunityId);
        if ($validationResult['status'] == false) {
            $this->logEventsError($validationResult['message']);
            return $this->doErrorFormatDetails($validationResult['message']);
        }

        DB::beginTransaction();
        try {
            /* OpportunityContact Details */
            $this->ContactOpportunitiesRepositories->removeOpportunityContact($userId, $companyId, $contactId, $opportunityId);
            $this->logEvents(config('constants.Success.RemoveOpportunity') . ' ' . $opportunityId);

            /* Opportunity Details */
            $this->ContactOpportunitiesRepositories->removeOpportunity($userId, $companyId, $contactId, $opportunityId);
            $this->logEvents(config('constants.Success.RemoveOpportunity') . ' ' . $opportunityId);

            DB::commit();
            return $this->doSuccessFormatDetails(config('constants.Success.DeleteOpportunity'));
        } catch (Exception $e) {
            DB::rollback();
            $this->logEventsError($e->getMessage());
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }
    /* Validation for Id Has present or Not */
    public function opportunityIdVaidation($opportunityId)
    {

        $opportunityDetails['status'] = true;
        $opportunityDetails['message'] = '';
        $opportunityDetail = Opportunity::whereId($opportunityId)->exists();
        if ($opportunityDetail == false) {
            $this->logEventsError(config('constants.Errors.OpportunityId'));
            $opportunityDetails['status'] = false;
            $opportunityDetails['message'] = config('constants.Errors.OpportunityId');
            return $opportunityDetails;
        }
        $opportunityContactDetailsResult = OppContact::whereOpp_id($opportunityId)->exists();
        if ($opportunityContactDetailsResult == false) {
            $this->logEventsError(config('constants.Errors.OpportunityId'));
            $opportunityDetails['status'] = false;
            $opportunityDetails['message'] = config('constants.Errors.OpportunityId');
            return $opportunityDetails;
        }

        return $opportunityDetails;
    }

    public function doFormat($contactOpportunity)
    {
        $opportunityData['opportunity_id'] = ($contactOpportunity->id) ? $contactOpportunity->id : 0;
        $opportunityData['contact_id'] = ($contactOpportunity->contact_id) ? $contactOpportunity->contact_id : 0;
        $opportunityData['name'] = ($contactOpportunity->opp_name) ? $contactOpportunity->opp_name : '';
        $opportunityData['stage'] = isset($contactOpportunity->oppStage->name) ? $contactOpportunity->oppStage->name : '';
        $opportunityData['stage_id'] = isset($contactOpportunity->oppStage->id) ? $contactOpportunity->oppStage->id : 0;
        $opportunityData['closing'] = ($contactOpportunity->close_date) ? $contactOpportunity->close_date : '';
        $opportunityData['opportunity_type'] = isset($contactOpportunity->oppType->name) ? $contactOpportunity->oppType->name : '';
        $opportunityData['opportunity_type_id'] = isset($contactOpportunity->oppType->id) ? $contactOpportunity->oppType->id : 0;
        $opportunityDefaultSign = config('constants.Company.opportunity_default_sign');
        $opportunityData['opportunity_sign'] = ($contactOpportunity->company->opp_sign == null) ? $opportunityDefaultSign : (($contactOpportunity->company->opp_sign == '') ? $opportunityDefaultSign : $contactOpportunity->company->opp_sign);
        $opportunityData['value'] = ($contactOpportunity->value) ? (float)$contactOpportunity->value : 0;
        $opportunityData['prob'] = ($contactOpportunity->probability) ? $contactOpportunity->probability : 0;
        $oppHistoryValue = isset($contactOpportunity->OppHistory->value) ? $contactOpportunity->OppHistory->value : 0.00;
        $oppHistoryProbability = isset($contactOpportunity->OppHistory->probability) ? $contactOpportunity->OppHistory->probability : 0.00;
        $opportunityData['derived'] = ($oppHistoryValue * $oppHistoryProbability / 100);
        $opportunityData['owner'] = ($contactOpportunity->user->first_name) ? $contactOpportunity->user->first_name . ' ' . $contactOpportunity->user->last_name : '';
        $opportunityData['owner_id'] = ($contactOpportunity->user->id) ? $contactOpportunity->user->id : 0;
        $opportunityPersonal = $contactOpportunity->oppPersonnel()->get();
        $opportunityContacts = $contactOpportunity->oppContact()->get();
        $personalData = [];
        $contactData = [];
        foreach ($opportunityPersonal as $key => $personal) {
            $personalData[$key]['id'] = ($personal->id);
            $personalData[$key]['name'] =  ($personal->first_name . ' ' . $personal->last_name);
        }
        foreach ($opportunityContacts as $key => $opportunityContact) {
            $contactData[$key]['id'] = ($opportunityContact->id);
            $contactData[$key]['name'] =  ($opportunityContact->first_name . ' ' . $opportunityContact->last_name);
        }
        $opportunityData['linked_contacts'] =  $personalData;
        $opportunityData['linked_accounts'] =  $contactData;
        $opportunityData['note'] = ($contactOpportunity->notes) ? $contactOpportunity->notes : '';

        return $opportunityData;
    }
    public function doSuccessFormatDetails($data)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response'] = $data;

        return $result;
    }
    public function doPeopleRoleSuccessFormatDetails($data)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response']['people_role'] = $data;

        return $result;
    }

    public function doUpdatedResultFormatDetails($status, $message)
    {
        $result['status']  = $status;
        $result['message'] = $message;
        $result['response'] = '';
        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails;
        $result['response'] = '';

        return $result;
    }

    public function logEvents($messageDetails = null)
    {
        $contactEventLog['User_id']      = $this->userId;
        $contactEventLog['Company_id']   = $this->companyId;
        $contactEventLog['Message']      = $messageDetails;

        Log::notice($contactEventLog);

        return true;
    }

    public function logEventsError($messageDetails = null)
    {
        $contactEventLog['User_id']      = $this->userId;
        $contactEventLog['Company_id']   = $this->companyId;
        $contactEventLog['Message']      = $messageDetails;

        Log::error($contactEventLog);

        return true;
    }

    /* ===============================EVENT========================== */
    public function doEventAdd($request, $userId, $userCompanyId, $contactId, $eventName)
    {
        /* Event Creation */
        $eventAddService        = new EventAddService();
        $eventAddService->addEvent(
            $eventName,
            $eventCompanyId          = $userCompanyId,
            $eventUserId             = $userId,
            $eventContactId          = $contactId,
            $eventOtherUserId        = null,
            $eventCallId             = null,
            $eventStartTime          = null,
            $eventDuration           = null,
            $eventBadgeId            = null,
            $eventGooglePlace        = null,
            $eventTwilioCallId       = null,
            $eventContactEventFormId = null,
            $eventAppointmentId      = null,
            $eventFollowupId         = null,
            $eventMessage            = null
        );

        return true;
    }
    /* ===============================EVENT-END====================== */
}
