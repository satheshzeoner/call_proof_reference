<?php

namespace App\Services\Contact;

use Exception;

/* Models */
use App\Models\Auth\AuthUser;
use App\Models\Contact\Contact;
use App\Models\ContactMenu\ContactMenulist;
use App\Models\Eventform\EventformContactType;

/* Repositories */
use App\Repositories\Contact\ContactMenuRepositories;

class ContactMenuServices
{
    public function __construct()
    {
        $this->ContactRepositories        = new ContactMenuRepositories();
    }

    public function getContactMenu($request, $userId)
    {
        try {
            $userContactMenuListId  = AuthUser::find($userId)->userProfile->contactmenulist_id;
            $contactMenuName        = ContactMenulist::find($userContactMenuListId);
            $userCompanyId          = AuthUser::find($userId)->userProfile->company_id;
            $isStartAppointment     = AuthUser::find($userId)->userProfile->is_start_appointment;
            $userCompanyPeopleTab   = AuthUser::find($userId)->userProfile->userCompany->people_tab;
            $contactId              = ($request->has('contact_id')) ? $request->get('contact_id') : null;
            $menuId                 = ($request->has('menu_id')) ? $request->get('menu_id') : null;
            $contact                = Contact::where('id', $contactId)->first();

            $contactMenus = $this->ContactRepositories->getContactMenuDetails($userCompanyId, $userCompanyPeopleTab, $menuId, $isStartAppointment, $userContactMenuListId);

            $dynamicContactMenuDetails = [];

            foreach ($contactMenus as $key => $contactMenu) {
                $isValid = 1; //valid event form menu
                $errorMessage = "";
                $dynamicContactMenuDetails[$key]['title'] = ($contactMenu->title) ? ($contactMenu->title) : $contactMenu->name;
                if ($contactMenu->location) {
                    if (str_contains($contactMenu->location, 'omegaedi.com')) {
                        $dynamicContactMenuDetails[$key]['regular_id'] = 24;
                        $dynamicContactMenuDetails[$key]['account_id'] = (isset($contact->account)) ? $contact->account : "0";
                    }
                } else {
                    $dynamicContactMenuDetails[$key]['regular_id'] = $contactMenu->type_id;
                    $dynamicContactMenuDetails[$key]['account_id'] = "0";
                }
                $dynamicContactMenuDetails[$key]['event_form_id']   = ($contactMenu->event_form_id) ? ($contactMenu->event_form_id)                  : 0;
                $dynamicContactMenuDetails[$key]['treat_as']        = ($contactMenu->event_form_treat_as_id) ? ($contactMenu->event_form_treat_as_id) : 0;
                $dynamicContactMenuDetails[$key]['redirection_url'] = ($contactMenu->location) ? ($contactMenu->location)                            : "";
                $existEventForm = EventformContactType::where('event_form_id', $contactMenu->event_form_id)->count();

                if ($existEventForm) {
                    $checkIsValid = EventformContactType::where('event_form_id', $contactMenu->event_form_id)->where('contact_type_id', $contact->contact_type_id)->count();
                    if (!$checkIsValid) {
                        $isValid = 0;
                        $errorMessage = config('constants.Errors.contactMenuErrorMessage');
                    }
                }
                $dynamicContactMenuDetails[$key]['valid_id']      = $isValid;
                $dynamicContactMenuDetails[$key]['error_message'] = $errorMessage;
            }
            $result['status']                = true;
            $result['message']               = 'Success';
            $result['response']['list_name'] = isset($contactMenuName->name) ? $contactMenuName->name : '';
            $result['response']['dynamic_contact_menu'] = $dynamicContactMenuDetails;
            return $result;
        } catch (Exception $e) {
            $result['status']   = false;
            $result['message']  = $e->getMessage();
            $result['response'] = '';
            return $result;
        }
    }
}
