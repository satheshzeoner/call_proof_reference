<?php

namespace App\Services\Contact;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;

/* Models */
use App\Models\Auth\AuthUser;
use App\Models\Label\Label;

/* Repositories */
use App\Repositories\Contact\ContactLabelRepositories;

class ContactLabelService
{
    public function __construct()
    {
        $this->ContactLabelRepositories = new ContactLabelRepositories();
    }

    /* Label List*/
    public function doGetLabels($request, $userId, $contactId)
    {
        try {
            $userCompanyId   = AuthUser::find($userId)->userProfile->company_id;
            $contact = $this->ContactLabelRepositories->isContactExist($request, $contactId);
            if (!$contact) {
                Helper::customError(400, config('constants.Errors.ContactNotFound'), $request->all(), config('constants.ServiceName.ContactLabel'));
                return $this->doErrorFormatDetails(config('constants.Errors.ContactNotFound'));
            }
            $contactLabel['labels'] = $this->ContactLabelRepositories->getContactLabelsDetail(
                $request,
                $userId,
                $userCompanyId,
                $contactId
            );
            return $this->doSuccessFormatDetails($contactLabel);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function doAddContactLabel($request, $userId, $contactId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $contact = $this->ContactLabelRepositories->isContactExist($request, $contactId);
            if (!$contact) {
                Helper::customError(400, config('constants.Errors.ContactNotFound'), $request->all(), config('constants.ServiceName.ContactLabel'));
                return $this->doErrorFormatDetails(config('constants.Errors.ContactNotFound'));
            }
            $contactLabel = $this->ContactLabelRepositories->doAddContactLabelDetails(
                $request,
                $userCompanyId,
                $contactId
            );

            if ($contactLabel) {
                $status = true;
                $message = config('constants.Success.AddContactLabel');
                return $this->doUpdatedResultFormatDetails($status, $message);
            } else {
                $status = false;
                $message = config('constants.Errors.AddContactLabel');
                return $this->doUpdatedResultFormatDetails($status, $message);
            }
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    /* Delete Contact Label*/
    public function doRemoveContactLabel($request, $userId, $userCompanyId, $contactId, $labelId)
    {
        $this->companyId                     = $userCompanyId;
        $this->userId                        = $userId;

        /* Validations */
        $validationResult = $this->labelIdVaidation($labelId);
        if ($validationResult['status'] == false) {
            $this->logContactLabelError($validationResult['message']);
            return $this->doErrorFormatDetails($validationResult['message']);
        }

        DB::beginTransaction();
        try {

            /* Remove Contact Label */
            $this->ContactLabelRepositories->removeContactLabel($request, $contactId, $labelId);
            $this->ContactLabelRepositories->removeLabel($request, $labelId);
            $this->logContactLabel(config('constants.Success.RemoveContactLabel') . ' ' . $labelId);

            DB::commit();
            return $this->doSuccessFormatDetails(config('constants.Success.DeleteContactLabel'));
        } catch (Exception $e) {
            DB::rollback();
            $this->logContactLabelError($e->getMessage());
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    /* Validation for Id Has present or Not */
    public function labelIdVaidation($labelId)
    {
        $contactLabelDetail['status'] = true;
        $contactLabelDetail['message'] = '';
        $contactLabel = Label::whereId($labelId)->exists();
        if ($contactLabel == false) {
            $this->logContactLabelError(config('constants.Errors.ContactLabelId'));
            $contactLabelDetail['status'] = false;
            $contactLabelDetail['message'] = config('constants.Errors.ContactLabelId');
            return $contactLabelDetail;
        }
        return $contactLabelDetail;
    }
    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails;
        $result['response'] = '';

        return $result;
    }
    public function doSuccessFormatDetails($data)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response'] = $data;

        return $result;
    }
    public function doUpdatedResultFormatDetails($status, $message)
    {
        $result['status']  = $status;
        $result['message'] = $message;
        $result['response'] = '';
        return $result;
    }

    public function logContactLabel($messageDetails = null)
    {
        $contactLabelLog['User_id']      = $this->userId;
        $contactLabelLog['Company_id']   = $this->companyId;
        $contactLabelLog['Message']      = $messageDetails;

        Log::notice($contactLabelLog);

        return true;
    }

    public function logContactLabelError($messageDetails = null)
    {
        $contactLabelLog['User_id']      = $this->userId;
        $contactLabelLog['Company_id']   = $this->companyId;
        $contactLabelLog['Message']      = $messageDetails;

        Log::error($contactLabelLog);

        return true;
    }
}
