<?php

namespace App\Services\Contact;

use App\Helpers\Helper;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

/* Models */
use App\Models\Auth\AuthUser;
use App\Models\Contact\ContactPhone;
use App\Models\Contact\ContactInfoEmail;

/* Repository */
use App\Repositories\Contact\ContactRepositories;

class ContactService
{
    public function __construct()
    {
        $this->ContactRepositories  = new ContactRepositories();
    }

    public function getContactType($request, $userId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $contactTypeDetails['contact'] = $this->ContactRepositories->getContactType(
                $request,
                $userCompanyId
            );
            return $this->doSuccessFormatDetails($contactTypeDetails);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function getReverseGeocode($request, $userId, $latitude, $longitude)
    {
        try {
            $responseData['contact'] = $this->ContactRepositories->getReverseGeocode(
                $request,
                $latitude,
                $longitude
            );
            return $this->doSuccessFormatDetails($responseData);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function getPeopleRole($request, $userId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $peopleRoleDetails = $this->ContactRepositories->getPeopleRoleDetails(
                $request,
                $userCompanyId
            );
            return $this->doPeopleRoleSuccessFormatDetails($peopleRoleDetails);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function getContactPhone($request, $userId, $contactId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;

            $phoneContactDetails = $this->getContactPhoneCompanyList($userId, $userCompanyId, $contactId);

            $result['company_phones']  = $phoneContactDetails['company_list'];
            $result['personel_phones'] = $phoneContactDetails['personel_list'];

            return $this->doSuccessFormatDetails($result);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    /* Get Company Phone Details */
    public function getContactPhoneCompanyList($userId, $companyId, $contactId)
    {
        /* Get Contact Related Phone Details */
        $contactPhoneList         = $this->ContactRepositories->getContactPhoneListDetails($contactId);

        $contactPhoneId           = collect($contactPhoneList)->pluck('id');

        /* Get Contact Personel Related ContactPhone Id List of Arrays */
        $contactPersonelList      = $this->ContactRepositories->getContactPersonelListDetails($contactPhoneId);

        /* Specific Personel id DISTINCT the Id Values */
        $contactPersonelId        = collect($contactPersonelList)->pluck('personnel_id');
        $contactPersonelId        = collect($contactPersonelId)->unique()->toArray();

        $contactPersonelMatchedId = collect($contactPersonelList)->pluck('contactphone_id');

        /* Get Personal List Details */
        $contactPersonelDetails      = $this->ContactRepositories->getContactPersonelDetails($contactPersonelId);

        /* Form the Personal related Phone Contacts */
        $personelPhoneDetails = [];
        foreach ($contactPersonelDetails as $contactPersonelDetailsValue) {

            $contactPhoneDetails['personel_id'] = $contactPersonelDetailsValue['id'];
            $contactPhoneDetails['personel_name'] = $contactPersonelDetailsValue['first_name'] . ' ' . $contactPersonelDetailsValue['last_name'];

            /* Get Contact Phone Listed Details Filtered with Matched Ids */
            $contactPhoneResult = $this->doFormattingPersonelDetails($this->ContactRepositories->getContactPersonelPhoneListDetails($contactPersonelDetailsValue['id'], $contactPersonelMatchedId));

            $contactPhoneDetails['personel_phone_numer'] = $contactPhoneResult;

            array_push($personelPhoneDetails, $contactPhoneDetails);
        }

        /* ---------------------------------CONTACT PERSONEL RESULT --------------------------------- */

        $contactCompanyList      = $this->ContactRepositories->getContactCompanyPhoneDetails($contactId, $contactPersonelMatchedId);

        $contactCompanyResult      = $this->doFormattingCompanyDetails($contactCompanyList);

        $finalPhoneResult['company_list']  = $contactCompanyResult;
        $finalPhoneResult['personel_list'] = $personelPhoneDetails;

        /* ---------------------------------CONTACT COMPANY RESULT --------------------------------- */

        return $finalPhoneResult;
    }

    /* formatting Company Phone details */
    public function doFormattingCompanyDetails($contactCompanyList)
    {
        $contactPhoneCompanyList = [];

        foreach ($contactCompanyList as $contactCompanyListValue) {
            $companyPhoneDetails['phone_id']           = $contactCompanyListValue['id'];
            $companyPhoneDetails['phone_type_id']      = $contactCompanyListValue['phone_type_id'];
            $companyPhoneDetails['phone_type_name']    = $contactCompanyListValue['phone_type']['name'];
            $companyPhoneDetails['phone_number']       = $contactCompanyListValue['phone_number'];
            $companyPhoneDetails['phone_extension']    = $contactCompanyListValue['ext'];
            $companyPhoneDetails['phone_country_code'] = $contactCompanyListValue['country_code'];

            array_push($contactPhoneCompanyList, $companyPhoneDetails);
        }

        return $contactPhoneCompanyList;
    }

    /* formatting Personel Phone details */
    public function doFormattingPersonelDetails($contactCompanyList)
    {
        $contactPhoneCompanyList = [];

        foreach ($contactCompanyList as $contactCompanyListValue) {
            $companyPhoneDetails['phone_id']           = $contactCompanyListValue['id'];
            $companyPhoneDetails['phone_type_id']      = $contactCompanyListValue['phone_type_id'];
            $companyPhoneDetails['phone_type_name']    = $contactCompanyListValue['phone_type_name'];
            $companyPhoneDetails['phone_number']       = $contactCompanyListValue['phone_number'];
            $companyPhoneDetails['phone_extension']    = $contactCompanyListValue['ext'];
            $companyPhoneDetails['phone_country_code'] = $contactCompanyListValue['country_code'];

            array_push($contactPhoneCompanyList, $companyPhoneDetails);
        }

        return $contactPhoneCompanyList;
    }

    public function doAddContactPhone($request, $userId, $contactId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $selectedContactPersonnel = $request->has('associated_contacts') ? collect(json_decode($request->get('associated_contacts'))) : [];
            $phoneNumber = preg_replace("/[^0-9]/", "", $request->phone_number);
            $contactPhoneDetail = $this->ContactRepositories->doAddContactPhoneDetails(
                $request,
                $userCompanyId,
                $contactId,
                $phoneNumber,
                $selectedContactPersonnel
            );
            return $this->doUpdatedResultFormatDetails($contactPhoneDetail['status'], $contactPhoneDetail['message']);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function doUpdateContactPhone($request, $userId, $contactId, $contactPhoneId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $selectedContactPersonnel = $request->has('associated_contacts') ? collect(json_decode($request->get('associated_contacts'))) : [];
            $phoneNumber = preg_replace("/[^0-9]/", "", $request->phone_number);
            $contactPhoneDetail = $this->ContactRepositories->doUpdateContactPhoneDetails(
                $request,
                $userCompanyId,
                $contactId,
                $contactPhoneId,
                $phoneNumber,
                $selectedContactPersonnel
            );
            return $this->doUpdatedResultFormatDetails($contactPhoneDetail['status'], $contactPhoneDetail['message']);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    /* Delete Contact Phone*/
    public function destroyContactPhone($request, $userId, $companyId, $contactId, $contactPhoneId)
    {
        $this->companyId                     = $companyId;
        $this->userId                        = $userId;

        /* Validations */
        $validationResult = $this->contactPhoneIdVaidation($contactPhoneId);
        if ($validationResult['status'] == false) {
            $this->logEventsError($validationResult['message']);
            return $this->doErrorFormatDetails($validationResult['message']);
        }

        DB::beginTransaction();
        try {
            /* Remove Contact Default Phone */
            $this->ContactRepositories->removeContactDefaultPhone($userId, $companyId, $contactId, $contactPhoneId);

            /* Remove Contact Phone */
            $this->ContactRepositories->removeContactPhone($userId, $companyId, $contactId, $contactPhoneId);
            $this->logEvents(config('constants.Success.RemoveContactPhone') . ' ' . $contactPhoneId);

            DB::commit();
            return $this->doSuccessFormatDetails(config('constants.Success.DeleteContactPhone'));
        } catch (Exception $e) {
            DB::rollback();
            $this->logEventsError($e->getMessage());
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }
    /* Validation for Id Has present or Not */
    public function contactPhoneIdVaidation($contactPhoneId)
    {
        $contactPhoneDetail['status'] = true;
        $contactPhoneDetail['message'] = '';
        $contactPhone = ContactPhone::whereId($contactPhoneId)->exists();
        if ($contactPhone == false) {
            $this->logEventsError(config('constants.Errors.ContactPhoneId'));
            $contactPhoneDetail['status'] = false;
            $contactPhoneDetail['message'] = config('constants.Errors.ContactPhoneId');
            return $contactPhoneDetail;
        }
        return $contactPhoneDetail;
    }

    public function doGetContactInfoEmail($request, $userId, $contactId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $ContactInfoEmailDetail['contact_info_email_details'] = $this->ContactRepositories->doGetContactInfoEmailDetails(
                $request,
                $userCompanyId,
                $contactId
            );
            $result = $this->doSuccessFormatDetails($ContactInfoEmailDetail);
            return $result;
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function doAddContactInfoEmail($request, $userId, $contactId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $contactInfoEmail = $this->ContactRepositories->doAddContactInfoEmailDetails(
                $request,
                $userCompanyId,
                $contactId
            );

            if ($contactInfoEmail) {
                $status = true;
                $message = config('constants.Success.AddContactInfoEmail');
                return $this->doUpdatedResultFormatDetails($status, $message);
            } else {
                $status = false;
                $message = config('constants.Errors.AddContactInfoEmail');
                return $this->doUpdatedResultFormatDetails($status, $message);
            }
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function doUpdateContactInfoEmail($request, $userId, $contactId, $contactInfoEmailId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $contactInfoEmail = $this->ContactRepositories->doUpdateContactInfoEmailDetails(
                $request,
                $userCompanyId,
                $contactId,
                $contactInfoEmailId
            );

            if ($contactInfoEmail) {
                $status = true;
                $message = config('constants.Success.UpdateContactInfoEmail');
                return $this->doUpdatedResultFormatDetails($status, $message);
            } else {
                $status = false;
                $message = config('constants.Errors.UpdateContactInfoEmail');
                return $this->doUpdatedResultFormatDetails($status, $message);
            }
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    /* Delete Contact Info Email*/
    public function doRemoveContactInfoEmail($request, $userId, $companyId, $contactId, $contactInfoEmailId)
    {
        $this->companyId                     = $companyId;
        $this->userId                        = $userId;

        /* Validations */
        $validationResult = $this->contactInfoEmailIdVaidation($contactInfoEmailId);
        if ($validationResult['status'] == false) {
            $this->logEventsError($validationResult['message']);
            return $this->doErrorFormatDetails($validationResult['message']);
        }

        DB::beginTransaction();
        try {
            /* Remove Contact Info Email */
            $this->ContactRepositories->removeContactInfoEmail($request, $contactInfoEmailId);
            $this->logEvents(config('constants.Success.RemoveContactInfoEmail') . ' ' . $contactInfoEmailId);

            DB::commit();
            return $this->doSuccessFormatDetails(config('constants.Success.DeleteContactInfoEmail'));
        } catch (Exception $e) {
            DB::rollback();
            $this->logEventsError($e->getMessage());
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    /* Validation for Id Has present or Not */
    public function contactInfoEmailIdVaidation($contactInfoEmailId)
    {
        $contactInfoEmailDetail['status'] = true;
        $contactInfoEmailDetail['message'] = '';
        $contactInfoEmail = ContactInfoEmail::whereId($contactInfoEmailId)->exists();
        if ($contactInfoEmail == false) {
            $this->logEventsError(config('constants.Errors.ContactInfoEmailId'));
            $contactInfoEmailDetail['status'] = false;
            $contactInfoEmailDetail['message'] = config('constants.Errors.ContactInfoEmailId');
        }
        return $contactInfoEmailDetail;
    }

    public function doUpdateSelectedUsers($request, $userId, $contactId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $selectedContactUsers = $request->has('user_id') ? collect(json_decode($request->get('user_id'))) : [];
            $contactUser = $this->ContactRepositories->doUpdateSelectedUsersDetails(
                $request,
                $userCompanyId,
                $contactId,
                $selectedContactUsers
            );

            if ($contactUser) {
                $status = true;
                $message = config('constants.Success.UpdateContactSelectedUsers');
                return $this->doUpdatedResultFormatDetails($status, $message);
            } else {
                $status = false;
                $message = config('constants.Errors.UpdateContactSelectedUsers');
                return $this->doUpdatedResultFormatDetails($status, $message);
            }
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function doGetContactFilterCustomFields($request, $userId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $customFieldTable = config('constants.CustomField.custom_field_table');
            $customFieldDetail['custom_fields'] = $this->ContactRepositories->getContactFilterCustomFieldDetails(
                $request,
                $userCompanyId,
                $customFieldTable
            );
            return $this->doSuccessFormatDetails($customFieldDetail);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function doGetContactParentCompany($request, $userId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $title = $request->title;
            $contactParentCompanyDetail['parent_company_detail'] = $this->ContactRepositories->getContactParentCompany(
                $request,
                $userCompanyId,
                $title
            );
            return $this->doSuccessFormatDetails($contactParentCompanyDetail);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function doSuccessFormatDetails($data)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response'] = $data;

        return $result;
    }
    public function doPeopleRoleSuccessFormatDetails($data)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response']['people_role'] = $data;

        return $result;
    }



    public function getPhoneFormattedAttribute($phone)
    {
        if ($phone) {
            $phone = preg_replace("/[^0-9]/", "", $phone); //Remove all non-numers
            return substr($phone, 0, 3) . "-" . substr($phone, 3, 3) . "-" . substr($phone, 6);
        }
        return '';
    }

    public function doUpdatedResultFormatDetails($status, $message)
    {
        $result['status']  = $status;
        $result['message'] = $message;
        $result['response'] = '';
        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails;
        $result['response'] = '';

        return $result;
    }

    public function logEvents($messageDetails = null)
    {
        $contactEventLog['User_id']      = $this->userId;
        $contactEventLog['Company_id']   = $this->companyId;
        $contactEventLog['Message']      = $messageDetails;

        Log::notice($contactEventLog);

        return true;
    }

    public function logEventsError($messageDetails = null)
    {
        $contactEventLog['User_id']      = $this->userId;
        $contactEventLog['Company_id']   = $this->companyId;
        $contactEventLog['Message']      = $messageDetails;

        Log::error($contactEventLog);

        return true;
    }
}
