<?php

namespace App\Services\Contact;

use App\Helpers\Helper;
use Exception;

/* Models */
use App\Models\Auth\AuthUser;

/* Repository */
use App\Repositories\Contact\ContactListRepositories;

class ContactListServices
{
    public function __construct()
    {
        $this->ContactListRepositories        = new ContactListRepositories();
    }

    public function getContactList($request, $userId)
    {
        try {
            $userLatitude      = (AuthUser::find($userId)->userProfile->latitude) ? AuthUser::find($userId)->userProfile->latitude : 0;
            $userLongitude     = (AuthUser::find($userId)->userProfile->longitude) ? AuthUser::find($userId)->userProfile->longitude : 0;
            $userCompanyId     = AuthUser::find($userId)->userProfile->company_id;

            $contactList = $this->ContactListRepositories->getContactListDetails($request, $userLatitude, $userLongitude, $userCompanyId);

            $result['status']   = true;
            $result['message']  = 'Success';
            $result['response'] = $contactList;
            $result['paginationDetails'] = Helper::customPagination($contactList);

            return $result;
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function getContactNoteDetails($userId, $companyId, $contactId)
    {
        try {

            $contactNoteList = $this->ContactListRepositories->getContactNotePreviousList($userId, $companyId, $contactId);
            $contactNoteList = $this->doFormatContactNote($contactNoteList);
            return $this->doSuccessFormatDetails($contactNoteList);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function doFormatContactNote($contactNote)
    {
        $contactNoteDetails = [];
        foreach ($contactNote as $contactNotekey => $contactNoteValue) {
            $contactNoteDetails[$contactNotekey]['note_id']          = $contactNoteValue['id'];
            $contactNoteDetails[$contactNotekey]['note']             = $contactNoteValue['note'];
            $contactNoteDetails[$contactNotekey]['created']          = $contactNoteValue['created'];
            $contactNoteDetails[$contactNotekey]['note_source_type'] = (isset($contactNoteValue['note_source_type'])) ? $contactNoteValue['note_source_type'] : '';
        }

        return $contactNoteDetails;
    }

    public function doSuccessFormatDetails($contactDetails)
    {
        $result['status']   = true;
        $result['message']  = 'Success';
        $result['response'] = $contactDetails;

        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails;
        $result['response'] = '';

        return $result;
    }
}
