<?php

namespace App\Services\Contact;


use FFI\Exception;

/* Models */
use App\Models\Auth\AuthUser;

/* Repository */
use App\Repositories\Contact\ContactViewRepositories;
use Illuminate\Support\Carbon;

class ContactViewServices
{
    public function __construct()
    {
        $this->ContactViewRepositories = new ContactViewRepositories();
    }

    public function getContactData($request, $userId, $contactId)
    {
        try {
            $customFieldTable = config('constants.ContactView.custom_field_table');
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $showHidden = config('constants.ContactView.cfield_show_hidden');
            $showImgFields = config('constants.ContactView.cfield_show_img_fields');

            $contactView = $this->ContactViewRepositories->getContactDataDetails($request, $userCompanyId, $contactId);
            $customFields = $this->ContactViewRepositories->getCustomField(
                $request,
                $userId,
                $userCompanyId,
                $customFieldTable,
                $showHidden,
                $showImgFields,
                $contactId
            );
            $contactNotes = $this->ContactViewRepositories->getContactNotes(
                $request,
                $userId,
                $userCompanyId,
                $contactId,
                $contactView

            );
            $contactParentCompanyNotes = $this->ContactViewRepositories->getContactParentCompanyNotes(
                $request,
                $userId,
                $userCompanyId,
                $contactView
            );
            $data['contact'] = $this->doContactFormat($contactView, $userId);;
            $data['company_details'] = ($customFields) ? $customFields : [];
            $data['notes']['default_notes'] = ($contactNotes['default_notes']) ? $contactNotes['default_notes'] : [];
            $data['notes']['all_notes'] = ($contactNotes['all_notes']) ? $contactNotes['all_notes'] : [];
            $data['notes']['parent_company_notes'] = ($contactParentCompanyNotes) ? ($contactParentCompanyNotes) : [];
            return $this->doSuccessFormatDetails($data);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e);
        }
    }
    public function doContactFormat($contactView, $userId)
    {
        $city = isset($contactView['contact']->city) ? $contactView['contact']->city : '';
        $state = isset($contactView['state']->abbr) ? $contactView['state']->abbr : '';
        $zipcode = isset($contactView['contact']->zip) ? $contactView['contact']->zip : '';
        $phone = isset($contactView['contactPhone']->phone_number) ? $contactView['contactPhone']->phone_number : '';
        $phone = $this->getPhoneFormattedAttribute($phone);
        $userLatitude  = (AuthUser::find($userId)->userProfile->latitude) ? AuthUser::find($userId)->userProfile->latitude : null;
        $userLongitude = (AuthUser::find($userId)->userProfile->longitude) ? AuthUser::find($userId)->userProfile->longitude : null;
        $contactLatitude = isset($contactView['contact']->latitude) ? $contactView['contact']->latitude : null;
        $contactLongitude = isset($contactView['contact']->longitude) ? $contactView['contact']->longitude : null;
        $lastContacted = isset($contactView['contact']->last_contacted) ? $contactView['contact']->last_contacted : null;
        $contactViewData['contact_id'] = isset($contactView['contact']->id) ? $contactView['contact']->id : 0;
        $contactViewData['account_type'] = isset($contactView['contactType']->name) ? $contactView['contactType']->name : '';
        $contactViewData['account_type_id'] = isset($contactView['contact']->contact_type_id) ? $contactView['contact']->contact_type_id : 0;
        $contactViewData['account_number'] = isset($contactView['contact']->account) ? $contactView['contact']->account : '';
        $contactViewData['company_name'] = isset($contactView['contactCompany']->name) ? $contactView['contactCompany']->name : '';
        $contactViewData['parent_company'] = isset($contactView['contact']->title) ? $contactView['contact']->title : '';
        $contactViewData['address1'] = isset($contactView['contact']->address) ? $contactView['contact']->address : '';
        $contactViewData['address2'] =   isset($contactView['contact']->address2) ? ($contactView['contact']->address2) : '';
        $contactViewData['city'] = $city;
        $contactViewData['state'] = $state;
        $contactViewData['state_id'] = isset($contactView['contact']->state_id) ? $contactView['contact']->state_id : 0;
        $contactViewData['zipcode'] = $zipcode;
        $contactViewData['country'] = isset($contactView['country']->name) ? $contactView['country']->name : '';
        $contactViewData['country_id'] = isset($contactView['contact']->country_id) ? $contactView['contact']->country_id : 0;
        $contactViewData['company_email'] = isset($contactView['contact']->email) ? $contactView['contact']->email : '';
        $contactViewData['phone'] = $phone;
        $contactViewData['phone_type'] = isset($contactView['contactPhoneType']->name) ? $contactView['contactPhoneType']->name : '';
        $contactViewData['phone_type_id'] = isset($contactView['contactPhone']->phone_type_id) ? $contactView['contactPhone']->phone_type_id : 0;
        $contactViewData['country_code'] = isset($contactView['contactPhone']->country_code) ? $contactView['contactPhone']->country_code : '';
        $contactViewData['website'] = isset($contactView['contact']->website) ? $contactView['contact']->website : '';
        $contactViewData['last_contacted'] = $lastContacted ? (Carbon::parse($lastContacted)->format('M d,Y g:i A')) : 'Never';
        $contactViewData['latitude'] = isset($contactView['contact']->latitude) ? $contactView['contact']->latitude : '';
        $contactViewData['longitude'] = isset($contactView['contact']->longitude) ? $contactView['contact']->longitude : '';
        $contactViewData['distance'] = $this->getDistance($userLatitude, $userLongitude, $contactLatitude, $contactLongitude, config('constants.Contact.distance_unit'));
        $contactPersonnel = isset($contactView['contactPersonnel']) ? $contactView['contactPersonnel'] : [];
        $contactPersonnelData = [];
        foreach ($contactPersonnel as $key => $personnel) {
            $contactPersonnelData[$key] = ($personnel->first_name . ' ' . $personnel->last_name);
        }
        $contactViewData['people'] = (implode(",", array_filter($contactPersonnelData)));

        $contactUsers = isset($contactView['contactUsers']) ? $contactView['contactUsers'] : [];
        $contactUsersData = [];
        foreach ($contactUsers as $key => $contactUser) {
            $contactUsersData[$key] = ($contactUser->first_name . ' ' . $contactUser->last_name);
        }
        $contactViewData['users'] = (implode(",", $contactUsersData));

        $contactUsersIdData = [];
        foreach ($contactUsers as $key => $contactUser) {
            $contactUsersIdData[$key] = ($contactUser->id . ' ' . $contactUser->id);
        }
        $contactViewData['users_id'] = (implode(",", $contactUsersIdData));

        $contactInfoEmail = isset($contactView['contactInfoEmails']) ? $contactView['contactInfoEmails'] : [];
        $contactInfoEmailData = [];
        foreach ($contactInfoEmail as $key => $contactEmail) {
            $contactInfoEmailData[$key] = ($contactEmail->email);
        }
        $contactViewData['email'] = (implode(",", $contactInfoEmailData));
        return $contactViewData;
    }

    function getDistance($userLatitude, $userLongitude, $contactLatitude, $contactLongitude, $unit)
    {
        if ($userLatitude && $userLongitude && $contactLatitude && $contactLongitude) {
            if (($userLatitude == $userLongitude) && ($contactLatitude == $contactLongitude)) {
                return '0';
            } else {
                $theta = $userLatitude - $userLongitude;
                $distance = sin(deg2rad($userLatitude)) * sin(deg2rad($userLongitude)) +  cos(deg2rad($userLatitude)) * cos(deg2rad($userLongitude)) * cos(deg2rad($theta));
                $distance = acos($distance);
                $distance = rad2deg($distance);
                $miles = $distance * 60 * 1.1515;
                $unit = strtoupper($unit);

                if ($unit == config('constants.Contact.distance_unit_kilometers')) {
                    return ($miles * 1.609344);
                } else if ($unit == config('constants.Contact.distance_unit_nautical_miles')) {
                    return ($miles * 0.8684);
                } else {
                    return round($miles, 2) . config('constants.Contact.distance_unit');
                }
            }
        } else {
            return '0';
        }
    }
    public function getPhoneFormattedAttribute($phone)
    {
        if ($phone) {
            $phone = preg_replace("/[^0-9]/", "", $phone); //Remove all non-numeric
            return substr($phone, 0, 3) . "-" . substr($phone, 3, 3) . "-" . substr($phone, 6);
        }
        return '';
    }
    public function doSuccessFormatDetails($data)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response'] = $data;

        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails->getMessage();
        $result['response'] = '';

        return $result;
    }
}
