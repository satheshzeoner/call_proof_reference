<?php

namespace App\Services\EventForm;

use App\Helpers\Helper;
use Illuminate\Support\Carbon;
use Exception;

/* Models */
use App\Models\Auth\AuthUser;

/* Repository */
use App\Repositories\EventForm\EventFormPreviousListRepositories;
use App\Repositories\EventForm\EventFormRepositories;
use App\Repositories\EventForm\EventFormAddRepositories;
use App\Repositories\EventForm\EventFormEditRepositories;
use App\Repositories\Contact\ContactViewRepositories;
use App\Repositories\Contact\ContactRepositories;
use App\Repositories\Contact\ContactPersonnelRepositories;

class EventFormListService
{
    public function __construct()
    {
        $this->EventFormRepositories                = new EventFormRepositories();
        $this->EventPreviousListRepositories        = new EventFormPreviousListRepositories();
        $this->EventFormAddDetails                  = new EventFormAddRepositories();
        $this->EventFormEditRepositories            = new EventFormEditRepositories();
        $this->ContactViewRepositories              = new ContactViewRepositories();
        $this->ContactRepositories                  = new ContactRepositories();
        $this->ContactPersonnelRepositories         = new ContactPersonnelRepositories();
    }

    # =============================================
    # =       EventForm Previous List             =
    # =============================================

    public function getPreviousEventList($userId, $companyId, $contactId, $searchEvent)
    {
        try {
            $eventDetails = $this->EventPreviousListRepositories->getPreviousEventDetails($userId, $companyId, $contactId, $searchEvent);

            $eventFormatDetails = $this->attributeTransformation($eventDetails, $contactId);

            return $this->doSuccessFormatWithPagignationDetails($eventFormatDetails, $eventDetails);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    /* Pagination Process */
    public function doSuccessFormatWithPagignationDetails($eventsDeails, $eventsResult)
    {
        $result['status']             = true;
        $result['message']            = 'Success';
        $result['response']['eventform'] = $eventsDeails;
        $result['response']['paginationDetails'] = Helper::customPagination($eventsResult);

        return $result;
    }

    # =============================================
    # =       EventForm Select List               =
    # =============================================

    public function getSelectEventFormDetails($userId, $companyId, $contactId, $eventFormId)
    {
        try {
            $eventSelectDetails = $this->EventFormRepositories->getSelectEventDetails($userId, $companyId, $contactId, $eventFormId);
            return $this->doSuccessFormat($eventSelectDetails);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }
    # =============================================
    # =       EventForm People Select List        =
    # =============================================

    public function getpeopleEventFormDetails($userId, $companyId, $contactId)
    {
        try {
            $eventPeopleDetails = $this->EventFormRepositories->getPeopleforSpecificContact($userId, $companyId, $contactId);
            return $this->doSuccessFormat($eventPeopleDetails);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }
    # =============================================
    # =       EventForm Add Details               =
    # =============================================

    public function getEventFormAddDetails($userId, $companyId, $contactId, $eventFormId)
    {
        try {

            /* Contact Details */
            $userCompanyId     = AuthUser::find($userId)->userProfile->company_id;
            $contactView       =  $this->ContactViewRepositories->getContactDataDetails($request = null, $userCompanyId, $contactId);

            /* Event Details */
            $eventDetails      = $this->EventFormAddDetails->getEventFormAddDetails($userId, $companyId, $contactId, $eventFormId);

            $eventAddDetails['contact_details']       = $this->doFormatForContactDetails($contactView);

            $eventAddDetails['eventform_info']        = $this->getEventFormDetailsAddEventForm($userId, $companyId, $contactId, $eventFormId);

            $eventAddDetails['eventform_details']     = $this->doFormattingEventFormDetails($eventDetails);

            return $this->doSuccessFormat($eventAddDetails);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    /* getEventFormDetailsAddEventForm */
    public function getEventFormDetailsAddEventForm($userId, $companyId, $contactId, $eventFormId)
    {
        $eventFormDetails = $this->EventFormRepositories->getSpecificEventDetails($userId, $companyId, $contactId, $eventFormId);

        $contactEventFormInfo['event_form_id']           = config('constants.default.DefaultId');
        $contactEventFormInfo['event_form_name']         = $eventFormDetails->name;
        $contactEventFormInfo['event_form_created_date'] = '';

        return $contactEventFormInfo;
    }

    /* eventDetails */
    public function doFormattingEventFormDetails($eventDetails)
    {
        $eventFormDetails = [];

        foreach ($eventDetails as $eventDetailskey => $eventDetailsValue) {

            /* Array is associative */
            if (array_keys($eventDetailsValue) !== range(0, count($eventDetailsValue) - 1)) {
                /* EventForm TypeId Set */
                if ($eventDetailsValue['rfield_id'] != '' || $eventDetailsValue['cfield_type_id'] != '') {
                    if ($eventDetailsValue['cfield_type_id'] == '') {
                        $eventDetailsValue['cfield_type_id'] = config('constants.FieldType.textIdArray'); /* For Text Type Field */
                        $eventDetailsValue['custom_field_type'] = config('constants.FieldType.textArray');
                    }

                    $eventAssociativeDetails['name']                           = $eventDetailsValue['name'];
                    $eventAssociativeDetails['position']                       = $eventDetailsValue['position'];
                    $eventAssociativeDetails['points']                         = $eventDetailsValue['points'];
                    $eventAssociativeDetails['required']                       = (isset($eventDetailsValue['required'])) ? $eventDetailsValue['required'] : false;
                    $eventAssociativeDetails['custom_field_id']                = $eventDetailsValue['id'];
                    $eventAssociativeDetails['custom_field_inner_id']          = (int)$eventDetailsValue['cfield_id'];
                    $eventAssociativeDetails['custom_field_regular_id']        = (int)$eventDetailsValue['rfield_id'];
                    $eventAssociativeDetails['custom_field_type_id']           = (int)$eventDetailsValue['cfield_type_id'];
                    $eventAssociativeDetails['custom_field_option_default_id'] = (string)$eventDetailsValue['cfield_option_default_id'];
                    $eventAssociativeDetails['value']                          = '';
                    $eventAssociativeDetails['custom_field_type']              = (isset($eventDetailsValue['custom_field_type'])) ? $eventDetailsValue['custom_field_type'] : [];
                    $eventAssociativeDetails['custom_field_option']            = (isset($eventDetailsValue['custom_field_option'])) ? $eventDetailsValue['custom_field_option'] : [];

                    array_push($eventFormDetails, $eventAssociativeDetails);
                }
            } else { /* Array is associative */
                $eventDetailsCollectValue = collect($eventDetailsValue)->first();
                if ($eventDetailsCollectValue['rfield_id'] != '' || $eventDetailsCollectValue['cfield_type_id'] != '') {
                    if ($eventDetailsCollectValue['cfield_type_id'] == '') {
                        $eventDetailsValue['cfield_type_id'] = config('constants.FieldType.textIdArray'); /* For Text Type Field */
                        $eventDetailsValue['custom_field_type'] = config('constants.FieldType.textArray');
                    }

                    $eventDetailsCollect['name']                           = $eventDetailsCollectValue['name'];
                    $eventDetailsCollect['position']                       = $eventDetailsCollectValue['position'];
                    $eventDetailsCollect['points']                         = $eventDetailsCollectValue['points'];
                    $eventDetailsCollect['required']                       = (isset($eventDetailsCollectValue['required'])) ? $eventDetailsCollectValue['required'] : false;;
                    $eventDetailsCollect['custom_field_id']                = $eventDetailsCollectValue['id'];
                    $eventDetailsCollect['custom_field_inner_id']          = (int)$eventDetailsCollectValue['cfield_id'];
                    $eventDetailsCollect['custom_field_regular_id']        = (int)$eventDetailsCollectValue['rfield_id'];
                    $eventDetailsCollect['custom_field_type_id']           = (int)$eventDetailsCollectValue['cfield_type_id'];
                    $eventDetailsCollect['custom_field_option_default_id'] = (string)$eventDetailsCollectValue['cfield_option_default_id'];
                    $eventDetailsCollect['value']                          = '';
                    $eventDetailsCollect['custom_field_type']              = (isset($eventDetailsCollectValue['custom_field_type'])) ? $eventDetailsCollectValue['custom_field_type'] : [];
                    $eventDetailsCollect['custom_field_option']            = (isset($eventDetailsCollectValue['custom_field_option'])) ? $eventDetailsCollectValue['custom_field_option'] : [];

                    array_push($eventFormDetails, $eventDetailsCollect);
                }
            }
        }

        return $eventFormDetails;
    }

    /* ContactEventForm Edit Details */
    public function getEditContactEventFormDetails($userId, $companyId, $contactId, $EventFormId, $contactEventFormId)
    {

        try {

            /* Contact Details */
            $userCompanyId     = AuthUser::find($userId)->userProfile->company_id;
            $contactView       =  $this->ContactViewRepositories->getContactDataDetails($request = null, $userCompanyId, $contactId);

            /* Event Details */
            $eventDetails                             = $this->EventFormAddDetails->getContactEventFormEditDetails($userId, $companyId, $contactId, $EventFormId, $contactEventFormId);

            $eventAddDetails['contact_details']       = $this->doFormatForContactDetails($contactView);

            $eventAddDetails['eventform_info']        = $this->getEventFormDetailsEditEventForm($userId, $companyId, $contactId, $EventFormId, $contactEventFormId, $eventDetails);

            $eventAddDetails['people_details']        = $this->doFormatforEditPeopleDetails($companyId, $contactId, $contactEventFormId);

            $eventAddDetails['eventform_details']     = $this->doFormatforEditContactDetails($eventDetails, $contactEventFormId);

            return $this->doSuccessFormat($eventAddDetails);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    /* getEventFormDetailsAddEventForm */
    public function getEventFormDetailsEditEventForm($userId, $companyId, $contactId, $eventFormId, $contactEventFormId, $eventDetails)
    {
        $eventFormDetails = $this->EventFormRepositories->getSpecificEventDetails($userId, $companyId, $contactId, $eventFormId);

        $contactEventFormDetails = $this->EventFormRepositories->getSpecificContactEventDetails($contactEventFormId);

        $day      = Carbon::parse($contactEventFormDetails->created)->format('l');
        $month    = Carbon::parse($contactEventFormDetails->created)->format('M');
        $dateInt  = Carbon::parse($contactEventFormDetails->created)->format('d');
        $year     = Carbon::parse($contactEventFormDetails->created)->format('Y');
        $fullTime = Carbon::parse($contactEventFormDetails->created)->format('g:ia');

        $submittedFormatDate = $month . '. ' . $dateInt . ', ' . $year . ', ' . $fullTime;

        $contactEventFormInfo['event_form_id']           = (int)$contactEventFormId;
        $contactEventFormInfo['event_form_name']         = $eventFormDetails->name;
        $contactEventFormInfo['event_form_created_date'] = $submittedFormatDate;

        return $contactEventFormInfo;
    }

    public function doFormatforEditPeopleDetails($companyId, $contactId, $contactEventFormId)
    {
        /* Get Contact People List */
        $peopleDetails = $this->ContactPersonnelRepositories->getContactPersonnelDetails(
            $request = null,
            $companyId,
            $contactId
        );

        /* Get Selected people List Id */
        $selectedPeopleId = $this->EventFormEditRepositories->getSelectedPeopleOptions($contactEventFormId);

        $contactPeople = [];
        foreach ($peopleDetails as  $peopleDetailsValue) {
            $peopleResultDetails['full_name']  = $peopleDetailsValue['first_name'] . ' ' . $peopleDetailsValue['last_name'];
            $peopleResultDetails['id']         = $peopleDetailsValue['id'];
            $peopleResultDetails['first_name'] = $peopleDetailsValue['first_name'];
            $peopleResultDetails['last_name']  = $peopleDetailsValue['last_name'];
            $peopleResultDetails['title']      = $peopleDetailsValue['title'];
            array_push($contactPeople, $peopleResultDetails);
        }

        $peopleSelectedDetails['people_field_selected_id'] = implode(',', $selectedPeopleId);
        $peopleSelectedDetails['people_field_option']      = $contactPeople;

        return $peopleSelectedDetails;
    }

    public function doFormatforEditContactDetails($eventDetails, $contactEventFormId)
    {
        $eventFormDetails = [];

        foreach ($eventDetails as $eventDetailskey => $eventDetailsValue) {

            /* Array is associative */
            if (array_keys($eventDetailsValue) !== range(0, count($eventDetailsValue) - 1)) {
                /* EventForm TypeId Set */
                if ($eventDetailsValue['rfield_id'] != '' || $eventDetailsValue['cfield_type_id'] != '') {
                    if ($eventDetailsValue['cfield_type_id'] == '') {
                        $eventDetailsValue['cfield_type_id'] = config('constants.FieldType.textIdArray'); /* For Text Type Field */
                        $eventDetailsValue['custom_field_type'] = config('constants.FieldType.textArray');
                    }

                    $eventAssociativeDetails['name']                           = $eventDetailsValue['name'];
                    $eventAssociativeDetails['position']                       = $eventDetailsValue['position'];
                    $eventAssociativeDetails['points']                         = $eventDetailsValue['points'];
                    $eventAssociativeDetails['required']                       = (isset($eventDetailsValue['required'])) ? $eventDetailsValue['required'] : false;
                    $eventAssociativeDetails['custom_field_id']                = $eventDetailsValue['id'];
                    $eventAssociativeDetails['custom_field_inner_id']          = (int)$eventDetailsValue['cfield_id'];
                    $eventAssociativeDetails['custom_field_regular_id']        = (int)$eventDetailsValue['rfield_id'];
                    $eventAssociativeDetails['custom_field_type_id']           = (int)$eventDetailsValue['cfield_type_id'];

                    $typeId = $eventDetailsValue['cfield_type_id'];
                    if (
                        $typeId == config('constants.EventFieldType.select') ||
                        $typeId == config('constants.EventFieldType.radio') ||
                        $typeId == config('constants.EventFieldType.checkbox') ||
                        $typeId == config('constants.EventFieldType.multipleSelect')
                    ) {
                        $eventAssociativeDetails['custom_field_option_default_id']             = (string)$this->EventFormAddDetails->getSpecificCustomFieldEditValues($contactEventFormId, $eventDetailsValue['id'], $eventDetailsValue['cfield_type_id']);
                        $eventAssociativeDetails['value']                                      = '';
                    } else {
                        $eventAssociativeDetails['custom_field_option_default_id'] = (string)$eventDetailsValue['cfield_option_default_id'];
                        $eventAssociativeDetails['value']                          = $this->EventFormAddDetails->getSpecificCustomFieldEditValues($contactEventFormId, $eventDetailsValue['id'], $eventDetailsValue['cfield_type_id']);
                    }
                    $eventAssociativeDetails['custom_field_type']              = (isset($eventDetailsValue['custom_field_type'])) ? $eventDetailsValue['custom_field_type'] : [];
                    $eventAssociativeDetails['custom_field_option']            = (isset($eventDetailsValue['custom_field_option'])) ? $eventDetailsValue['custom_field_option'] : [];

                    array_push($eventFormDetails, $eventAssociativeDetails);
                }
            } else { /* Array is associative */
                $eventDetailsCollectValue = collect($eventDetailsValue)->first();
                if ($eventDetailsCollectValue['rfield_id'] != '' || $eventDetailsCollectValue['cfield_type_id'] != '') {
                    if ($eventDetailsCollectValue['cfield_type_id'] == '') {
                        $eventDetailsValue['cfield_type_id'] = config('constants.FieldType.textIdArray'); /* For Text Type Field */
                        $eventDetailsValue['custom_field_type'] = config('constants.FieldType.textArray');
                    }

                    $eventDetailsCollect['name']                           = $eventDetailsCollectValue['name'];
                    $eventDetailsCollect['position']                       = $eventDetailsCollectValue['position'];
                    $eventDetailsCollect['points']                         = $eventDetailsCollectValue['points'];
                    $eventDetailsCollect['required']                       = (isset($eventDetailsCollectValue['required'])) ? $eventDetailsCollectValue['required'] : false;;
                    $eventDetailsCollect['custom_field_id']                = $eventDetailsCollectValue['id'];
                    $eventDetailsCollect['custom_field_inner_id']          = (int)$eventDetailsCollectValue['cfield_id'];
                    $eventDetailsCollect['custom_field_regular_id']        = (int)$eventDetailsCollectValue['rfield_id'];
                    $eventDetailsCollect['custom_field_type_id']           = (int)$eventDetailsCollectValue['cfield_type_id'];
                    $eventDetailsCollect['custom_field_option_default_id'] = (string)$eventDetailsCollectValue['cfield_option_default_id'];

                    $typeId = $eventDetailsCollectValue['cfield_type_id'];
                    if (
                        $typeId == config('constants.EventFieldType.select') ||
                        $typeId == config('constants.EventFieldType.radio') ||
                        $typeId == config('constants.EventFieldType.checkbox') ||
                        $typeId == config('constants.EventFieldType.multipleSelect')
                    ) {
                        $eventDetailsCollect['custom_field_option_default_id']             = (string)$this->EventFormAddDetails->getSpecificCustomFieldEditValues($contactEventFormId, $eventDetailsCollectValue['id'], $eventDetailsCollectValue['cfield_type_id']);
                        $eventDetailsCollect['value']                                      = '';
                    } else {
                        $eventDetailsCollect['custom_field_option_default_id']             = (string)$eventDetailsCollectValue['cfield_option_default_id'];
                        $eventDetailsCollect['value']                          = $this->EventFormAddDetails->getSpecificCustomFieldEditValues($contactEventFormId, $eventDetailsCollectValue['id'], $eventDetailsCollectValue['cfield_type_id']);
                    }
                    $eventDetailsCollect['custom_field_type']              = (isset($eventDetailsCollectValue['custom_field_type'])) ? $eventDetailsCollectValue['custom_field_type'] : [];
                    $eventDetailsCollect['custom_field_option']            = (isset($eventDetailsCollectValue['custom_field_option'])) ? $eventDetailsCollectValue['custom_field_option'] : [];

                    array_push($eventFormDetails, $eventDetailsCollect);
                }
            }
        }

        return $eventFormDetails;
    }



    public function doFormatForContactDetails($contactView)
    {
        $city = isset($contactView['contact']->city) ? $contactView['contact']->city : '';
        $state = isset($contactView['state']->abbr) ? $contactView['state']->abbr : '';
        $zipcode = isset($contactView['contact']->zip) ? $contactView['contact']->zip : '';
        $phone = isset($contactView['contactPhone']->phone_number) ? $contactView['contactPhone']->phone_number : '';
        $phone = $this->getPhoneFormattedAttribute($phone);
        $contactViewData['contact_id'] = isset($contactView['contact']->id) ? $contactView['contact']->id : 0;
        $contactViewData['account_type'] = isset($contactView['contactType']->name) ? $contactView['contactType']->name : '';
        $contactViewData['account_type_id'] = isset($contactView['contact']->contact_type_id) ? $contactView['contact']->contact_type_id : 0;
        $contactViewData['account_number'] = isset($contactView['contact']->account) ? $contactView['contact']->account : '';
        $contactViewData['company_name'] = isset($contactView['contactCompany']->name) ? $contactView['contactCompany']->name : '';
        $contactViewData['parent_company'] = isset($contactView['contact']->title) ? $contactView['contact']->title : '';
        $contactViewData['address1'] = isset($contactView['contact']->address) ? $contactView['contact']->address : '';
        $contactViewData['address2'] =   isset($contactView['contact']->address2) ? ($contactView['contact']->address2) : '';
        $contactViewData['city'] = $city;
        $contactViewData['state'] = $state;
        $contactViewData['state_id'] = isset($contactView['contact']->state_id) ? $contactView['contact']->state_id : 0;
        $contactViewData['zipcode'] = $zipcode;
        $contactViewData['country'] = isset($contactView['country']->name) ? $contactView['country']->name : '';
        $contactViewData['country_id'] = isset($contactView['contact']->country_id) ? $contactView['contact']->country_id : 0;
        $contactViewData['company_email'] = isset($contactView['contact']->email) ? $contactView['contact']->email : '';
        $contactViewData['phone'] = $phone;
        $contactViewData['phone_type_id'] = isset($contactView['contactPhone']->phone_type_id) ? $contactView['contactPhone']->phone_type_id : 0;
        $contactViewData['country_code'] = isset($contactView['contactPhone']->country_code) ? $contactView['contactPhone']->country_code : '';
        $contactViewData['website'] = isset($contactView['contact']->website) ? $contactView['contact']->website : '';

        return $contactViewData;
    }

    public function getPhoneFormattedAttribute($phone)
    {
        if ($phone) {
            $phone = preg_replace("/[^0-9]/", "", $phone); //Remove all non-numers
            return substr($phone, 0, 3) . "-" . substr($phone, 3, 3) . "-" . substr($phone, 6);
        }
        return '';
    }

    public function attributeTransformation($eventDetails, $contactId)
    {
        /* Pagination Format Changed */
        $eventDetails = collect($eventDetails['data'])->toArray();

        $eventFormatDetails = [];
        foreach ($eventDetails as $eventKey => $eventDetailsvalue) {
            if ($eventDetailsvalue['event_form']['id']) {

                $submittedDate = '';
                if ($eventDetailsvalue['created'] == null) {
                    $submittedFormatDate = '';
                    $submittedDate       = '';
                } else {
                    $day      = Carbon::parse($eventDetailsvalue['created'])->format('l');
                    $month    = Carbon::parse($eventDetailsvalue['created'])->format('M');
                    $dateInt  = Carbon::parse($eventDetailsvalue['created'])->format('d');
                    $year     = Carbon::parse($eventDetailsvalue['created'])->format('Y');
                    $fullTime = Carbon::parse($eventDetailsvalue['created'])->format('H:i:s A');

                    $submittedFormatDate = $month . '. ' . $dateInt . ',' . $year . ',' . $fullTime;
                    $submittedDate = Carbon::parse($eventDetailsvalue['created'])->format('d-m-Y');
                }

                /* CompanyDetails */
                $companyDetails = $this->EventPreviousListRepositories->getCompanyDetails($eventDetailsvalue['contact_company']['id']);

                $eventFormat['Event_id']          = $eventDetailsvalue['id'];
                $eventFormat['contact_id']        = $eventDetailsvalue['contact_id'];
                $eventFormat['company_name']      = $eventDetailsvalue['contact_company']['company_name'];
                $eventFormat['company_address']   = ($companyDetails->address  == null) ? '' : $companyDetails->address;
                $eventFormat['company_state']     = ($companyDetails->state_id == null) ? '' : $companyDetails->state_id;
                $eventFormat['company_city']      = ($companyDetails->city     == null) ? '' : $companyDetails->city;
                $eventFormat['company_zip']       = ($companyDetails->zip      == null) ? '' : $companyDetails->zip;
                $eventFormat['event_form_id']     = $eventDetailsvalue['event_form_id'];
                $eventFormat['points']            = $eventDetailsvalue['points'];
                $eventFormat['created']           = $eventDetailsvalue['created'];
                $eventFormat['event_form_id']     = $eventDetailsvalue['event_form']['id'];
                $eventFormat['event_form_name']   = $eventDetailsvalue['event_form']['name'];
                $eventFormat['event_position']    = $eventDetailsvalue['event_form']['position'];
                $eventFormat['event_points']      = $eventDetailsvalue['event_form']['points'];
                $eventFormat['submitted_by']      = $this->EventPreviousListRepositories->getSubmittedByDetails($contactId);
                $eventFormat['submitted_on']      = $submittedFormatDate;
                $eventFormat['submitted_on_date'] = $submittedDate;
                if (count($eventDetailsvalue['contact_personnal']) > 0) {
                    $eventFormat['People']    = $this->attributePeopleTransformation($eventDetailsvalue['contact_personnal']);
                } else {
                    $eventFormat['People']    = '';
                }

                array_push($eventFormatDetails, $eventFormat);
            }
        }
        return $eventFormatDetails;
    }

    public function attributePeopleTransformation($contactEventFormPeopledetails)
    {
        $fullName = [];
        foreach ($contactEventFormPeopledetails as $peoplekey => $peoplevalue) {
            $combineName = $peoplevalue['first_name'] . ' ' . $peoplevalue['last_name'];
            array_push($fullName, $combineName);
        }
        return implode(',', $fullName);
    }


    public function doSuccessFormat($responseResult)
    {
        $result['status']   = true;
        $result['message']  = 'Success';
        $result['response'] = $responseResult;
        return $result;
    }

    public function doErrorFormat($responseResult)
    {
        Helper::logError($responseResult->getMessage(), 401, $requestdetails = null, config('constants.ServiceName.Event'));
        $result['status']   = false;
        $result['message']  = $responseResult->getMessage();
        $result['response'] = '';
        return $result;
    }
}
