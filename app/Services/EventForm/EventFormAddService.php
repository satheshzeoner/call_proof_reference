<?php

namespace App\Services\EventForm;

use App\Helpers\Helper;
use Illuminate\Support\Facades\Log;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

/* Models */
use App\Models\Contact\Contact;
use App\Models\Eventform\Eventform;
use App\Models\Cfield\Cfield;

/* Repository */
use App\Repositories\EventForm\EventFormPreviousListRepositories;
use App\Repositories\EventForm\EventFormAddRepositories;

/* FieldType Details */
use App\Services\EventForm\EventFormFieldTypeService;

/* EventService */
use App\Services\Event\EventAddService;


class EventFormAddService
{
    public function __construct()
    {
        $this->EventPreviousListRepositories = new EventFormPreviousListRepositories();
        $this->EventFormAddRepositories      = new EventFormAddRepositories();
        $this->EventFieldTypeService         = new EventFormFieldTypeService();

        /* Initialze For LogSytem */
        $this->companyId                     = '';
        $this->userId                        = '';
        $this->contactId                     = '';
        $this->EventFormId                   = '';
    }

    /* Event ADD Form Service */
    public function eventFormAddProcess($userId, $companyId, $contactId, $eventFormId, $request)
    {
        $requestDetails = $request->all();

        /* Requested POST as Converted to Array */
        $formDetails     = collect(json_decode($requestDetails['form_field']));
        $peopleDetails   = collect(json_decode($requestDetails['people']));
        if (isset($requestDetails['schedule_task'])) {
            $scheduleDetails = collect(json_decode($requestDetails['schedule_task']))->first();
        } else {
            $scheduleDetails = '';
        }

        /* Nested Object to Array Convertion */
        $formDetails      = json_decode(json_encode($formDetails), true);
        $peopleDetails    = json_decode(json_encode($peopleDetails), true);

        $this->companyId   = $companyId;
        $this->userId      = $userId;
        $this->contactId   = $contactId;
        $this->EventFormId = $eventFormId;

        try {

            /* Validations */
            $validationResult = $this->doEventFormAddValidations($userId, $companyId, $contactId, $eventFormId, $formDetails);
            if ($validationResult['status'] == false) {
                return $this->doErrorFormat($validationResult['message']);
            }

            /* doStore EventForm Insertion */

            DB::beginTransaction();

            $storeResult = $this->doStoreEventFormProcessing($userId, $companyId, $contactId, $eventFormId, $formDetails, $peopleDetails, $scheduleDetails, $request);

            DB::commit();

            if ($storeResult['status'] == false) {

                DB::rollback();
                return $this->doErrorFormat($storeResult['message']);
            }

            return $this->doSuccessFormat(config('constants.Success.EventForm'));

            /* doStore EventForm Insertion END */
        } catch (Exception $e) {
            return $this->doErrorFormat($e->getMessage());
        }
    }

    public function doStoreEventFormProcessing($userId, $companyId, $contactId, $eventFormId, $formDetails, $peopleDetails, $scheduleDetails, $request)
    {
        /* initial Status  */
        $eventFormResult['status'] = true;
        $eventFormResult['message'] = '';
        /* Contact EventForm Store Process */
        $contactEventFormId = $this->doStoreContactEventform($userId, $companyId, $contactId, $eventFormId);

        /* MultiSelect People contacteventformpersonnel Store  */
        $contactEventFormPersonnelId = $this->doStorePeople($contactEventFormId, $peopleDetails);

        /* customField Values Store */
        $customFieldResult = $this->storeCustomFieldValues($userId, $contactId, $companyId, $contactEventFormId, $formDetails, $request);
        if ($customFieldResult['status'] == false) {
            return $customFieldResult;
        }

        /* ByCheked { Schedule Task Now } Store process {followup} */
        if (!empty($scheduleDetails)) {
            $followUpResult = $this->storeSchedultTask($userId, $contactId, $scheduleDetails, $request);
            if ($followUpResult['status'] == false) {
                return $followUpResult;
            }
        }

        /* Event Store Process */
        $this->doEventAdd($userId, $companyId, $contactId, $contactEventFormId, config('constants.Event_name.event_form'));

        return $eventFormResult;
    }

    public function doStoreContactEventform($userId, $companyId, $contactId, $eventFormId)
    {
        /* Get Cordinnation */
        $contactDetails = $this->EventFormAddRepositories->getContactsCoordination($contactId);

        $contactEventFormDetails['company_id']    = $companyId;
        $contactEventFormDetails['user_id']       = $userId;
        $contactEventFormDetails['contact_id']    = $contactId;
        $contactEventFormDetails['event_form_id'] = $eventFormId;
        $contactEventFormDetails['latitude']      = ($contactDetails->latitude) ? $contactDetails->latitude  : null;
        $contactEventFormDetails['longitude']     = ($contactDetails->longitude) ? $contactDetails->longitude : null;
        $contactEventFormDetails['points']        = 0;
        $contactEventFormDetails['created']       = now();

        $contactEventFormId = $this->EventFormAddRepositories->storeContactEventForm($contactEventFormDetails);

        /* Log For ContactEventCreation */
        $this->logEvents('ContactEventForm Created id =' . $contactEventFormId);

        return $contactEventFormId;
    }

    /* MultiSelect People Store in ContactEvent Personal Table */
    public function doStorePeople($contactEventFormId, $peopleDetails)
    {
        if ($peopleDetails == null) {
            return true;
        }

        $contactPersonalDetails = [];
        foreach ($peopleDetails as $Personalkey => $peopleDetailsValue) {
            $contactPersonalDetails['contacteventform_id'] = $contactEventFormId;
            $contactPersonalDetails['personnel_id']        = $peopleDetailsValue['id'];
            $contactPersonalId = $this->EventFormAddRepositories->storeContactEventFormPersonnel($contactPersonalDetails);
            $this->logEvents(config('constants.Success.ContactEventFormPersonal') . ' = ' . $contactPersonalId);
        }

        return true;
    }

    /* Store Dynamic CustomField with Value */
    public function storeCustomFieldValues($userId, $contactId, $companyId, $contactEventFormId, $formDetails, $request)
    {
        foreach ($formDetails as $formDetailsValue) {

            /* TextField */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.text')) {
                $fieldResult = $this->EventFieldTypeService->textField($formDetailsValue);
            }
            /* SelectField */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.select')) {
                $fieldResult = $this->EventFieldTypeService->selectField($formDetailsValue);
            }
            /* RadioField */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.radio')) {
                $fieldResult = $this->EventFieldTypeService->RadioField($formDetailsValue);
            }
            /* checkbox */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.checkbox')) {
                $fieldResult = $this->EventFieldTypeService->checkboxField($formDetailsValue);
            }
            /* textarea */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.textarea')) {
                $fieldResult = $this->EventFieldTypeService->textareaField($formDetailsValue);
            }
            /* date */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.date')) {
                $fieldResult = $this->EventFieldTypeService->dateField($formDetailsValue);
            }
            /* time */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.time')) {
                $fieldResult = $this->EventFieldTypeService->timeField($formDetailsValue);
            }
            /* datetime */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.datetime')) {
                $fieldResult = $this->EventFieldTypeService->datetimeField($formDetailsValue);
            }
            /* integer */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.integer')) {
                $fieldResult = $this->EventFieldTypeService->integerField($formDetailsValue);
            }
            /* decimal */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.decimal')) {
                $fieldResult = $this->EventFieldTypeService->decimalField($formDetailsValue);
            }
            /* auto_integer */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.autoInteger')) {
                $fieldResult = $this->EventFieldTypeService->autoIntegerField($formDetailsValue);
            }
            /* image */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.image')) {
                $fieldResult = $this->EventFieldTypeService->imageField($formDetailsValue, $request);

                /* File Path Store in ContactImage */
                $contactImageDetails['user_id']               = $userId;
                $contactImageDetails['company_id']            = $companyId;
                $contactImageDetails['contact_id']            = $contactId;
                $contactImageDetails['cfield_id']             = $formDetailsValue['custom_field_id'];
                $contactImageDetails['contact_event_form_id'] = $contactEventFormId;
                $contactImageDetails['image']                 = $fieldResult['custom_field_value'];
                $contactImageDetails['created']               = now();

                $contactImage = $this->EventFormAddRepositories->storeCustomFieldFilePath($contactImageDetails);
            }
            /* multiple_select */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.multipleSelect')) {
                $fieldResult = $this->EventFieldTypeService->multipleSelectField($formDetailsValue);
            }

            /* Store in customField Details */
            if ($fieldResult['status'] == true && $formDetailsValue['custom_field_type_id'] != config('constants.EventFieldType.multipleSelect')) {

                $customField['cfield_id'] = $formDetailsValue['custom_field_id'];
                $customField['entity_id'] = $contactEventFormId;
                $customField['cf_value']  = $fieldResult['custom_field_value'];
                $customField['updated']   = now();

                /* store Custom Field Values */
                $customFieldValueId = $this->EventFormAddRepositories->storeCustomFieldValue($customField);

                /* Log Creation */
                $this->logEvents(config('constants.Success.CustomFieldValue') . ':' . $customFieldValueId);
            }

            /* Multi Options Value Insertion Process */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.multipleSelect')) {
                $multiValue = explode(',', $fieldResult['custom_field_value']);
                try {

                    foreach ($multiValue as  $multiValueValue) {
                        $customMultiField['cfield_id'] = $formDetailsValue['custom_field_id'];
                        $customMultiField['entity_id'] = $contactEventFormId;
                        $customMultiField['option_id'] = $multiValueValue;
                        $customMultiField['updated']   = now();

                        /* store CustomFieldValues */
                        $customMultiValueId = $this->EventFormAddRepositories->storeMultiValueOption($customMultiField);

                        /* Log Creation */
                        $this->logEvents(config('constants.Success.MultiFieldOption') . ':' . $customMultiValueId);
                    }
                } catch (Exception $e) {
                    /* End of Section */
                    if ($fieldResult['status'] == false) {
                        return $fieldResult;
                        break;
                    }
                }
            }

            /* End of Section */
            if ($fieldResult['status'] == false) {
                return $fieldResult;
                break;
            }
        }

        $finalCustomFieldResult['status'] = true;
        $finalCustomFieldResult['message'] = '';

        return $finalCustomFieldResult;
    }

    /* ScheduleTask Store Process */
    public function storeSchedultTask($userId, $contactId, $scheduleDetails)
    {
        /* Initial Response Set */
        $scheduleResult['status']  = true;
        $scheduleResult['message'] = '';

        /* check the Schedule Task request Details */
        if ($scheduleDetails == '') {
            return $scheduleResult;
        }

        /* DateTime Formatting */
        $dateTimeDetails = $scheduleDetails->task_date . ' ' . $scheduleDetails->task_hour . ':' . $scheduleDetails->task_min . ':00 ' . $scheduleDetails->task_meridiem;

        /* Strcturing DateTime Format */
        $dateTimeDetails = Carbon::parse($dateTimeDetails)->format('Y-m-d H:i:s');

        /* TaskAssignment as User Assigned for FollowUp */
        if ($scheduleDetails->task_assignment == '') {
            $scheduleResult['status']  = false;
            $scheduleResult['message'] = config('constants.Errors.TaskAssignmentId');
        }

        $taskAssignmentUserId   = explode(',', $scheduleDetails->task_assignment);

        /* People List Check */
        if ($scheduleDetails->task_people == '') {
            $taskAssignmentPeopleId = '';
        } else {
            $taskAssignmentPeopleId = explode(',', $scheduleDetails->task_people);
        }

        foreach ($taskAssignmentUserId as $taskAssignmentkey => $taskAssignmentvalue) {

            $followupDetails['user_id']         = $taskAssignmentvalue;
            $followupDetails['contact_id']      = $contactId;
            $followupDetails['start']           = $dateTimeDetails;
            $followupDetails['duration']        = $scheduleDetails->duration;
            $followupDetails['send_google_cal'] = $scheduleDetails->send_google_cal;
            $followupDetails['created']         = now();

            try {
                $followupId = $this->EventFormAddRepositories->storeFollowupValue($followupDetails);
                $this->logEvents(config('constants.Success.Followup') . ' Created Id :' . $followupId);
            } catch (Exception $e) {
                $this->logEventsError($e->getMessage());
                $scheduleResult['status']  = false;
                $scheduleResult['message'] = config('constants.Errors.AddEventForm');
                return $scheduleResult;
            }

            if ($taskAssignmentPeopleId != '') {
                foreach ($taskAssignmentPeopleId as $key => $taskAssignmentPeoplevalue) {
                    $followupPersonalDetails['followup_id']  = $followupId;
                    $followupPersonalDetails['personnel_id'] = $taskAssignmentPeoplevalue;

                    try {
                        $followupPersonalId = $this->EventFormAddRepositories->storeFollowupPersonalValue($followupPersonalDetails);
                        $this->logEvents(config('constants.Success.FollowupPersonnel') . ' Created Id :' . $followupPersonalId);
                    } catch (Exception $e) {
                        $this->logEventsError($e->getMessage());
                        $scheduleResult['status']  = false;
                        $scheduleResult['message'] = config('constants.Errors.AddEventForm');
                        return $scheduleResult;
                    }
                }
            }
        }
        return $scheduleResult;
    }


    /* Each Field Validation Process */
    public function doEventFormAddValidations($userId, $companyId, $contactId, $eventFormId, $formDetails)
    {

        $validationResult['status'] = true;
        $validationResult['message'] = '';

        /* ContactId Validation */
        if (Contact::whereCompany_id($companyId)->whereId($contactId)->exists() == false) {
            $validationResult['status']  = false;
            $validationResult['message'] = config('constants.Errors.ContactId');
            return $validationResult;
        }
        /* EventId Validation */
        if (Eventform::whereCompany_id($companyId)->whereId($eventFormId)->exists() == false) {
            $validationResult['status']  = false;
            $validationResult['message'] = config('constants.Errors.EventFormId');
            return $validationResult;
        }

        /* Field Validation */
        foreach ($formDetails as $formDetailskey => $formDetailsvalue) {

            /* Required Validations */
            if ($formDetailsvalue['required'] == true) {
                if ($formDetailsvalue['value'] == '' && $formDetailsvalue['custom_field_option_default_id'] == '') {
                    $validationResult['status']  = false;
                    $validationResult['message'] = $formDetailsvalue['label'] . ' Required';
                    return $validationResult;
                }
            }

            /* Custom Field Type Validation */
            if ($formDetailsvalue['custom_field_type_id'] != null && $formDetailsvalue['custom_field_type_id'] != 1) {

                if (Cfield::whereId($formDetailsvalue['custom_field_id'])->whereCfield_type_id($formDetailsvalue['custom_field_type_id'])->exists() == false) {
                    $validationResult['status']  = false;
                    $validationResult['message'] = $formDetailsvalue['label'] . ' custom Field id/Field Type Id Invalid';
                    return $validationResult;
                }
            }
        }
        return $validationResult;
    }

    public function doSuccessFormat($responseResult)
    {
        $result['status']   = true;
        $result['message']  = 'Success';
        $result['response'] = $responseResult;
        return $result;
    }

    public function doErrorFormat($responseResult)
    {
        Helper::logError($responseResult, 401, $requestdetails = null, config('constants.ServiceName.Event'));
        $result['status']   = false;
        $result['message']  = $responseResult;
        $result['response'] = '';
        return $result;
    }

    public function logEvents($messageDetails = null)
    {
        $eventAddLog['User_id']      = $this->userId;
        $eventAddLog['Company_id']   = $this->companyId;
        $eventAddLog['Contact_id']   = $this->contactId;
        $eventAddLog['EventForm_id'] = $this->EventFormId;
        $eventAddLog['Message']      = $messageDetails;

        Log::notice($eventAddLog);

        return true;
    }

    public function logEventsError($messageDetails = null)
    {
        $eventAddLog['User_id']      = $this->userId;
        $eventAddLog['Company_id']   = $this->companyId;
        $eventAddLog['Contact_id']   = $this->contactId;
        $eventAddLog['EventForm_id'] = $this->EventFormId;
        $eventAddLog['Message']      = $messageDetails;

        Log::error($eventAddLog);

        return true;
    }

    /* ===============================EVENT========================== */
    public function doEventAdd($eventUserId, $eventCompanyId, $eventContactId, $eventContactEventFormId, $eventName)
    {
        /* Event Creation */
        $eventAddService        = new EventAddService();
        $eventAddService->addEvent(
            $eventName,
            $eventCompanyId,
            $eventUserId,
            $eventContactId,
            $eventOtherUserId        = null,
            $eventCallId             = null,
            $eventStartTime          = null,
            $eventDuration           = null,
            $eventBadgeId            = null,
            $eventGooglePlace        = null,
            $eventTwilioCallId       = null,
            $eventContactEventFormId,
            $eventAppointmentId      = null,
            $eventFollowupId         = null,
            $eventMessage            = null
        );

        return true;
    }
    /* ===============================EVENT-END====================== */
}
