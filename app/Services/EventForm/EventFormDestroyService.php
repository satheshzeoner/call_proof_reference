<?php

namespace App\Services\EventForm;

use App\Helpers\Helper;
use Illuminate\Support\Facades\Log;
use Exception;
use Illuminate\Support\Facades\DB;

/* Models */
use App\Models\Contact\ContactEventform;

/* Repositories */
use App\Repositories\EventForm\EventFormDestroyRepositories;

class EventFormDestroyService
{
    public function __construct()
    {
        $this->EventFormDestroyRepositories  = new EventFormDestroyRepositories();

        /* Initialze For LogSytem */
        $this->companyId                     = '';
        $this->userId                        = '';
        $this->contactEventFormId            = '';
    }

    /* ContactEventForm Remove Service */
    public function contactEventFormRemoveProcess($userId, $companyId, $contactEventFormId)
    {
        $this->companyId   = $companyId;
        $this->userId      = $userId;
        $this->EventFormId = $contactEventFormId;

        /* Validations Check is ContactEventFormId is Present */
        if (ContactEventform::whereId($contactEventFormId)->exists() == false) {
            return $this->doErrorFormat(config('constants.Errors.ContactEventFormId'));
        }

        /* Remove Process */
        $contactEventFormRemoveResult = $this->EventFormDestroyRepositories->destroyContactEventForm($contactEventFormId);

        if ($contactEventFormRemoveResult == true) {
            $this->logEvents(config('constants.Success.ContactEventForm'));
            return $this->doSuccessFormat(config('constants.Success.ContactEventForm'));
        } else {
            $this->logEventsError(config('constants.Errors.ContactEventForm'));
            return $this->logEventsError(config('constants.Errors.ContactEventForm'));
        }
    }

    public function doSuccessFormat($responseResult)
    {
        $result['status']   = true;
        $result['message']  = 'Success';
        $result['response'] = $responseResult;
        return $result;
    }

    public function doErrorFormat($responseResult)
    {
        Helper::logError($responseResult, 401, $requestdetails = null, config('constants.ServiceName.Event'));
        $result['status']   = false;
        $result['message']  = $responseResult;
        $result['response'] = '';
        return $result;
    }

    public function logEvents($messageDetails = null)
    {
        $eventRemoveLog['User_id']      = $this->userId;
        $eventRemoveLog['Company_id']   = $this->companyId;
        $eventRemoveLog['EventForm_id'] = $this->EventFormId;
        $eventRemoveLog['Message']      = $messageDetails;

        Log::notice($eventRemoveLog);

        return true;
    }

    public function logEventsError($messageDetails = null)
    {
        $eventRemoveLog['User_id']      = $this->userId;
        $eventRemoveLog['Company_id']   = $this->companyId;
        $eventRemoveLog['EventForm_id'] = $this->EventFormId;
        $eventRemoveLog['Message']      = $messageDetails;

        Log::error($eventRemoveLog);

        return true;
    }
}
