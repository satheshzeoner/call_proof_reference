<?php

namespace App\Services\EventForm;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/* Model */

use App\Models\Cfield\CfieldOption;

class EventFormFieldTypeService
{
    /* Text */
    public function textField($fieldDetails)
    {
        $resultField['status']             = true;
        $resultField['message']            = '';
        $resultField['custom_field_value'] = $fieldDetails['value'];

        if ($fieldDetails['value'] == '') {
            $resultField['status']  = false;
            $resultField['message'] = config('constants.FieldType.text');
            return $resultField;
        }
        return $resultField;
    }

    /* Select */
    public function selectField($fieldDetails)
    {
        $resultField['status']             = true;
        $resultField['message']            = '';
        $resultField['custom_field_value'] = $fieldDetails['custom_field_option_default_id'];
        $customFieldOption = CfieldOption::whereCfield_id($fieldDetails['custom_field_id'])
            ->whereid($fieldDetails['custom_field_option_default_id'])
            ->exists();

        if ($customFieldOption == false) {
            $resultField['status']  = false;
            $resultField['message'] = config('constants.FieldType.select');
            return $resultField;
        }

        return $resultField;
    }

    /* Radio */
    public function RadioField($fieldDetails)
    {
        $resultField['status']             = true;
        $resultField['message']            = '';
        $resultField['custom_field_value'] = $fieldDetails['custom_field_option_default_id'];
        $customFieldOption = CfieldOption::whereCfield_id($fieldDetails['custom_field_id'])
            ->whereid($fieldDetails['custom_field_option_default_id'])
            ->exists();

        if ($customFieldOption == false) {
            $resultField['status']  = false;
            $resultField['message'] = config('constants.FieldType.radio');
            return $resultField;
        }

        return $resultField;
    }


    /* checkbox */
    public function checkboxField($fieldDetails)
    {
        $resultField['status']             = true;
        $resultField['message']            = '';
        $resultField['custom_field_value'] = $fieldDetails['value'];

        if ($fieldDetails['value'] == '') {
            $resultField['status']  = false;
            $resultField['message'] = config('constants.FieldType.checkbox');
            return $resultField;
        }
        return $resultField;
    }

    /* textarea */
    public function textareaField($fieldDetails)
    {
        $resultField['status']             = true;
        $resultField['message']            = '';
        $resultField['custom_field_value'] = $fieldDetails['value'];

        if ($fieldDetails['value'] == '') {
            $resultField['status']  = false;
            $resultField['message'] = config('constants.FieldType.textarea');
            return $resultField;
        }
        return $resultField;
    }

    /* date */
    public function dateField($fieldDetails)
    {
        $resultField['status']             = true;
        $resultField['message']            = '';
        $resultField['custom_field_value'] = $fieldDetails['value'];

        if ($fieldDetails['value'] == '') {
            $resultField['status']  = false;
            $resultField['message'] = config('constants.FieldType.date');
            return $resultField;
        }
        return $resultField;
    }

    /* time */
    public function timeField($fieldDetails)
    {
        $resultField['status']             = true;
        $resultField['message']            = '';
        $resultField['custom_field_value'] = $fieldDetails['value'];

        if ($fieldDetails['value'] == '') {
            $resultField['status']  = false;
            $resultField['message'] = config('constants.FieldType.time');
            return $resultField;
        }
        return $resultField;
    }

    /* datetime */
    public function datetimeField($fieldDetails)
    {
        $resultField['status']             = true;
        $resultField['message']            = '';
        $resultField['custom_field_value'] = $fieldDetails['value'];

        if ($fieldDetails['value'] == '') {
            $resultField['status']  = false;
            $resultField['message'] = config('constants.FieldType.datetime');
            return $resultField;
        }
        return $resultField;
    }

    /* integer */
    public function integerField($fieldDetails)
    {
        $resultField['status']             = true;
        $resultField['message']            = '';
        $resultField['custom_field_value'] = $fieldDetails['value'];

        if ($fieldDetails['value'] == '') {
            $resultField['status']  = false;
            $resultField['message'] = config('constants.FieldType.integer');
            return $resultField;
        }
        return $resultField;
    }

    /* decimal */
    public function decimalField($fieldDetails)
    {
        $resultField['status']             = true;
        $resultField['message']            = '';
        $resultField['custom_field_value'] = $fieldDetails['value'];

        if ($fieldDetails['value'] == '') {
            $resultField['status']  = false;
            $resultField['message'] = config('constants.FieldType.decimal');
            return $resultField;
        }
        return $resultField;
    }

    /* auto_integer */
    public function autoIntegerField($fieldDetails)
    {
        $resultField['status']             = true;
        $resultField['message']            = '';
        $resultField['custom_field_value'] = $fieldDetails['value'];

        if ($fieldDetails['value'] == '') {
            $resultField['status']  = false;
            $resultField['message'] = config('constants.FieldType.autoInteger');
            return $resultField;
        }
        return $resultField;
    }

    /* image */
    public function imageField($fieldDetails, $request)
    {

        /* Image Validation */
        if (!$request->hasFile('form_field_file_' . $fieldDetails['custom_field_id'])) {
            $resultField['status']             = false;
            $resultField['message']            = config('constants.FieldType.image');
            $resultField['custom_field_value'] = $fieldDetails['value'];

            return $resultField;
        }

        $fileFullName = $request->file('form_field_file_' . $fieldDetails['custom_field_id'])->getClientOriginalName();

        $filename  = pathinfo($fileFullName, PATHINFO_FILENAME);
        $extension = pathinfo($fileFullName, PATHINFO_EXTENSION);

        $fileFormatName = $filename . "_" . now()->format('YmdHis') . "." . $extension;

        $path     = $request->file('form_field_file_' . $fieldDetails['custom_field_id'])->move(public_path("storage/contactEventFormImage"), $fileFormatName);

        $fieldDetails['value'] = url('storage/contactEventFormImage', $fileFormatName);

        $resultField['status']             = true;
        $resultField['message']            = '';
        $resultField['custom_field_value'] = $fieldDetails['value'];

        return $resultField;
    }

    /* multiple_select */
    public function multipleSelectField($fieldDetails)
    {
        $resultField['status']             = true;
        $resultField['message']            = '';
        $resultField['custom_field_value'] = $fieldDetails['custom_field_option_default_id'];

        if ($fieldDetails['custom_field_option_default_id'] == '') {
            $resultField['status']  = false;
            $resultField['message'] = config('constants.FieldType.multipleSelect');
            return $resultField;
        }
        return $resultField;
    }
}
