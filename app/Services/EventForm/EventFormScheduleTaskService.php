<?php

namespace App\Services\EventForm;

use App\Helpers\Helper;
use Illuminate\Support\Facades\Log;
use Exception;


/* Repository */
use App\Repositories\EventForm\EventFormScheduleTaskRepositories;

class EventFormScheduleTaskService
{
    public function __construct()
    {
        $this->EventFormScheduleTask = new EventFormScheduleTaskRepositories();
    }

    public function getEventScheduleTaskDetails($userId, $companyId)
    {
        try {

            /* Task Assignment User List */
            $taskAssignmentList = $this->EventFormScheduleTask->getTaskAssignment($companyId);

            $eventScheduleDetails['task_assignment']         = $this->doFormatTaskAssignment($taskAssignmentList);
            $eventScheduleDetails['task_action']             = $this->EventFormScheduleTask->getTaskActionDetails($companyId);
            $eventScheduleDetails['task_date']               = $this->dateDetails();
            $eventScheduleDetails['task_time']               = $this->timeDataSet();
            $eventScheduleDetails['task_duration']           = config('constants.Scheduletime.duration');
            $eventScheduleDetails['add_google_calender']     = true;

            return $this->doSuccessFormat($eventScheduleDetails);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    /* Time Spliting DataSet */
    public function timeDataSet()
    {
        $time['default_id'] = [
            'hour' => "08",
            'minutes' => "00",
            'meridiem' => 'AM',
        ];
        $time['hours']    = config('constants.Scheduletime.hours');
        $time['minutes']  = config('constants.Scheduletime.minutes');
        $time['meridiem'] = config('constants.Scheduletime.meridiem');

        return $time;
    }

    /* Date Spliting Dataset */
    public function dateDetails()
    {

        $dateResult = [
            array('name' => config('constants.Scheduletime.current_date'), 'value' => now()->format('d-m-Y'), 'default' => true),
            array('name' => config('constants.Scheduletime.tomorrow_date'), 'value' => now()->tomorrow()->format('d-m-Y'), 'default' => false),
            array('name' => config('constants.Scheduletime.one_week_date'), 'value' => now()->addDays(7)->format('d-m-Y'), 'default' => false),
            array('name' => config('constants.Scheduletime.one_month_date'), 'value' => now()->addMonths(1)->format('d-m-Y'), 'default' => false),
            array('name' => config('constants.Scheduletime.three_month_date'), 'value' => now()->addMonths(3)->format('d-m-Y'), 'default' => false),
            array('name' => config('constants.Scheduletime.six_month_date'), 'value' => now()->addMonths(6)->format('d-m-Y'), 'default' => false),
            array('name' => config('constants.Scheduletime.one_year_date'), 'value' => now()->addYears(1)->format('d-m-Y'), 'default' => false)
        ];

        return $dateResult;
    }

    /* doFormatting for TaskList Assignment */
    public function doFormatTaskAssignment($taskAssignmentDetails)
    {
        $resultTaskAssignment = [];
        foreach ($taskAssignmentDetails as $taskAssignkey => $taskAssignmentDetailsvalue) {
            $firstName = $taskAssignmentDetailsvalue['user_auth']['first_name'];
            $lastName  = $taskAssignmentDetailsvalue['user_auth']['last_name'];
            $resultList['user_id'] = $taskAssignmentDetailsvalue['user_auth']['id'];
            $resultList['name'] = $firstName . ' ' . $lastName;
            array_push($resultTaskAssignment, $resultList);
        }
        return $resultTaskAssignment;
    }

    public function doSuccessFormat($responseResult)
    {
        $result['status']   = true;
        $result['message']  = 'Success';
        $result['response'] = $responseResult;
        return $result;
    }

    public function doErrorFormat($responseResult)
    {
        Helper::logError($responseResult->getMessage(), 401, $requestdetails = null, config('constants.ServiceName.Event'));
        $result['status']   = false;
        $result['message']  = $responseResult->getMessage();
        $result['response'] = '';
        return $result;
    }
}
