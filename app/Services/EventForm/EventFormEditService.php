<?php

namespace App\Services\EventForm;

use App\Helpers\Helper;
use Illuminate\Support\Facades\Log;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

/* Models */
use App\Models\Contact\Contact;
use App\Models\Eventform\Eventform;
use App\Models\Cfield\Cfield;

/* Repository */
use App\Repositories\EventForm\EventFormPreviousListRepositories;
use App\Repositories\EventForm\EventFormAddRepositories;
use App\Repositories\EventForm\EventFormEditRepositories;

/* FieldType Details */
use App\Services\EventForm\EventFormFieldTypeService;


class EventFormEditService
{
    public function __construct()
    {
        $this->EventPreviousListRepositories = new EventFormPreviousListRepositories();
        $this->EventFormAddRepositories      = new EventFormAddRepositories();
        $this->EventFieldTypeService         = new EventFormFieldTypeService();
        $this->EventFormEditRepositories     = new EventFormEditRepositories();

        /* Initialze For LogSytem */
        $this->companyId                     = '';
        $this->userId                        = '';
        $this->contactId                     = '';
        $this->EventFormId                   = '';
    }



    /* Event EDIT Form Service */
    public function eventFormUpdateProcess($userId, $companyId, $contactId, $EventFormId, $contactEventFormId, $request)
    {
        $requestDetails = $request->all();

        /* Requested POST as Converted to Array */
        $formDetails     = collect(json_decode($requestDetails['form_field']));
        $peopleDetails   = collect(json_decode($requestDetails['people']));
        if (isset($requestDetails['schedule_task'])) {
            $scheduleDetails = collect(json_decode($requestDetails['schedule_task']))->first();
        } else {
            $scheduleDetails = '';
        }

        /* Nested Object to Array Convertion */
        $formDetails      = json_decode(json_encode($formDetails), true);
        $peopleDetails    = json_decode(json_encode($peopleDetails), true);

        $this->companyId   = $companyId;
        $this->userId      = $userId;
        $this->contactId   = $contactId;
        $this->EventFormId = $EventFormId;

        try {

            /* Validations */
            $validationResult = $this->doEventFormEditValidations($userId, $companyId, $contactId, $EventFormId, $formDetails);
            if ($validationResult['status'] == false) {
                return $this->doErrorFormat($validationResult['message']);
            }

            /* doStore EventForm Updation */

            DB::beginTransaction();

            $storeResult = $this->doUpdateEventFormProcessing($userId, $companyId, $contactId, $EventFormId, $formDetails, $contactEventFormId, $peopleDetails, $scheduleDetails, $request);

            DB::commit();

            if ($storeResult['status'] == false) {

                DB::rollback();
                return $this->doErrorFormat($storeResult['message']);
            }

            return $this->doSuccessFormat(config('constants.Success.EditEventForm'));

            /* doStore EventForm Insertion END */
        } catch (Exception $e) {
            return $this->doErrorFormat($e->getMessage());
        }
    }

    public function doUpdateEventFormProcessing($userId, $companyId, $contactId, $eventFormId, $formDetails, $contactEventFormId, $peopleDetails, $scheduleDetails, $request)
    {
        /* initial Status  */
        $eventFormResult['status'] = true;
        $eventFormResult['message'] = '';

        /* MultiSelect People contacteventformpersonnel Store  */
        $contactEventFormPersonnelId = $this->doUpdatePeople($contactEventFormId, $peopleDetails);

        /* customField Values Store */
        $customFieldResult = $this->updateCustomFieldValues($userId, $contactId, $companyId, $contactEventFormId, $formDetails, $request);
        if ($customFieldResult['status'] == false) {
            return $customFieldResult;
        }

        return $eventFormResult;
    }

    public function doStoreContactEventform($userId, $companyId, $contactId, $eventFormId)
    {
        /* Get Cordinnation */
        $contactDetails = $this->EventFormAddRepositories->getContactsCoordination($contactId);

        $contactEventFormDetails['company_id']    = $companyId;
        $contactEventFormDetails['user_id']       = $userId;
        $contactEventFormDetails['contact_id']    = $contactId;
        $contactEventFormDetails['event_form_id'] = $eventFormId;
        $contactEventFormDetails['latitude']      = ($contactDetails->latitude) ? $contactDetails->latitude  : null;
        $contactEventFormDetails['longitude']     = ($contactDetails->longitude) ? $contactDetails->longitude : null;
        $contactEventFormDetails['points']        = 0;
        $contactEventFormDetails['created']       = now();

        $contactEventFormId = $this->EventFormAddRepositories->storeContactEventForm($contactEventFormDetails);

        /* Log For ContactEventCreation */
        $this->logEvents('ContactEventForm Created id =' . $contactEventFormId);

        return $contactEventFormId;
    }

    /* MultiSelect People Store in ContactEvent Personal Table */
    public function doUpdatePeople($contactEventFormId, $peopleDetails)
    {
        /* Remove Existing People in ContactEventForm */
        $this->EventFormEditRepositories->removeSpecificContactEventFormId($contactEventFormId);

        if ($peopleDetails == null) {
            return true;
        }

        $contactPersonalDetails = [];
        foreach ($peopleDetails as $Personalkey => $peopleDetailsValue) {
            $contactPersonalDetails['contacteventform_id'] = $contactEventFormId;
            $contactPersonalDetails['personnel_id']        = $peopleDetailsValue['id'];
            $contactPersonalId = $this->EventFormEditRepositories->storeContactEventFormPersonnel($contactPersonalDetails);
            $this->logEvents(config('constants.Success.ContactEventFormPersonal') . ' = ' . $contactPersonalId);
        }

        return true;
    }

    /* Store Dynamic CustomField with Value */
    public function updateCustomFieldValues($userId, $contactId, $companyId, $contactEventFormId, $formDetails, $request)
    {
        foreach ($formDetails as $formDetailsValue) {

            /* TextField */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.text')) {
                $fieldResult = $this->EventFieldTypeService->textField($formDetailsValue);
            }
            /* SelectField */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.select')) {
                $fieldResult = $this->EventFieldTypeService->selectField($formDetailsValue);
            }
            /* RadioField */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.radio')) {
                $fieldResult = $this->EventFieldTypeService->RadioField($formDetailsValue);
            }
            /* checkbox */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.checkbox')) {
                $fieldResult = $this->EventFieldTypeService->checkboxField($formDetailsValue);
            }
            /* textarea */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.textarea')) {
                $fieldResult = $this->EventFieldTypeService->textareaField($formDetailsValue);
            }
            /* date */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.date')) {
                $fieldResult = $this->EventFieldTypeService->dateField($formDetailsValue);
            }
            /* time */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.time')) {
                $fieldResult = $this->EventFieldTypeService->timeField($formDetailsValue);
            }
            /* datetime */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.datetime')) {
                $fieldResult = $this->EventFieldTypeService->datetimeField($formDetailsValue);
            }
            /* integer */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.integer')) {
                $fieldResult = $this->EventFieldTypeService->integerField($formDetailsValue);
            }
            /* decimal */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.decimal')) {
                $fieldResult = $this->EventFieldTypeService->decimalField($formDetailsValue);
            }
            /* auto_integer */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.autoInteger')) {
                $fieldResult = $this->EventFieldTypeService->autoIntegerField($formDetailsValue);
            }
            /* image */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.image')) {
                $fieldResult = $this->EventFieldTypeService->imageField($formDetailsValue, $request);

                /* File Path Store in ContactImage */
                $contactImageDetails['user_id']               = $userId;
                $contactImageDetails['company_id']            = $companyId;
                $contactImageDetails['contact_id']            = $contactId;
                $contactImageDetails['cfield_id']             = $formDetailsValue['custom_field_id'];
                $contactImageDetails['contact_event_form_id'] = $contactEventFormId;
                $contactImageDetails['image']                 = $fieldResult['custom_field_value'];
                $contactImageDetails['created']               = now();

                $contactImage = $this->EventFormEditRepositories->updateContactImageDetails($contactImageDetails);
            }
            /* multiple_select */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.multipleSelect')) {
                $fieldResult = $this->EventFieldTypeService->multipleSelectField($formDetailsValue);

                /* Remove Existing MultiValue */
                $this->EventFormEditRepositories->removeSpecificContactEventFormMultiValueId($contactEventFormId, $formDetailsValue['custom_field_id']);

                /* MultiValue Update */
                if (isset($fieldResult['custom_field_value'])) {

                    $multiIdValues = explode(',', $fieldResult['custom_field_value']);

                    foreach ($multiIdValues as $multiValue) {
                        $multiCustomField['entity_id'] = $contactEventFormId;
                        $multiCustomField['updated']   = now();
                        $multiCustomField['cfield_id'] = $formDetailsValue['custom_field_id'];
                        $multiCustomField['option_id'] = $multiValue;

                        /* MultiValueOptions Added */
                        $multiValueId = $this->EventFormEditRepositories->updateNewContactEventFormMultiValueId($multiCustomField);

                        $this->logEvents(config('constants.Success.MultiFieldOption') . $multiValueId);
                    }
                }
            }

            /* Store in customField Details */
            if ($fieldResult['status'] == true) {

                $customField['cfield_id'] = $formDetailsValue['custom_field_id'];
                $customField['entity_id'] = $contactEventFormId;
                $customField['cf_value']  = $fieldResult['custom_field_value'];
                $customField['updated']   = now();

                /* store CustomFieldValues */
                $contactPersonalId = $this->EventFormEditRepositories->updateCustomFieldValue($customField);
            }

            /* End of Section */
            if ($fieldResult['status'] == false) {
                return $fieldResult;
                break;
            }
        }

        $finalCustomFieldResult['status'] = true;
        $finalCustomFieldResult['message'] = '';

        return $finalCustomFieldResult;
    }

    /* ScheduleTask Store Process */
    public function storeSchedultTask($userId, $contactId, $scheduleDetails)
    {
        /* Initial Response Set */
        $scheduleResult['status']  = true;
        $scheduleResult['message'] = '';

        /* check the Schedule Task request Details */
        if ($scheduleDetails == '') {
            return $scheduleResult;
        }

        /* DateTime Formatting */
        $dateTimeDetails = $scheduleDetails->task_date . ' ' . $scheduleDetails->task_hour . ':' . $scheduleDetails->task_min . ':00 ' . $scheduleDetails->task_meridiem;

        /* Strcturing DateTime Format */
        $dateTimeDetails = Carbon::parse($dateTimeDetails)->format('Y-m-d H:i:s');

        /* TaskAssignment as User Assigned for FollowUp */
        if ($scheduleDetails->task_assignment == '') {
            $scheduleResult['status']  = false;
            $scheduleResult['message'] = config('constants.Errors.TaskAssignmentId');
        }

        $taskAssignmentUserId   = explode(',', $scheduleDetails->task_assignment);

        /* People List Check */
        if ($scheduleDetails->task_people == '') {
            $taskAssignmentPeopleId = '';
        } else {
            $taskAssignmentPeopleId = explode(',', $scheduleDetails->task_people);
        }

        foreach ($taskAssignmentUserId as $taskAssignmentkey => $taskAssignmentvalue) {

            $followupDetails['user_id']         = $taskAssignmentvalue;
            $followupDetails['contact_id']      = $contactId;
            $followupDetails['start']           = $dateTimeDetails;
            $followupDetails['duration']        = $scheduleDetails->duration;
            $followupDetails['send_google_cal'] = $scheduleDetails->send_google_cal;
            $followupDetails['created']         = now();

            try {
                $followupId = $this->EventFormAddRepositories->storeFollowupValue($followupDetails);
                $this->logEvents(config('constants.Success.Followup') . ' Created Id :' . $followupId);
            } catch (Exception $e) {
                $this->logEventsError($e->getMessage());
                $scheduleResult['status']  = false;
                $scheduleResult['message'] = config('constants.Errors.AddEventForm');
                return $scheduleResult;
            }

            if ($taskAssignmentPeopleId != '') {
                foreach ($taskAssignmentPeopleId as $key => $taskAssignmentPeoplevalue) {
                    $followupPersonalDetails['followup_id']  = $followupId;
                    $followupPersonalDetails['personnel_id'] = $taskAssignmentPeoplevalue;

                    try {
                        $followupPersonalId = $this->EventFormAddRepositories->storeFollowupPersonalValue($followupPersonalDetails);
                        $this->logEvents(config('constants.Success.FollowupPersonnel') . ' Created Id :' . $followupPersonalId);
                    } catch (Exception $e) {
                        $this->logEventsError($e->getMessage());
                        $scheduleResult['status']  = false;
                        $scheduleResult['message'] = config('constants.Errors.AddEventForm');
                        return $scheduleResult;
                    }
                }
            }
        }
        return $scheduleResult;
    }


    /* Each Field Validation Process */
    public function doEventFormEditValidations($userId, $companyId, $contactId, $eventFormId, $formDetails)
    {

        $validationResult['status'] = true;
        $validationResult['message'] = '';

        /* ContactId Validation */
        if (Contact::whereCompany_id($companyId)->whereId($contactId)->exists() == false) {
            $validationResult['status']  = false;
            $validationResult['message'] = config('constants.Errors.ContactId');
            return $validationResult;
        }
        /* EventId Validation */
        if (Eventform::whereCompany_id($companyId)->whereId($eventFormId)->exists() == false) {
            $validationResult['status']  = false;
            $validationResult['message'] = config('constants.Errors.EventFormId');
            return $validationResult;
        }

        /* Field Validation */
        foreach ($formDetails as $formDetailskey => $formDetailsvalue) {

            /* Required Validations */
            if ($formDetailsvalue['required'] == true) {
                if ($formDetailsvalue['value'] == '' && $formDetailsvalue['custom_field_option_default_id'] == '') {
                    $validationResult['status']  = false;
                    $validationResult['message'] = $formDetailsvalue['label'] . ' Required';
                }
            }

            /* Custom Field Type Validation */
            if ($formDetailsvalue['custom_field_type_id'] != null && $formDetailsvalue['custom_field_type_id'] != 1) {

                if (Cfield::whereId($formDetailsvalue['custom_field_id'])->whereCfield_type_id($formDetailsvalue['custom_field_type_id'])->exists() == false) {
                    $validationResult['status']  = false;
                    $validationResult['message'] = $formDetailsvalue['label'] . ' custom Field id/Field Type Id Invalid';
                }
            }
        }
        return $validationResult;
    }

    public function doSuccessFormat($responseResult)
    {
        $result['status']   = true;
        $result['message']  = 'Success';
        $result['response'] = $responseResult;
        return $result;
    }

    public function doErrorFormat($responseResult)
    {
        Helper::logError($responseResult, 401, $requestdetails = null, config('constants.ServiceName.Event'));
        $result['status']   = false;
        $result['message']  = $responseResult;
        $result['response'] = '';
        return $result;
    }

    public function logEvents($messageDetails = null)
    {
        $eventEditLog['User_id']      = $this->userId;
        $eventEditLog['Company_id']   = $this->companyId;
        $eventEditLog['Contact_id']   = $this->contactId;
        $eventEditLog['EventForm_id'] = $this->EventFormId;
        $eventEditLog['Message']      = $messageDetails;

        Log::notice($eventEditLog);

        return true;
    }

    public function logEventsError($messageDetails = null)
    {
        $eventEditLog['User_id']      = $this->userId;
        $eventEditLog['Company_id']   = $this->companyId;
        $eventEditLog['Contact_id']   = $this->contactId;
        $eventEditLog['EventForm_id'] = $this->EventFormId;
        $eventEditLog['Message']      = $messageDetails;

        Log::error($eventEditLog);

        return true;
    }
}
