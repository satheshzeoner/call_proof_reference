<?php

namespace App\Services\TwilioCountry;

use Exception;

/* Repositories */
use App\Repositories\TwilioCountry\TwilioCountryRepositories;

class TwilioCountryService
{
    public function __construct()
    {
        $this->TwilioCountryRepositories = new TwilioCountryRepositories();
    }

    public function getTwilioCountry($request)
    {
        try {

            $list = $this->TwilioCountryRepositories->getTwilioCountryDetails($request);
            return $this->doSuccessFormatDetails($list);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e);
        }
    }

    public function doSuccessFormatDetails($data)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response']['twilio_country'] = $data;

        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails->getMessage();
        $result['response'] = '';

        return $result;
    }
}
