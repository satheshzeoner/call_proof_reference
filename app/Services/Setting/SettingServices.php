<?php

namespace App\Services\Setting;

use Carbon\Carbon;
use Exception;

/* Models */
use App\Models\Auth\AuthUser;
use App\Models\TaskNotificationMinuteChoice\TaskNotificationMinuteChoice;

/* Repositories */
use App\Repositories\Setting\SettingRepositories;

class SettingServices
{
    public function __construct()
    {
        $this->SettingRepositories = new SettingRepositories();
    }

    /* Settings List*/
    public function getSettings($request, $userId)
    {
        try {
            $googleCalSync              = AuthUser::find($userId)->userProfile->google_cal_sync;
            $hideAppointmentPopup       = AuthUser::find($userId)->userProfile->hide_mobile_appt_popup;
            $taskPopupSetting           = AuthUser::find($userId)->userProfile->task_popup_disabled;
            $isContactPageAsHomepage    = AuthUser::find($userId)->userProfile->is_contact_page_as_homepage;
            $isLogsEnabled              = AuthUser::find($userId)->userProfile->userCompany->is_logs_enabled;
            $isStartAppointment         = AuthUser::find($userId)->userProfile->is_start_appointment;
            $selectedValue              = (AuthUser::find($userId)->userProfile->task_notification_time_id) ? (AuthUser::find($userId)->userProfile->task_notification_time_id) : 1;

            $choices = $this->SettingRepositories->getAllBySelectedId($selectedValue);
            return $this->successFormatDetails($googleCalSync, $hideAppointmentPopup, $taskPopupSetting, $isContactPageAsHomepage, $isLogsEnabled, $isStartAppointment, $choices);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    /* Get Settings Information */
    public function getSettingsInfo($request, $userId, $companyId)
    {
        try {
            $settingsInfo = $this->SettingRepositories->getSettingsInfoDetails($userId, $companyId);
            return $this->doSettingsInfoSuccessDetails($userId, $companyId, $settingsInfo);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function updateSettings($request, $userId)
    {
        try {
            $companyId                     =  AuthUser::find($userId)->userProfile->company_id;
            $syncTaskOnDeviceCalender      = $request->sync_task_on_device_calender;
            $hideMobileAppointmentPopup    = $request->hide_mobile_appointment_popup;
            $taskPopUpSetting              = $request->task_popup_setting;
            $isContactPageAsHomepage       = $request->is_contact_page_as_homepage;
            $isLogsEnabled                 = $request->is_logs_enabled;
            $isStartAppointment            = $request->is_start_appointment;
            $taskNotificationTimeId        = $request->task_notification_time_id;
            $isSettingsUpdated = $this->SettingRepositories->updateSettings($userId, $companyId, $syncTaskOnDeviceCalender, $hideMobileAppointmentPopup, $taskPopUpSetting, $isContactPageAsHomepage, $isLogsEnabled, $isStartAppointment, $taskNotificationTimeId);
            if ($isSettingsUpdated) {
                $message = config('constants.Success.Settings');
            } else {
                $message = config('constants.Errors.updationError');
            }
            return $this->doUpdatedResultFormatDetails($isSettingsUpdated, $message);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function successFormatDetails($googleCalSync, $hideAppointmentPopup, $taskPopupSetting, $isContactPageAsHomepage, $isLogsEnabled, $isStartAppointment, $choices)
    {
        $result['status']                                     = true;
        $result['message']                                    = 'Success';
        $result['response']['google_cal_sync']                = $googleCalSync;
        $result['response']['hide_appt_popup']                = $hideAppointmentPopup;
        $result['response']['task_popup_setting']             = $taskPopupSetting;
        $result['response']['is_contact_page_as_homepage']    = $isContactPageAsHomepage;
        $result['response']['is_logs_enabled']                = $isLogsEnabled;
        $result['response']['is_start_appointment']           = $isStartAppointment;
        $result['response']['device_notification']['choices'] = $choices;

        return $result;
    }

    public function doSettingsInfoSuccessDetails($userId, $companyId, $settingsDetails)
    {
        $settingsInfo     = collect($settingsDetails)->first();

        /* Gether Info From EventForm */
        $eventFormDetails = $this->SettingRepositories->getEventFormId($userId, $companyId)->id;
        $eventFormId = (isset($eventFormDetails)) ?  $eventFormDetails : '';

        /* Gether Info From Company */
        $companyDetails = $this->SettingRepositories->getCompanyInfo($companyId);

        /* Gether Info From UserSettings CompanySetting GpsInfo */
        $userSettingGpsInfo   = $this->SettingRepositories->getUserSettingGpsInfo($userId);
        $userSettingGpsResult = (isset($userSettingGpsInfo->value)) ?  (int)($userSettingGpsInfo->value) : 0;

        /* Gether Info From UserSettings userSetting sync_calls */
        $userSettingSyncCallInfo   = $this->SettingRepositories->getUserSettingSyncCallInfo($userId);
        $userSettingSyncCallResult = (isset($userSettingSyncCallInfo->value)) ?  $userSettingSyncCallInfo->value : 3600;

        /* Gether Info From UserSettings userSetting sync_gps */
        $userSettingSyncGpsInfo   = $this->SettingRepositories->getUserSettingSyncGpsInfo($userId);
        $userSettingSyncGpsResult = (isset($userSettingSyncGpsInfo->value)) ?  $userSettingSyncGpsInfo->value : 900;

        $settingsResultDetails['manager']                     = ($settingsInfo['manager'] == true) ? config('constants.Usersetting.value') : 0;
        /* Retrived From UserSetting */
        $settingsResultDetails['gps']                         = $userSettingGpsResult;
        $settingsResultDetails['sync_calls']                  = $userSettingSyncCallResult;
        $settingsResultDetails['sync_gps']                    = $userSettingSyncGpsResult;
        /* Retrived From Company */
        $settingsResultDetails['logo']                        = (isset($companyDetails->logo)) ? $companyDetails->logo : '';
        $settingsResultDetails['background_image']            = (isset($companyDetails->bg_image)) ? $companyDetails->bg_image : '';
        $settingsResultDetails['people']                      = ($companyDetails->people_tab == true) ? config('constants.Usersetting.value') : 0;
        $settingsResultDetails['event_form']                  = (isset($settingsInfo['event_form_id'])) ? $settingsInfo['event_form_id'] : 0;
        $settingsResultDetails['is_hide_mobile_appt_popup']   = ($settingsInfo['hide_mobile_appt_popup'] == true) ? config('constants.Usersetting.value') : 0;
        $settingsResultDetails['is_start_appointment']        = ($settingsInfo['is_start_appointment'] == true) ? config('constants.Usersetting.value') : 0;
        $settingsResultDetails['is_contact_page_as_homepage'] = ($settingsInfo['is_contact_page_as_homepage'] == true) ? config('constants.Usersetting.value') : 0;
        $settingsResultDetails['is_logs_enabled']             = ($companyDetails->is_logs_enabled == true) ? config('constants.Usersetting.value') : 0;
        $settingsResultDetails['event_form_id']               = $eventFormId;

        $result['status']                      = true;
        $result['message']                     = 'Success';
        $result['response']['settings']   = $settingsResultDetails;

        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails;
        $result['response'] = '';

        return $result;
    }

    public function doUpdatedResultFormatDetails($isSettingsUpdated, $message)
    {
        $result['status']  = $isSettingsUpdated;
        $result['message'] = $message;
        $result['response'] = '';
        return $result;
    }
}
