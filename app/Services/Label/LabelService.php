<?php

namespace App\Services\Label;

use Exception;

/* Models */
use App\Models\Auth\AuthUser;

/* Repositories */
use App\Repositories\Label\LabelRepositories;

class LabelService
{
    public function __construct()
    {
        $this->LabelRepositories = new LabelRepositories();
    }

    public function doGetLabels($request, $userId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $labels = $this->LabelRepositories->getLabelDetails($request, $userId, $userCompanyId);
            return $this->doSuccessFormatDetails($labels);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e);
        }
    }

    public function doSuccessFormatDetails($data)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response']['users'] = $data;

        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails->getMessage();
        $result['response'] = '';

        return $result;
    }
}
