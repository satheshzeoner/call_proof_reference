<?php

namespace App\Services\Event;

use App\Helpers\Helper;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Exception;


/* Repository */
use App\Repositories\Event\EventAddRepositories;

class EventAddService
{
    public function __construct()
    {
        $this->EventAddRepositories = new EventAddRepositories();
    }

    # =============================================
    # =       Events Add Process                  =
    # =============================================

    public function addEvent(
        $eventName,
        $eventCompanyId,
        $eventUserId,
        $eventContactId,
        $eventOtherUserId,
        $eventCallId,
        $eventStartTime,
        $eventDuration,
        $eventBadgeId,
        $eventGooglePlace,
        $eventTwilioCallId,
        $eventContactEventFormId,
        $eventAppointmentId,
        $eventFollowupId,
        $eventMessage
    ) {

        /* Get EventName Based EventDetails */
        $eventTypeDetails = $this->EventAddRepositories->getEventTypeDetails($eventCompanyId, $eventName);

        /* Group the Datas Collected Points and Related From Functions */
        $Event['user_id']         = $eventUserId;
        $Event['event_type_id']   = $eventTypeDetails->id;
        $Event['contact_id']      = $eventContactId;
        $Event['other_user_id']   = $eventOtherUserId;
        $Event['call_id']         = $eventCallId;
        $Event['start']           = $eventStartTime;
        $Event['duration']        = $eventDuration;
        $Event['badge_id']        = $eventBadgeId;
        $Event['created']         = now();
        $Event['popular']         = now();
        $Event['google_place_id'] = $eventGooglePlace;
        $Event['cef_id']          = $eventContactEventFormId;
        $Event['tc_id']           = $eventTwilioCallId;
        $Event['points']          = $this->EventAddRepositories->getEventPoints($eventCompanyId, $eventTypeDetails->id)->points;
        $Event['company_id']      = $eventCompanyId;
        $Event['appointment_id']  = $eventAppointmentId;
        $Event['message']         = $eventMessage;
        $Event['followup_id']     = $eventFollowupId;

        /* Store Event Details */
        $eventTypeDetails = $this->EventAddRepositories->doStoreEvent($Event);

        /* logs */
        if ($eventTypeDetails['status'] == true) {
            $eventLog['Event_id']   = $eventTypeDetails['message'];
            $eventLog['Event_name'] = $eventName;
            Log::notice($eventLog);
        } else {
            Log::error($eventTypeDetails['message']);
        }
    }
}
