<?php

namespace App\Services\Event;

use App\Helpers\Helper;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Exception;


/* Repository */
use App\Repositories\Event\EventListRepositories;

class EventListService
{
    public function __construct()
    {
        $this->EventListRepositories                = new EventListRepositories();
    }

    # =============================================
    # =       Events List Details                 =
    # =============================================

    public function getEventsList($userId, $companyId, $request)
    {
        try {
            $eventListDetails = $this->EventListRepositories->getEventList($userId, $companyId, $request);
            $eventListFormatDetails = $this->doFormattingEventDetails($eventListDetails, $userId, $companyId);
            return $this->doSuccessFormatWithPagignationDetails($eventListFormatDetails, $eventListDetails);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    public function doFormattingEventDetails($eventDetails, $userId, $companyId)
    {
        $eventsResult = [];

        /* Pagination Format */
        $eventDetails = collect($eventDetails['data'])->toArray();

        /* userDetails */
        $userDetails    = Auth::user();
        $userId         = $userDetails->id;
        $userName       = $userDetails->first_name . ' ' . $userDetails->last_name;

        /* Is Manager { Permission For Clickable } */
        // $clickableCheckStatus = 0;

        /* UserProfile Details */
        $userProfileDetails = $this->EventListRepositories->getUserProfileDetails($userId);

        // dd($userProfileDetails->manager);


        foreach ($eventDetails as $eventValue) {

            $eventDetailsResult['event_id']        = $eventValue['id'];

            /* EventType */
            $eventDetailsResult['event_type_id']          = $eventValue['event_type_id'];
            $eventDetailsResult['event_type_name']        = $eventValue['event_type']['name'];
            $eventDetailsResult['event_type_description'] = $eventValue['event_type']['desc'];
            $eventDetailsResult['event_type_image']       = $eventValue['event_type']['image'];
            $eventDetailsResult['event_user_id']          = $userId;
            $eventDetailsResult['event_user_name']        = $userName;

            /* Company */
            if ($eventValue['event_company'] != null) {
                $eventDetailsResult['event_company_id']   = $eventValue['event_company']['id'];
                $eventDetailsResult['event_company_name'] = $eventValue['event_company']['name'];
            } else {
                $eventDetailsResult['event_company_id']   = config('constants.default.DefaultId');
                $eventDetailsResult['event_company_name'] = '';
            }

            /* EventContact */
            if ($eventValue['event_contact'] != null) {
                $eventDetailsResult['event_contact_id']   = $eventValue['event_contact']['id'];
                $eventDetailsResult['event_contact_name'] = $eventValue['event_contact']['first_name'] . ' ' . $eventValue['event_contact']['last_name'];
                $eventDetailsResult['event_contact_people'] = $this->getContactPeopleDetails($eventValue['event_contact']['id']);
            } else {
                $eventDetailsResult['event_contact_id']   = config('constants.default.DefaultId');
                $eventDetailsResult['event_contact_name'] = '';
                $eventDetailsResult['event_contact_people'] = '';
            }

            /* Other User */
            if ($eventValue['event_otheruser'] != null) {
                $eventDetailsResult['event_otheruser_id']   = $eventValue['event_otheruser']['id'];
                $eventDetailsResult['event_otheruser_name'] = $eventValue['event_otheruser']['first_name'] . ' ' . $eventValue['event_otheruser']['first_name'];
            } else {
                $eventDetailsResult['event_otheruser_id']   = config('constants.default.DefaultId');
                $eventDetailsResult['event_otheruser_name'] = '';
            }

            /* Call Details */
            if ($eventValue['call_id'] != null) {
                $callDetails = $this->EventListRepositories->getCallDetails($eventValue['call_id']);
                if (isset($callDetails)) {
                    $eventDetailsResult['event_call_id']          = $eventValue['call_id'];
                    $eventDetailsResult['event_call_type']        = $callDetails['call_type']['name'];
                    $eventDetailsResult['event_call_phonenumber'] = $callDetails['phone_number'];
                } else {
                    $eventDetailsResult['event_call_id']          = config('constants.default.DefaultId');
                    $eventDetailsResult['event_call_type']        = '';
                    $eventDetailsResult['event_call_phonenumber'] = '';
                }
            } else {
                $eventDetailsResult['event_call_id']          = config('constants.default.DefaultId');
                $eventDetailsResult['event_call_type']        = '';
                $eventDetailsResult['event_call_phonenumber'] = '';
            }

            /* Badge */
            if ($eventValue['event_badge'] != null) {
                $eventDetailsResult['event_badge_id']   = $eventValue['event_badge']['id'];
                $eventDetailsResult['event_badge_name'] = $eventValue['event_badge']['name'];
            } else {
                $eventDetailsResult['event_badge_id']   = config('constants.default.DefaultId');
                $eventDetailsResult['event_badge_name'] = '';
            }

            /* GoolglePlace */
            if ($eventValue['event_googleplace'] != null) {
                $eventDetailsResult['event_googleplace_id']        = $eventValue['event_googleplace']['id'];
                $eventDetailsResult['event_googleplace_name']      = $eventValue['event_googleplace']['name'];
                $eventDetailsResult['event_googleplace_address']   = $eventValue['event_googleplace']['address'];
                $eventDetailsResult['event_googleplace_city']      = $eventValue['event_googleplace']['city'];
                $eventDetailsResult['event_googleplace_latitude']  = $eventValue['event_googleplace']['latitude'];
                $eventDetailsResult['event_googleplace_longitude'] = $eventValue['event_googleplace']['longitude'];
                $eventDetailsResult['event_googleplace_distance']  = $eventValue['event_googleplace']['distance'];
                $eventDetailsResult['event_googleplace_distance']  = $eventValue['event_googleplace']['distance'];
            } else {
                $eventDetailsResult['event_googleplace_id']        = config('constants.default.DefaultId');
                $eventDetailsResult['event_googleplace_name']      = '';
                $eventDetailsResult['event_googleplace_address']   = '';
                $eventDetailsResult['event_googleplace_city']      = '';
                $eventDetailsResult['event_googleplace_latitude']  = '';
                $eventDetailsResult['event_googleplace_longitude'] = '';
                $eventDetailsResult['event_googleplace_distance']  = '';
                $eventDetailsResult['event_googleplace_distance']  = '';
            }

            /* Appointment */
            if ($eventValue['event_appointment'] != null) {
                $eventDetailsResult['event_appointment_id']        = $eventValue['event_appointment']['id'];
                $eventDetailsResult['event_appointment_notes']     = $eventValue['event_appointment']['notes'];
                $eventDetailsResult['event_appointment_latitude']  = $eventValue['event_appointment']['latitude'];
                $eventDetailsResult['event_appointment_longitude'] = $eventValue['event_appointment']['longitude'];
                $eventDetailsResult['event_appointment_duration']  = $eventValue['event_appointment']['duration'];
            } else {
                $eventDetailsResult['event_appointment_id']        = config('constants.default.DefaultId');
                $eventDetailsResult['event_appointment_notes']     = '';
                $eventDetailsResult['event_appointment_latitude']  = '';
                $eventDetailsResult['event_appointment_longitude'] = '';
                $eventDetailsResult['event_appointment_duration']  = '';
            }

            /* EventFollow */
            if ($eventValue['event_follow'] != null) {
                $eventDetailsResult['event_follow_id']    = $eventValue['followup_id'];
                $eventDetailsResult['event_follow_notes'] = $eventValue['event_follow']['notes'];
            } else {
                $eventDetailsResult['event_follow_id']   = config('constants.default.DefaultId');
                $eventDetailsResult['event_follow_notes'] = '';
            }

            /* ContactEventForm */
            if ($eventValue['cef_id'] != null) {
                $callDetails = $this->EventListRepositories->getContactEventFormDetails($eventValue['cef_id']);
                if (isset($callDetails)) {
                    $eventDetailsResult['event_contact_event_form_id']   = $eventValue['cef_id'];
                    $eventDetailsResult['event_contact_event_form_name'] = $callDetails['event_form']['name'];
                } else {
                    $eventDetailsResult['event_contact_event_form_id']          = config('constants.default.DefaultId');
                    $eventDetailsResult['event_contact_event_form_name']        = '';
                }
            } else {
                $eventDetailsResult['event_contact_event_form_id']          = config('constants.default.DefaultId');
                $eventDetailsResult['event_contact_event_form_name']        = '';
            }


            /* Twiliocall */
            if ($eventValue['event_twilio_call'] != null) {
                $eventDetailsResult['event_twilio_call_id']           = $eventValue['event_twilio_call']['id'];
                $eventDetailsResult['event_twilio_call_status']       = $eventValue['event_twilio_call']['call_status'];
                $eventDetailsResult['event_twilio_call_carrier_name'] = $eventValue['event_twilio_call']['carrier_name'];
                $eventDetailsResult['event_twilio_call_caller_name']  = $eventValue['event_twilio_call']['caller_name'];
            } else {
                $eventDetailsResult['event_twilio_call_id']           = config('constants.default.DefaultId');
                $eventDetailsResult['event_twilio_call_status']       = '';
                $eventDetailsResult['event_twilio_call_carrier_name'] = '';
                $eventDetailsResult['event_twilio_call_caller_name']  = '';
            }

            /* Time Details */

            /* Start Date */
            $startDate = $this->getDateTimeFormatDetails($eventValue['start']);

            $eventDetailsResult['event_start']        = $startDate['dateFormat'];
            $eventDetailsResult['event_start_date']   = $startDate['date'];

            /* Created Date */
            $createdDate = $this->getDateTimeFormatDetails($eventValue['created']);

            $eventDetailsResult['event_created']      = $createdDate['dateFormat'];
            $eventDetailsResult['event_created_date'] = $createdDate['date'];

            /* Duration */
            $eventDetailsResult['event_time_duration'] = $this->durationTimeCalculation($eventValue['created']);
            $eventDetailsResult['event_message'] = isset($eventValue['message']) ? $eventValue['message'] : '';

            /* Clickable Access */
            $eventDetailsResult['event_clickable_access'] = $this->eventClickableAccess($userProfileDetails, $eventValue['event_contact']['id']);

            array_push($eventsResult, $eventDetailsResult);
        }
        return $eventsResult;
    }

    /* Clickable Access */
    public function eventClickableAccess($userProfileDetails, $contactId)
    {
        /* CHECKING Just My Events Condition */
        if ($userProfileDetails->just_my_events == true) {
            if (isset($contactId)) {
                $dd = $this->getAssignedAccessDetails(
                    $userProfileDetails->user_id,
                    $contactId
                );
            }
        }

        /* CHECKING Hide My Events Condition */
        if ($userProfileDetails->hide_my_events == true) {
            if (isset($contactId)) {
                $dd = $this->hideMyDetails(
                    $userProfileDetails->user_id,
                    $contactId
                );
            }
        }

        /* CHECKING Is Manager Condition */
        if ($userProfileDetails->manager == true) {
            return true;
        } else {
            if (isset($contactId)) {

                /* CHECKING Contact Radius Option has {All Contact} */
                if ($userProfileDetails->contact_radius == config('constants.ContactRadius.AllContacts')) {
                    return true;
                }

                /* CHECKING Contact Radius Option has {Show Market Contacts} */

                /* Get Assigned or unAssigned Valid Check */
                $unAssignedDetails = $this->EventListRepositories->getUserSettingAssignedStatus($userProfileDetails->user_id);

                /* Validate Assigned Status */
                if (isset($unAssignedDetails)) {
                    $unAssignedStatus = (int)$unAssignedDetails->value;
                } else {
                    $unAssignedStatus = config('constants.ContactRadius.DefaultAssignedLead');
                }

                /* Checked ShowMarket and UnChecked Assigned */
                if ($userProfileDetails->contact_radius == config('constants.ContactRadius.ShowMarketContacts') && $unAssignedStatus == config('constants.ContactRadius.DefaultAssignedLead')) {
                    return $this->getMarketContactsAccessDetails(
                        $userProfileDetails->user_id,
                        $userProfileDetails->company_id,
                        $contactId
                    );
                }

                /* Checked ShowMarket and Checked Assigned */
                if ($userProfileDetails->contact_radius == config('constants.ContactRadius.ShowMarketContacts') && $unAssignedStatus == config('constants.ContactRadius.DefaultAssignedLeadActive')) {
                    return $this->getAssignedAccessDetails(
                        $userProfileDetails->user_id,
                        $contactId
                    );
                }

                /* Checked Assigned Contacts & AssignedLead is Unchecked */
                if ($userProfileDetails->contact_radius == config('constants.ContactRadius.AssignedContacts') && $unAssignedStatus == config('constants.ContactRadius.DefaultAssignedLeadActive')) {
                    return $this->getAssignedAccessDetails(
                        $userProfileDetails->user_id,
                        $contactId
                    );
                }
            }
            return false;
        }
    }


    /* Show Market Contacts */
    public function getMarketContactsAccessDetails($userId, $companyId, $contactId)
    {
        /* Check Details in user_market */
        $marketDetails = $this->EventListRepositories->getUserMarketDetails($userId, $companyId);
        $marketUserId  = collect($marketDetails)->pluck('user_id');

        /* Check Contact Rep Details in user_market */
        $contactDetails = $this->EventListRepositories->getContactRepDetails($marketUserId, $contactId);

        return $contactDetails;
    }

    /* ContactRep Validate */
    public function getAssignedAccessDetails($userId, $contactId)
    {
        return $this->EventListRepositories->getContactRepDetails(array($userId), $contactId);
    }

    /* ContactRep Hide for User Validate */
    public function hideMyDetails($userId, $contactId)
    {
        return $this->EventListRepositories->hideContactRepDetails(array($userId), $contactId);
    }

    public function getContactPeopleDetails($contactId)
    {
        $contactPersonalDetails = $this->EventListRepositories->getContactPersonalDetails($contactId);

        $contactPeople = '';

        if (isset($contactPersonalDetails)) {
            $contactPeopleNames = [];
            foreach ($contactPersonalDetails as $ContactPersonalValue) {
                $fullName = $ContactPersonalValue['first_name'] . ' ' . $ContactPersonalValue['last_name'];
                array_push($contactPeopleNames, $fullName);
            }
            $contactPeople = implode(',', $contactPeopleNames);
            return $contactPeople;
        } else {
            return $contactPeople;
        }
    }

    public function getDateTimeFormatDetails($dateTime)
    {
        $month    = Carbon::parse($dateTime)->format('M');
        $dateInt  = Carbon::parse($dateTime)->format('d');
        $year     = Carbon::parse($dateTime)->format('Y');
        $fullTime = Carbon::parse($dateTime)->format('H:i:s A');

        $dateForm['dateFormat'] = $month . '. ' . $dateInt . ',' . $year . ',' . $fullTime;
        $dateForm['date'] = Carbon::parse($dateTime)->format('d-m-Y');

        return $dateForm;
    }

    public function durationTimeCalculation($startDate)
    {
        $start  = new Carbon($startDate);
        $end    = new Carbon(now());

        $hours    = $start->diffInHours($end);
        $minutes  = $start->diff($end)->format('%I');
        $seconds  = $start->diff($end)->format('%S');
        $fullTime = $hours . ':' . $minutes . ':' . $seconds;

        return $fullTime;
    }


    public function doSuccessFormatWithPagignationDetails($eventsDeails, $eventsResult)
    {
        $result['status']             = true;
        $result['message']            = 'Success';
        $result['response']['events'] = $eventsDeails;
        $result['response']['paginationDetails'] = Helper::customPagination($eventsResult);

        return $result;
    }

    public function doSuccessFormat($responseResult)
    {
        $result['status']   = true;
        $result['message']  = 'Success';
        $result['response'] = $responseResult;
        return $result;
    }

    public function doErrorFormat($responseResult)
    {
        Helper::logError($responseResult->getMessage(), 401, $requestdetails = null, config('constants.ServiceName.Event'));
        $result['status']   = false;
        $result['message']  = $responseResult->getMessage();
        $result['response'] = '';
        return $result;
    }
}
