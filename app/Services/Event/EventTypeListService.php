<?php

namespace App\Services\Event;

use App\Helpers\Helper;
use Illuminate\Support\Facades\Log;
use Exception;

/* Models */
use App\Models\Event\EventType;

/* Repository */
use App\Repositories\Event\EventTypeRepositories;

class EventTypeListService
{
    public function __construct()
    {
        $this->EventTypeRepositories                = new EventTypeRepositories();
    }

    # =============================================
    # =       EventType List                      =
    # =============================================

    public function getEventTypeList($userId, $companyId)
    {
        try {
            $eventTypeDetails = $this->EventTypeRepositories->getEventType($userId, $companyId);
            $eventTypeDetails = $this->doFormattingEventType($eventTypeDetails);
            return $this->doSuccessFormat($eventTypeDetails);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    public function doFormattingEventType($eventTypeDetails)
    {
        $eventTypeResult = [];

        foreach ($eventTypeDetails as $eventTypeDetailsValue) {
            $eventTypeDetailsValue['event_type_name'] = ucwords(str_replace("_", " ", $eventTypeDetailsValue['event_name']));
            array_push($eventTypeResult, $eventTypeDetailsValue);
        }

        return $eventTypeResult;
    }


    public function doSuccessFormat($responseResult)
    {
        $result['status']   = true;
        $result['message']  = 'Success';
        $result['response'] = $responseResult;
        return $result;
    }

    public function doErrorFormat($responseResult)
    {
        Helper::logError($responseResult->getMessage(), 401, $requestdetails = null, config('constants.ServiceName.Event'));
        $result['status']   = false;
        $result['message']  = $responseResult->getMessage();
        $result['response'] = '';
        return $result;
    }
}
