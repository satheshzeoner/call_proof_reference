<?php

namespace App\Services\Signup;

use App\Helpers\Helper;
use Illuminate\Support\Facades\DB;

/* Models */
use App\Models\Mail\SignupMail;
use App\Models\Event\Event;

/* Repositories */
use App\Repositories\Signup\SignupStoreRepositories;
use App\Repositories\Signup\SignupCloneRepositories;

/* EventService */
use App\Services\Event\EventAddService;

class SignupServices
{

    public function __construct()
    {
        $this->masterDefaultTemplateId  = Helper::masterDefaultCompanySetting();
        $this->signupMail               = new SignupMail();
        $this->Event                    = new Event();

        $this->SignupStoreRepositories  = new SignupStoreRepositories();
        $this->SignupCloneRepositories  = new SignupCloneRepositories();
    }

    protected $similarTableStore = ['cp_market', 'cp_contacttype', 'cp_menucust', 'cp_menucolcust', 'cp_menurowcust', 'cp_menuurlcust', 'cp_peoplerole', 'cp_opptype', 'cp_oppstage', 'cp_followuptype'];


    public function doStore($request, $appleSigninTos = null)
    {

        /* Inserion Process Start */

        // DB::beginTransaction();

        $phoneType  = Helper::DefaultPhonetype();

        $ipDetails = Helper::getIpRelatedDetails();

        $authUserResult    = $this->SignupStoreRepositories->doAuthUserStore($request);
        $userId            = $authUserResult['id'];
        $companyResult     = $this->SignupStoreRepositories->doCompanyStore($request);
        $companyId         = $companyResult['id'];
        $usersettingResult = $this->SignupStoreRepositories->doUsersettingStore($request, $userId);
        $usersettingId     = $usersettingResult['id'];
        $userPhoneResult   = $this->SignupStoreRepositories->doPhoneStore($request, $userId, $companyId, $phoneType);
        $userPhoneId       = $userPhoneResult['id'];
        $userProfileResult = $this->SignupStoreRepositories->doUserProfileStore($request, $userId, $companyId, $userPhoneId, $ipDetails);
        $userProfileId     = $userProfileResult['id'];

        if (empty($companyResult['id'])) {
            $result = Helper::customError(401, config('constants.Errors.companyNotCreation'));
            return response()->json($result, 200);
        }

        if (empty($authUserResult['id'])) {
            $result = Helper::customError(401, config('constants.Errors.userNotCreation'));
            return response()->json($result, 200);
        }

        if (
            $authUserResult['status']    == false  ||
            $companyResult['status']     == false  ||
            $usersettingResult['status'] == false  ||
            $userPhoneResult['status']   == false  ||
            $userProfileResult['status'] == false
        ) {
            $result = Helper::customError(401, config('constants.Errors.database'));
            return response()->json($result, 200);
        } else {

            /* Template Clone Process */

            /* Loop Implementation */
            foreach ($this->similarTableStore as $tableKey => $tableName) {
                $fetchData    = Helper::getDefaultCompanyDetails($tableName, $this->masterDefaultTemplateId);
                foreach ($fetchData as $key => $value) {
                    $value['company_id'] = $companyId;
                    unset($value['id']);
                    DB::table($tableName)->insertOrIgnore($value);
                }
            }

            $customfield        = $this->SignupCloneRepositories->doCfieldStore($companyId);
            $eventForm          = $this->SignupCloneRepositories->doEventFormStore($companyId);
            $contactMenu        = $this->SignupCloneRepositories->doContactMenuStore($companyId);
            $placesCategoryList = $this->SignupCloneRepositories->doPlacesCategoryListStore($companyId);
            $title              = $this->SignupCloneRepositories->doTitleStore($companyId);
            $contactCompany     = $this->SignupCloneRepositories->doContactCompanyStore($companyId, $userId);
            $badgePointEvent    = $this->SignupCloneRepositories->doBadgeStore($companyId, $userId);

            /* Event Store Process */
            $this->doEventAdd($userId, $companyId, config('constants.Event_name.signup'));

            /* For AppleSignup Only */
            if (!empty($appleSigninTos)) {
                $appleSigninTos = $this->SignupCloneRepositories->doAppleSignintos($companyId, $userId, $appleSigninTos);
            }
            /* Email Notification */
            //$emailSend         = $this->signupMail->sendNotifyNewMemberEmail($request,$Company_id,$user_id,$ip_details);
            return true;
        }
    }

    /* ===============================EVENT========================== */
    public function doEventAdd($userId, $companyId, $eventName)
    {
        /* Event Creation */
        $eventAddService        = new EventAddService();
        $eventAddService->addEvent(
            $eventName,
            $eventCompanyId          = $companyId,
            $eventUserId             = $userId,
            $eventContactId          = null,
            $eventOtherUserId        = null,
            $eventCallId             = null,
            $eventStartTime          = null,
            $eventDuration           = null,
            $eventBadgeId            = null,
            $eventGooglePlace        = null,
            $eventTwilioCallId       = null,
            $eventContactEventFormId = null,
            $eventAppointmentId      = null,
            $eventFollowupId         = null,
            $eventMessage            = null
        );

        return true;
    }
    /* ===============================EVENT-END====================== */
}
