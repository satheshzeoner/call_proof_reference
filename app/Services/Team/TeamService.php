<?php

namespace App\Services\Team;

use Exception;

/* Models */
use App\Models\Auth\AuthUser;

/* Repositories */
use App\Repositories\Team\TeamRepositories;

class TeamService
{
    public function __construct()
    {
        $this->TeamRepositories = new TeamRepositories();
    }

    public function doGetTeam($request, $userId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $teams = $this->TeamRepositories->getTeamDetails($request, $userId, $userCompanyId);
            $teamDetail = $this->doFormat($teams);
            return $this->doSuccessFormatDetails($teamDetail);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e);
        }
    }

    public function doFormat($teams)
    {
        $teamDetail = [];
        foreach ($teams as $teamKey => $team) {
            $teamDetail[$teamKey]['team_id'] = ($team->id) ? $team->id : 0;
            $teamDetail[$teamKey]['team_name'] = $team->first_name . ' ' . $team->last_name;
            $teamDetail[$teamKey]['title'] = isset($team->userProfile->title) ? $team->userProfile->title->name : '';
            $teamDetail[$teamKey]['phone'] = isset($team->userProfile->userPhone) ? $team->userProfile->userPhone->phone_number : '';
        }
        return $teamDetail;
    }

    public function doSuccessFormatDetails($data)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response']['teams'] = $data;

        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails->getMessage();
        $result['response'] = '';

        return $result;
    }
}
