<?php

namespace App\Services\Opportunity;

use Exception;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

/* Models */
use App\Models\Auth\AuthUser;
use App\Models\Contact\Contact;
use App\Models\Opportunity\Opportunity;
use App\Models\Opportunity\OppContact;

/* EventService */
use App\Services\Event\EventAddService;

/* Repositories */
use App\Repositories\Opportunity\OpportunityRepositories;

class OpportunityService
{
    public function __construct()
    {
        $this->OpportunityRepositories = new OpportunityRepositories();
    }

    public function getOpportunities($request, $userId, $oppUserId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;

            $isMarketManager = AuthUser::find($userId)->userProfile->market_manager;
            $isManager = AuthUser::find($userId)->userProfile->manager;
            $userCompanyPeopletab = AuthUser::find($userId)->userProfile->userCompany->people_tab;
            $dataList = $this->OpportunityRepositories->getOpportunityDetails($userId, $isMarketManager, $isManager, $userCompanyId, $oppUserId, $request);
            $oppData['opportunities'] = $this->doFormat($dataList, $userCompanyPeopletab);
            return $this->doSuccessFormatDetails($oppData);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function getOpportunitiesType($request, $userId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $opportunityType['opportunity_type'] = $this->OpportunityRepositories->getOpportunityTypeDetails($request, $userId, $userCompanyId);
            return $this->doSuccessFormatDetails($opportunityType);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function getopportunitiesStage($request, $userId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $opportunityStage['opportunity_stage'] = $this->OpportunityRepositories->getopportunitiesStageDetails($request, $userId, $userCompanyId);
            return $this->doSuccessFormatDetails($opportunityStage);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }



    /* Opportunities Based On Contacts */
    public function getContactOpportunities($request, $userId, $contactId)
    {
        try {
            $contact = Contact::find($contactId);
            $opportunityContact = ($contact) ? $contact->oppContact()->get() : [];
            $userCompanyPeopletab = AuthUser::find($userId)->userProfile->userCompany->people_tab;
            $oppData['opportunities'] = $this->doFormat($opportunityContact, $userCompanyPeopletab);
            return $this->doSuccessFormatDetails($oppData);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function updateOpportunities($request, $userId, $opportunityId)
    {
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;

            $opportunitiesDetail = $this->OpportunityRepositories->updateOpportunitiesDetails(
                $request,
                $userCompanyId,
                $opportunityId
            );

            if ($opportunitiesDetail) {
                $status = true;
                $message = config('constants.Success.EditOpportunities');
                /* Event Store Process */
                $eventName = config('constants.Event_name.opportunity_updated');
                $this->doEventAdd($request, $userId, $userCompanyId, $eventName);
                return $this->doUpdatedResultFormatDetails($status, $message);
            } else {
                $status = false;
                $message = config('constants.Errors.EditOpportunities');
                return $this->doUpdatedResultFormatDetails($status, $message);
            }
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    /* Delete Opportunity*/
    public function destroyOpportunity($request, $userId, $companyId, $opportunityId)
    {
        $this->companyId                     = $companyId;
        $this->userId                        = $userId;

        /* Validations */
        $validationResult = $this->opportunityIdVaidation($opportunityId);
        if ($validationResult['status'] == false) {
            $this->logEventsError($validationResult['message']);
            return $this->doErrorFormatDetails($validationResult['message']);
        }

        DB::beginTransaction();
        try {
            /* Remove Opportunity Contact Details */
            $this->OpportunityRepositories->removeOpportunityContact($userId, $companyId, $opportunityId);
            /* Remove Opportunity History Details */
            $this->OpportunityRepositories->removeOpportunityHistory($userId, $companyId, $opportunityId);
            /* Remove Opportunity Personnel Details */
            $this->OpportunityRepositories->removeOpportunityPersonnel($userId, $companyId, $opportunityId);
            /* Remove Opportunity Details */
            $removeOpportunity = $this->OpportunityRepositories->removeOpportunity($userId, $companyId, $opportunityId);
            $this->logEvents(config('constants.Success.RemoveOpportunity') . ' ' . $opportunityId);
            DB::commit();
            if ($removeOpportunity) {
                return $this->doSuccessFormatDetails(config('constants.Success.DeleteOpportunity'));
            } else {
                $this->logEventsError(config('constants.Errors.OpportunityNotFound'));
                $opportunityDetails['status'] = false;
                $opportunityDetails['message'] = config('constants.Errors.OpportunityNotFound');
                return $opportunityDetails;
            }
        } catch (Exception $e) {
            DB::rollback();
            $this->logEventsError($e->getMessage());
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }
    /* Validation for Id Has present or Not */
    public function opportunityIdVaidation($opportunityId)
    {
        $opportunityDetails['status'] = true;
        $opportunityDetails['message'] = '';
        $opportunityDetail = Opportunity::whereId($opportunityId)->exists();
        if ($opportunityDetail == false) {
            $this->logEventsError(config('constants.Errors.OpportunityId'));
            $opportunityDetails['status'] = false;
            $opportunityDetails['message'] = config('constants.Errors.OpportunityId');
            return $opportunityDetails;
        }

        return $opportunityDetails;
    }

    public function doFormat($dataList, $userCompanyPeopletab)
    {
        $opportunityData = [];
        foreach ($dataList as $dataKey => $data) {
            $opportunityData[$dataKey]['opportunity_id'] = ($data->id) ? $data->id : 0;
            $opportunityData[$dataKey]['contact_id'] = ($data->contact_id) ? $data->contact_id : 0;
            $opportunityData[$dataKey]['name'] = ($data->opp_name) ? $data->opp_name : '';
            $opportunityData[$dataKey]['stage'] = isset($data->oppStage->name) ? $data->oppStage->name : '';
            $opportunityData[$dataKey]['stage_id'] = isset($data->oppStage->id) ? $data->oppStage->id : 0;
            $opportunityData[$dataKey]['closing'] = ($data->close_date) ? Carbon::parse($data->close_date)->format('d-m-Y') : '';
            $opportunityData[$dataKey]['opportunity_type'] = isset($data->oppType->name) ? $data->oppType->name : '';
            $opportunityData[$dataKey]['opportunity_type_id'] = isset($data->oppType->id) ? $data->oppType->id : 0;
            $opportunityDefaultSign = config('constants.Company.opportunity_default_sign');
            $opportunityData[$dataKey]['opportunity_sign'] = ($data->company->opp_sign == null) ? $opportunityDefaultSign : (($data->company->opp_sign == '') ? $opportunityDefaultSign : $data->company->opp_sign);
            $opportunityData[$dataKey]['value'] = ($data->value) ? (float)$data->value : 0;
            $opportunityData[$dataKey]['prob'] = ($data->probability) ? $data->probability : 0;
            $oppHistoryValue = isset($data->OppHistory->value) ? $data->OppHistory->value : 0.00;
            $oppHistoryProbability = isset($data->OppHistory->probability) ? $data->OppHistory->probability : 0.00;
            $opportunityData[$dataKey]['derived'] = ($oppHistoryValue * $oppHistoryProbability / 100);
            $opportunityData[$dataKey]['owner'] = ($data->user->first_name) ? $data->user->first_name . ' ' . $data->user->last_name : '';
            $opportunityData[$dataKey]['owner_id'] = ($data->user->id) ? $data->user->id : 0;
            $opportunityPersonal = $data->oppPersonnel()->get();
            $opportunityContacts = $data->oppContact()->get();
            $personalData = [];
            $contactData = [];
            foreach ($opportunityPersonal as $key => $personal) {
                $personalData[$key]['id'] = ($personal->id);
                $personalData[$key]['name'] =  ($personal->first_name . ' ' . $personal->last_name);
            }
            foreach ($opportunityContacts as $key => $opportunityContact) {
                $contactData[$key]['id'] = ($opportunityContact->id);
                $contactData[$key]['name'] =  ($opportunityContact->first_name . ' ' . $opportunityContact->last_name);
            }
            $opportunityData[$dataKey]['linked_contacts'] =  $personalData;
            $opportunityData[$dataKey]['linked_accounts'] =  $contactData;
            $opportunityData[$dataKey]['note'] = ($data->notes) ? $data->notes : '';
        }
        return $opportunityData;
    }

    public function doSuccessFormatDetails($data)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response'] = $data;

        return $result;
    }

    public function doUpdatedResultFormatDetails($status, $message)
    {
        $result['status']  = $status;
        $result['message'] = $message;
        $result['response'] = '';
        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails;
        $result['response'] = '';

        return $result;
    }
    public function logEvents($messageDetails = null)
    {
        $opportunityLog['User_id']      = $this->userId;
        $opportunityLog['Company_id']   = $this->companyId;
        $opportunityLog['Message']      = $messageDetails;

        Log::notice($opportunityLog);

        return true;
    }

    public function logEventsError($messageDetails = null)
    {
        $opportunityLog['User_id']      = $this->userId;
        $opportunityLog['Company_id']   = $this->companyId;
        $opportunityLog['Message']      = $messageDetails;

        Log::error($opportunityLog);

        return true;
    }

    /* ===============================EVENT========================== */
    public function doEventAdd($request, $userId, $userCompanyId, $eventName)
    {
        /* Event Creation */
        $eventAddService        = new EventAddService();
        $eventAddService->addEvent(
            $eventName,
            $eventCompanyId          = $userCompanyId,
            $eventUserId             = $userId,
            $eventContactId          = null,
            $eventOtherUserId        = null,
            $eventCallId             = null,
            $eventStartTime          = null,
            $eventDuration           = null,
            $eventBadgeId            = null,
            $eventGooglePlace        = null,
            $eventTwilioCallId       = null,
            $eventContactEventFormId = null,
            $eventAppointmentId      = null,
            $eventFollowupId         = null,
            $eventMessage            = null
        );

        return true;
    }
    /* ===============================EVENT-END====================== */
}
