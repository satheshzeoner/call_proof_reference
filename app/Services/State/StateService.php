<?php

namespace App\Services\State;

use Exception;
use Carbon\Carbon;

/* Models */
use App\Models\Auth\AuthUser;
use App\Models\Contact\Contact;

/* Repositories */
use App\Repositories\State\StateRepositories;

class StateService
{
    public function __construct()
    {
        $this->StateRepositories = new StateRepositories();
    }

    public function getState($request)
    {
        try {

            $stateList = $this->StateRepositories->getStateDetails($request);
            return $this->doSuccessFormatDetails($stateList);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e);
        }
    }


    public function doSuccessFormatDetails($data)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response']['state'] = $data;

        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails->getMessage();
        $result['response'] = '';

        return $result;
    }
}
