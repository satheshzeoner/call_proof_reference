<?php

namespace App\Services\Places;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;

use Exception;

/* Repositories */
use App\Repositories\Places\PlacesRepositories;
use App\Repositories\Places\PlacesContactAddRepositories;
use App\Repositories\Contact\ContactRepositories;

class PlacesContactAddServices
{
    public function __construct()
    {
        $this->PlacesRepositories                  = new PlacesRepositories();
        $this->PlacesContactAddRepositories        = new PlacesContactAddRepositories();
        $this->ContactRepositories                 = new ContactRepositories();

        /* Initialze For LogSytem */
        $this->companyId                     = '';
        $this->userId                        = '';
    }

    /* Get List */
    public function doStorePlaceContact($request, $userId, $companyId)
    {
        $this->companyId                     = $companyId;
        $this->userId                        = $userId;

        /* Check this PlaceId Already Added in CallProof */
        $placeContactVerify = $this->PlacesRepositories->checkPlaceIdInContacts($companyId, $request['place_id']);
        if ($placeContactVerify == true) {
            return $this->doErrorFormatDetails('PlaceContactId Already Added');
        }

        /* Get Specific Place Contact Details */
        $placeContactDetail = collect($this->PlacesRepositories->getSpecificPlaceContactDetails($request['place_id']))->first();

        if (!isset($placeContactDetail)) {
            $this->logEventsError(config('constants.Errors.placesIdEmptyDetails'));
            return $this->doErrorFormatDetails(config('constants.Errors.AddPlaceContact'));
        }

        DB::beginTransaction();
        try {

            /* store ContactCompany details */
            $contactCompanyId = $this->doStoreContactCompany($companyId, $placeContactDetail);
            $this->logEvents(config('constants.Places.placesContactCompanyInserted') . '=>' . $contactCompanyId);

            /* store Contact Details */
            $contactId = $this->doStoreContact($userId, $companyId, $contactCompanyId, $placeContactDetail);
            $this->logEvents(config('constants.Places.placesContactInserted') . '=>' . $contactId);

            /* store Contact Phone Details ContactId , */
            $contactPhoneId = $this->doStoreContactPhone($userId, $companyId, $placeContactDetail, $contactId);
            $this->logEvents(config('constants.Places.placesContactPhoneInserted') . '=>' . $contactPhoneId);

            DB::commit();
            return $this->doSuccessFormatDetails(config('constants.Success.AddPlaceContact'));
        } catch (Exception $e) {
            DB::rollback();
            $this->logEventsError($e->getMessage());
            return $this->doErrorFormatDetails(config('constants.Errors.AddPlaceContact'));
        }
    }

    /* cp_contactcompany Format Details */
    public function doStoreContactCompany($companyId, $placeContactDetails)
    {
        $contactCompany['company_id']         = $companyId;
        $contactCompany['name']               = $placeContactDetails['company_name'];
        $contactCompany['created']            = now();

        $contactCompanyId = $this->PlacesContactAddRepositories->storeContactCompanyDetails($contactCompany);

        return $contactCompanyId;
    }

    /* cp_contact Format Details */
    public function doStoreContact($userId, $companyId, $contactCompanyId, $placeContactDetails)
    {
        /* Get Location Details for State & country Details */
        $locationDetails = $this->ContactRepositories->getReverseGeocode(
            $request = null,
            $placeContactDetails['latitude'],
            $placeContactDetails['longitude']
        );

        /* Get ContactType Id Prospect */
        $contactTypeDetails = $this->PlacesContactAddRepositories->getContactTypeDetails($companyId);

        $contact['contact_type_id']    = $contactTypeDetails;
        $contact['contact_company_id'] = $contactCompanyId;
        $contact['name']               = $placeContactDetails['contact_name'];
        $contact['title']              = $placeContactDetails['contact_name'];
        $contact['zip']                = $placeContactDetails['zip_code'];
        $contact['state_id']           = $locationDetails['state_id'];
        $contact['country_id']         = $locationDetails['country_id'];
        $contact['phone_number']       = $placeContactDetails['phone_number'];
        $contact['latitude']           = $placeContactDetails['latitude'];
        $contact['longitude']          = $placeContactDetails['longitude'];
        $contact['website']            = $placeContactDetails['website'];
        $contact['address']            = $placeContactDetails['address'];
        $contact['business_type']      = $placeContactDetails['business_type'];
        $contact['county']             = $placeContactDetails['county'];
        $contact['annual_sales']       = $placeContactDetails['annual_sales'];
        $contact['place_id']           = $placeContactDetails['id'];
        $contact['employee_count']     = $placeContactDetails['employee_count'];
        $contact['company_id']         = $companyId;
        $contact['created_by_id']      = $userId;
        $contact['user_id']            = $userId;
        $contact['created']            = now();
        $contact['updated']            = now();
        $contact['unknown']            = false;
        $contact['do_not_sms']         = false;
        $contact['assigned']           = false;

        $contactCompanyId = $this->PlacesContactAddRepositories->storeContactDetails($contact);

        return $contactCompanyId;
    }

    /* cp_contactphone Format Details */
    public function doStoreContactPhone($userId, $companyId, $placeContactDetails, $contactId)
    {
        $contactPhone['contact_id']     = $contactId;
        $contactPhone['phone_type_id']  = config('constants.Places.placesContactPhoneTypeId');
        $contactPhone['phone_number']   = $placeContactDetails['phone_number'];
        $contactPhone['created']        = now();
        $contactPhone['company_id']     = $companyId;
        $contactPhone['unknown']        = config('constants.Places.placesContactUnknown');
        $contactPhone['is_active_lead'] = false;
        $contactPhone['is_in_dnc']      = false;
        $contactPhone['is_in_dnd']      = false;
        $contactPhone['user_id']        = $userId;

        $contactPhoneId = $this->PlacesContactAddRepositories->storeContactPhoneDetails($contactPhone);

        return $contactPhoneId;
    }

    public function doSuccessFormatDetails($placesDetails)
    {
        $result['status']                      = true;
        $result['message']                     = 'Success';
        $result['response'] = $placesDetails;

        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails;
        $result['response'] = '';

        return $result;
    }

    public function logEvents($messageDetails = null)
    {
        $placeContactAddLog['User_id']      = $this->userId;
        $placeContactAddLog['Company_id']   = $this->companyId;
        $placeContactAddLog['Message']      = $messageDetails;

        Log::notice($placeContactAddLog);

        return true;
    }

    public function logEventsError($messageDetails = null)
    {
        $placeContactAddLog['User_id']      = $this->userId;
        $placeContactAddLog['Company_id']   = $this->companyId;
        $placeContactAddLog['Message']      = $messageDetails;

        Log::error($placeContactAddLog);

        return true;
    }
}
