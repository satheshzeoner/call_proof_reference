<?php

namespace App\Services\Places;

use App\Helpers\Helper;

use Exception;

/* Repositories */
use App\Repositories\Places\PlacesRepositories;
use App\Repositories\Places\PlacesNearestRepositories;
use App\Repositories\Contact\ContactRepositories;


class PlacesServices
{
    public function __construct()
    {
        $this->PlacesRepositories        = new PlacesRepositories();
        $this->PlacesNearestRepositories = new PlacesNearestRepositories();
        $this->ContactRepositories       = new ContactRepositories();
    }

    /* Get List*/
    public function getPlacesCategoryList($request, $userId, $companyId)
    {
        try {
            /* Get Category List Id */
            $placeCategoryListDetails = $this->PlacesRepositories->getPlaceCategoryList($userId, $companyId);

            /* Get Places Details */
            $placeCategoryDetails = $this->PlacesRepositories->getPlaceCategoryDetails($userId, $companyId, $placeCategoryListDetails->id);

            $placeCategoryResult  = $this->doFormattingPlacesCat($placeCategoryDetails);

            return $this->doSuccessFormatDetails($placeCategoryResult);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e->getMessage());
        }
    }

    public function getNearestPlacesContactList($request, $userId, $companyId, $latitude, $longitude)
    {
        /* Validations */
        $placeContactValidationResult = $this->placesContactValidations($latitude, $longitude);
        if ($placeContactValidationResult['status'] == false) {
            return $this->doErrorFormatDetails($placeContactValidationResult['message']);
        }

        try {
            /* lat & lon Details */
            $cordinateDetails = $this->ContactRepositories->getReverseGeocode($request, $latitude, $longitude);

            /* Get Category List Id */
            $placeNearestPlaceDetails = $this->PlacesNearestRepositories->getNearestPlaceContactDetails($request, $userId, $companyId, $latitude, $longitude, strtolower($cordinateDetails['state']));

            /* Formatting Details Contacts */
            $placeContactResult = $this->doFormattingPlaceContactDetails($placeNearestPlaceDetails, $companyId);

            return $this->doSuccessFormatWithPagignationDetails($placeNearestPlaceDetails, $placeContactResult);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e);
        }
    }

    public function placesContactValidations($latitude, $longitude)
    {
        $validation['status']  = true;
        $validation['message'] = '';

        /* Empty Validations */
        if ($latitude == '' || $longitude == '') {
            $validation['status']  = false;
            $validation['message'] = config('constants.Errors.LatitudeAndLongitudeRequired');
            return $validation;
        }

        /* Proper Cordination Check */
        if (!preg_match('/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/', $latitude)) {
            $validation['status']  = false;
            $validation['message'] = config('constants.Errors.latitude');
            return $validation;
        }

        if (!preg_match('/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/', $longitude)) {
            $validation['status']  = false;
            $validation['message'] = config('constants.Errors.longitude');
            return $validation;
        }

        return $validation;
    }

    /* Places Contacts Details List */
    public function doFormattingPlaceContactDetails($placeNearestPlaceDetails, $companyId)
    {
        $placeContactsResult = [];

        /* Sort By Distance ASC ORDER */
        $placeNearestPlaceDetails = collect($placeNearestPlaceDetails['data'])->sortBy('distance')->toArray();

        foreach ($placeNearestPlaceDetails as $placeNearestPlaceDetailsvalue) {

            $placeContact['place_contact_id']             = $placeNearestPlaceDetailsvalue['id'];
            $placeContact['place_contact_distance']       = (float)number_format($placeNearestPlaceDetailsvalue['distance'], 2);
            $placeContact['place_contact_company_name']   = trim($placeNearestPlaceDetailsvalue['company_name']);
            $placeContact['place_contact_contact_name']   = trim($placeNearestPlaceDetailsvalue['contact_name']);
            $placeContact['place_contact_address']        = trim($placeNearestPlaceDetailsvalue['address']);
            $placeContact['place_contact_annual_sales']   = trim($placeNearestPlaceDetailsvalue['annual_sales']);
            $placeContact['place_contact_city']           = trim($placeNearestPlaceDetailsvalue['city']);
            $placeContact['place_contact_county']         = trim($placeNearestPlaceDetailsvalue['county']);
            $placeContact['place_contact_employee_code']  = (int)trim($placeNearestPlaceDetailsvalue['employee_code']);
            $placeContact['place_contact_employee_count'] = trim($placeNearestPlaceDetailsvalue['employee_count']);
            $placeContact['place_contact_fax']            = trim($placeNearestPlaceDetailsvalue['fax']);
            $placeContact['place_contact_gender']         = trim($placeNearestPlaceDetailsvalue['gender']);
            $placeContact['place_contact_business_type']  = trim($placeNearestPlaceDetailsvalue['business_type']);
            $placeContact['place_contact_latitude']       = (float)trim($placeNearestPlaceDetailsvalue['latitude']);
            $placeContact['place_contact_longitude']      = (float)trim($placeNearestPlaceDetailsvalue['longitude']);
            $placeContact['place_contact_phone_number']   = trim($this->getPhoneFormattedAttribute($placeNearestPlaceDetailsvalue['phone_number']));
            $placeContact['place_contact_sales_code']     = trim($placeNearestPlaceDetailsvalue['sales_code']);
            $placeContact['place_contact_sic_code']       = trim($placeNearestPlaceDetailsvalue['sic_code']);
            $placeContact['place_contact_state_code']     = trim($placeNearestPlaceDetailsvalue['state_code']);
            $placeContact['place_contact_title']          = trim($placeNearestPlaceDetailsvalue['title']);
            $placeContact['place_contact_website']        = trim($placeNearestPlaceDetailsvalue['website']);
            $placeContact['place_contact_zip_code']       = trim($placeNearestPlaceDetailsvalue['zip_code']);
            $placeContact['place_contact_geom']           = trim($placeNearestPlaceDetailsvalue['geom']);

            /* Check Places Contacts in CallProof Contacts */
            $placeContact['place_contact_exists_status']  = $this->PlacesNearestRepositories->checkPlacesContactInCallProof($placeNearestPlaceDetailsvalue['id'], $companyId);

            /* Define Range for Nearest Places */
            if ($placeNearestPlaceDetailsvalue['distance'] < config('constants.Places.limitRadius') && $placeNearestPlaceDetailsvalue['distance'] > 0) {
                array_push($placeContactsResult, $placeContact);
            }
        }

        return $placeContactsResult;
    }

    /* Transform Formatting Places Category Details */
    public function doFormattingPlacesCat($placesDetails)
    {
        $placeCategoryList = [];
        foreach ($placesDetails as  $placesDetailsValue) {
            $placeCategoryDetails['place_id']       = $placesDetailsValue['id'];
            $placeCategoryDetails['place_name']     = $placesDetailsValue['name'];
            $placeCategoryDetails['place_position'] = $placesDetailsValue['position'];
            array_push($placeCategoryList, $placeCategoryDetails);
        }
        return $placeCategoryList;
    }

    /* PhoneNumber Formatting */
    public function getPhoneFormattedAttribute($phone)
    {
        if ($phone) {
            $phone = preg_replace("/[^0-9]/", "", $phone); //Remove all non-numeric
            return substr($phone, 0, 3) . "-" . substr($phone, 3, 3) . "-" . substr($phone, 6);
        }
        return '';
    }

    public function doSuccessFormatWithPagignationDetails($placeNearestPlaceDetails, $placeContactResult)
    {
        $result['status']            = true;
        $result['message']           = 'Success';
        $result['response']['places_contacts']         = $placeContactResult;
        $result['response']['paginationDetails'] = Helper::customPagination($placeNearestPlaceDetails);

        return $result;
    }

    public function doSuccessFormatDetails($placesDetails)
    {
        $result['status']                      = true;
        $result['message']                     = 'Success';
        $result['response']['places_category'] = $placesDetails;

        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails;
        $result['response'] = '';

        return $result;
    }
}
