<?php

namespace App\Services\PhoneType;

use Exception;

/* Repositories */
use App\Repositories\PhoneType\PhoneTypeRepositories;

class PhoneTypeService
{
    public function __construct()
    {
        $this->PhoneTypeRepositories = new PhoneTypeRepositories();
    }

    public function getPhoneType($request)
    {
        try {

            $phoneTypeList = $this->PhoneTypeRepositories->getPhoneTypeDetails($request);
            return $this->doSuccessFormatDetails($phoneTypeList);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e);
        }
    }

    public function doSuccessFormatDetails($data)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response']['phone_type'] = $data;

        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails->getMessage();
        $result['response'] = '';

        return $result;
    }
}
