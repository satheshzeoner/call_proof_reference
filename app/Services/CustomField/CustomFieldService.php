<?php

namespace App\Services\CustomField;

use App\Helpers\Helper;
use Illuminate\Support\Facades\Log;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

/* Models */
use App\Models\Contact\Contact;
use App\Models\Eventform\Eventform;
use App\Models\Cfield\Cfield;

/* Repository */
use App\Repositories\CustomField\CustomFieldRepositories;

/* FieldType Details */
use App\Services\EventForm\EventFormFieldTypeService;


class CustomFieldService
{
    public function __construct()
    {
        $this->CustomFieldRepositories         = new CustomFieldRepositories();
        $this->EventFieldTypeService           = new EventFormFieldTypeService();

        /* Initialze For LogSytem */
        $this->companyId                     = '';
        $this->userId                        = '';
        $this->contactId                     = '';
    }


    public function doUpdateCustomFieldsProcessing($request, $userId, $companyId, $entityId, $formDetails)
    {
        /* initial Status  */
        $result['status'] = true;
        $result['message'] = '';

        /* customField Values Store */
        $customFieldResult = $this->updateCustomFieldValues($request, $userId, $companyId, $entityId, $formDetails);
        if ($customFieldResult['status'] == false) {
            return $customFieldResult;
        }

        return $result;
    }

    /* Store Dynamic CustomField with Value */
    public function updateCustomFieldValues($request, $userId, $companyId, $entityId, $formDetails)
    {
        foreach ($formDetails as $formDetailsValue) {

            /* TextField */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.text')) {
                $fieldResult = $this->EventFieldTypeService->textField($formDetailsValue);
            }
            /* SelectField */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.select')) {
                $fieldResult = $this->EventFieldTypeService->selectField($formDetailsValue);
            }
            /* RadioField */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.radio')) {
                $fieldResult = $this->EventFieldTypeService->RadioField($formDetailsValue);
            }
            /* checkbox */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.checkbox')) {
                $fieldResult = $this->EventFieldTypeService->checkboxField($formDetailsValue);
            }
            /* textarea */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.textarea')) {
                $fieldResult = $this->EventFieldTypeService->textareaField($formDetailsValue);
            }
            /* date */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.date')) {
                $fieldResult = $this->EventFieldTypeService->dateField($formDetailsValue);
            }
            /* time */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.time')) {
                $fieldResult = $this->EventFieldTypeService->timeField($formDetailsValue);
            }
            /* datetime */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.datetime')) {
                $fieldResult = $this->EventFieldTypeService->datetimeField($formDetailsValue);
            }
            /* integer */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.integer')) {
                $fieldResult = $this->EventFieldTypeService->integerField($formDetailsValue);
            }
            /* decimal */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.decimal')) {
                $fieldResult = $this->EventFieldTypeService->decimalField($formDetailsValue);
            }
            /* auto_integer */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.autoInteger')) {
                $fieldResult = $this->EventFieldTypeService->autoIntegerField($formDetailsValue);
            }
            /* image */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.image')) {
                $fieldResult = $this->EventFieldTypeService->imageField($formDetailsValue, $request);
            }
            /* multiple_select */
            if ($formDetailsValue['custom_field_type_id'] == config('constants.EventFieldType.multipleSelect')) {
                $fieldResult = $this->EventFieldTypeService->multipleSelectField($formDetailsValue);
            }

            /* Store in customField Details */
            if ($fieldResult['status'] == true) {

                $customField['cfield_id'] = $formDetailsValue['custom_field_id'];
                $customField['entity_id'] = $entityId;
                $customField['cf_value']  = $fieldResult['custom_field_value'];
                $customField['updated']   = now();

                /* store CustomFieldValues */
                $this->CustomFieldRepositories->updateCustomFieldValue($customField);
            }

            /* End of Section */
            if ($fieldResult['status'] == false) {
                return $fieldResult;
                break;
            }
        }

        $finalCustomFieldResult['status'] = true;
        $finalCustomFieldResult['message'] = '';

        return $finalCustomFieldResult;
    }

    public function doSuccessFormat($responseResult)
    {
        $result['status']   = true;
        $result['message']  = 'Success';
        $result['response'] = $responseResult;
        return $result;
    }

    public function doErrorFormat($responseResult)
    {
        Helper::logError($responseResult, 401, $requestdetails = null, config('constants.ServiceName.Event'));
        $result['status']   = false;
        $result['message']  = $responseResult;
        $result['response'] = '';
        return $result;
    }
}
