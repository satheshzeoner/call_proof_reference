<?php

namespace App\Services\Country;

use Exception;

/* Repositories */
use App\Repositories\Country\CountryRepositories;

class CountryService
{
    public function __construct()
    {
        $this->CountryRepositories = new CountryRepositories();
    }

    public function getCountry($request)
    {
        try {

            $countryList = $this->CountryRepositories->getCountryDetails($request);
            return $this->doSuccessFormatDetails($countryList);
        } catch (Exception $e) {
            return $this->doErrorFormatDetails($e);
        }
    }

    public function doSuccessFormatDetails($data)
    {
        $result['status'] = true;
        $result['message'] = 'Success';
        $result['response']['country'] = $data;

        return $result;
    }

    public function doErrorFormatDetails($messageDeatails)
    {
        $result['status']   = false;
        $result['message']  = $messageDeatails->getMessage();
        $result['response'] = '';

        return $result;
    }
}
