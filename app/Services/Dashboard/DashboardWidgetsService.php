<?php

namespace App\Services\Dashboard;

use Carbon\Carbon;

/* Models */
use App\Models\Widget\WidgetList;

/* Repositories */
use App\Repositories\Dashboard\DashboardWidgetsRepositories;

class DashboardWidgetsService
{
    public function __construct()
    {
        $this->DashboardWidgetsRepositories = new DashboardWidgetsRepositories();
    }

    /* ============================== GET Widgets ============================== */

    /* Get Active or Enabled Widgets Details */
    public function getWidgetsOrderingActive($userId)
    {
        $widgetsActiveResult = $this->DashboardWidgetsRepositories->getActiveWidgetsOrder($userId);

        return $widgetsActiveResult;
    }

    /* Get In-Active or Disabled Enabled Widgets Details */
    public function getWidgetsOrderingInActive($userId)
    {
        $widgetsInActiveResult = $this->DashboardWidgetsRepositories->getInActiveWidgetsOrder($userId);

        return $widgetsInActiveResult;
    }

    /* ==============================POST Widgets============================== */

    /* do Store the WidgetOrdering Details to Table */
    public function doStoreWidgetPosition($userId, $activeOrder, $inactiveOrder, $position)
    {

        /* Check if This Already Exist */
        $widgetDetails = $this->DashboardWidgetsRepositories->findByUserId($userId);

        /* if There is No Data in DB then Create Initail Rows and Return */
        if (!$widgetDetails) {
            $resultWidgetStore = $this->DashboardWidgetsRepositories->doWidgetInitialStore($userId);
            if ($resultWidgetStore == true) {
                $widgetDetails = $this->DashboardWidgetsRepositories->findByUserId($userId);
            }
        }

        if ($activeOrder != '') {
            foreach ($activeOrder as $widgetActiveListkey => $widgetActiveListvalue) {
                $this->DashboardWidgetsRepositories->doActiveUpdatewidget($widgetActiveListvalue, $position[$widgetActiveListkey]);
            }
        }

        if ($inactiveOrder != '') {
            foreach ($inactiveOrder as $widgetInactiveListvalue) {
                $position = 0;
                if ($widgetInactiveListvalue != null) {
                    $this->DashboardWidgetsRepositories->doInActiveUpdatewidget($widgetInactiveListvalue, $position);
                }
            }
        }

        return true;
    }

    /* Validations for All Param Array */
    public function doValidationWidgets($activeOrder, $inactiveOrder, $position)
    {
        $totalWidgets = WidgetList::count();

        /* Check where widgets and widgetsAvail */
        if ($activeOrder == '' && $inactiveOrder == '') {
            $result['status'] = false;
            $result['message'] = config('constants.Errors.widgetBothEmpty');
            return $result;
        }

        $countOfActiveOrder   = ($activeOrder != '') ? count($activeOrder) : 0;
        $countOfInActiveOrder = ($inactiveOrder != '') ? count($inactiveOrder) : 0;
        $countOfPosition      = ($position != '') ? count($position) : 0;

        if ($countOfActiveOrder != $totalWidgets && $inactiveOrder == '') {
            $result['status'] = false;
            $result['message'] = config('constants.Errors.widgetAvailableRequired');
            return $result;
        }

        if ($countOfInActiveOrder != $totalWidgets && $activeOrder == '') {
            $result['status'] = false;
            $result['message'] = config('constants.Errors.widgetRequired');
            return $result;
        }

        if ($countOfActiveOrder != $countOfPosition) {
            $result['status'] = false;
            $result['message'] = config('constants.Errors.widgetMismatch');
            return $result;
        }

        $duplicateCheck = collect($activeOrder)->intersect($inactiveOrder)->count();

        if ($duplicateCheck != 0) {
            $result['status'] = false;
            $result['message'] = config('constants.Errors.widgetDuplicate');
            return $result;
        }

        $result['status'] = true;
        return $result;
    }
}
