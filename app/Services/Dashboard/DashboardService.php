<?php

namespace App\Services\Dashboard;

use Carbon\Carbon;

/* Models */
// use App\Models\Event\Event;
use App\Models\Widget\Widget;

/* Repositories */
use App\Repositories\Dashboard\DashboardRepositories;
use App\Repositories\Dashboard\DashboardWidgetsRepositories;

class DashboardService
{

    public function __construct()
    {
        $this->DashboardRepositories        = new DashboardRepositories();
        $this->DashboardWidgetsRepositories = new DashboardWidgetsRepositories();
        $this->chartCapacitySize            = 5;
        $this->callsTimeframeColumn         = 'start';
        $this->emailTimeframeColumn         = 'created';
        $this->appointmentTimeframeColumn   = 'start';
        $this->widgetAppointment            = 'Appointments';
        $this->widgetCalls                  = 'Calls';
        $this->widgetEmails                 = 'Emails';
    }

    /* Get getEventdetails Array of Today and Yesterday */
    public function getEventdetails($userId)
    {
        $eventYesterday["day"]               = 'Yesterday';
        $eventYesterday["date"]              = Carbon::yesterday()->format('M d');
        $eventYesterday["call_count"]        = $this->DashboardRepositories->getCountForYesterdayCall($userId);
        $eventYesterday["appointment_count"] = $this->DashboardRepositories->getCountForYesterdayAppointment($userId);
        $eventYesterday["email_count"]       = $this->DashboardRepositories->getEmailmsgYesterdayCount($userId);

        $eventToday["day"]                   = 'Today';
        $eventToday["date"]                  = Carbon::today()->format('M d');
        $eventToday["call_count"]            = $this->DashboardRepositories->getCountForTodayCall($userId);
        $eventToday["appointment_count"]     = $this->DashboardRepositories->getCountForTodayAppointment($userId);
        $eventToday["email_count"]           = $this->DashboardRepositories->getEmailmsgTodayCount($userId);

        $event = [];
        array_push($event, $eventYesterday);
        array_push($event, $eventToday);

        return $event;
    }

    /* Get Set of WidgetsOrder Array Form */
    public function getWidgetsOrder($activeOrder)
    {
        $widgetOrder['widgets']           = $activeOrder;
        return $widgetOrder;
    }

    /* Get Set of getWidgets Three Set Of Arrays function */
    public function getWidgets($userId, $dates)
    {
        $Widgets = [];

        /* storeInTables */
        $widgetOrder = $this->storeWidgetOrdering($userId);

        foreach ($widgetOrder as $widgetOrderkey => $widgetOrdervalue) {

            if ($widgetOrdervalue['name'] == $this->widgetAppointment) {
                $wedgetAppointmentsSet['event'] = $this->widgetAppointment;
                $wedgetAppointmentsSet['info']  = $this->infoEventAppointment($userId, $dates);
                array_push($Widgets, $wedgetAppointmentsSet);
            }

            if ($widgetOrdervalue['name'] == $this->widgetCalls) {
                $wedgetCallsSet['event']        = $this->widgetCalls;
                $wedgetCallsSet['info']         = $this->infoEventCalls($userId, $dates);
                array_push($Widgets, $wedgetCallsSet);
            }
            if ($widgetOrdervalue['name'] == $this->widgetEmails) {
                $wedgetEmailsSet['event']       = $this->widgetEmails;
                $wedgetEmailsSet['info']        = $this->infoEventEmails($userId, $dates);
                array_push($Widgets, $wedgetEmailsSet);
            }
        }

        return $Widgets;
    }

    /* Get Appointment Details for Given UserId and Dates */
    public function infoEventAppointment($userId, $dates)
    {
        $progressCallsDetails = $this->DashboardRepositories->getAppointmentCallsDetailsBasedonDates($userId, $dates['progressFromDate'], $dates['progressToDate']);
        $previousCallsDetails = $this->DashboardRepositories->getAppointmentCallsDetailsBasedonDates($userId, $dates['previousFromDate'], $dates['previousToDate']);

        $chartPack['current_data']  = $this->doChartProgress($progressCallsDetails, $dates['progressFromDate'], $dates['hours'], $this->appointmentTimeframeColumn);
        $chartPack['previous_data'] = $this->doChartProgress($previousCallsDetails, $dates['previousFromDate'], $dates['hours'], $this->appointmentTimeframeColumn);

        $countProgressCalls = count($progressCallsDetails);
        $countPreviousCalls = count($previousCallsDetails);
        $percentage         = $this->percentageCalculation($countPreviousCalls, $countProgressCalls);

        $appointment["title"]                = 'My Appointments';
        $appointment["progress_total_calls"] = $countProgressCalls;
        $appointment["progress_from_date"]   = Carbon::create($dates['progressFromDate'])->format('M d');
        $appointment["progress_to_date"]     = Carbon::create($dates['progressToDate'])->format('M d');
        $appointment["previous_total_calls"] = $countPreviousCalls;
        $appointment["previous_from_date"]   = Carbon::create($dates['previousFromDate'])->format('M d');
        $appointment["previous_to_date"]     = Carbon::create($dates['previousToDate'])->format('M d');
        $appointment["percentage"]           = $percentage;
        $appointment["chart_data"]           = $chartPack;

        $appointmentResult = [];
        array_push($appointmentResult, $appointment);

        return $appointmentResult;
    }

    public function infoEventCalls($userId, $dates)
    {
        /* All Calls Details */
        $progressCallsDetails = $this->DashboardRepositories->getCallsDetailsBasedonDates($userId, $dates['progressFromDate'], $dates['progressToDate']);
        $previousCallsDetails = $this->DashboardRepositories->getCallsDetailsBasedonDates($userId, $dates['previousFromDate'], $dates['previousToDate']);

        /* My Outbound Phone Calls */
        $myOutboundPhoneCalls = $this->getMyoutboundPhoneCalls($dates, $progressCallsDetails, $previousCallsDetails);
        $myInboundPhoneCalls  = $this->getMyinboundPhoneCalls($dates, $progressCallsDetails, $previousCallsDetails);
        $myAllCalls           = $this->getAllCalls($dates, $progressCallsDetails, $previousCallsDetails);

        $eventCalls = [];
        array_push($eventCalls, $myOutboundPhoneCalls);
        array_push($eventCalls, $myInboundPhoneCalls);
        array_push($eventCalls, $myAllCalls);

        return $eventCalls;
    }

    /* -----------------------------------------CALLS----------------------------------------- */
    public function getMyinboundPhoneCalls($dates, $progressCallsDetails, $previousCallsDetails)
    {
        $currentProgressData = collect($progressCallsDetails)->where('call_type_id', 1);
        $currentPreviousData = collect($previousCallsDetails)->where('call_type_id', 1);

        $chartPack['current_data']  = $this->doChartProgress($currentProgressData, $dates['progressFromDate'], $dates['hours'], $this->callsTimeframeColumn);
        $chartPack['previous_data'] = $this->doChartProgress($currentPreviousData, $dates['previousFromDate'], $dates['hours'], $this->callsTimeframeColumn);

        $countProgressCalls = count($currentProgressData);
        $countPreviousCalls = count($currentPreviousData);
        $percentage         = $this->percentageCalculation($countPreviousCalls, $countProgressCalls);

        $myoutboundPhoneCalls["title"]                = 'My Inbound Phone Calls';
        $myoutboundPhoneCalls["progress_total_calls"] = $countProgressCalls;
        $myoutboundPhoneCalls["progress_from_date"]   = Carbon::create($dates['progressFromDate'])->format('M d');
        $myoutboundPhoneCalls["progress_to_date"]     = Carbon::create($dates['progressToDate'])->format('M d');
        $myoutboundPhoneCalls["previous_total_calls"] = $countPreviousCalls;
        $myoutboundPhoneCalls["previous_from_date"]   = Carbon::create($dates['previousFromDate'])->format('M d');
        $myoutboundPhoneCalls["previous_to_date"]     = Carbon::create($dates['previousToDate'])->format('M d');
        $myoutboundPhoneCalls["percentage"]           = $percentage;
        $myoutboundPhoneCalls["chart_data"]           = $chartPack;

        return $myoutboundPhoneCalls;
    }

    public function getMyoutboundPhoneCalls($dates, $progressCallsDetails, $previousCallsDetails)
    {

        $currentProgressData = collect($progressCallsDetails)->where('call_type_id', 2);
        $currentPreviousData = collect($previousCallsDetails)->where('call_type_id', 2);

        $chartPack['current_data']  = $this->doChartProgress($currentProgressData, $dates['progressFromDate'], $dates['hours'], $this->callsTimeframeColumn);
        $chartPack['previous_data'] = $this->doChartProgress($currentPreviousData, $dates['previousFromDate'], $dates['hours'], $this->callsTimeframeColumn);

        $countProgressCalls = count($currentProgressData);
        $countPreviousCalls = count($currentPreviousData);
        $percentage         = $this->percentageCalculation($countPreviousCalls, $countProgressCalls);

        $myoutboundPhoneCalls["title"]                = 'My Outbound Phone Calls';
        $myoutboundPhoneCalls["progress_total_calls"] = $countProgressCalls;
        $myoutboundPhoneCalls["progress_from_date"]   = Carbon::create($dates['progressFromDate'])->format('M d');
        $myoutboundPhoneCalls["progress_to_date"]     = Carbon::create($dates['progressToDate'])->format('M d');
        $myoutboundPhoneCalls["previous_total_calls"] = $countPreviousCalls;
        $myoutboundPhoneCalls["previous_from_date"]   = Carbon::create($dates['previousFromDate'])->format('M d');
        $myoutboundPhoneCalls["previous_to_date"]     = Carbon::create($dates['previousToDate'])->format('M d');
        $myoutboundPhoneCalls["percentage"]           = $percentage;
        $myoutboundPhoneCalls["chart_data"]           = $chartPack;

        return $myoutboundPhoneCalls;
    }

    public function getAllCalls($dates, $progressCallsDetails, $previousCallsDetails)
    {
        $chartPack['current_data']  = $this->doChartProgress($progressCallsDetails, $dates['progressFromDate'], $dates['hours'], $this->callsTimeframeColumn);
        $chartPack['previous_data'] = $this->doChartProgress($previousCallsDetails, $dates['previousFromDate'], $dates['hours'], $this->callsTimeframeColumn);

        $countProgressCalls = count($progressCallsDetails);
        $countPreviousCalls = count($previousCallsDetails);
        $percentage         = $this->percentageCalculation($countPreviousCalls, $countProgressCalls);

        $allCalls["title"]                = 'All Calls';
        $allCalls["progress_total_calls"] = $countProgressCalls;
        $allCalls["progress_from_date"]   = Carbon::create($dates['progressFromDate'])->format('M d');
        $allCalls["progress_to_date"]     = Carbon::create($dates['progressToDate'])->format('M d');
        $allCalls["previous_total_calls"] = $countPreviousCalls;
        $allCalls["previous_from_date"]   = Carbon::create($dates['previousFromDate'])->format('M d');
        $allCalls["previous_to_date"]     = Carbon::create($dates['previousToDate'])->format('M d');
        $allCalls["percentage"]           = $percentage;
        $allCalls["chart_data"]           = $chartPack;

        return $allCalls;
    }

    public function infoEventEmails($userId, $dates)
    {
        /* All Email Details */
        $progressEmailDetails = $this->DashboardRepositories->getEmailDetailsBasedonDates($userId, $dates['progressFromDate'], $dates['progressToDate']);
        $previousEmailDetails = $this->DashboardRepositories->getEmailDetailsBasedonDates($userId, $dates['previousFromDate'], $dates['previousToDate']);

        /* My Outbound Phone Email */
        $myOutboundPhoneEmail = $this->getMyoutboundPhoneEmail($dates, $progressEmailDetails, $previousEmailDetails);
        $myInboundPhoneEmail  = $this->getMyinboundPhoneEmail($dates, $progressEmailDetails, $previousEmailDetails);
        $myAllEmail           = $this->getAllEmail($dates, $progressEmailDetails, $previousEmailDetails);

        $eventEmail = [];
        array_push($eventEmail, $myOutboundPhoneEmail);
        array_push($eventEmail, $myInboundPhoneEmail);
        array_push($eventEmail, $myAllEmail);

        return $eventEmail;
    }

    public function getMyoutboundPhoneEmail($dates, $progressEmailDetails, $previousEmailDetails)
    {
        /* email_status_id = 3 as Sent */
        $currentProgressData = collect($progressEmailDetails)->where('email_status_id', 3);
        $currentPreviousData = collect($previousEmailDetails)->where('email_status_id', 3);

        $chartPack['current_data']  = $this->doChartProgress($currentProgressData, $dates['progressFromDate'], $dates['hours'], $this->emailTimeframeColumn);
        $chartPack['previous_data'] = $this->doChartProgress($currentPreviousData, $dates['previousFromDate'], $dates['hours'], $this->emailTimeframeColumn);

        $countProgressEmails = count($currentProgressData);
        $countPreviousEmails = count($currentPreviousData);
        $percentage         = $this->percentageCalculation($countPreviousEmails, $countProgressEmails);

        $myoutboundPhoneCalls["title"]                = 'My Outbound Emails';
        $myoutboundPhoneCalls["progress_total_calls"] = $countProgressEmails;
        $myoutboundPhoneCalls["progress_from_date"]   = Carbon::create($dates['progressFromDate'])->format('M d');
        $myoutboundPhoneCalls["progress_to_date"]     = Carbon::create($dates['progressToDate'])->format('M d');
        $myoutboundPhoneCalls["previous_total_calls"] = $countPreviousEmails;
        $myoutboundPhoneCalls["previous_from_date"]   = Carbon::create($dates['previousFromDate'])->format('M d');
        $myoutboundPhoneCalls["previous_to_date"]     = Carbon::create($dates['previousToDate'])->format('M d');
        $myoutboundPhoneCalls["percentage"]           = $percentage;
        $myoutboundPhoneCalls["chart_data"]           = $chartPack;

        return $myoutboundPhoneCalls;
    }

    public function getMyinboundPhoneEmail($dates, $progressEmailDetails, $previousEmailDetails)
    {
        /* email_status_id = 3 as Queued */
        $currentProgressData = collect($progressEmailDetails)->where('email_status_id', 2);
        $currentPreviousData = collect($previousEmailDetails)->where('email_status_id', 2);

        $chartPack['current_data']  = $this->doChartProgress($currentProgressData, $dates['progressFromDate'], $dates['hours'], $this->emailTimeframeColumn);
        $chartPack['previous_data'] = $this->doChartProgress($currentPreviousData, $dates['previousFromDate'], $dates['hours'], $this->emailTimeframeColumn);

        $countProgressCalls = count($currentProgressData);
        $countPreviousCalls = count($currentPreviousData);
        $percentage         = $this->percentageCalculation($countPreviousCalls, $countProgressCalls);

        $myoutboundPhoneCalls["title"]                = 'My Inbound Emails';
        $myoutboundPhoneCalls["progress_total_calls"] = $countProgressCalls;
        $myoutboundPhoneCalls["progress_from_date"]   = Carbon::create($dates['progressFromDate'])->format('M d');
        $myoutboundPhoneCalls["progress_to_date"]     = Carbon::create($dates['progressToDate'])->format('M d');
        $myoutboundPhoneCalls["previous_total_calls"] = $countPreviousCalls;
        $myoutboundPhoneCalls["previous_from_date"]   = Carbon::create($dates['previousFromDate'])->format('M d');
        $myoutboundPhoneCalls["previous_to_date"]     = Carbon::create($dates['previousToDate'])->format('M d');
        $myoutboundPhoneCalls["percentage"]           = $percentage;
        $myoutboundPhoneCalls["chart_data"]           = $chartPack;

        return $myoutboundPhoneCalls;
    }

    public function getAllEmail($dates, $progressEmailDetails, $previousEmailDetails)
    {
        /* All Calls From Email */
        $chartPack['current_data']  = $this->doChartProgress($progressEmailDetails, $dates['progressFromDate'], $dates['hours'], $this->emailTimeframeColumn);
        $chartPack['previous_data'] = $this->doChartProgress($previousEmailDetails, $dates['previousFromDate'], $dates['hours'], $this->emailTimeframeColumn);

        $countProgressEmail = count($progressEmailDetails);
        $countPreviousEmail = count($previousEmailDetails);
        $percentage         = $this->percentageCalculation($countPreviousEmail, $countProgressEmail);

        $allEmail["title"]                = 'All Email';
        $allEmail["progress_total_calls"] = $countProgressEmail;
        $allEmail["progress_from_date"]   = Carbon::create($dates['progressFromDate'])->format('M d');
        $allEmail["progress_to_date"]     = Carbon::create($dates['progressToDate'])->format('M d');
        $allEmail["previous_total_calls"] = $countPreviousEmail;
        $allEmail["previous_from_date"]   = Carbon::create($dates['previousFromDate'])->format('M d');
        $allEmail["previous_to_date"]     = Carbon::create($dates['previousToDate'])->format('M d');
        $allEmail["percentage"]           = $percentage;
        $allEmail["chart_data"]           = $chartPack;

        return $allEmail;
    }
    /* -----------------------------------------PERCENTAGE CALCULATION----------------------------------------- */
    public function percentageCalculation($originalNumber, $newNumber)
    {
        /* Subtract Two Numbers */
        $subtractResult = $newNumber - $originalNumber;

        if ($subtractResult == 0) {
            return 0;
        }

        /* Subtract Result Divisible By Original */
        if ($originalNumber != 0) {
            $divResult = $subtractResult / $originalNumber;
        } else {
            $divResult = $subtractResult / 1;
        }

        /* divResult has Multiply with 100 */
        $percentageResult = $divResult * 100;

        /* signFormat with + or - */
        return intval(sprintf("%+d", $percentageResult));
    }

    /* -----------------------------------------CHART DATA CALCULATION----------------------------------------- */

    /* get Result DataSet and Filter the Hour Based Details Push to the Array plotDetails Set */
    public function doChartProgress($dataSet, $startingDate, $hours, $tableColumnName)
    {
        $collection = collect($dataSet);

        $plotDetailsSet = [];
        $fromDate       = $startingDate; /* Initially Set From Date */

        /* Loop For ChartCapacitySize */
        for ($eachDay = 0; $eachDay < $this->chartCapacitySize; $eachDay++) {
            /* Add Specific Hours For Next Date Duration */
            $hoursAddedDate = Carbon::parse($fromDate)->addHour($hours)->format('Y-m-d H:i:s');
            /* filter By Duration of Hours Data */
            $filtered       = $collection->whereBetween($tableColumnName, [$fromDate, $hoursAddedDate]);
            array_push($plotDetailsSet, count($filtered)); /* Push To Array */
            $fromDate = $hoursAddedDate; /* Again Update the Last Date As From Date For Itration */
        }

        return $plotDetailsSet;
    }

    /* do Store the WidgetOrdering Details to Table */
    public function storeWidgetOrdering($userId)
    {
        /* Check if This Already Exist */
        $widgetDetails = $this->DashboardRepositories->findWidgetsDetailsByUserId($userId);

        /* if There is No Data in DB then Create Initail Rows and Return */
        if (!$widgetDetails) {
            $resultWidgetStore = $this->DashboardWidgetsRepositories->doWidgetInitialStore($userId);
            if ($resultWidgetStore == true) {
                $widgetDetails = Widget::whereuser_id($userId)->get()->toArray();
            }
        }

        /* Find All Active State Widget Ordering Details */
        $widgetResult = $this->DashboardWidgetsRepositories->getActiveWidgetsOrder($userId);

        return $widgetResult;
    }
}
