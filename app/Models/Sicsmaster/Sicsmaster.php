<?php

namespace App\Models\Sicsmaster;

use Illuminate\Database\Eloquent\Model;


class Sicsmaster extends Model
{
    /**
     * The Another database table used by the model.
     *
     * @var string
     */
    protected $connection = 'pgsql_cppoi';
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cppoi_sicsmaster';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "address",
        "annual_sales",
        "city",
        "company_name",
        "contact_name",
        "county",
        "employee_code",
        "employee_count",
        "fax",
        "gender",
        "business_type",
        "latitude",
        "longitude",
        "phone_number",
        "sales_code",
        "sic_code",
        "state_code",
        "title",
        "website",
        "zip_code",
        "geom",
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    //const CREATED_AT = 'created';
    //const UPDATED_AT = 'updated';
}
