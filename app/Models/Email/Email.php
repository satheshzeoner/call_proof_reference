<?php

namespace App\Models\Email;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_email';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "company_id",
        "user_id",
        "email_type_id",
        "email_status_id",
        "hashed",
        "subject",
        "txt",
        "html",
        "opened",
        "attempts",
        "retry",
        "updated",
        "created",
        "report_id",
        "export_run_id",
        "email"
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    // public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';
}
