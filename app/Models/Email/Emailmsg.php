<?php

namespace App\Models\Email;

use Illuminate\Database\Eloquent\Model;

class Emailmsg extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_emailmsg';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            "company_id",
            "user_id",
            "account_id",
            "contact_id",
            "contact_type_id",
            "content",
            "text_plain",
            "text_html",
            "to_email",
            "from_email",
            "message_id",
            "url_hash",
            "subject",
            "inbox",
            "sent_date",
            "created",
            "bucket_name",
            "bucket_path"
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    // const CREATED_AT = 'created';
    // const UPDATED_AT = 'updated';

}
