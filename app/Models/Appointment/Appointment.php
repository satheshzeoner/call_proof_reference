<?php

namespace App\Models\Appointment;

use Illuminate\Database\Eloquent\Model;

use App\Models\Contact\Contact;
use App\Models\Eventform\Eventform;

class Appointment extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_appointment';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "user_id",
        "contact_id",
        "start",
        "latitude",
        "longitude",
        "duration",
        "notes",
        "updated",
        "created",
        "title",
        "address",
        "item_id",
        "html_link",
        "scheduled",
        "stop",
        "company_id",
        "event_form_id"
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    // const CREATED_AT = 'created';
    // const UPDATED_AT = 'updated';

    public function contact()
    {
        return $this->belongsTo(Contact::class, 'contact_id', 'id');
    }
    public function eventForm()
    {
        return $this->belongsTo(Eventform::class);
    }
}
