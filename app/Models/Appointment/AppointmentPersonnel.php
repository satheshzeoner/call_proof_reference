<?php

namespace App\Models\Appointment;

use Illuminate\Database\Eloquent\Model;

class AppointmentPersonnel extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_appointmentpersonnel';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "appointment_id",
        "personnel_id",
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    // const CREATED_AT = 'created';
    // const UPDATED_AT = 'updated';

}
