<?php

namespace App\Models\Event;

use Illuminate\Database\Eloquent\Model;

/* Models */
use App\Models\Eventtype\Eventtype;
use App\Models\Contact\Contact;
use App\Models\Auth\AuthUser;
use App\Models\Badge\Badge;
use App\Models\Google\GooglePlace;
use App\Models\Followup\Followup;
use App\Models\Appointment\Appointment;
use App\Models\Twilio\TwilioCall;
use App\Models\Company\Company;

class Event extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_event';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "user_id",
        "event_type_id",
        "contact_id",
        "other_user_id",
        "call_id",
        "start",
        "duration",
        "badge_id",
        "created",
        "popular",
        "google_place_id",
        "cef_id",
        "tc_id",
        "points",
        "company_id",
        "appointment_id",
        "message",
        "followup_id",
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    // const CREATED_AT = 'created';
    // const UPDATED_AT = 'updated';

    public function eventType()
    {
        return $this->hasOne(Eventtype::class, 'id', 'event_type_id');
    }

    public function eventContact()
    {
        return $this->hasOne(Contact::class, 'id', 'contact_id');
    }

    public function eventFollow()
    {
        return $this->hasOne(Followup::class, 'id', 'followup_id');
    }

    public function eventOtheruser()
    {
        return $this->hasOne(AuthUser::class, 'id', 'other_user_id');
    }

    public function eventBadge()
    {
        return $this->hasOne(Badge::class, 'id', 'badge_id');
    }

    public function eventGoogleplace()
    {
        return $this->hasOne(GooglePlace::class, 'id', 'google_place_id');
    }

    public function eventAppointment()
    {
        return $this->hasOne(Appointment::class, 'id', 'appointment_id');
    }

    public function eventTwilioCall()
    {
        return $this->hasOne(TwilioCall::class, 'id', 'tc_id');
    }

    public function eventCompany()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }
}
