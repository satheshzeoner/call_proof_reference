<?php

namespace App\Models\Cfield;

use Illuminate\Database\Eloquent\Model;
use App\Models\Cfield\CfieldType;
use App\Models\Cfield\CfieldOption;

class Cfield extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_cfield';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'name', 'cfield_table_id', 'cfield_type_id', 'cfield_option_default_id', 'position', 'rfield_id', 'points', 'cfield_id', 'hide_on_checkout', 'required', 'lock'
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    // const CREATED_AT = 'created';
    // const UPDATED_AT = 'updated';

    public function customFieldType()
    {
        return $this->hasOne(CfieldType::class, 'id', 'cfield_type_id');
    }

    public function customFieldOption()
    {
        return $this->hasMany(CfieldOption::class);
    }
}
