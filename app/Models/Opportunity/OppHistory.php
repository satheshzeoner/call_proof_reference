<?php

namespace App\Models\Opportunity;

use Illuminate\Database\Eloquent\Model;
use DB;
use Helper;
use Exception;
use App\Models\Auth\AuthUser;


class OppHistory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_opphistory';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "opp_id",
        "opp_stage_id",
        "probability",
        "value",
        "close_date",
        "created",
        "notes",
        "opp_type_id",
        "opp_name",

    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    //public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    const CREATED_AT = 'created';
    //const UPDATED_AT = 'updated';
}
