<?php

namespace App\Models\Opportunity;

use Illuminate\Database\Eloquent\Model;
use DB;
use Helper;
use Exception;
use App\Models\Auth\AuthUser;


class OppPersonnel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_opppersonnel';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "company_id",
        "name",
        "position",
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    //const CREATED_AT = 'created';
    //const UPDATED_AT = 'updated';
}
