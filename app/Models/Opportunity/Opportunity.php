<?php

namespace App\Models\Opportunity;

use Illuminate\Database\Eloquent\Model;
use App\Models\Opportunity\OppStage;
use App\Models\Opportunity\OppType;
use App\Models\Company\Company;
use App\Models\Opportunity\OppHistory;
use App\Models\Auth\AuthUser;
use App\Models\Contact\ContactPersonnel;
use App\Models\Contact\Contact;
use Illuminate\Support\Carbon;

class Opportunity extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_opp';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "company_id",
        "user_id",
        "contact_id",
        "created",
        "updated",
        "opp_type_id",
        "opp_name",
        "opp_stage_id",
        "probability",
        "value",
        "close_date",
        "notes",
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    //public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';


    public function getCloseDateAttribute($value)
    {
        if ($value) {
            return (Carbon::parse($value)->format('d-m-Y'));
        }
    }

    public function setCloseDateAttribute($value)
    {
        if ($value) {
            $this->attributes['close_date'] = (Carbon::parse($value)->format('Y-m-d H:i:s'));
        }
    }

    public function oppStage()
    {
        return $this->belongsTo(OppStage::class, 'opp_stage_id');
    }
    public function oppType()
    {
        return $this->belongsTo(OppType::class, 'opp_type_id');
    }
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
    public function OppHistory()
    {
        return $this->hasOne(OppHistory::class, 'opp_id');
    }
    public function user()
    {
        return $this->belongsTo(AuthUser::class, 'user_id');
    }
    public function oppPersonnel()
    {
        return $this->belongsToMany(ContactPersonnel::class, 'cp_opppersonnel', 'opp_id', 'personnel_id');
    }
    public function oppContact()
    {
        return $this->belongsToMany(Contact::class, 'cp_oppcontact', 'opp_id', 'contact_id');
    }
}
