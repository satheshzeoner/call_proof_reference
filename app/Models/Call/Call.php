<?php

namespace App\Models\Call;

use Illuminate\Database\Eloquent\Model;
use App\Models\Call\CallType;

class Call extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_call';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "user_id",
        "call_type_id",
        "phone_number",
        "start",
        "duration",
        "contact_phone_id",
        "user_phone_id",
        "last_lookup",
        "lookup_count",
        "google_place_id",
        "local_id",
        "company_id",
        "contact_id"
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    // const CREATED_AT = 'created';
    // const UPDATED_AT = 'updated';

    public function callType()
    {
        return $this->hasOne(CallType::class, 'id', 'call_type_id');
    }
}
