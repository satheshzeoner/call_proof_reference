<?php

namespace App\Models\Followup;

use Illuminate\Database\Eloquent\Model;
use App\Models\Followup\FollowupPersonnel;
use App\Models\Followup\FollowupType;
use App\Models\Contact\Contact;


class Followup extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_followup';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "user_id",
        "contact_id",
        "start",
        "appointment_id",
        "duration",
        "notes",
        "updated",
        "created",
        "completed",
        "contact_phone_id",
        "item_id",
        "html_link",
        "send_google_cal",
        "contacteventform_id",
        "followup_type_id",
        "send_office_cal",
        "office_html_link",
        "office_item_id",
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    // public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';


    public function followPeople()
    {
        return $this->hasMany(FollowupPersonnel::class, 'followup_id', 'id');
    }

    public function contactFollow()
    {
        return $this->hasOne(Contact::class, 'id', 'contact_id');
    }

    public function taskType()
    {
        return $this->hasOne(FollowupType::class, 'id', 'followup_type_id');
    }
}
