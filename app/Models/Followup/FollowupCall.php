<?php

namespace App\Models\Followup;

use Illuminate\Database\Eloquent\Model;

class FollowupCall extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_followupcall';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "company_id",
        "followup_id",
        "user_id",
        "contact_id",
        "contact_phone_id",
        "contact_id",
        "contact_id",
        "twilio_call_id",
        "response_time",
        "new_upgrade",
        "upgrade_time",
        "store_id",
        "is_followup_call",
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = true;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    const CREATED_AT = 'created';
    // const UPDATED_AT = 'updated';


}
