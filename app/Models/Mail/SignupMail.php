<?php

namespace App\Models\Mail;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Mail;

class SignupMail extends Authenticatable
{
    /**
     * The database table used by the model.
     *
     * @var string
     */

    public function sendNotifyNewMemberEmail($userData, $companyId, $userId, $ipDetails)
    {

        if (empty($companyId)) {
            return true;
        }

        $appName = config('constants.Appname.Name');

        $companyUrlReferal = '';

        $message = '';
        $message .= 'Someone signed up for ' . $appName;
        $message .= '<br> <br>';
        $message .= 'Name: ' . $userData['contact'];
        $message .= '<br>';
        $message .= 'Company: ' . $userData['company'];
        $message .= '<br>';
        $message .= 'Email: ' . $userData['email'];
        $message .= '<br>';
        $message .= 'When: ' . date('Y/m/d H:i:s');;
        $message .= '<br>';
        $message .= 'Phone: ' . $userData['phone'];
        $message .= '<br>';
        $message .= 'Referer URL: ' . $companyUrlReferal;
        $message .= '<br>';
        $message .= 'Recent Signups: https://app.callproof.com/admin/cp/userprofile/?manager__exact=1';
        $message .= '<br>';
        $message .= 'IP address information for : ' . $userData['contact'];
        $message .= '<br>';
        $message .= 'IP Address: ' . $ipDetails->query;
        $message .= '<br>';
        $message .= 'Country: ' . $ipDetails->country;
        $message .= '<br>';
        $message .= 'City: ' . $ipDetails->city;
        $message .= '<br>';
        $message .= 'location: ' . $ipDetails->regionName;
        $message .= '<br>';
        $message .= 'timezone: ' . $ipDetails->timezone;


        $details['title'] = 'Call Proof';
        $details['body'] = $message;
        //Mail::to(config('constants.Signupemail.from_email'))->send(new \App\Mail\SignupMail($details));

        /* Absolute User Creation */
        $this->absoluteUserInsertion($companyId, $userData);
    }




    public function absoluteUserInsertion($companyId, $userData)
    {   //DB::beginTransaction();

        $defaultSignUpContactAccount                 = config('constants.Signupemail.DEFAULT_SIGNUP_CONTACT_ACCOUNT');
        $defaultSignUpContactAccountContactTypeMob   = config('constants.Signupemail.DEFAULT_SIGNUP_CONTACT_ACCOUNT_CONTACTTYPE_MOB');
        $defaultSignUpContactAccountLeadSource       = config('constants.Signupemail.DEFAULT_SIGNUP_CONTACT_ACCOUNT_LEAD_SCOURCE');


        /* get_first_manager */
        $selfManager = DB::table('cp_userprofile')->where('manager', true)->orderby('id', 'ASC')->first();

        /* For Mobile Config */
        $defaultSignUpContactAccountLeadSourceOptionApp = config('constants.Signupemail.DEFAULT_SIGNUP_CONTACT_ACCOUNT_LEAD_SCOURCE_OPTION_APP');

        $prevContact                                           = 'App Signup';


        foreach ($defaultSignUpContactAccount as $key => $defaultValue) {

            $customFieldId = $defaultSignUpContactAccountLeadSource[$key];
            $optionId = $defaultSignUpContactAccountLeadSourceOptionApp[$key];

            $firstLastName = explode(' ', $userData['contact']);
            // var_dump($first_last_name);
            $companyId = $defaultValue;

            $signupId = $defaultSignUpContactAccountContactTypeMob[$key];

            $contactCompanyData['company_id'] = $companyId;
            $contactCompanyData['name']       = $userData['company'];
            $contactCompanyData['created']    = now();
            $contactCompanyData['updated']    = now();

            DB::table('cp_contactcompany')->insert($contactCompanyData);
            $contactCompanyInsertedId = DB::getPdo()->lastInsertId();

            /*-----------------------cp_contactcompany-----------------------*/

            $contactData['company_id']         = $companyId;
            $contactData['created']            = now();
            $contactData['updated']            = now();
            $contactData['first_name']         = $firstLastName[0];
            $contactData['last_name']          = $firstLastName[1];
            $contactData['email']              = $userData['email'];
            $contactData['created_by_id']      = $selfManager->id;
            $contactData['account']            = '';
            $contactData['unknown']            = False;
            $contactData['do_not_sms']         = False;
            $contactData['assigned']           = True;
            $contactData['website']            = '';
            $contactData['address']            = '';
            $contactData['address2']           = '';
            $contactData['city']               = '';
            $contactData['zip']                = '';
            $contactData['contact_company_id'] = $contactCompanyInsertedId;
            $contactData['contact_type_id']    = $signupId;

            DB::table('cp_contact')->insert($contactData);
            $contactInsertedId = DB::getPdo()->lastInsertId();

            /* ----------------------cp_contact------------------------------*/
            $customField = DB::table('cp_cfield')
                ->where('company_id', $companyId)
                ->where('id', $customFieldId)->first();

            if (!empty($customFieldId)) {

                $customFieldOption = DB::table('cp_cfieldoption')
                    ->where('cfield_id', $customField->id)
                    ->where('id', $optionId)->first();

                $customFieldValue['cfield_id'] = $customFieldId;
                $customFieldValue['entity_id'] = $contactInsertedId;
                $customFieldValue['cf_value']  = $customFieldOption->id;
                $customFieldValue['updated']   = now();

                DB::table('cp_cfieldvalue')->insert($customFieldValue);
                $customFieldValueInsertedId = DB::getPdo()->lastInsertId();
            }

            /* ----------------------cp_cfieldvalue------------------------------*/

            $phoneType = DB::table('cp_phonetype')
                ->where('name', 'Office')
                ->first();

            $contactPhone['company_id']      = $companyId;
            $contactPhone['contact_id']      = $contactInsertedId;
            $contactPhone['phone_type_id']   = $phoneType->id;
            $contactPhone['phone_number']    = $userData['phone'];
            $contactPhone['unknown']         = False;
            $contactPhone['created']         = now();
            $contactPhone['updated']         = now();

            DB::table('cp_contactphone')->insert($contactPhone);
            $contactPhoneInsertedId = DB::getPdo()->lastInsertId();

            /* ----------------------cp_contactphone------------------------------*/

            $companySetting = DB::table('cp_companysetting')->where('name', 'referer_hash')->first();
            if (!empty($companySetting)) {
                $referer = DB::table('cp_referer')->where('hash', $companySetting->value)->first();
                $referervar = DB::table('cp_referervar')->where('referer_id', $referer->id)->get();
                // var_dump($referervar);
                $referervar = Helper::make_array($referervar);

                foreach ($referervar as $key => $value) {
                    $contactNote['company_id'] = $companyId;
                    $contactNote['contact_id'] = $contactInsertedId;
                    $contactNote['note']    = $value['name'] . ':' . $value['content'];
                    $contactNote['user_id']    = $selfManager->id;
                    $contactNote['created'] = now();

                    DB::table('cp_contactnote')->insert($contactNote);
                    $contactNoteInsertedId = DB::getPdo()->lastInsertId();
                }
            }
        }
    }
}
