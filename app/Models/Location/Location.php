<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model;


class Location extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_location';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "user_id",
        "provider_id",
        "altitude",
        "accuracy",
        "latitude",
        "longitude",
        "speed",
        "bearing",
        "created",
        "address",
        "city",
        "state_id",
        "zip",
        "last_geocode",
        "geocode_count",
        "geo_track_type",
        "link_id",
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    //const CREATED_AT = 'created';
    //const UPDATED_AT = 'updated';
}
