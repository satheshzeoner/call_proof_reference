<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_company';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'address2', 'city', 'state_id', 'zip', 'country_id', 'latitude', 'longitude', 'website', 'image', 'last_activity', 'activity_30', 'activity_90', 'balance', 'twilio', 'dealer', 'disabled', 'gryphon', 'gryphon_number', 'gryphon_pin', 'transaction_months', 'transaction_commission', 'gryphon_campaign', 'referer_url_id', 'sales_rep_single_mode', 'call_buy_days', 'hide_personal_calls', 'contact_map_hours', 'twilio_record', 'twilio_conference', 'beep_minutes', 'check_email', 'imap_long_check_days', 'bg_image', 'new_events', 'callproof_rep_name', 'is_carrier_caller_enabled', 'is_openvbx_enabled', 'is_twilio_client_forward_enable', 'is_beacon_enabled', 'is_app_call_without_beacon', 'is_copy_note', 'is_login_limit', 'is_call_forward_to_carrier', 'is_call_forward_to_default_salesrep', 'is_youtube_video_enabled', 'is_yellow_button_enabled', 'is_green_button_enabled', 'is_blue_button_enabled', 'is_account_inc', 'account_inc', 'people_tab', 'opp_sign', 'is_custom_field_required', 'is_event_form_required', 'thinq', 'is_number_of_stats_shown'
    ];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    // protected $attributes = [
    //     'is_staff'     => false,
    //     'is_active'    => true,
    //     'is_superuser' => false,
    // ];


    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';
}
