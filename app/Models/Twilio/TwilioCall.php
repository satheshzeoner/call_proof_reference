<?php

namespace App\Models\Twilio;

use Illuminate\Database\Eloquent\Model;


class TwilioCall extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_twiliocall';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'call_type_id',
        'call_sid',
        'company_id',
        'contact_id',
        'contact_phone_id',
        'user_id',
        'user_phone_id',
        'dealer_store_phone_id',
        'start',
        'duration',
        'recording',
        'call_status',
        'rating',
        'call_result_id',
        'notes',
        'old_id',
        'caller',
        'last_lookup',
        'lookup_count',
        'google_place_id',
        'twilio_msg_id',
        'first_leg_call_sid',
        'tollfree',
        'twilio_phone_id',
        'conference',
        'last_beep',
        'carrier_name',
        'caller_name',
        'dailer_number',
        'country_code',
        'is_voice_mail',
        'received_on',
        'other_number',
        'sip_user',
        'call_evaluation_id',
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    //const CREATED_AT = 'created';
    //const UPDATED_AT = 'updated';
}
