<?php

namespace App\Models\Twilio;

use Illuminate\Database\Eloquent\Model;


class TwilioSms extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_twiliosms';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'contact_id',
        'contact_phone_id',
        'user_id',
        'twilio_phone_id',
        'sms_type_id',
        'start',
        'msg',
        'sent',
        'seen',
        'failed',
        'carrier_name',
        'caller_name',
        'is_new_contact_sms',
        'wait_time',
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    //const CREATED_AT = 'created';
    //const UPDATED_AT = 'updated';
}
