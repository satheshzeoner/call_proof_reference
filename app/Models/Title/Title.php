<?php

namespace App\Models\Title;

use Illuminate\Database\Eloquent\Model;

class Title extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_title';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "company_id",
        "name",
        "image",
        "contactmenulist_id",
        "places_cat_list_id"
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    // const CREATED_AT = 'created';
    // const UPDATED_AT = 'updated';

}
