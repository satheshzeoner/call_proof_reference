<?php

namespace App\Models\Contact;

use Illuminate\Database\Eloquent\Model;

use App\Models\PhoneType\PhoneType;
use App\Models\Contact\ContactPersonnel;

class ContactPhone extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_contactphone';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "contact_id",
        "phone_type_id",
        "phone_number",
        "ext",
        "created",
        "updated",
        "activated",
        "eligible",
        "call_result_id",
        "last_contacted",
        "hidden",
        "dealer_store_id",
        "model",
        "do_not_call",
        "company_id",
        "unknown",
        "carrier_name",
        "caller_name",
        "lookup_updated_on",
        "country_code",
        "personnel_id",
        "call_count",
        "is_dealer_imported",
        "imported_date",
        "description",
        "contact_type_id",
        "is_active_lead",
        "is_in_dnc",
        "is_in_dnd"
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = true;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    public function phoneType()
    {
        return $this->belongsTo(PhoneType::class, 'phone_type_id', 'id');
    }
    public function contactPhonepersonnel()
    {
        return $this->belongsToMany(ContactPersonnel::class, 'cp_contactphonepersonnel', 'contactphone_id', 'personnel_id');
    }
}
