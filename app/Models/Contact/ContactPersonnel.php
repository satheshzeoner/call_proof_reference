<?php

namespace App\Models\Contact;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

use App\Models\People\PeopleRole;
use App\Models\Contact\ContactPhone;

class ContactPersonnel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_contactpersonnel';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "company_id",
        "contact_id",
        "first_name",
        "last_name",
        "title",
        "last_contacted",
        "personnel_notes",
        "created",
        "updated",
        "cell_phone",
        "email",
        "peoplerole_id",
        "image",
        "is_disabled",
        "moved_to"
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    // public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    public function getLastContactedAttribute($value)
    {
        if ($value) {
            return (Carbon::parse($value)->format('d-m-Y'));
        }
    }

    public function setLastContactedAttribute($value)
    {
        if ($value) {
            $this->attributes['last_contacted'] = (Carbon::parse($value)->format('Y-m-d H:i:s'));
        }
    }

    public function PeopleRole()
    {
        return $this->belongsTo(PeopleRole::class, 'peoplerole_id');
    }

    public function contactPhone()
    {
        return $this->belongsToMany(ContactPhone::class, 'cp_contactphonepersonnel', 'personnel_id', 'contactphone_id');
    }
}
