<?php

namespace App\Models\Contact;

use Illuminate\Database\Eloquent\Model;
use App\Models\Eventform\Eventform;
use App\Models\Contact\ContactRep;
use App\Models\Contact\ContactPersonnel;
use App\Models\Company\Company;

class ContactEventform extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_contacteventform';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "company_id",
        "user_id",
        "contact_id",
        "event_form_id",
        "created",
        "points",
        "latitude",
        "longitude",
        "completed",
        "appointment_id",
        "completed_date"
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    // const CREATED_AT = 'created';
    // const UPDATED_AT = 'updated';

    public function eventForm()
    {
        return $this->belongsTo(Eventform::class);
    }

    public function contactPersonnal()
    {
        return $this->hasMany(ContactPersonnel::class, 'contact_id', 'contact_id');
    }

    public function contactCompany()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }
}
