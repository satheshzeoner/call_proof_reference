<?php

namespace App\Models\Contact;

use Illuminate\Database\Eloquent\Model;
use App\Models\Opportunity\Opportunity;
use App\Models\Contact\ContactType;
use App\Models\Contact\ContactCompany;
use App\Models\State\State;
use App\Models\Country\Country;
use App\Models\Contact\ContactPhone;
use App\Models\Contact\ContactInfoEmail;
use App\Models\Contact\ContactPersonnel;
use App\Models\Label\Label;



class Contact extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'cp_contact';

  /**
   * The primary key associated with the table.
   *
   * @var string
   */
  protected $primaryKey = 'id';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    "company_id",
    "created_by_id",
    "contact_type_id",
    "first_name",
    "last_name",
    "email",
    "address",
    "address2",
    "city",
    "state_id",
    "zip",
    "country_id",
    "website",
    "latitude",
    "longitude",
    "last_contacted",
    "default_phone_id",
    "image",
    "created",
    "updated",
    "account",
    "contact_company_id",
    "last_geocode",
    "geocode_count",
    "invoice",
    "title",
    "unknown",
    "do_not_sms",
    "assigned",
    "place_id",
    "district",
    "sales_type",
    "related_esn",
    "related_product",
    "handset_type",
    "sync_with_ac",
    "zap_facebook_lead",
    "annual_sales",
    "business_type",
    "employee_count",
    "county",
  ];

  /**
   * The Default TimeStamp.
   *
   * @var array
   */
  // public $timestamps = false;

  /**
   * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
   *
   * @var array
   */
  const CREATED_AT = 'created';
  const UPDATED_AT = 'updated';

  public function oppContact()
  {
    return $this->belongsToMany(Opportunity::class, 'cp_oppcontact', 'contact_id', 'opp_id');
  }
  public function contactType()
  {
    return $this->belongsTo(ContactType::class);
  }
  public function contactCompany()
  {
    return $this->belongsTo(ContactCompany::class);
  }
  public function State()
  {
    return $this->belongsTo(State::class);
  }
  public function Country()
  {
    return $this->belongsTo(Country::class);
  }
  public function contactPhone()
  {
    return $this->belongsTo(ContactPhone::class, 'default_phone_id', 'id');
  }
  public function ContactInfoEmail()
  {
    return $this->hasMany(ContactInfoEmail::class, 'contact_id');
  }
  public function ContactPersonnel()
  {
    return $this->hasMany(ContactPersonnel::class, 'contact_id');
  }
  public function ContactPhoneDetail()
  {
    return $this->hasMany(ContactPhone::class, 'contact_id');
  }
  public function contactImages()
  {
    return $this->hasMany(ContactImage::class, 'contact_id');
  }
  public function contactLabel()
  {
    return $this->belongsToMany(Label::class, 'cp_contactlabel', 'contact_id', 'label_id');
  }
}
