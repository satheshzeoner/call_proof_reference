<?php

namespace App\Models\Contact;

use Illuminate\Database\Eloquent\Model;
use App\Models\Contact\ContactNoteSourceType;


class ContactNote extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_contactnote';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "contact_id",
        "user_id",
        "note",
        "created",
        "company_id",
        "sourcetype_id",
        "source_id",
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    // const CREATED_AT = 'created';
    // const UPDATED_AT = 'updated';

    public function noteSourceType()
    {
        return $this->hasOne(ContactNoteSourceType::class, 'id', 'source_id');
    }
}
