<?php

namespace App\Models\Contact;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\AuthUser;

class ContactRep extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_contactrep';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "contact_id",
        "user_id",
        "created",
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    // const CREATED_AT = 'created';
    // const UPDATED_AT = 'updated';

    public function userDetails()
    {
        return $this->hasOne(AuthUser::class, 'id', 'user_id');
    }
}
