<?php

namespace App\Models\Contact;

use Illuminate\Database\Eloquent\Model;

use App\Models\Cfield\Cfield;

class ContactImage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_contactimage';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "company_id",
        "user_id",
        "contact_id",
        "cfield_id",
        "contact_event_form_id",
        "image",
        "created",
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    // const CREATED_AT = 'created';
    // const UPDATED_AT = 'updated';

    public function contactEventForm()
    {
        return $this->belongsTo(ContactEventform::class);
    }
    public function customField()
    {
        return $this->belongsTo(Cfield::class, 'cfield_id', 'id');
    }
}
