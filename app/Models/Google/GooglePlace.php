<?php

namespace App\Models\Google;

use Illuminate\Database\Eloquent\Model;

class GooglePlace extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_googleplace';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "id",
        "gp_search_id",
        "gp_id",
        "reference",
        "name",
        "vicinity",
        "address",
        "city",
        "state_id",
        "zip",
        "country_id",
        "phone_number",
        "website",
        "latitude",
        "longitude",
        "distance",
        "created",
        "updated",
        "company_id",
        "created_by_id",
        "annual_sales",
        "business_type",
        "employee_count",
        "county",
        "contact_name",
        "title",
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    // public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';
}
