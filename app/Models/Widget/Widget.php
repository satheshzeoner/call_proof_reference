<?php

namespace App\Models\Widget;

use Illuminate\Database\Eloquent\Model;
use App\Models\Widget\WidgetList;

class Widget extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_widget';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'widget_type_id', 'status', 'position',
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    // public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public function widgetsList()
    {
        return $this->belongsTo(WidgetList::class, 'widget_type_id');
    }
}
