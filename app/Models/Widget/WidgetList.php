<?php

namespace App\Models\Widget;

use Illuminate\Database\Eloquent\Model;
use App\Models\Widget\Widget;

class WidgetList extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_widgetlist';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'widget_type', 'name',
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    // public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public function widgetsDetails()
    {
        return $this->hasOne(Widget::class, 'widget_type_id');
    }
}
