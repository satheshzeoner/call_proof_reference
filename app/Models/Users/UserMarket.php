<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class UserMarket extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_usermarket';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
}
