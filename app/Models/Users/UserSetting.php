<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_usersetting';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'value', 'updated', 'manager_daily_activity_additional_email', 'report_type'
    ];

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    public $timestamps = false;
}
