<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;
use App\Models\Company\Company;
use App\Models\Users\User;
use App\Models\Users\UserPhone;
use App\Models\Title\Title;

class UserProfile extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_userprofile';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'company_id', 'address', 'address2', 'city', 'state_id', 'zip', 'country_id', 'default_phone_id', 'manager', 'temppass', 'secret', 'image', 'title_id', 'updated', 'latitude', 'longitude', 'contact_radius', 'last_geocode', 'geocode_count', 'delete_contacts', 'gryphon_number', 'gryphon_pin', 'gryphon_campaign', 'store_id', 'market_id', 'hide_events', 'contacts_assigned_count', 'contacts_radius_count', 'contacts_unassigned_count', 'cal_future_appt_id', 'cal_followup_id', 'hide_my_events', 'just_my_events', 'limit_to_market', 'user_timezone', 'no_auto_timezone', 'hide_rep_assignment', 'market_manager', 'access_token', 'can_view_company_directory', 'is_login', 'is_twilio_client', 'device_type', 'device_id', 'is_carrier_caller_enabled_for_call', 'is_openvbx_enabled', 'twilio_notification_call_status', 'is_client_forward_enable', 'is_login_browser', 'device_id_app', 'is_login_app', 'can_login_multiple_device_dealer_app', 'device_udid', 'is_twilio_phone_number_permission', 'message_caller_id', 'is_facebook_lead_import', 'contactmenulist_id', 'is_export', 'c_android_version', 'b_android_version', 'c_ios_version', 'b_ios_version', 'out_of_store_call', 'is_disabled', 'is_twilio_phone_number_delete_permission', 'is_mail_campaign_permission', 'is_delete_personnel', 'is_start_appointment', 'is_company_activity_stats', 'is_sales_dash_video_permission', 'office_cal_followup_id', 'is_followup_google_calendar_checked', 'is_followup_office_calendar_checked', 'places_cat_list_id', 'task_notification_time_id', 'mobile_last_login', 'is_show_task_popup', 'task_popup_disabled', 'google_cal_sync', 'is_telemarketing_manager', 'hide_mobile_appt_popup', 'is_contact_page_as_homepage', 'call_notification_option_id', 'is_call_notification_enabled', 'is_transcription_permission', 'is_transcription_settings_permission', 'can_send_mms', 'call_review_automation_permission'
    ];

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    public $timestamps = false;

    public function userCompany()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function userAuth()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    public function title()
    {
        return $this->hasOne(Title::class, 'id', 'title_id');
    }
    public function userPhone()
    {
        return $this->belongsTo(UserPhone::class, 'default_phone_id', 'id');
    }
}
