<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class UserDevicelog extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_userdevicelog';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';
}
