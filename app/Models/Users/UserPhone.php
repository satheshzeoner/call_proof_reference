<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class UserPhone extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_userphone';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'phone_type_id', 'phone_number', 'ext', 'company_id', 'country_code'
    ];

    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';
}
