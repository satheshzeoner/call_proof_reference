<?php

namespace App\Models\Users;

use App\Models\Event\Event;
use App\Models\Users\UserProfile;


use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Auth;
use DB;

/* EventService */
use App\Services\Event\EventAddService;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'auth_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'first_name', 'last_name', 'email', 'password', 'is_active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function validateForPassportPasswordGrant()
    {
        return true;
    }

    public function LoginUserEvent($email, $password)
    {
        /* Get User Id and Company id For Event Creation */
        $userDetails           = Auth::user();
        $userProfile           = UserProfile::where('user_id', $userDetails->id)->first('company_id');
        $this->AddEventModel   = new Event();

        /* Event Store Process */
        $this->doEventAdd($userDetails->id, $userProfile->company_id, config('constants.Event_name.mobile_login'));

        return true;
    }

    /* ===============================EVENT========================== */
    public function doEventAdd($userId, $companyId, $eventName)
    {
        /* Event Creation */
        $eventAddService        = new EventAddService();
        $eventAddService->addEvent(
            $eventName,
            $eventCompanyId          = $companyId,
            $eventUserId             = $userId,
            $eventContactId          = null,
            $eventOtherUserId        = null,
            $eventCallId             = null,
            $eventStartTime          = null,
            $eventDuration           = null,
            $eventBadgeId            = null,
            $eventGooglePlace        = null,
            $eventTwilioCallId       = null,
            $eventContactEventFormId = null,
            $eventAppointmentId      = null,
            $eventFollowupId         = null,
            $eventMessage            = null
        );

        return true;
    }
    /* ===============================EVENT-END====================== */
}
