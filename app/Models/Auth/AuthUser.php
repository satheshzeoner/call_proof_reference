<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use App\Models\Users\UserProfile;
use App\Models\Market\Market;
use App\Models\Company\Company;

class AuthUser extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'auth_user';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'first_name', 'last_name', 'email', 'password', 'is_staff', 'is_active', 'is_superuser', 'cache_password'
    ];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'is_staff'     => false,
        'is_active'    => true,
        'is_superuser' => false,
    ];


    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    const CREATED_AT = 'date_joined';
    const UPDATED_AT = 'last_login';


    public function userProfile()
    {
        return $this->hasOne(UserProfile::class, 'user_id');
    }
    public function userMarket()
    {
        return $this->belongsToMany(Market::class, 'cp_usermarket', 'user_id', 'market_id');
    }
    public function userMarketCompany()
    {
        return $this->belongsToMany(Company::class, 'cp_usermarket', 'user_id', 'company_id');
    }
}
