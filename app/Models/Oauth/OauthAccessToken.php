<?php

namespace App\Models\Oauth;

use Illuminate\Database\Eloquent\Model;

use App\Models\Oauth\OauthRefreshToken;

class OauthAccessToken extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'oauth_access_tokens';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "user_id",
        "client_id",
        "name",
        "scopes",
        "revoked",
        "created_at",
        "updated_at",
        "expires_at",
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    // public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}
