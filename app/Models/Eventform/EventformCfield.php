<?php

namespace App\Models\Eventform;

use Illuminate\Database\Eloquent\Model;

use App\Models\Cfield\Cfield;


class EventformCfield extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_eventformcfield';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "event_form_id",
        "cfield_id",
        "position",
        "required"
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';

    public function cFieldForm()
    {
        return $this->hasOne(Cfield::class, 'id', 'cfield_id');
    }
}
