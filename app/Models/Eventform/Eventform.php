<?php

namespace App\Models\Eventform;

use Illuminate\Database\Eloquent\Model;

class Eventform extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cp_eventform';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "company_id", "name", "position", "post_back_method", "post_back_url", "post_back_secret", "points", "is_schedule_followup", "send_email_to_rep", "from_id_callproof", "event_form_treat_as_id", "is_after_call_form", "hide", "prefill_data"
    ];

    /**
     * The Default TimeStamp.
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * The Created By Assigned as Current TimeStamp & UPDATED_AT Assigned as Current TimeStamp.
     *
     * @var array
     */
    // const CREATED_AT = 'created';
    // const UPDATED_AT = 'updated';

}
