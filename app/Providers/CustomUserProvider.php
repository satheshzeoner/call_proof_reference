<?php

namespace App\Providers;

use Illuminate\Auth\EloquentUserProvider as UserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;


class CustomUserProvider extends UserProvider
{

    /**
     * Overrides the framework defaults validate credentials method 
     *
     * @param UserContract $user
     * @param array $credentials
     * @return bool
     */
    public function validateCredentials(UserContract $user, array $credentials)
    {
        $plain = $credentials['password'];
        return $this->verify_Password($user->getAuthPassword(), $plain);
    }


    function verify_Password($dbString, $password)
    {
        $pieces = explode("$", $dbString);

        if ($pieces[0] != 'pbkdf2_sha256') {
            return false;
        }

        $iterations = $pieces[1];
        $salt = $pieces[2];
        $oldHash = $pieces[3];

        $hash = hash_pbkdf2("SHA256", $password, $salt, $iterations, 0, true);
        $hash = base64_encode($hash);

        if ($hash == $oldHash) {
            return true;
        } else {
            return false;
        }
    }
}
