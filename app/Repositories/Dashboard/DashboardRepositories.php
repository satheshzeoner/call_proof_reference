<?php

namespace App\Repositories\Dashboard;

use Carbon\Carbon;

/* Models */

use App\Models\Widget\Widget;
use App\Models\Widget\WidgetList;
use App\Models\Call\Call;
use App\Models\Appointment\Appointment;
use App\Models\Email\Email;
use App\Models\Email\Emailmsg;

class DashboardRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    public function findDetailsbyTypeName($widgetType)
    {
        return WidgetList::where('name', $widgetType)->get()->toArray();
    }

    /* Widgets */
    public function findWidgetsDetailsByUserId($userId)
    {
        return Widget::whereuser_id($userId)->get()->toArray();
    }
    /* Widgets Table */

    /* Calls Table */

    /* Get Total Count For Yesterday Calls */
    public function getCountForYesterdayCall($userId)
    {
        return Call::whereUser_id($userId)->whereDate('start', '=', Carbon::yesterday()->format('Y-m-d'))->count();
    }

    /* Get Total Count For Today Calls */
    public function getCountForTodayCall($userId)
    {
        return Call::whereUser_id($userId)->whereDate('start', '=', Carbon::today()->format('Y-m-d'))->count();
    }

    /* get Calls Details for Given Dates */
    public function getCallsDetailsBasedonDates($userId, $fromDate, $toDate)
    {
        return Call::whereUser_id($userId)->whereDate('start', '>=', $fromDate)->whereDate('start', '<=', $toDate)->get()->toArray();
    }

    /* Calls Table End */

    /* Appointment Table */
    /* Get Total Count For Yesterday Appointment */
    public function getCountForYesterdayAppointment($userId)
    {
        return Appointment::whereUser_id($userId)->whereDate('start', '=', Carbon::yesterday()->format('Y-m-d'))->count();
    }

    /* Get Total Count For Today Appointment */
    public function getCountForTodayAppointment($userId)
    {
        return Appointment::whereUser_id($userId)->whereDate('start', '=', Carbon::today()->format('Y-m-d'))->count();
    }

    /* get Calls Details for Given Dates */
    public function getAppointmentCallsDetailsBasedonDates($userId, $fromDate, $toDate)
    {
        return Appointment::whereUser_id($userId)->whereDate('start', '>=', $fromDate)->whereDate('start', '<=', $toDate)->get()->toArray();
    }
    /* Appointment Table End */

    /* Email Table */
    /* get Email Details for Given Dates */
    public function getEmailDetailsBasedonDates($userId, $fromDate, $toDate)
    {
        return Email::whereUser_id($userId)->whereDate('opened', '>=', $fromDate)->whereDate('opened', '<=', $toDate)->get()->toArray();
    }
    /* Email Table End */

    /* Emailmsg table */
    /* Get Total Count For Yesterday Calls */
    public function getEmailmsgYesterdayCount($userId)
    {
        return Emailmsg::whereUser_id($userId)->whereDate('sent_date', '=', Carbon::yesterday()->format('Y-m-d'))->count();
    }

    /* Get Total Count For Today Calls */
    public function getEmailmsgTodayCount($userId)
    {
        return Emailmsg::whereUser_id($userId)->whereDate('sent_date', '=', Carbon::today()->format('Y-m-d'))->count();
    }
    /* Emailmsg table End */

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
