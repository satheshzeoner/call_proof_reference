<?php

namespace App\Repositories\Dashboard;

/* Models */

use App\Models\Widget\Widget;
use App\Models\Widget\WidgetList;

class DashboardWidgetsRepositories
{
    /* ===============================CREATE=============================== */
    /* Widget Process */
    public function doWidgetInitialStore($userId)
    {
        $WidgetList = WidgetList::all()->toArray();

        foreach ($WidgetList as $WidgetListkey => $WidgetListvalue) {

            $widgetDetails['user_id']        = $userId; /* Current UserId */
            $widgetDetails['widget_type_id'] = $WidgetListvalue['id']; /* Id From WidgetList */
            $widgetDetails['status']         = 1; /* Initailly as Active */
            $widgetDetails['position']       = $WidgetListkey + 1; /* Position Set */

            Widget::create($widgetDetails);
        }

        return true;
    }
    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */
    /* Get By UserId */
    public function findByUserId($userId)
    {
        return Widget::whereuser_id($userId)->get()->toArray();
    }

    /* Get All Active State Widget Ordering Details */
    public function getAllActiveWidgetsOrder($userId)
    {
        $widgetActiveResult = Widget::with(['widgetsList' => function ($widgetsListJoin) {
            $widgetsListJoin->select(array('id', 'widget_type', 'name'));
        }])
            ->select('user_id', 'widget_type_id', 'status', 'position')
            ->where('user_id', $userId)
            ->orderBy('position', 'ASC')
            ->where('status', '1')
            ->get()
            ->map(function ($widget) {
                return $this->doWidgetDetailsFormat($widget);
            });

        return $widgetActiveResult;
    }

    /* Get Present Active State Widgets Ordering */
    public function getActiveWidgetsOrder($userId)
    {
        $widgetActiveResult = Widget::with(['widgetsList' => function ($widgetsListJoin) {
            $widgetsListJoin->select(array('id', 'widget_type', 'name'));
        }])
            ->select('user_id', 'widget_type_id', 'status', 'position')
            ->where('user_id', $userId)
            ->orderBy('position', 'ASC')
            ->where('status', '1')
            ->get()
            ->map(function ($widget) {
                return $this->doWidgetDetailsFormat($widget);
            });

        return $widgetActiveResult;
    }

    /* Get Present In-Active State Widgets Ordering */
    public function getInActiveWidgetsOrder($userId)
    {
        $widgetInActiveResult = Widget::with(['widgetsList' => function ($widgetsListJoin) {
            $widgetsListJoin->select(array('id', 'widget_type', 'name'));
        }])
            ->select('user_id', 'widget_type_id', 'status', 'position')
            ->where('user_id', $userId)
            ->orderBy('position', 'ASC')
            ->where('status', '0')
            ->get()
            ->map(function ($widget) {
                return $this->doWidgetDetailsFormat($widget);
            });

        return $widgetInActiveResult;
    }
    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */
    public function doActiveUpdatewidget($activeList, $position)
    {
        Widget::where('widget_type_id', $activeList)
            ->update(['position' => $position, 'status' => 1]);
        return true;
    }

    public function doInActiveUpdatewidget($activeList, $position)
    {
        Widget::where('widget_type_id', $activeList)
            ->update(['position' => $position, 'status' => 0]);
        return true;
    }
    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */
    /* Transform To Widget Format */
    public function doWidgetDetailsFormat($widgets)
    {
        return [
            'name'     => $widgets->widgetsList->name,
            'id'       => $widgets->widgetsList->id,
            'position' => $widgets->position,
        ];
    }
    /* ===============================FORMATTING-END====================== */
}
