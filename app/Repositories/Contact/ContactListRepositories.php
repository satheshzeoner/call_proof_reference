<?php

namespace App\Repositories\Contact;

use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;

/* Models */
use App\Models\Contact\Contact;
use App\Models\Contact\ContactNote;

class ContactListRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */
    public function getContactListDetails($request, $userLatitude, $userLongitude, $userCompanyId)
    {
        $contactList = Contact::select(
            'cp_contact.id as contact_id',
            'cp_contactcompany.name as company_name',
            DB::raw("CASE WHEN ( cp_contact.address='') THEN null else cp_contact.address end as address1"),
            DB::raw("CASE WHEN ( cp_contact.address2 ='' and cp_contact.city='' and cp_state.abbr='' and cp_contact.zip='') THEN null else trim(CONCAT_WS(' ',cp_contact.address2,cp_contact.city,cp_state.abbr,cp_contact.zip)) end as address2"),
            DB::raw("CASE WHEN (cp_contactphone.phone_number IS NULL) THEN null WHEN (cp_contactphone.phone_number='') THEN null else concat( LEFT(cp_contactphone.phone_number,3) , '-' , substr(cp_contactphone.phone_number,4,3),'-',RIGHT(cp_contactphone.phone_number,4)) END as phone"),
            DB::raw("CASE WHEN (cp_contact.latitude IS NULL) THEN null WHEN (cp_contact.longitude IS NULL) THEN null WHEN ($userLatitude=0) THEN null WHEN ($userLongitude=0) THEN null else concat(ROUND((point(cp_contact.longitude,cp_contact.latitude)<@>point($userLongitude,$userLatitude))::numeric,2),'M') end as Distance"),
            DB::raw("CASE WHEN (cp_contact.last_contacted IS NULL) THEN 'Never' else to_char(cp_contact.last_contacted, 'FMMonth DD,YYYY,hh12:mi a.m.')  END as last_contacted"),
            DB::raw("(SELECT string_agg(CONCAT(auth_user.first_name,' ',auth_user.last_name), ',') as sales_reps FROM cp_contactrep LEFT JOIN auth_user ON (cp_contactrep.user_id= auth_user.id)WHERE cp_contactrep.contact_id = cp_contact.id GROUP BY cp_contact.id) as sales_reps"),
            DB::raw("(SELECT string_agg(DISTINCT(CONCAT(cp_contactpersonnel.first_name,' ',cp_contactpersonnel.last_name)), ',') as people FROM cp_contactpersonnel WHERE cp_contactpersonnel.contact_id = cp_contact.id GROUP BY cp_contact.id) as people")
        )
            ->join('cp_company', 'cp_company.id', 'cp_contact.company_id')
            ->join('cp_contactcompany', 'cp_contactcompany.id', 'cp_contact.contact_company_id')
            ->leftjoin('cp_contactphone', 'cp_contactphone.id', 'cp_contact.default_phone_id')
            ->leftjoin('cp_state', 'cp_state.id', 'cp_contact.state_id')
            ->where('cp_company.id', $userCompanyId);
        if ($request->has('search')) {
            $contactList = $contactList->where('cp_contactcompany.name', 'ilike', '%' . $request->get('search') . '%');
        }
        if ($request->has('parent_company')) {
            $contactList = $contactList->where('cp_contact.title', $request->parent_company);
        }
        $contactList = $contactList->orderBy('cp_contact.id', 'ASC')->paginate(config('constants.Pagination.itemsPerPage'))->toArray();

        return $contactList;
    }


    /* Contact-List Get For Specific ContactId */
    public function getContactNotePreviousList($userId, $companyId, $contactId)
    {
        return ContactNote::with('noteSourceType')
            ->whereUser_id($userId)
            ->whereCompany_id($companyId)
            ->whereContact_id($contactId)
            ->get()
            ->toArray();
    }


    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
