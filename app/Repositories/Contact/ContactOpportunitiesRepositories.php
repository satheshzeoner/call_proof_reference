<?php

namespace App\Repositories\Contact;

use Illuminate\Support\Carbon;
/* Models */

use App\Models\Contact\ContactType;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;
use App\Models\State\State;
use App\Models\Country\Country;
use App\Models\People\PeopleRole;
use App\Models\Contact\Contact;
use App\Models\Contact\ContactPhone;
use App\Models\Contact\ContactPersonnel;
use App\Models\Opportunity\Opportunity;
use App\Models\Opportunity\OppContact;
use App\Models\Opportunity\OppPersonnel;



class ContactOpportunitiesRepositories
{
    /* ===============================CREATE=============================== */

    public function addContactOpportunitiesDetails(
        $request,
        $userCompanyId,
        $userId,
        $contactId
    ) {
        try {
            $contact = $this->getContact($request, $contactId);
            if (!$contact) {
                Helper::customError(400, config('constants.Errors.ContactNotFound'), $request->all(), config('constants.ServiceName.ContactOpportunity'));
                return false;
            }

            $linkedAccounts = ($request->has('linked_accounts')) ? collect(json_decode($request->linked_accounts)) : [$contactId];
            $linkedContacts = ($request->has('linked_contacts')) ? collect(json_decode($request->linked_contacts))  : [];
            DB::beginTransaction();
            $opportunity = new Opportunity();
            $opportunity->opp_name = $request->opportunity_name;
            $opportunity->opp_stage_id = $request->opportunity_stage_id;
            $opportunity->opp_type_id = $request->opportunity_type_id;
            $opportunity->company_id =  $userCompanyId;
            $opportunity->contact_id = $contactId;
            $opportunity->user_id = $request->owner_id;
            $opportunity->fill($request->all());
            $opportunity->save();

            $opportunityContact = new OppContact();
            $opportunityContact->company_id = $userCompanyId;
            $opportunityContact->contact_id = $contactId;
            $opportunityContact->opp_id = $opportunity->id;
            $opportunityContact->created = now();
            $opportunityContact->save();

            if ($opportunity) {
                foreach ($linkedAccounts as $key => $linkedAccount) {
                    $opportunityContact = new OppContact();
                    $opportunityContact->company_id = $userCompanyId;
                    $opportunityContact->contact_id = $linkedAccount;
                    $opportunityContact->opp_id = $opportunity->id;
                    $opportunityContact->created = now();
                    $opportunityContact->save();
                }
                foreach ($linkedContacts as $key => $linkedContact) {
                    $opportunityContact = new OppPersonnel();
                    $opportunityContact->opp_id = $opportunity->id;
                    $opportunityContact->personnel_id = $linkedContact;
                    $opportunityContact->save();
                }
            }
            DB::commit();
            return $opportunity;
        } catch (Exception $e) {
            DB::rollBack();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.ContactOpportunity'));
            return false;
        }
    }



    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    public function getContact($request, $contactId)
    {
        $contact = Contact::where('id', $contactId)->first();
        return $contact;
    }

    public function getContactOpportunityDetails(
        $request,
        $userCompanyId,
        $userId,
        $contactId,
        $opportunityId
    ) {
        $opportunity = Opportunity::whereId($opportunityId)
            ->whereContact_id($contactId)
            ->first();
        return $opportunity;
    }



    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */
    public function editContactOpportunitiesDetails($request, $userCompanyId, $contactId, $opportunityId)
    {
        try {
            $contact = $this->getContact($request, $contactId);
            if (!$contact) {
                Helper::customError(400, config('constants.Errors.ContactNotFound'), $request->all(), config('constants.ServiceName.ContactOpportunity'));
                return false;
            }
            $linkedAccounts = ($request->has('linked_accounts')) ? collect(json_decode($request->linked_accounts)) : [$contactId];
            $linkedContacts = ($request->has('linked_contacts')) ? collect(json_decode($request->linked_contacts))  : [];
            DB::beginTransaction();
            $opportunity = Opportunity::find($opportunityId);
            $opportunity->opp_name = $request->opportunity_name;
            $opportunity->opp_stage_id = $request->opportunity_stage_id;
            $opportunity->opp_type_id = $request->opportunity_type_id;
            $opportunity->user_id = $request->owner_id;
            $opportunity->fill($request->all());
            $opportunity->save();

            if ($opportunity) {
                $existingOpportunityContact = OppContact::where('company_id', $userCompanyId)->where('opp_id', $opportunity->id)->delete();
                foreach ($linkedAccounts as $key => $linkedAccount) {
                    $opportunityContact = new OppContact();
                    $opportunityContact->company_id = $userCompanyId;
                    $opportunityContact->contact_id = $linkedAccount;
                    $opportunityContact->opp_id = $opportunity->id;
                    $opportunityContact->created = now();
                    $opportunityContact->save();
                }
                foreach ($linkedContacts as $key => $linkedContact) {
                    $opportunityPersonnel = OppPersonnel::where('opp_id', $opportunity->id)->first();
                    if ($opportunityPersonnel) {
                        $opportunityPersonnel->opp_id = $opportunity->id;
                        $opportunityPersonnel->personnel_id = $linkedContact;
                        $opportunityPersonnel->save();
                    }
                }
                $opportunityContact = new OppContact();
                $opportunityContact->company_id = $userCompanyId;
                $opportunityContact->contact_id = $contactId;
                $opportunityContact->opp_id = $opportunity->id;
                $opportunityContact->created = now();
                $opportunityContact->save();
            }
            DB::commit();
            return $opportunity;
        } catch (Exception $e) {
            DB::rollBack();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.ContactOpportunity'));
            return false;
        }
    }
    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */
    /* Remove Specific Opportunity */
    public function removeOpportunity($userId, $companyId, $contactId, $opportunityId)
    {
        return Opportunity::whereCompany_id($companyId)
            ->whereUser_id($userId)
            ->whereContact_id($contactId)
            ->whereId($opportunityId)
            ->delete();
    }

    /* Remove Specific Opportunity Contact */

    public function removeOpportunityContact($userId, $companyId, $contactId, $opportunityId)
    {
        return OppContact::whereCompany_id($companyId)
            ->whereOpp_id($opportunityId)
            ->whereContact_id($contactId)
            ->delete();
    }
    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
