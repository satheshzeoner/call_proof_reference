<?php

namespace App\Repositories\Contact;

/* Models */

use App\Models\Contact\Contact;
use App\Models\Contact\ContactRep;
use App\Models\Appointment\Appointment;
use App\Models\Eventform\Eventform;
use App\Models\Provider\Provider;
use App\Models\Location\Location;
use App\Models\State\State;
use App\Models\Appointment\AppointmentPersonnel;

//TODO temporarily commented , need to be removed after finalizing
//use App\Models\Contact\ContactPersonnel;
//use App\Models\Cfield\CfieldType;
//use App\Models\Cfield\CfieldTable;
//use App\Models\Cfield\Cfield;
//use App\Models\Cfield\CfieldAuto;
//use App\Models\Contact\ContactType;
//use App\Models\Contact\ContactCompany;
//use App\Models\Contact\ContactPhone;
//use App\Models\Opportunity\OppContact;

use Exception;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;

class ContactAppointmentRepositories
{
    /* ===============================CREATE=============================== */
    public function doAppointmentStore(
        $request,
        $userCompanyId,
        $userId,
        $contact,
        $start,
        $latitude,
        $longitude,
        $eventFormId
    ) {


        try {
            DB::beginTransaction();
            $appointment = new Appointment();
            $appointment->user_id = $userId;
            $appointment->company_id = $userCompanyId;
            $appointment->contact_id = $contact->id;
            $appointment->duration = config('constants.ContactAppointment.appointment_duration');
            $appointment->scheduled = config('constants.ContactAppointment.appointment_scheduled');
            $appointment->start = $start;
            $appointment->created = now();
            $appointment->updated = now();

            if ($latitude) {
                $appointment->latitude = $latitude;
            }
            if ($longitude) {
                $appointment->longitude = $longitude;
            }
            $eventForm = Eventform::where('id', $eventFormId)->first();
            if ($eventForm) {
                $appointment->event_form_id = $eventForm->id;
            }
            $appointment->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contactAppointment'));
            return false;
        }
        return $appointment;
    }


    public function doLocationLogStore(
        $request,
        $userId,
        $latitude,
        $longitude,
        $startAppointment,
        $appointment
    ) {

        try {
            if ($latitude && $longitude) {
                DB::beginTransaction();
                $provider = Provider::where('name', config('constants.Provider.name'))->first();
                $location = new Location();
                $location->user_id = $userId;
                $location->provider_id = $provider->id;
                $location->geocode_count = config('constants.Location.defalut_geocode_count');
                $location->created = now();
                $location->latitude = $latitude;
                $location->longitude = $longitude;
                $location->geo_track_type  = $startAppointment;
                $location->link_id   = $appointment->id;
                $location->save();
                DB::commit();
                return $location;
            } else {
                Helper::customError(400, config('constants.Errors.LatitudeAndLongitudeRequired'), $request->all(), config('constants.ServiceName.contactAppointment'));
                return false;
            }
        } catch (Exception $e) {
            DB::rollback();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contactAppointment'));
            return false;
        }
    }
    public function doReverseGeocode(
        $request,
        $contact,
        $start,
        $locationLog,
        $latitude,
        $longitude
    ) {
        try {
            $mapApiId = config('constants.ContactAppointment.MAP_API_ID');
            $mapApiCode = config('constants.ContactAppointment.MAP_API_CODE');
            $geoCodeBaseUrl = config('constants.ContactAppointment.geo_base_code_url');
            $geoAppCodeUrl = config('constants.ContactAppointment.geo_app_code_url');
            $geoCodeAddressUrl = config('constants.ContactAppointment.geo_address_url');
            $geoCode =  $geoCodeBaseUrl . $mapApiId . $geoAppCodeUrl . $mapApiCode . $geoCodeAddressUrl . $latitude . ',' . $longitude;
            $geoCodeResponse =  file_get_contents($geoCode);
            $output = json_decode($geoCodeResponse);
            $response = $output->Response->View;
            if ($response) {
                $resAddress = $output->Response->View[0]->Result[0]->Location->Address;
                $resHouseNumber = isset($resAddress->HouseNumber) ? $resAddress->HouseNumber : '';
                $resBuilding  = isset($resAddress->Building) ? $resAddress->Building : '';
                $resStreet  = isset($resAddress->Street) ? $resAddress->Street : '';
                $resCity  = isset($resAddress->City) ? $resAddress->City : '';
                $resState  = isset($resAddress->State) ? $resAddress->State : '';
                $resCountry  = isset($resAddress->Country) ? $resAddress->Country : '';
                $resPostalCode  = isset($resAddress->PostalCode) ? $zipCode = $resAddress->PostalCode : '';
                if (strlen($resPostalCode) > config('constants.ContactAppointment.zip_max_limit')) {
                    $zipCode = substr($resPostalCode, 0, config('constants.ContactAppointment.zip_max_limit'));
                }
                if (strlen($resPostalCode) == config('constants.ContactAppointment.zip_min_limit')) {
                    $zipCode = config('constants.ContactAppointment.add_before_zip_code') . $resPostalCode;
                }
                $address  = trim($resHouseNumber . ' ' . $resBuilding . ' ' . $resStreet);
                $state = State::where('abbr', $resState)->first(); // check resState valid
                //save locations
                if ($locationLog) {
                    DB::beginTransaction();
                    if ($address || $resCity || $resPostalCode || $state) {
                        if ($state) {
                            $locationLog->state_id = $state->id;
                        }
                        $locationLog->address     = $address;
                        $locationLog->city        = $resCity;
                        $locationLog->zip         = $zipCode;
                        if ($locationLog->geocode_count) {
                            $newGeocodeCount = $locationLog->geocode_count + 1;
                        } else {
                            $newGeocodeCount = 1;
                        }
                        $locationLog->geocode_count = $newGeocodeCount;
                        $locationLog->save();
                        $locationLog->last_geocode = now();
                        if ($locationLog->last_geocode) {
                            $locationLog->last_geocode->addDays(2 * $newGeocodeCount);
                        } else {
                            $locationLog->last_geocode = now()->addDays(2 * $newGeocodeCount);
                        }
                        $locationLog->save();
                        $contact->last_contacted = $start;
                        $contact->save();
                        DB::commit();
                    }
                }
                return true;
            }
        } catch (Exception $e) {
            DB::rollback();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contactAppointment'));
            return false;
        }
    }

    public function doAppointmentPersonnelStore(
        $request,
        $appointment,
        $contactPersonnel
    ) {

        try {
            DB::beginTransaction();
            $appointmentPersonnel = new AppointmentPersonnel;
            $appointmentPersonnel->appointment_id = $appointment->id;
            $appointmentPersonnel->personnel = $contactPersonnel->id;
            $appointmentPersonnel->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contactAppointment'));
            return false;
        }
    }

    public function ContactUserStore(
        $request,
        $contact,
        $userId
    ) {
        try {
            DB::beginTransaction();
            $contactUser = new ContactRep();
            $contactUser->contact_id = $contact->id;
            $contactUser->user_id = $userId;
            $contactUser->created = now();
            $contactUser->save();
            $contact->assigned = true;
            $contact->created = now();
            $contact->save();
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contactAppointment'));
            return false;
        }
    }

    public function isSalesUserSingleModeContactUserStore(
        $request,
        $contact,
        $userId,
        $salesUserSingleMode
    ) {
        try {
            if ($contact) {
                $contactUser = ContactRep::where('user_id', $userId)->where('contact_id', $contact->id)->first();
                if ($contactUser) {
                    return;
                }
                if ($salesUserSingleMode) {
                    $contactUser = ContactRep::where('contact_id', $contact->id)->first();
                    if (!$contactUser) {
                        $this->ContactUserStore($request, $contact, $userId);
                    }
                } else {
                    $this->ContactUserStore($request, $contact, $userId);
                }
                return true;
            }
            return;
        } catch (Exception $e) {
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contactAppointment'));
            return false;
        }
    }

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */
    public function getContact(
        $request,
        $contactId,
        $userCompanyId
    ) {
        $contact = Contact::where('id', $contactId)
            ->where('company_id', $userCompanyId)
            ->first();
        return $contact;
    }
    public function getAppointment(
        $request,
        $userId
    ) {
        $appointment = Appointment::where('user_id', $userId)
            ->where('scheduled', config('constants.ContactAppointment.appointment_scheduled'))
            ->where('duration', config('constants.ContactAppointment.appointment_duration'))
            ->orderBy('id', 'DESC')
            ->first();
        return $appointment;
    }

    public function getPreviousAppointment(
        $request,
        $userId
    ) {
        $appointment = Appointment::where('user_id', $userId)
            ->where('duration', config('constants.ContactAppointment.appointment_duration'))
            ->get();
        return $appointment;
    }

    public function getCurrentAppointmentDetails(
        $request,
        $userId
    ) {
        $appointment = Appointment::where('user_id', $userId)
            ->where('duration', config('constants.ContactAppointment.appointment_duration'))
            ->orderBy('id', 'DESC')
            ->first();
        return $appointment;
    }
    //TODO temporarily commented , need to be removed after finalizing
    // public function getCustomField(
    //     $request,
    //     $userId,
    //     $userCompanyId,
    //     $customFieldTable,
    //     $showHidden,
    //     $showImgFields,
    //     $contactId
    // ) {
    //     try {
    //         $typeImage = CfieldType::where('name', config('constants.ContactAppointment.cfield_type_image'))->first();
    //         $typeImageId = ($typeImage) ? $typeImage->id : 0;
    //         $customFieldTable  = CfieldTable::where('name', $customFieldTable)->first();
    //         $customFieldTableId = ($customFieldTable) ? $customFieldTable->id : 0;
    //         $customFields = Cfield::select('cp_cfield.id as id', 'cp_cfield.name as name', 'cp_cfield.position as position', 'cp_cfield.position as position', 'cp_cfield.required as required', 'cp_cfield.id as custom_field_id', 'cp_cfield.cfield_id as custom_field_inner_id', 'cp_cfield.cfield_type_id', 'cp_cfield.cfield_option_default_id', 'cp_cfieldvalue.cf_value as value')
    //             ->where('cp_cfield.company_id', $userCompanyId)
    //             ->where('cp_cfield.cfield_table_id', $customFieldTableId)
    //             ->where('cp_cfieldvalue.entity_id', $contactId)
    //             ->where('cp_cfield.hide_on_checkout', config('constants.ContactAppointment.cfield_hide_on_checkout'))
    //             ->with(['customFieldType'])
    //             ->with(['customFieldOption' => function ($cFieldOptionJoin) {
    //                 $cFieldOptionJoin->orderBy('position', 'ASC');
    //             }])
    //             ->leftJoin('cp_cfieldvalue', 'cp_cfieldvalue.cfield_id', 'cp_cfield.id')
    //             ->orderBy('cp_cfield.position', 'ASC')->get();

    //         $customFieldData = [];

    //         foreach ($customFields as $key => $customField) {
    //             if ($showHidden) {
    //                 if ($customField->cfield_type_id == $typeImageId) {
    //                     if ($showImgFields) {
    //                         $customFieldData[$key] = $customField;
    //                     } else {
    //                         $customFieldData[$key] = $customField;
    //                     }
    //                 } else {
    //                     $customFieldAuto = CfieldAuto::where('company_id', $userCompanyId)->where('cfield_id', $customField->id)->first();
    //                     $customFieldAutoHide = $customFieldAuto ? $customFieldAuto->hide : null;
    //                     if (!$customFieldAutoHide) {
    //                         if ($customField->cfield_type_id == $typeImageId) {
    //                             if ($showImgFields) {
    //                                 $customFieldData[$key] = $customField;
    //                             } else {
    //                                 $customFieldData[$key] = $customField;
    //                             }
    //                         }
    //                     }
    //                 }
    //             } else {
    //                 $customFieldData[$key] = $customField;
    //             }
    //             $customFieldData[$key]['custom_field_type_id']           = $customFieldData[$key]['cfield_type_id'];
    //             $customFieldData[$key]['custom_field_option_default_id'] = $customFieldData[$key]['cfield_option_default_id'];
    //             unset($customFieldData[$key]['cfield_type_id']);
    //             unset($customFieldData[$key]['cfield_option_default_id']);
    //         }
    //         return $customFieldData;
    //     } catch (Exception $e) {
    //         Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contactAppointment'));
    //         return false;
    //     }
    // }

    public function getContactDetail(
        $request,
        $userId,
        $userCompanyId,
        $contactId
    ) {
        $contact = Contact::where('id', $contactId)->where('company_id', $userCompanyId)
            ->orderBy('id', 'DESC')
            ->first();
        $data['contact'] = $contact;
        $data['contactCompany'] = isset($contact->contactCompany) ? ($contact->contactCompany) : null;
        $data['state'] = isset($contact->State) ? ($contact->State) : null;
        $data['country'] = isset($contact->Country) ? ($contact->Country) : null;
        return $data;
    }



    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */
    public function doAutoEndPreviousAppointment(
        $request,
        $userId,
        $previousAppointments
    ) {
        try {
            DB::beginTransaction();
            foreach ($previousAppointments as $key => $appointment) {
                $appointment->update(['updated' => now(), 'duration' => config('constants.ContactAppointment.auto_end_appointment_duration')]);
            }
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contactAppointment'));
            return false;
        }
    }
    //TODO temporarily commented , need to be removed after finalizing
    // public function doContactPersonnelStore(
    //     $request,
    //     $appointment,
    //     $personnelIdList
    // ) {
    //     DB::beginTransaction();
    //     try {
    //         foreach ($personnelIdList as $key => $personnelId) {
    //             $contactPersonnel = ContactPersonnel::where('id', $personnelId)->first();
    //             if ($contactPersonnel) {
    //                 $contactPersonnel->last_contacted = now();
    //                 $contactPersonnel->save();
    //                 DB::commit();
    //                 $this->doAppointmentPersonnelStore($request, $appointment, $contactPersonnel);
    //             }
    //         }
    //         return true;
    //     } catch (Exception $e) {
    //         DB::rollback();
    //         Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contactAppointment'));
    //         return false;
    //     }
    // }

    public function doContactEndAppointmentStoreDetails(
        $request,
        $userId,
        $userCompanyId,
        $appointmentId,
        $contactId,
        $notes,
        $selectedContacts
    ) {

        try {
            DB::beginTransaction();
            $contactAppointment = Appointment::where('id', $appointmentId)
                ->where('user_id', $userId)
                ->where('contact_id', $contactId)
                ->where('company_id', $userCompanyId)
                ->orderBy('id', 'DESC')
                ->first();
            $contactAppointment->notes = $notes;
            $contactAppointment->stop = now();
            $contactAppointment->duration =  now()->diffInSeconds($contactAppointment->start);
            $contactAppointment->save();
            $appointmentPersonnel = AppointmentPersonnel::where('appointment_id', $appointmentId)
                ->delete();
            foreach ($selectedContacts as $key => $selectedContact) {
                $newAppointmentPersonnel = new AppointmentPersonnel();
                $newAppointmentPersonnel->appointment_id = $appointmentId;
                $newAppointmentPersonnel->personnel_id = $selectedContact;
                $newAppointmentPersonnel->save();
            }
            DB::commit();
            return $contactAppointment;
        } catch (Exception $e) {
            DB::rollback();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contactAppointment'));
            return false;
        }
    }

    //TODO temporarily commented , need to be removed after finalizing
    // public function doUpdateAccountInfoDetail(
    //     $request,
    //     $userId,
    //     $userCompanyId,
    //     $contactId,
    //     $updateAccountInfoDetail,
    //     $userCompanyPeopletab
    // ) {
    //     DB::beginTransaction();
    //     try {
    //         $accountType = ContactType::find($request->account_type_id);
    //         if (!$accountType) {
    //             Helper::customError(400, config('constants.Errors.AccountTypeNotFound'), $request->all(), config('constants.ServiceName.contactAppointment'));
    //             return false;
    //         }
    //         $contact = Contact::find($contactId);
    //         if (!$contact) {
    //             Helper::customError(400, config('constants.Errors.ContactNotFound'), $request->all(), config('constants.ServiceName.contactAppointment'));
    //             return false;
    //         }
    //         $contact->update([
    //             'account' => $updateAccountInfoDetail->account_number,
    //             'title' => $updateAccountInfoDetail->parent_company,
    //             'address' => $updateAccountInfoDetail->address,
    //             'address2' => $updateAccountInfoDetail->address2,
    //             'city' => $updateAccountInfoDetail->city,
    //             'state_id' => $updateAccountInfoDetail->state_id,
    //             'zip' => $updateAccountInfoDetail->zip_code,
    //             'email' => $updateAccountInfoDetail->company_email,
    //             'website' => $updateAccountInfoDetail->website,
    //         ]);
    //         $companyPhone = preg_replace('/(\W*)/', '', $updateAccountInfoDetail->company_phone);
    //         $contactCompany = $this->doContactCompanyStore($request, $updateAccountInfoDetail, $contact->contact_company_id);
    //         $contactPhone = ContactPhone::where('contact_id', $contact->id)->where('id', $contact->default_phone_id)->first();
    //         if ($contactPhone) {
    //             $contactPhone->update([
    //                 'phone_number' => $companyPhone,
    //                 'phone_type_id' => $updateAccountInfoDetail->phone_type_id,
    //                 'country_code' => $updateAccountInfoDetail->country_code,
    //             ]);
    //         }

    //         DB::commit();
    //     } catch (Exception $e) {
    //         DB::rollback();
    //         Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contactAppointment'));
    //         return false;
    //     }
    //     return $contact;
    // }

    //TODO temporarily commented , need to be removed after finalizing
    // public function doContactCompanyStore(
    //     $request,
    //     $requestData,
    //     $contactCompanyId
    // ) {
    //     DB::beginTransaction();
    //     try {
    //         $contactCompany = ContactCompany::find($contactCompanyId);
    //         $contactCompany->name = $requestData->company_name;
    //         $contactCompany->updated = now();
    //         $contactCompany->save();
    //         DB::commit();
    //     } catch (Exception $e) {
    //         DB::rollback();
    //         Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contactAppointment'));
    //         return false;
    //     }
    //     return $contactCompany;
    // }

    //TODO temporarily commented , need to be removed after finalizing
    // public function doContactSelectedOpportunitiesStoreDetails(
    //     $request,
    //     $userId,
    //     $userCompanyId,
    //     $appointmentId,
    //     $contactId,
    //     $selectedOpportunities
    // ) {
    //     DB::beginTransaction();
    //     try {

    //         $existingOpportunityContact = OppContact::where('opp_id', $appointmentId)->where('company_id', $userCompanyId)->where('contact_id', $contactId)
    //             ->delete();
    //         foreach ($selectedOpportunities as $key => $opportunityId) {
    //             $opportunity = new OppContact();
    //             $opportunity->opp_id = $opportunityId;
    //             $opportunity->contact_id = $contactId;
    //             $opportunity->company_id = $userCompanyId;
    //             $opportunity->created = now();
    //             $opportunity->save();
    //         }
    //         DB::commit();
    //         return true;
    //     } catch (Exception $e) {
    //         DB::rollback();
    //         Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contactAppointment'));
    //         return false;
    //     }
    // }

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
