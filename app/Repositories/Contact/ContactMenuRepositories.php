<?php

namespace App\Repositories\Contact;

/* Models */

use App\Models\Eventform\Eventform;
use App\Models\ContactMenu\ContactMenu;

class ContactMenuRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */
    public function getContactMenuDetails($userCompanyId, $userCompanyPeopleTab, $menuId, $isStartAppointment, $userContactMenuListId)
    {
        $eventFormId = Eventform::where('company_id', $userCompanyId)
            ->where('hide', true)
            ->get('id');
        $contactMenu = ContactMenu::select(
            'cp_eventform.name as title',
            'cp_contactmenu.name',
            'cp_contactmenu.type_id',
            'cp_contactmenu.event_form_id',
            'cp_eventform.event_form_treat_as_id',
            'cp_contactmenu.location',
            'cp_contactmenu.type_id'
        )
            ->leftJoin('cp_eventform', 'cp_eventform.id', 'cp_contactmenu.event_form_id');
        if ($userCompanyPeopleTab) {
            $contactMenu = $contactMenu->where('type_id', '!=', config('constants.ContactMenuType.add_new_person_to_company')); //hide add new person to company menu
        } else {
            $contactMenu = $contactMenu->where('type_id', '!=', config('constants.ContactMenuType.people')); //hide people menu
        }
        if ($menuId != 1) {
            $contactMenu = $contactMenu->where('type_id', '!=', config('constants.ContactMenuType.edit_task')); //hide Edit task menu
        } elseif ($menuId != 2) {
            $contactMenu = $contactMenu->where('type_id', '!=', config('constants.ContactMenuType.edit_appointment')); //hide Edit Appointment menu
        }
        if (!$isStartAppointment) {
            $contactMenu = $contactMenu->where('type_id', '!=', config('constants.ContactMenuType.appointment')); //hide Appointment menu
        }
        $contactMenus = $contactMenu->orderBy('cp_contactmenu.position', 'ASC')
            ->where('cp_contactmenu.contactmenulist_id', $userContactMenuListId ? $userContactMenuListId : null)
            ->where('cp_contactmenu.company_id', $userCompanyId)
            ->whereNotIn('cp_contactmenu.event_form_id', $eventFormId)
            ->where('cp_contactmenu.is_hide', false)
            ->get();

        return $contactMenus;
    }
    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
