<?php

namespace App\Repositories\Contact;

use App\Helpers\Helper;

/* Models */

use App\Models\Contact\Contact;
use App\Models\Contact\ContactCompany;
use App\Models\Contact\ContactType;
use App\Models\Contact\ContactPhone;
use App\Models\Contact\ContactNote;
use App\Models\Contact\ContactNoteSourceType;
use App\Models\Auth\AuthUser;
use App\Models\PhoneType\PhoneType;
use Exception;
use Illuminate\Support\Facades\DB;

class EditContactRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    public function doEditContact(
        $request,
        $userId,
        $contactId
    ) {
        DB::beginTransaction();
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $userCompanyPeopletab = AuthUser::find($userId)->userProfile->userCompany->people_tab;
            $requests = [];
            $requests['contact_id'] = $contactId;
            $requests['contact_type_id'] = $request->account_type_id;
            $requests['account'] = $request->account_number;
            $requests['name'] = $request->company_name;
            $requests['title'] = $request->parent_company;
            $requests['zip'] = $request->zipcode;
            $requests['state_id'] = ($request->state_id) ? $request->state_id : null;
            $requests['country_id'] = ($request->country_id) ? $request->country_id : null;
            $requests['phone_number'] = preg_replace('/(\W*)/', '', $request->phone);
            $requests['is_active_lead'] = false;
            $requests['email'] = $request->company_email;
            $requests['note'] = $request->notes;
            $requests['company_id'] = $userCompanyId;
            $requests['user_id'] = $userId;
            $requests['updated'] = now();
            $requestData = array_merge($request->all(), $requests);
            $notes = $request->notes;

            $accountType = ContactType::find($request->account_type_id);
            if (!$accountType) {
                Helper::customError(400, config('constants.Errors.AccountTypeNotFound'), $request->all(), config('constants.ServiceName.EditContact'));
                return false;
            }
            $contact = Contact::findorFail($contactId);
            $contactCompany = $this->doContactCompanyStore($request, $userCompanyPeopletab, $requests, $contact->contact_company_id);
            $requestData['contact_company_id'] = (isset($contactCompany->id)) ? ($contactCompany->id) : $request->contact_company_id;
            $contact->fill($requestData);
            $contact->save();
            DB::commit();

            $result['requestData'] = $requestData;
            $result['contact'] = $contact;
            $result['userCompanyId'] = $userCompanyId;
            $result['userCompanyPeopletab'] = $userCompanyPeopletab;
            $result['notes'] = $notes;
        } catch (Exception $e) {
            DB::rollback();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.EditContact'));
            return false;
        }
        return $result;
    }
    public function doContactPhoneStore(
        $request,
        $contactData
    ) {
        $contact = $contactData['contact'];
        $requestData = $contactData['requestData'];
        DB::beginTransaction();
        try {
            if (isset($requestData['phone']) && isset($requestData['phone_type_id'])) {
                $contactPhone = ContactPhone::where('contact_id', $contact->id)->where('id', $contact->default_phone_id)->first();
                if (!$contactPhone) {
                    $contactPhone = new ContactPhone();
                    $contactPhone->created = now();
                    $contactPhone->unknown = false;
                    $contactPhone->is_in_dnc = false;
                    $contactPhone->is_in_dnd = false;
                    $contactPhone->is_active_lead = false;
                    $contactPhone->phone_type_id = $requestData['phone_type_id'];
                }
                $contactPhone->fill($requestData);
                $contactPhone->save();
                DB::commit();
            }
        } catch (Exception $e) {
            DB::rollback();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.EditContact'));
            return false;
        }
        return true;
    }
    public function doContactCompanyStore(
        $request,
        $userCompanyPeopletab,
        $requestData,
        $contactCompanyId
    ) {
        DB::beginTransaction();
        try {

            if ($userCompanyPeopletab) {
                $contactCompany = ContactCompany::find($contactCompanyId);
                $contactCompany->fill($requestData);
                $contactCompany->save();
            } else {
                $contactCompany = ContactCompany::where('id', $requestData['contact_company_id'])->where('company_id', $requestData['company_id'])->first();
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.EditContact'));
            return false;
        }
        return $contactCompany;
    }
    public function doContactNoteStore(
        $request,
        $contactData
    ) {
        $contact = $contactData['contact'];
        $requestData = $contactData['requestData'];
        $notes = $contactData['notes'];
        DB::beginTransaction();
        try {
            if (!empty($notes)) {
                $contactNoteSourceType = ContactNoteSourceType::where('name', 'Add Contact')->first();
                $contactNote = ContactNote::where('contact_id', $contact->id)->first();
                $contactNote->sourcetype_id =  $contactNoteSourceType->id;
                $contactNote->source_id =  $contact->id;
                $contactNote->fill($requestData);
                $contactNote->save();
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.EditContact'));
            return false;
        }
        return true;
    }

    public function doUpdateContactParentCompany(
        $request,
        $userId,
        $companyId,
        $contactId,
        $parentCompany
    ) {
        DB::beginTransaction();
        try {
            $contact = Contact::where('id', $contactId)->where('company_id', $companyId)->first();
            $contact->title = $parentCompany;
            $contact->save();
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.EditContact'));
            return false;
        }
        return true;
    }

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
