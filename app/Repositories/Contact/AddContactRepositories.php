<?php

namespace App\Repositories\Contact;

/* Models */

use App\Models\Contact\Contact;
use App\Models\Contact\ContactCompany;
use App\Models\Contact\ContactType;
use App\Models\Contact\ContactPhone;
use App\Models\Contact\ContactNote;
use App\Models\Contact\ContactNoteSourceType;
use App\Models\Auth\AuthUser;
use App\Models\PhoneType\PhoneType;
use Exception;
use App\Helpers\Helper;
use Illuminate\Support\Facades\DB;

class AddContactRepositories
{
    /* ===============================CREATE=============================== */
    public function doAddContact(
        $request,
        $userId
    ) {
        DB::beginTransaction();
        try {
            $userCompanyId = AuthUser::find($userId)->userProfile->company_id;
            $userCompanyPeopletab = AuthUser::find($userId)->userProfile->userCompany->people_tab;
            $requests = [];
            $requests['contact_type_id'] = $request->account_type_id;
            $requests['account'] = $request->account_number;
            $requests['name'] = $request->company_name;
            $requests['title'] = $request->parent_company;
            $requests['zip'] = $request->zipcode;
            $requests['state_id'] = ($request->state_id) ? $request->state_id : null;
            $requests['country_id'] = ($request->country_id) ? $request->country_id : null;
            $requests['phone_number'] = preg_replace('/(\W*)/', '', $request->phone);
            $requests['email'] = $request->company_email;
            $requests['note'] = $request->notes;
            $requests['company_id'] = $userCompanyId;
            $requests['created_by_id'] = $userId;
            $requests['user_id'] = $userId;
            $requests['created'] = now();
            $requests['updated'] = now();
            $requests['unknown'] = false;
            $requests['do_not_sms'] = false;
            $requests['assigned'] = false;
            $notes = $request->notes;
            $requestData = array_merge($request->all(), $requests);
            $contactCompany = $this->doContactCompanyStore($request, $userCompanyPeopletab, $requestData);
            $requestData['contact_company_id'] = (isset($contactCompany->id)) ? ($contactCompany->id) : $request->contact_company_id;
            $contact = new Contact;
            $contact->company_id = $userCompanyId;
            $contact->fill($requestData);
            $contact->save();
            DB::commit();

            $result['requestData'] = $requestData;
            $result['contact'] = $contact;
            $result['userCompanyId'] = $userCompanyId;
            $result['userCompanyPeopletab'] = $userCompanyPeopletab;
            $result['notes'] = $notes;
        } catch (Exception $e) {
            DB::rollback();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.AddContact'));
            return false;
        }
        return $result;
    }
    public function doContactPhoneStore(
        $request,
        $contactData
    ) {
        $contact = $contactData['contact'];
        $requestData = $contactData['requestData'];
        DB::beginTransaction();
        try {
            if ($requestData['phone'] && $requestData['phone_type_id']) {
                $contactPhone = new ContactPhone();
                $contactPhone->contact_id = $contact->id;
                $contactPhone->is_active_lead = false;
                $contactPhone->is_in_dnc = false;
                $contactPhone->is_in_dnd = false;
                $contactPhone->fill($requestData);
                $contactPhone->save();
                $contact->default_phone_id = $contactPhone->id;
                $contact->save();
                DB::commit();
            }
        } catch (Exception $e) {
            DB::rollback();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.AddContact'));
            return false;
        }
        return true;
    }
    public function doContactCompanyStore(
        $request,
        $userCompanyPeopletab,
        $requestData
    ) {
        DB::beginTransaction();
        try {
            if ($userCompanyPeopletab) {
                $contactCompany = new ContactCompany();
                $contactCompany->fill($requestData);
                $contactCompany->save();
            } else {
                $contactCompany = ContactCompany::where('id', $requestData['contact_company_id'])->where('company_id', $requestData['company_id'])->first();
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.AddContact'));
            return false;
        }
        return $contactCompany;
    }
    public function doContactNoteStore(
        $request,
        $contactData
    ) {
        $contact = $contactData['contact'];
        $requestData = $contactData['requestData'];
        $notes = $contactData['notes'];
        DB::beginTransaction();
        try {
            if (!empty($notes)) {
                $contactNoteSourceType = ContactNoteSourceType::where('name', 'Add Contact')->first();
                $contactNote = new ContactNote;
                $contactNote->contact_id = $contact->id;
                $contactNote->sourcetype_id =  $contactNoteSourceType->id;
                $contactNote->source_id =  $contact->id;
                $contactNote->fill($requestData);
                $contactNote->save();
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.AddContact'));
            return false;
        }
        return true;
    }


    /* Add Contact Notes */
    public function doStoreContactNote($request, $contactNoteDetails)
    {
        try {
            DB::beginTransaction();
            $contactNoteId = ContactNote::create($contactNoteDetails);
            DB::commit();
            return $contactNoteId->id;
        } catch (Exception $e) {
            DB::rollback();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.AddContact'));
            return false;
        }
    }

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
