<?php

namespace App\Repositories\Contact;

use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;
use Exception;

/* Models */
use App\Models\Contact\Contact;
use App\Models\Contact\ContactImage;


class ContactImageRepositories
{
    /* ===============================CREATE=============================== */

    public function doUploadImageDetail(
        $request,
        $userId,
        $userCompanyId,
        $contactId,
        $contactImagePath
    ) {
        try {
            $validationResult['status'] = true;
            $validationResult['message'] = config('constants.Success.ContactImage');
            $contact = $this->isContactExist($request, $contactId);
            if (!$contact) {
                Helper::customError(400, config('constants.Errors.ContactNotFound'), $request->all(), config('constants.ServiceName.ContactImage'));
                $validationResult['status'] = false;
                $validationResult['message'] = config('constants.Errors.ContactNotFound');
                return $validationResult;
            }
            DB::beginTransaction();
            $contactImage = new ContactImage();
            $contactImage->company_id = $userCompanyId;
            $contactImage->user_id = $userId;
            $contactImage->contact_id = $contactId;
            $contactImage->image = $contactImagePath;
            $contactImage->created = now();
            $contactImage->save();
            DB::commit();
            return $validationResult;
        } catch (Exception $e) {
            DB::rollBack();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.ContactImage'));
            $validationResult['status'] = false;
            $validationResult['message'] = config('constants.Errors.ContactImage');
            return $validationResult;
        }
    }

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */
    public function getContact(
        $request,
        $userId,
        $userCompanyId,
        $contactId
    ) {
        $contact = Contact::whereId($contactId)->whereCompany_id($userCompanyId)
            ->first();
        return $contact;
    }

    public function getContactImageDetail(
        $request,
        $userId,
        $userCompanyId,
        $contactId
    ) {
        $contactImageDetail = ContactImage::whereCompany_id($userCompanyId)->whereUser_id($userId)
            ->whereContact_id($contactId)
            ->get();
        return $contactImageDetail;
    }

    public function showImageDetail(
        $request,
        $userId,
        $userCompanyId,
        $contactId,
        $imageId
    ) {
        $contactImageDetail = ContactImage::select('id as image_id', 'image as image_path')->whereId($imageId)->whereCompany_id($userCompanyId)->whereUser_id($userId)
            ->whereContact_id($contactId)
            ->get();
        return $contactImageDetail;
    }
    public function isContactExist($request, $contactId)
    {
        $contact = Contact::where('id', $contactId)->first();
        return $contact;
    }
    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */
    /* Remove Specific ContactImage */
    public function removeContactImage($request, $contactImageId)
    {
        return ContactImage::whereId($contactImageId)
            ->delete();
    }
    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
