<?php

namespace App\Repositories\Contact;

use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;
use Exception;

/* Models */
use App\Models\Contact\Contact;
use App\Models\Label\Label;


class ContactLabelRepositories
{
    /* ===============================CREATE=============================== */

    public function doAddContactLabelDetails(
        $request,
        $userCompanyId,
        $contactId
    ) {
        try {
            DB::beginTransaction();
            $label = new Label();
            $label->company_id = $userCompanyId;
            $label->name = $request->label;
            $label->save();

            $contact = Contact::find($contactId);
            $contact->contactLabel()->attach($label->id);
            DB::commit();
            return $label;
        } catch (Exception $e) {
            DB::rollBack();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.ContactLabel'));
            return false;
        }
    }

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    public function getContactLabelsDetail(
        $request,
        $userId,
        $userCompanyId,
        $contactId
    ) {
        $contact = Contact::find($contactId);
        $labelIdList = $contact->contactLabel()->pluck('label_id');
        $contactLabelDetail = Label::whereIn('id', $labelIdList)->whereCompany_id($userCompanyId);
        if ($request->has('search')) {
            $contactLabelDetail = $contactLabelDetail->where('name', 'ilike', '%' . $request->get('search') . '%');
        }

        return $contactLabelDetail->get();
    }

    public function isContactExist($request, $contactId)
    {
        $contact = Contact::where('id', $contactId)->first();
        return $contact;
    }

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */
    /* Remove Specific ContactLabel */
    public function removeContactLabel($request, $contactId, $labelId)
    {
        $contact = Contact::find($contactId);
        return $contact->contactLabel()->detach([$labelId]);
    }
    /* Remove Specific Label */
    public function removeLabel($request, $labelId)
    {
        return Label::whereId($labelId)
            ->delete();
    }
    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
