<?php

namespace App\Repositories\Contact;

use Exception;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;
/* Models */

use App\Models\Contact\Contact;
use App\Models\Contact\ContactNote;
use App\Models\Cfield\CfieldType;
use App\Models\Cfield\CfieldTable;
use App\Models\Cfield\Cfield;
use App\Models\Cfield\CfieldAuto;

class ContactViewRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */
    public function getContactDataDetails($request, $userCompanyId, $contactId)
    {
        $contact = Contact::where('id', $contactId)->where('company_id', $userCompanyId)->firstOrFail();
        $dataContactView['contact'] = $contact;
        $dataContactView['contactType'] = isset($contact->contactType) ? ($contact->contactType) : null;
        $dataContactView['contactCompany'] = isset($contact->contactCompany) ? ($contact->contactCompany) : null;
        $dataContactView['state'] = isset($contact->State) ? ($contact->State) : null;
        $dataContactView['country'] = isset($contact->Country) ? ($contact->Country) : null;
        $dataContactView['contactPhone'] = isset($contact->contactPhone) ? ($contact->contactPhone) : null;
        $dataContactView['contactPhoneType'] = isset($contact->contactPhone) ? $contact->contactPhone->phoneType : null;
        $dataContactView['contactPersonnel'] = isset($contact->ContactPersonnel) ? $contact->ContactPersonnel : null;
        $dataContactView['contactUsers'] = isset($contact->contactUsers) ? $contact->contactUsers : null;
        $dataContactView['contactInfoEmails'] = isset($contact->ContactInfoEmail) ? $contact->ContactInfoEmail : null;
        return $dataContactView;
    }


    public function getCustomField(
        $request,
        $userId,
        $userCompanyId,
        $customFieldTable,
        $showHidden,
        $showImgFields,
        $contactId
    ) {
        try {
            $customFieldData = [];
            $typeImage = CfieldType::where('name', config('constants.ContactView.cfield_type_image'))->first();
            $typeImageId = ($typeImage) ? $typeImage->id : 0;
            $customFieldTable  = CfieldTable::where('name', $customFieldTable)->first();
            $customFieldTableId = ($customFieldTable) ? $customFieldTable->id : 0;
            $customFields = Cfield::select('cp_cfield.id as id', 'cp_cfield.name as name', 'cp_cfield.position as position', 'cp_cfield.position as position', 'cp_cfield.required as required', 'cp_cfield.id as custom_field_id', 'cp_cfield.cfield_id as custom_field_inner_id', 'cp_cfield.cfield_type_id', 'cp_cfield.cfield_option_default_id', 'cp_cfieldvalue.cf_value as value')
                ->where('cp_cfield.company_id', $userCompanyId)
                ->where('cp_cfield.cfield_table_id', $customFieldTableId)
                ->where('cp_cfieldvalue.entity_id', $contactId)
                ->where('cp_cfield.hide_on_checkout', config('constants.ContactView.cfield_hide_on_checkout'))
                ->with(['customFieldType'])
                ->with(['customFieldOption' => function ($cFieldOptionJoin) {
                    $cFieldOptionJoin->orderBy('position', 'ASC');
                }])
                ->leftJoin('cp_cfieldvalue', 'cp_cfieldvalue.cfield_id', 'cp_cfield.id')
                ->orderBy('cp_cfield.position', 'ASC')->get();
            foreach ($customFields as $key => $customField) {
                if ($showHidden) {
                    if ($customField->cfield_type_id == $typeImageId) {
                        if ($showImgFields) {
                            $customFieldData[$key] = $customField;
                        } else {
                            $customFieldData[$key] = $customField;
                        }
                    } else {
                        $customFieldAuto = CfieldAuto::where('company_id', $userCompanyId)->where('cfield_id', $customField->id)->first();
                        $customFieldAutoHide = $customFieldAuto ? $customFieldAuto->hide : null;
                        if (!$customFieldAutoHide) {
                            if ($customField->cfield_type_id == $typeImageId) {
                                if ($showImgFields) {
                                    $customFieldData[$key] = $customField;
                                } else {
                                    $customFieldData[$key] = $customField;
                                }
                            }
                        }
                    }
                } else {
                    $customFieldData[$key] = $customField;
                }
                $customFieldData[$key]['custom_field_type_id']           = $customFieldData[$key]['cfield_type_id'];
                $customFieldData[$key]['custom_field_option_default_id'] = $customFieldData[$key]['cfield_option_default_id'];
                unset($customFieldData[$key]['cfield_type_id']);
                unset($customFieldData[$key]['cfield_option_default_id']);
            }
            return $customFieldData;
        } catch (Exception $e) {
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.ContactView'));
            return false;
        }
    }

    public function getContactNotes(
        $request,
        $userId,
        $userCompanyId,
        $contactId
    ) {
        try {
            $contactNoteList = ContactNote::select(
                'cp_contactnote.id as note_id',
                'cp_contactnote.note as notes',
                'auth_user.id as user_id',
                DB::raw("CONCAT(auth_user.first_name,' ',auth_user.last_name) as user_name")
            )
                ->where('cp_contactnote.contact_id', $contactId)
                ->where('cp_contactnote.user_id', $userId)
                ->where('cp_contactnote.company_id', $userCompanyId)
                ->join('auth_user', 'auth_user.id', 'cp_contactnote.user_id')
                ->orderBy('cp_contactnote.id', 'DESC');
            $contactNote['all_notes'] = $contactNoteList->get();
            $contactNote['default_notes'] = $contactNoteList->limit(config('constants.ContactNotes.default_notes_limit'))
                ->get();

            return $contactNote;
        } catch (Exception $e) {
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.ContactView'));
            return false;
        }
    }
    public function getContactParentCompanyNotes(
        $request,
        $userId,
        $userCompanyId,
        $contactView
    ) {
        try {
            $contactsId = Contact::where('company_id', $userCompanyId)
                ->where('title', $contactView->company_name)->pluck('id');
            $contactNotes = ContactNote::select('id as note_id', 'note as notes')
                ->whereIn('contact_id', $contactsId)
                ->limit(config('constants.ContactNotes.notes_limit'))
                ->get();
            return $contactNotes;
        } catch (Exception $e) {
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.ContactView'));
            return false;
        }
    }
    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
