<?php

namespace App\Repositories\Contact;

/* Models */

use App\Models\Contact\ContactType;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;
use App\Models\State\State;
use App\Models\Country\Country;
use App\Models\People\PeopleRole;
use App\Models\Contact\Contact;
use App\Models\Contact\ContactPhone;
use App\Models\Contact\ContactPersonnel;
use App\Models\Contact\ContactInfoEmail;
use App\Models\Contact\ContactRep;
use App\Models\Contact\ContactPhonepersonnel;
use App\Models\Contact\ContactNote;
use App\Models\Cfield\Cfield;
use App\Models\Cfield\CfieldTable;
use App\Models\Cfield\CfieldAuto;

class ContactRepositories
{
    /* ===============================CREATE=============================== */

    public function doAddContactPhoneDetails($request, $userCompanyId, $contactId, $phoneNumber, $selectedContactPersonnel)
    {
        try {
            $validationResult['status'] = true;
            $validationResult['message'] = config('constants.Success.AddContactPhone');
            $contact = $this->getContact($request, $contactId);
            if (!$contact) {
                Helper::customError(400, config('constants.Errors.ContactNotFound'), $request->all(), config('constants.ServiceName.contact'));
                $validationResult['status'] = false;
                $validationResult['message'] = config('constants.Errors.ContactNotFound');
                return $validationResult;
            }
            $existContactphone = $this->getContactPhoneExistDetail($request, $contactId, $phoneNumber);
            if ($existContactphone) {
                Helper::customError(400, config('constants.Errors.ContactPhoneExist'), $request->all(), config('constants.ServiceName.contact'));
                $validationResult['status'] = false;
                $validationResult['message'] = config('constants.Errors.ContactPhoneExist');
                return $validationResult;
            }
            DB::beginTransaction();
            $contactPhone = new ContactPhone();
            $contactPhone->unknown = false;
            $contactPhone->is_in_dnc = false;
            $contactPhone->is_in_dnd = false;
            $contactPhone->is_active_lead = false;
            $contactPhone->company_id = $userCompanyId;
            $contactPhone->contact_id = $contactId;
            $contactPhone->ext = $request->extension;
            $contactPhone->fill($request->all());
            $contactPhone->phone_number = $phoneNumber;
            $contactPhone->save();
            if ($contactPhone) {
                foreach ($selectedContactPersonnel as $key => $contactPersonnel) {
                    $contactPhonepersonnel = new ContactPhonepersonnel();
                    $contactPhonepersonnel->contactphone_id = $contactPhone->id;
                    $contactPhonepersonnel->personnel_id = $contactPersonnel;
                    $contactPhonepersonnel->save();
                }
            }
            DB::commit();
            return $validationResult;
        } catch (Exception $e) {
            DB::rollBack();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contact'));
            $validationResult['status'] = false;
            $validationResult['message'] = config('constants.Errors.AddContactPhone');
            return $validationResult;
        }
    }


    public function doUpdateContactPhoneDetails($request, $userCompanyId, $contactId, $contactPhoneId, $phoneNumber, $selectedContactPersonnel)
    {
        try {
            $validationResult['status'] = true;
            $validationResult['message'] = config('constants.Success.UpdateContactPhone');
            $contact = $this->getContact($request, $contactId);
            if (!$contact) {
                Helper::customError(400, config('constants.Errors.ContactNotFound'), $request->all(), config('constants.ServiceName.contact'));
                $validationResult['status'] = false;
                $validationResult['message'] = config('constants.Errors.ContactNotFound');
                return $validationResult;
            }
            $existPhone = $this->getPhoneExistDetail($request, $phoneNumber);
            if ($existPhone) {
                $existContactphone = $this->getContactPhoneExistDetail($request, $contactId, $phoneNumber);
                if (!$existContactphone) {
                    Helper::customError(400, config('constants.Errors.ContactPhoneExist'), $request->all(), config('constants.ServiceName.contact'));
                    $validationResult['status'] = false;
                    $validationResult['message'] = config('constants.Errors.ContactPhoneExist');
                    return $validationResult;
                }
            }
            DB::beginTransaction();
            $contactPhone =  ContactPhone::where('id', $contactPhoneId)->where('contact_id', $contactId)->where('phone_number', $phoneNumber)->where('company_id', $userCompanyId)->first();
            if (!$contactPhone) {
                $contactPhone =  ContactPhone::where('id', $contactPhoneId)->where('contact_id', $contactId)->where('company_id', $userCompanyId)->first();
            }

            if ($contactPhone) {
                $contactPhone->company_id = $userCompanyId;
                $contactPhone->contact_id = $contactId;
                $contactPhone->ext = $request->extension;
                $contactPhone->fill($request->all());
                $contactPhone->phone_number = $phoneNumber;
                $contactPhone->save();
                foreach ($selectedContactPersonnel as $key => $contactPersonnel) {
                    $existingContactPhonepersonnel =  ContactPhonepersonnel::where('contactphone_id', $contactPhone->id)->where('personnel_id', $contactPersonnel)->delete();
                    $contactPhonepersonnel = new ContactPhonepersonnel();
                    $contactPhonepersonnel->contactphone_id = $contactPhone->id;
                    $contactPhonepersonnel->personnel_id = $contactPersonnel;
                    $contactPhonepersonnel->save();
                }
            } else {
                Helper::customError(400, config('constants.Errors.ContactPhoneNotFound'), $request->all(), config('constants.ServiceName.contact'));
                $validationResult['status'] = false;
                $validationResult['message'] = config('constants.Errors.ContactPhoneNotFound');
                return $validationResult;
            }
            DB::commit();
            return $validationResult;
        } catch (Exception $e) {
            DB::rollBack();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contact'));
            $validationResult['status'] = false;
            $validationResult['message'] = config('constants.Errors.UpdateContactPhone');
            return $validationResult;
        }
    }

    public function doAddContactInfoEmailDetails($request, $userCompanyId, $contactId)
    {
        try {
            $contact = $this->getContact($request, $contactId);
            if (!$contact) {
                Helper::customError(400, config('constants.Errors.ContactNotFound'), $request->all(), config('constants.ServiceName.contact'));
                return false;
            }
            DB::beginTransaction();
            $contactInfoEmail = new ContactInfoEmail();
            $contactInfoEmail->company_id = $userCompanyId;
            $contactInfoEmail->contact_id = $contactId;
            $contactInfoEmail->fill($request->all());
            $contactInfoEmail->save();
            DB::commit();
            return $contactInfoEmail;
        } catch (Exception $e) {
            DB::rollBack();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contact'));
            return false;
        }
    }

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    public function getContactType($request, $userCompanyId)
    {
        $contactType = ContactType::select('id', 'name')->where('company_id', $userCompanyId)->get();
        return $contactType;
    }

    public function getPeopleRoleDetails($request, $userCompanyId)
    {
        $peopleRole = PeopleRole::select('id', 'name')->where('company_id', $userCompanyId)
            ->orderBy('id', 'ASC')->get();
        return $peopleRole;
    }

    public function getContact($request, $contactId)
    {
        $contact = Contact::where('id', $contactId)->first();
        return $contact;
    }
    public function getPhoneExistDetail($request, $phoneNumber)
    {
        $contactphone = ContactPhone::where('phone_number', $phoneNumber)->first();
        return $contactphone;
    }

    public function getContactPhoneExistDetail($request, $contactId, $phoneNumber)
    {
        $contactphone = ContactPhone::where('contact_id', $contactId)->where('phone_number', $phoneNumber)->first();
        return $contactphone;
    }

    public function getReverseGeocode(
        $request,
        $latitude,
        $longitude
    ) {
        try {
            $MapApiID = config('constants.ContactAppointment.MAP_API_ID');
            $MapApiCode = config('constants.ContactAppointment.MAP_API_CODE');
            $geocode =  'https://reverse.geocoder.api.here.com/6.2/reversegeocode.json?app_id=' . $MapApiID . '&app_code=' . $MapApiCode . '&maxresults=1&mode=retrieveAddresses&prox=' . $latitude . ',' . $longitude . ',500';
            $geocode =  file_get_contents($geocode);
            $output = json_decode($geocode);
            $response = $output->Response->View;
            if ($response) {
                $resAddress        = $output->Response->View[0]->Result[0]->Location->Address;
                $resHouseNumber    = isset($resAddress->HouseNumber) ? $resAddress->HouseNumber : '';
                $resBuilding       = isset($resAddress->Building) ? $resAddress->Building : '';
                $resStreet         = isset($resAddress->Street) ? $resAddress->Street : '';
                $data['city']      = isset($resAddress->City) ? $resAddress->City : '';
                $resState          = isset($resAddress->State) ? $resAddress->State : '';
                $resCountry        = isset($resAddress->AdditionalData[0]) ? $resAddress->AdditionalData[0]->value : '';
                $resPostalCode = isset($resAddress->PostalCode) ? $zipCode = $resAddress->PostalCode : '';
                if (strlen($resPostalCode) > config('constants.ContactAppointment.zip_max_limit')) {
                    $zipCode = substr($resPostalCode, 0, config('constants.ContactAppointment.zip_max_limit'));
                }
                if (strlen($resPostalCode) == config('constants.ContactAppointment.zip_min_limit')) {
                    $zipCode = config('constants.ContactAppointment.add_before_zip_code') . $resPostalCode;
                }
                $data['zipcode']         = $zipCode;
                $data['address1']        = trim($resHouseNumber . ' ' . $resBuilding . ' ' . $resStreet);
                $state                   = State::where('abbr', $resState)->first(); // check resState valid
                $country                 = Country::where('name', $resCountry)->first(); // check resCountry valid
                $data['state']           = ($resState) ? $resState : '';
                $data['state_id']        = ($state) ? $state->id : 0;
                $data['country']         = ($resCountry) ? $resCountry : '';
                $data['country_id']      = ($country) ? $country->id : 0;

                return $data;
            }
        } catch (Exception $e) {
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contact'));
            return false;
        }
    }

    public function doGetContactInfoEmailDetails($request, $userCompanyId, $contactId)
    {
        $contactInfoEmail = ContactInfoEmail::select(
            'id',
            'email'
        )
            ->where('company_id', $userCompanyId)
            ->where('contact_id', $contactId)
            ->orderBy('id', 'ASC')->get();
        return $contactInfoEmail;
    }

    public function getContactFilterCustomFieldDetails(
        $request,
        $userCompanyId,
        $customFieldTable
    ) {
        $customFieldData = [];
        $customFieldTable  = CfieldTable::where('name', $customFieldTable)->first();
        $customFieldTableId = ($customFieldTable) ? $customFieldTable->id : 0;
        $customFields = Cfield::select('cp_cfield.id as id', 'cp_cfield.name as name', 'cp_cfield.position as position', 'cp_cfield.position as position', 'cp_cfield.required as required', 'cp_cfield.id as custom_field_id', 'cp_cfield.cfield_id as custom_field_inner_id', 'cp_cfield.cfield_type_id', 'cp_cfield.cfield_option_default_id')
            ->where('cp_cfield.company_id', $userCompanyId)
            ->where('cp_cfield.cfield_table_id', $customFieldTableId)
            ->where('cp_cfield.hide_on_checkout', config('constants.CustomField.cfield_hide_on_checkout'))
            ->with(['customFieldType'])
            ->with(['customFieldOption' => function ($cFieldOptionJoin) {
                $cFieldOptionJoin->orderBy('position', 'ASC');
            }])
            ->orderBy('cp_cfield.position', 'ASC')->get();
        foreach ($customFields as $key => $customField) {
            $customFieldAuto = CfieldAuto::where('company_id', $userCompanyId)->where('cfield_id', $customField->id)->first();
            $customFieldAutoHide = $customFieldAuto ? $customFieldAuto->hide : null;
            if (!$customFieldAutoHide) {
                $customFieldData[$key] = $customField;
            }
        }
        $customFieldData[$key]['custom_field_type_id']           = $customFieldData[$key]['cfield_type_id'];
        $customFieldData[$key]['custom_field_option_default_id'] = $customFieldData[$key]['cfield_option_default_id'];
        unset($customFieldData[$key]['cfield_type_id']);
        unset($customFieldData[$key]['cfield_option_default_id']);
        return $customFieldData;
    }

    public function getContactParentCompany($request, $userCompanyId, $title)
    {
        $contact = Contact::select('id', 'title')->whereCompany_id($userCompanyId)
            ->where('title', 'ilike', '%' . $title . '%')
            ->orderBy('title', 'ASC')
            ->limit(config('constants.Contact.parent_company_list_limit'))
            ->get();
        return $contact;
    }

    /* Get ContactPhone List */
    public function getContactPhoneListDetails($contactId)
    {
        $contactPhoneDetails = ContactPhone::select('id', 'phone_number')
            ->whereContact_id($contactId)
            ->get()
            ->toArray();
        return $contactPhoneDetails;
    }

    /* Get ContactPhone List */
    public function getContactPersonelListDetails($contactPhoneId)
    {
        $contactPersonelDetails = ContactPhonepersonnel::whereIn('contactphone_id', $contactPhoneId)
            ->get();
        return $contactPersonelDetails;
    }


    /* CompanyPhone Details */
    public function getContactCompanyPhoneDetails($contactId, $contactPhoneId)
    {
        $contactPhoneDetails = ContactPhone::whereContact_id($contactId)
            ->whereNotIn('id', $contactPhoneId)
            ->with('phoneType')
            ->get()
            ->toArray();
        return $contactPhoneDetails;
    }

    /* Get Contact Personel Details */
    public function getContactPersonelDetails($contactPersonelId)
    {
        $contactPersonelDetails = ContactPersonnel::select('id', 'first_name', 'last_name', 'title', 'personnel_notes')
            ->whereIn('id', $contactPersonelId)
            ->get()
            ->toArray();
        return $contactPersonelDetails;
    }

    /* Get Contact Personel Details */
    public function getContactPersonelPhoneListDetails($contactPersonelId, $contactPhoneId)
    {
        $contactPhoneId = ContactPhonepersonnel::where('personnel_id', $contactPersonelId)
            ->whereIn('contactphone_id', $contactPhoneId)
            ->get()
            ->pluck('contactphone_id');

        $contactPhoneDetails = ContactPhone::select('id', 'phone_type_id', 'phone_number', 'ext', 'country_code')
            ->whereIn('id', $contactPhoneId)
            ->with('phoneType')
            ->get()
            ->filter(function ($contactPhoneDetails) {
                $contactPhoneDetails['phone_type_name'] = $contactPhoneDetails['phoneType']['name'];
                $contactPhoneDetails['phone_extension'] = $contactPhoneDetails['ext'];
                unset($contactPhoneDetails['phoneType']);
                return $contactPhoneDetails;
            });

        return collect($contactPhoneDetails)->toArray();
    }

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    public function doUpdateContactInfoEmailDetails($request, $userCompanyId, $contactId, $contactInfoEmailId)
    {
        try {
            $contact = $this->getContact($request, $contactId);
            if (!$contact) {
                Helper::customError(400, config('constants.Errors.ContactNotFound'), $request->all(), config('constants.ServiceName.contact'));
                return false;
            }
            DB::beginTransaction();
            $contactInfoEmail = ContactInfoEmail::whereId($contactInfoEmailId)->whereCompany_id($userCompanyId)->whereContact_id($contactId)->first();
            if ($contactInfoEmail) {
                $contactInfoEmail->company_id = $userCompanyId;
                $contactInfoEmail->contact_id = $contactId;
                $contactInfoEmail->updated = now();
                $contactInfoEmail->fill($request->all());
                $contactInfoEmail->save();
                DB::commit();
            }
            return $contactInfoEmail;
        } catch (Exception $e) {
            DB::rollBack();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contact'));
            return false;
        }
    }

    public function doUpdateSelectedUsersDetails($request, $userCompanyId, $contactId, $selectedContactUsers)
    {
        try {
            $contact = $this->getContact($request, $contactId);
            if (!$contact) {
                Helper::customError(400, config('constants.Errors.ContactNotFound'), $request->all(), config('constants.ServiceName.contact'));
                return false;
            }
            DB::beginTransaction();
            $existingContactUser = ContactRep::where('contact_id', $contactId)->whereIn('user_id', $selectedContactUsers)
                ->delete();
            foreach ($selectedContactUsers as $key => $selectedUser) {
                $contactUser = new ContactRep();
                $contactUser->contact_id = $contactId;
                $contactUser->user_id = $selectedUser;
                $contactUser->created = now();
                $contactUser->save();
            }
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contact'));
            return false;
        }
    }

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */
    /* Remove Specific ContactPhone */
    public function removeContactPhone($userId, $companyId, $contactId, $contactPhoneId)
    {
        return ContactPhone::whereCompany_id($companyId)
            ->whereContact_id($contactId)
            ->whereId($contactPhoneId)
            ->delete();
    }

    /* Remove Specific Opportunity */
    public function removeContactDefaultPhone($userId, $companyId, $contactId, $contactPhoneId)
    {
        return Contact::whereDefault_phone_id($contactPhoneId)->update(['default_phone_id' => null]);
    }

    /* Remove Specific Contact InfoEmail */
    public function removeContactInfoEmail($request, $contactInfoEmailId)
    {
        return ContactInfoEmail::whereId($contactInfoEmailId)->delete();
    }

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
