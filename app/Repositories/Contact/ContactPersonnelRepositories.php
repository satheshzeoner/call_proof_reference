<?php

namespace App\Repositories\Contact;

use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;
use Exception;

/* Models */
use App\Models\Contact\Contact;
use App\Models\Contact\ContactPersonnel;
use App\Models\Contact\ContactPhone;
use App\Models\PhoneType\PhoneType;
use App\Models\Contact\ContactPhonepersonnel;
use App\Models\Followup\FollowupCall;
use App\Models\Followup\Followup;
use App\Models\Twilio\TwilioSms;
use App\Models\Twilio\TwilioCall;

class ContactPersonnelRepositories
{
    /* ===============================CREATE=============================== */

    public function doAddContactPersonnelDetail(
        $request,
        $userCompanyId,
        $contact,
        $phones
    ) {
        try {
            DB::beginTransaction();
            $contactPersonnel = new ContactPersonnel();
            $contactPersonnel->company_id = $userCompanyId;
            $contactPersonnel->contact_id = $contact->id;
            $contactPersonnel->personnel_notes = $request->contact_notes;
            $contactPersonnel->peoplerole_id = $request->people_role_id;
            $contactPersonnel->is_disabled = false;
            $contactPersonnel->fill($request->all());
            $contactPersonnel->save();
            DB::commit();
            if ($contactPersonnel) {
                foreach ($phones as $key => $phone) {
                    $contactPhone = new ContactPhone();
                    $contactPhone->contact_id = $contact->id;
                    $contactPhone->company_id = $userCompanyId;
                    $contactPhone->unknown = false;
                    $contactPhone->is_in_dnc = false;
                    $contactPhone->is_in_dnd = false;
                    $contactPhone->is_active_lead = false;
                    $contactPhone->phone_number =  $phone->phone_number;
                    $contactPhone->phone_type_id =  $phone->phone_type_id;
                    $contactPhone->country_code =  $phone->country_code;
                    $contactPhone->ext =  $phone->extension;
                    $contactPhone->contact_type_id =  $contact->contact_type_id;
                    $contactPhone->save();
                    $contactPhonepersonnel = new ContactPhonepersonnel();
                    $contactPhonepersonnel->personnel_id = $contactPersonnel->id;
                    $contactPhonepersonnel->contactphone_id = $contactPhone->id;
                    $contactPhonepersonnel->save();
                    DB::commit();
                }
            }

            return $contactPersonnel;
        } catch (Exception $e) {
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.ContactPersonnel'));
            DB::rollBack();
            return false;
        }
    }



    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    public function getContactPersonnelDetails($request, $userCompanyId, $contactId)
    {

        $contactPersonnel = ContactPersonnel::whereCompany_id($userCompanyId)
            ->whereContact_id($contactId)
            ->whereIs_disabled(false)
            ->orderBy('id', 'ASC')->get();
        return $contactPersonnel;
    }

    public function getSpecificContactPersonnelDetails($request, $userCompanyId, $contactId, $personnelId)
    {
        $contactPersonnel = ContactPersonnel::whereId($personnelId)
            ->whereContact_id($contactId)
            ->get();

        return $contactPersonnel;
    }

    public function isContactExist($request, $contactId)
    {
        $contact = Contact::where('id', $contactId)->first();
        return $contact;
    }

    public function isContactPhoneExist($request, $contactId, $phoneId, $phoneNumber)
    {
        if ($phoneId) {
            $contactPhone = ContactPhone::where('id', '!=', $phoneId)->whereContact_id($contactId)->wherePhone_number($phoneNumber)->first();
        } else {
            $contactPhone = ContactPhone::whereContact_id($contactId)->wherePhone_number($phoneNumber)->first();
        }

        return $contactPhone;
    }

    public function isContactPersonnelExist($request, $personnelId, $contactId, $userCompanyId)
    {
        $contactPersonnel = ContactPersonnel::whereId($personnelId)->whereContact_id($contactId)->whereCompany_id($userCompanyId)->first();
        return $contactPersonnel;
    }

    public function getPhoneTypeDetail($request)
    {
        $phoneType['cell'] = PhoneType::whereName(config('constants.PhoneType.Cell'))->first();
        $phoneType['office'] = PhoneType::whereName(config('constants.PhoneType.Office'))->first();
        return $phoneType;
    }

    public function getContactPhonePersonnel(
        $request,
        $personnelId
    ) {
        $contactPhonepersonnel = ContactPhonepersonnel::wherePersonnel_id($personnelId)->first();
        return $contactPhonepersonnel;
    }
    public function getContact($request, $contactId)
    {
        $contact = Contact::where('id', $contactId)->first();
        return $contact;
    }

    public function getContactPersonnelExistDetail($request, $personnelId)
    {
        $contactPersonnel = ContactPersonnel::whereId($personnelId)->first();
        return $contactPersonnel;
    }
    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */
    public function doUpdateContactPersonnelAndPhoneDetails(
        $request,
        $userCompanyId,
        $contact,
        $phones,
        $personnelId
    ) {
        try {

            $contactPersonnel = ContactPersonnel::whereId($personnelId)->first();
            if ($contactPersonnel) {
                DB::beginTransaction();
                $contactPersonnel->company_id = $userCompanyId;
                $contactPersonnel->contact_id = $contact->id;
                $contactPersonnel->personnel_notes = $request->contact_notes;
                $contactPersonnel->peoplerole_id = $request->people_role_id;
                $contactPersonnel->fill($request->all());
                $contactPersonnel->save();
                DB::commit();

                foreach ($phones as $key => $phone) {
                    $contactPhone =  ContactPhone::whereContact_id($contact->id)->whereCompany_id($userCompanyId)->first();
                    $contactPhone->phone_number =  $phone->phone_number;
                    $contactPhone->phone_type_id =  $phone->phone_type_id;
                    $contactPhone->country_code =  $phone->country_code;
                    $contactPhone->ext =  $phone->extension;
                    $contactPhone->contact_type_id =  $contact->contact_type_id;
                    $contactPhone->save();
                    DB::commit();
                }
            }
            return $contactPersonnel;
        } catch (Exception $e) {
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.ContactPersonnel'));
            DB::rollBack();
            return false;
        }
    }

    public function doUpdateContactPersonnelDetails(
        $request,
        $userCompanyId,
        $personnelId,
        $contactId
    ) {
        try {
            $validationResult['status'] = true;
            $validationResult['message'] = config('constants.Success.PersonnelMoved');
            $contact = $this->getContact($request, $contactId);
            if (!$contact) {
                Helper::customError(400, config('constants.Errors.ContactNotFound'), $request->all(), config('constants.ServiceName.ContactPersonnel'));
                $validationResult['status'] = false;
                $validationResult['message'] = config('constants.Errors.ContactNotFound');
                return $validationResult;
            }
            $isValidContactPersonnel = $this->getContactPersonnelExistDetail($request, $personnelId);
            if (!$isValidContactPersonnel) {
                Helper::customError(400, config('constants.Errors.ContactPersonnelId'), $request->all(), config('constants.ServiceName.ContactPersonnel'));
                $validationResult['status'] = false;
                $validationResult['message'] = config('constants.Errors.ContactPersonnelId');
                return $validationResult;
            }
            $oldContactPersonnel = ContactPersonnel::whereId($personnelId)->whereCompany_id($userCompanyId)->whereIs_disabled(false)->first();
            if ($oldContactPersonnel) {
                $this->oldContactId = $oldContactPersonnel->contact_id;
                $newContactPersonnel = $oldContactPersonnel->replicate();
                $newContactPersonnel->contact_id = $contactId;
                $newContactPersonnel->save();
                $this->newPersonnelId =  $newContactPersonnel->id;
                $oldContactPersonnel->is_disabled = true;
                $oldContactPersonnel->moved_to = $contactId;
                $oldContactPersonnel->save();
            } else {
                $validationResult['status'] = false;
                $validationResult['message'] = config('constants.Errors.ContactPersonnelNotFound');
            }
            return $validationResult;
        } catch (Exception $e) {
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.ContactPersonnel'));
            $validationResult['status'] = false;
            $validationResult['message'] = config('constants.Errors.PersonnelMoved');
            return $validationResult;
        }
    }

    public function doUpdateContactDetails(
        $request,
        $userCompanyId,
        $personnelId,
        $contactId
    ) {
        try {
            $validationResult['status'] = true;
            $validationResult['message'] = config('constants.Success.PersonnelMoved');
            $oldContactPersonnel = ContactPersonnel::whereId($personnelId)->whereCompany_id($userCompanyId)->whereIs_disabled(false)->first();
            $contact = Contact::whereId($contactId)->whereCompany_id($userCompanyId)->first();
            if (!$contact) {
                Helper::customError(400, config('constants.Errors.ContactNotFound'), $request->all(), config('constants.ServiceName.ContactPersonnel'));
                $validationResult['status'] = false;
                $validationResult['message'] = config('constants.Errors.ContactNotFound');
                return $validationResult;
            }
            if ($contact && $oldContactPersonnel) {
                if ($contact->last_contacted && $oldContactPersonnel->last_contacted) {
                    if ($contact->last_contacted  < $oldContactPersonnel->last_contacted) {
                        $contact->last_contacted = $oldContactPersonnel->last_contacted;
                    }
                }
                $contact->save();
            }

            return $validationResult;
        } catch (Exception $e) {
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.ContactPersonnel'));
            $validationResult['status'] = false;
            $validationResult['message'] = config('constants.Errors.PersonnelMoved');
            return $validationResult;
        }
    }

    public function updatePhonePersonnel(
        $request,
        $userCompanyId,
        $contactId,
        $personnelId,
        $contactPhonePersonnel,
        $phoneType
    ) {
        try {
            $validationResult['status'] = true;
            $validationResult['message'] = config('constants.Success.PersonnelMoved');
            $oldContactPersonnel = ContactPersonnel::whereId($personnelId)->whereCompany_id($userCompanyId)->whereIs_disabled(false)->first();
            $contactPhonePersonnelDetail = ContactPhonepersonnel::wherePersonnel_id($personnelId)->get();
            foreach ($contactPhonePersonnelDetail as $key => $contactPhonePersonnel) {
                $contactPhone = $contactPhonePersonnel->contactPhone;
                $phoneNumber = ($contactPhone) ? $contactPhone->phone_number : '';
                $contactPhoneDetail = ContactPhone::whereContact_id($contactId)->wherePhone_number($phoneNumber)->first();
                if ($contactPhoneDetail) {
                    if (isset($this->newPersonnelId)) {
                        $contactPhonePersonnel->contactphone_id = $contactPhoneDetail->id;
                        $contactPhonePersonnel->personnel_id = $this->newPersonnelId;
                        $contactPhonePersonnel->save();
                    }
                }
                if ($contactPhone) {
                    $this->contactPhoneId = $contactPhone->id;
                    $contactPhone->phoneType->id = 4;
                    if ($contactPhone->phoneType->name != config('constants.PhoneType.Cell')) {
                        $contactPhoneType = PhoneType::whereName(config('constants.PhoneType.Office'))->first();
                        if ($contactPhoneType) {
                            $contactPhone->phone_type_id = $contactPhoneType->id;
                            $contactPhone->save();
                        }
                        if (isset($this->oldContactId)) {
                            $oldContact = Contact::whereId($this->oldContactId)->first();
                        }
                        if ($oldContact) {
                            if ($oldContact->default_phone_id = $contactPhone->id) {
                                $oldContact->default_phone_id = null;
                                $oldContact->save();
                                $contactPhone->contact_id = $contactId;
                                $contactPhone->save();
                            }
                        }
                    }
                }
            }
            return $validationResult;
        } catch (Exception $e) {
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.ContactPersonnel'));
            $validationResult['status'] = false;
            $validationResult['message'] = config('constants.Errors.PersonnelMoved');
            return $validationResult;
        }
    }

    public function updateFollowupCall(
        $request,
        $contactId
    ) {
        try {
            $validationResult['status'] = true;
            $validationResult['message'] = config('constants.Success.PersonnelMoved');
            if (isset($this->contactPhoneId) && isset($this->oldContactId)) {
                $followupCall = FollowupCall::whereContact_phone_id($this->contactPhoneId)->whereContact_id($this->oldContactId)->first();
                if ($followupCall) {
                    $followupCall->contactId = $contactId;
                    $followupCall->save();
                }
            }
            return $validationResult;
        } catch (Exception $e) {
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.ContactPersonnel'));
            $validationResult['status'] = false;
            $validationResult['message'] = config('constants.Errors.PersonnelMoved');
            return $validationResult;
        }
    }

    public function updateFollowup(
        $request,
        $contactId
    ) {
        try {
            $validationResult['status'] = true;
            $validationResult['message'] = config('constants.Success.PersonnelMoved');
            if (isset($this->contactPhoneId) && isset($this->oldContactId)) {
                $followup = Followup::whereContact_phone_id($this->contactPhoneId)->whereContact_id($this->oldContactId)->first();
                if ($followup) {
                    $followup->contactId = $contactId;
                    $followup->save();
                }
            }
            return $validationResult;
        } catch (Exception $e) {
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.ContactPersonnel'));
            $validationResult['status'] = false;
            $validationResult['message'] = config('constants.Errors.PersonnelMoved');
            return $validationResult;
        }
    }

    public function updateTwilioSms(
        $request,
        $contactId
    ) {
        try {
            $validationResult['status'] = true;
            $validationResult['message'] = config('constants.Success.PersonnelMoved');
            if (isset($this->contactPhoneId) && isset($this->oldContactId)) {
                $twilioSms = TwilioSms::whereContact_phone_id($this->contactPhoneId)->whereContact_id($this->oldContactId)->first();
                if ($twilioSms) {
                    $twilioSms->contactId = $contactId;
                    $twilioSms->save();
                }
            }
            return $validationResult;
        } catch (Exception $e) {
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.ContactPersonnel'));
            $validationResult['status'] = false;
            $validationResult['message'] = config('constants.Errors.PersonnelMoved');
            return $validationResult;
        }
    }
    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */
    /* Remove Specific Contact Personnel*/
    public function removeContactPersonnel($userId, $companyId, $contactId, $personnelId)
    {
        return ContactPersonnel::whereId($personnelId)
            ->delete();
    }
    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
