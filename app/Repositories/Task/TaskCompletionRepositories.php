<?php

namespace App\Repositories\Task;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

/* Models */

use App\Models\Followup\Followup;
use App\Models\Contact\Contact;
use App\Models\Contact\ContactPhone;
use App\Models\Contact\ContactPersonnel;
use App\Models\Contact\ContactRep;


class TaskCompletionRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* Get Task Details */
    public function getTaskCompletionDetails($userId, $companyId, $request)
    {
        /* Filteration */
        $taskDetails = Followup::with(['contactFollow' => function ($taskDetailsJoin) use ($request) {
            if (isset($request['search'])) {
                $taskDetailsJoin->where('first_name', 'ilike', '%' . $request['search'] . '%');
                $taskDetailsJoin->orWhere('last_name', 'ilike', '%' . $request['search'] . '%');
            }
            $taskDetailsJoin->select('id', 'first_name', 'last_name', DB::raw("CASE WHEN (cp_contact.last_contacted IS NULL) THEN 'Never' else to_char(cp_contact.last_contacted, 'FMMonth DD,YYYY,hh12:mi a.m.')  END as last_contacted"));
        }]);

        $taskDetails =  $taskDetails->whereUser_id($userId);
        $taskDetails =  $taskDetails->with('taskType');

        /* Filter Value Based Results */
        if (isset($request['task_type'])) {
            $taskDetails =  $taskDetails->whereFollowup_type_id($request['task_type']);
        }
        if (isset($request['task_status'])) {
            $taskDetails =  $taskDetails->whereCompleted($request['task_status']);
        }
        if (isset($request['start_date'])) {
            $taskDetails =  $taskDetails->where('start', '>=', Carbon::parse($request['start_date'])->format('Y-m-d'));
        }
        if (isset($request['end_date'])) {
            $taskDetails =  $taskDetails->where('start', '<=', Carbon::parse($request['end_date'])->format('Y-m-d'));
        }

        $taskDetails =  $taskDetails->paginate(config('constants.Pagination.taskcompletionitemsPerPage'));
        $taskDetails =  $taskDetails->toArray();

        return $taskDetails;
    }

    /* getContactPhoneDetails */
    public function getContactPhoneDetails($contact)
    {
        /* Get Phone Details */
        $contactPhone     = ContactPhone::whereContact_id($contact['contact_follow']['id'])->first();
        $contactPersonnel = ContactPersonnel::select(DB::raw('CONCAT(first_name, \' \',last_name) as full_name'), 'last_contacted')->whereContact_id($contact['contact_follow']['id'])->get()->pluck('full_name');
        $contactRep       = collect(ContactRep::whereContact_id($contact['contact_follow']['id'])->with('userDetails')->get()->toArray());

        $contact['contact_follow']['phone_number'] = (isset($contactPhone)) ? $contactPhone->phone_number : '';
        $contact['contact_follow']['phone_type']   = (isset($contactPhone)) ? $contactPhone->phone_type_id : '';
        $contact['contact_follow']['people']       = (isset($contactPersonnel)) ? implode(",", $contactPersonnel->toArray()) : '';
        $contact['contact_follow']['rep']          = $contactRep->first()['user_details']['username'];

        return $contact;
    }



    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* Task-Updations */
    public function taskCompletionStatusUpdate($userId, $companyId, $paramDetails, $taskId)
    {
        $routeContactResult = Followup::whereId($taskId)
            ->update($paramDetails);
        return true;
    }

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
