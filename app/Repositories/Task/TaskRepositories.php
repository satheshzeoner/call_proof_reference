<?php

namespace App\Repositories\Task;

/* Models */

use App\Models\Followup\Followup;
use App\Models\Followup\FollowupPersonnel;
use App\Models\Followup\FollowupType;


class TaskRepositories
{
    /* ===============================CREATE=============================== */

    /* followup ScheduleTask */
    public function storeFollowupValue($followupFieldValue)
    {
        $followUpDetails = Followup::create($followupFieldValue);
        return $followUpDetails->id;
    }

    /* followup personal ScheduleTask */
    public function storeFollowupPersonalValue($followupPersonnalValue)
    {
        $followUpPersonalDetails = FollowupPersonnel::create($followupPersonnalValue);
        return $followUpPersonalDetails->id;
    }

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */
    /* Get FollowUp Specific Task Details */
    public function getFollowupDetails($contactId, $taskId)
    {
        return Followup::whereId($taskId)
            ->with('followPeople')
            ->get()
            ->toArray();
    }

    /* Get Task-Type Details */
    public function getTaskTypeListDetails($companyId)
    {
        return FollowupType::select('id as task_id', 'name as task_name', 'position', 'is_default')
            ->whereCompany_id($companyId)
            ->get()
            ->toArray();
    }
    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* followup Update ScheduleTask */
    public function updateFollowupValue($followupFieldValue, $followupId)
    {
        $followUpDetails = Followup::where('id', $followupId)
            ->update($followupFieldValue);
        return true;
    }

    /* followup Update personal ScheduleTask */
    public function updateFollowupPersonalValue($followupPersonnalValue, $followupId)
    {
        $followUpPersonalDetails = FollowupPersonnel::create($followupPersonnalValue);
        return $followUpPersonalDetails->id;
    }

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */
    public function removeSpecificFollowupId($taskId)
    {
        $followUpPersonelDetails = FollowupPersonnel::where('followup_id', $taskId)
            ->delete();
        return $followUpPersonelDetails;
    }
    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
