<?php

namespace App\Repositories\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;

/* Models */
use App\Models\Auth\AuthUser;

use Exception;

class ForgotPasswordRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */
    /* Validation for Id Has present or Not */
    public function getUserDetail($request, $email)
    {
        $user = AuthUser::whereEmail($email)->first();
        return $user;
    }
    public function getUserProfileDetail($request, $user)
    {
        $userProfile = $user->userProfile()->get();
        return $userProfile;
    }
    public function generateRandomSecretCode($request)
    {
        //we need to change random code same as classic version
        $randomSecretCode = md5(rand());
        return $randomSecretCode;
    }


    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */
    public function updateUserProfile($request, $userProfile, $randomGeneratedSecretCode)
    {
        try {
            DB::beginTransaction();
            $userProfile->secret = $randomGeneratedSecretCode;
            $userProfile->save();
            DB::commit();
            return $userProfile;
        } catch (Exception $e) {
            DB::rollback();
            Helper::customError(400, $e->getMessage(), $request->all(), config('constants.ServiceName.contactAppointment'));
            return false;
        }
    }
    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */

    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
