<?php

namespace App\Repositories\User;

use Illuminate\Support\Facades\DB;
/* Models */

use App\Models\Users\User;
use App\Models\Users\UserProfile;


class UserRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */
    public function getUsersDetails($request, $userId, $userCompanyId)
    {

        $userProfileUserId = UserProfile::where('company_id', $userCompanyId)->pluck('user_id');
        $users = User::select('id', DB::raw("CONCAT(first_name,' ', last_name) AS full_name"), 'username', 'email')->whereIn('id', $userProfileUserId)->get();

        return $users;
    }
    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
