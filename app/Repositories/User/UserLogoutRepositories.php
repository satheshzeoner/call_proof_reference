<?php

namespace App\Repositories\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/* Models */
use App\Models\Oauth\OauthAccessToken;
use App\Models\Oauth\OauthRefreshToken;

use Exception;

class UserLogoutRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */
    public function doRemoveUserLogout($userId, $companyId)
    {
        try {
            if (Auth::check()) {

                /* Access Token  */
                OauthAccessToken::where('user_id', '=', Auth::user()->id)
                    ->update([
                        'revoked' => true
                    ]);

                /* Get Access Token  */
                $accessToken = Auth::user()->token();

                /* Refresh Token */
                OauthRefreshToken::where('access_token_id', '=', $accessToken->id)
                    ->update([
                        'revoked' => true
                    ]);

                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
