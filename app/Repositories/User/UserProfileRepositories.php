<?php

namespace App\Repositories\User;

use Illuminate\Support\Facades\DB;
/* Models */

use App\Models\Users\User;
use App\Models\Users\UserProfile;


class UserProfileRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* UserProfile */
    public function updateUserProfileDetails($request, $userId, $companyId)
    {
        $userProfile = UserProfile::whereCompany_id($companyId)
            ->whereUser_id($userId)
            ->update($request);
        return true;
    }
    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
