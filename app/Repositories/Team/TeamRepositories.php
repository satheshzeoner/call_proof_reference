<?php

namespace App\Repositories\Team;

use Illuminate\Support\Facades\DB;

/* Models */
use App\Models\Auth\AuthUser;
use App\Models\Users\UserProfile;


class TeamRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */
    public function getTeamDetails($request, $userId, $userCompanyId)
    {
        $userProfileUserId = UserProfile::whereCompany_id($userCompanyId)->pluck('user_id');
        $users = AuthUser::whereIn('id', $userProfileUserId);
        if ($request->has('search')) {
            $users = $users->where(function ($query) use ($request) {
                $query->where('first_name', 'ilike', '%' . $request->search . '%')
                    ->orWhere('last_name', 'ilike', '%' . $request->search . '%');
            });
        }
        return $users->get();
    }
    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
