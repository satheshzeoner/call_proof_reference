<?php

namespace App\Repositories\Country;

/* Models */

use App\Models\Country\Country;


class CountryRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */
    public function getCountryDetails($request)
    {
        $countryList = Country::select('id', 'name')->get();
        return $countryList;
    }
    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
