<?php

namespace App\Repositories\Label;

use Illuminate\Support\Facades\DB;
/* Models */

use App\Models\Label\Label;


class LabelRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */
    public function getLabelDetails($request, $userId, $userCompanyId)
    {
        $labels = Label::select('id', 'name')->where('company_id', $userCompanyId)->get();
        return $labels;
    }
    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
