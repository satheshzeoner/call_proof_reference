<?php

namespace App\Repositories\Places;

/* Models */

use App\Models\Contact\Contact;
use App\Models\Contact\ContactCompany;
use App\Models\Contact\ContactPhone;
use App\Models\Contact\ContactType;

use Illuminate\Support\Facades\DB;

use Exception;

class PlacesContactAddRepositories
{
    /* ===============================CREATE=============================== */

    /* storeContactCompany Details */
    public function storeContactCompanyDetails($contactCompanyDetails)
    {
        $contactCompany = ContactCompany::create($contactCompanyDetails);
        return $contactCompany->id;
    }

    /* storeContact Details */
    public function storeContactDetails($contactDetails)
    {
        $contact = Contact::create($contactDetails);
        return $contact->id;
    }

    /* storeContactPhone Details */
    public function storeContactPhoneDetails($contactPhoneDetails)
    {
        $contactPhone = ContactPhone::create($contactPhoneDetails);
        return $contactPhone->id;
    }

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* Get Specific ContactType Details */
    public function getContactTypeDetails($companyId)
    {
        $contactTypeDetails = ContactType::whereCompany_id($companyId)
            ->where('name', 'ilike', '%' . config('constants.Places.placesContactTypeName') . '%')->first();
        if (isset($contactTypeDetails)) {
            return $contactTypeDetails->id;
        } else {
            return config('constants.Places.placesContactTypeDefault');
        }
    }

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */

    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
