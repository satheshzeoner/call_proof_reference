<?php

namespace App\Repositories\Places;

/* Models */

use App\Models\Sicsmaster\Sicsmaster;
use App\Models\PlacesCategory\PlacesCategory;
use App\Models\Contact\Contact;

use Illuminate\Support\Facades\DB;

use Exception;

class PlacesNearestRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* Get Nearest Places Contact Details */
    public function getNearestPlaceContactDetails($request, $userId, $companyId, $latitude, $longitude, $stateCode)
    {
        $paramDetails = $request->all();

        /* Point the latitude and longitude and Calculate the Nearest Contacts in Places Database with Range Distance */
        $placesContactDetails =  Sicsmaster::select(
            DB::raw("( " . config('constants.Places.meterToMiles') . " * acos( cos( radians($latitude) ) * cos( radians( latitude ) ) * 
            cos( radians( longitude ) - radians($longitude) ) + sin( radians($latitude) ) * 
            sin( radians( latitude ) ) ) ) AS distance"),
            '*'
        );
        $placesContactDetails =   $placesContactDetails->whereState_code($stateCode);

        /* Searching Filtration */

        /* Searching for Business Type Like .. RealEstate , RESTAURANTS..... */
        if (isset($paramDetails['business_type'])) {

            /* Get Bussiness Search Keyword */
            $businessTypeName     = $this->getBussinessTypeDetails($companyId, $paramDetails['business_type']);
            if (isset($businessTypeName)) {
                $placesContactDetails = $placesContactDetails->where('business_type', 'ilike', '%' . $businessTypeName . '%');
            }
        }

        /* Searching for Names Contacts Like DOUBLE DOWN TATTOO , DODLE CHIROPRACTIC ..... */
        if (isset($paramDetails['keyword'])) {
            $placesContactDetails = $placesContactDetails->where('company_name', 'ilike', '%' . $paramDetails['keyword'] . '%');
            $placesContactDetails = $placesContactDetails->where('contact_name', 'ilike', '%' . $paramDetails['keyword'] . '%');
        }

        /* Searching for Names Contacts Like 4738 W GLENDALE AVE ..... */
        if (isset($paramDetails['address'])) {
            $placesContactDetails = $placesContactDetails->where('address', 'ilike', '%' . $paramDetails['address'] . '%');
            $placesContactDetails = $placesContactDetails->where('city', 'ilike', '%' . $paramDetails['address'] . '%');
        }

        /* Searching for ZipCodes 85301-2732 ..... */
        if (isset($paramDetails['zip_code'])) {
            $placesContactDetails = $placesContactDetails->where('zip_code', 'ilike', '%' . $paramDetails['zip_code'] . '%');
        }

        /* Searching Filtration End */
        $placesContactDetails = $placesContactDetails->limit(config('constants.Places.placesContactlimit'));
        $placesContactDetails = $placesContactDetails->paginate(config('constants.Places.paginationPlaces'));
        $placesContactDetails = $placesContactDetails->toArray();

        return $placesContactDetails;
    }

    /* Get Bussiness Type Details */
    public function getBussinessTypeDetails($companyId, $bussinessTypeId)
    {
        $bussinessTypeDetails = PlacesCategory::whereId($bussinessTypeId)
            ->first();
        return $bussinessTypeDetails->name;
    }

    /* Check Places Contact Already Available in CallProof Contacts */
    public function checkPlacesContactInCallProof($placeId, $companyId)
    {
        return Contact::whereCompany_id($companyId)
            ->wherePlace_id($placeId)
            ->exists();
    }

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */

    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
