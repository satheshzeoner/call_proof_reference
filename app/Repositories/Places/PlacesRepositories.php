<?php

namespace App\Repositories\Places;

/* Models */

use App\Models\PlacesCategory\PlacesCategory;
use App\Models\PlacesCategory\PlacesCategoryList;
use App\Models\Sicsmaster\Sicsmaster;
use App\Models\Contact\Contact;

use Illuminate\Support\Facades\DB;

use Exception;

class PlacesRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* Get Places Category Details */
    public function getPlaceCategoryDetails($userId, $companyId, $categoryListId)
    {
        return PlacesCategory::whereCompany_id($companyId)
            ->whereCat_list_id($categoryListId)
            ->orderBy("position", "ASC")
            ->get()
            ->toArray();
    }

    /* Get PlacesCategoryList */
    public function getPlaceCategoryList($userId, $companyId)
    {
        return PlacesCategoryList::whereCompany_id($companyId)->first();
    }

    /* Get Specific PlaceContactDetails */
    public function getSpecificPlaceContactDetails($placeId)
    {
        return Sicsmaster::whereId($placeId)->get()->map(function ($placeContact) {
            $placeContact['company_name']   = trim($placeContact['company_name']);
            $placeContact['contact_name']   = trim($placeContact['contact_name']);
            $placeContact['address']        = trim($placeContact['address']);
            $placeContact['annual_sales']   = trim($placeContact['annual_sales']);
            $placeContact['city']           = trim($placeContact['city']);
            $placeContact['county']         = trim($placeContact['county']);
            $placeContact['employee_code']  = trim($placeContact['employee_code']);
            $placeContact['employee_count'] = trim($placeContact['employee_count']);
            $placeContact['fax']            = trim($placeContact['fax']);
            $placeContact['gender']         = trim($placeContact['gender']);
            $placeContact['business_type']  = trim($placeContact['business_type']);
            $placeContact['latitude']       = trim($placeContact['latitude']);
            $placeContact['longitude']      = trim($placeContact['longitude']);
            $placeContact['phone_number']   = trim($placeContact['phone_number']);
            $placeContact['sales_code']     = trim($placeContact['sales_code']);
            $placeContact['sic_code']       = trim($placeContact['sic_code']);
            $placeContact['state_code']     = trim($placeContact['state_code']);
            $placeContact['title']          = trim($placeContact['title']);
            $placeContact['website']        = trim($placeContact['website']);
            $placeContact['zip_code']       = trim($placeContact['zip_code']);
            $placeContact['geom']           = trim($placeContact['geom']);
            return $placeContact;
        });
    }

    /* Check PlaceId in Contact Tabel Already Present */
    public function checkPlaceIdInContacts($companyId, $placeId)
    {
        return Contact::whereCompany_id($companyId)
            ->wherePlace_id($placeId)->exists();
    }

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */

    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
