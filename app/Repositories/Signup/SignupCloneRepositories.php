<?php

namespace App\Repositories\Signup;

use App\Helpers\Helper;
use Illuminate\Support\Facades\DB;

/* Models */

use App\Models\Cfield\Cfield;
use App\Models\Cfield\CfieldOption;
use App\Models\Cfield\CfieldValue;
use App\Models\Category\CategorySic;
use App\Models\Eventtype\Eventtype;
use App\Models\Eventtype\EventtypePoint;
use App\Models\Eventform\Eventform;
use App\Models\Eventform\EventformCfield;
use App\Models\Eventform\EventformEmail;
use App\Models\Eventform\EventformContactType;
use App\Models\ContactMenu\ContactMenu;
use App\Models\ContactMenu\ContactMenulist;
use App\Models\PlacesCategory\PlacesCategory;
use App\Models\PlacesCategory\PlacesCategoryList;
use App\Models\Title\Title;
use App\Models\Contact\Contact;
use App\Models\Contact\ContactRep;
use App\Models\Contact\ContactCompany;
use App\Models\Contact\ContactEventform;
use App\Models\Contact\ContactEventformpersonnel;
use App\Models\Contact\ContactType;
use App\Models\Contact\ContactPersonnel;
use App\Models\Contact\ContactPhonepersonnel;
use App\Models\Contact\ContactPhone;
use App\Models\Contact\ContactNote;
use App\Models\People\PeopleRole;
use App\Models\Badge\Badge;
use App\Models\Users\UserProfile;
use App\Models\Followup\Followup;
use App\Models\Followup\FollowupType;
use App\Models\Followup\FollowupPersonnel;
use App\Models\Point\Point;
use App\Models\AppleSignintos\AppleSigninTos;



class SignupCloneRepositories
{

    protected $placesCategory     = 'cp_placescategory';
    protected $customField        = 'cp_cfield';
    protected $eventForm          = 'cp_eventform';
    protected $placesCategoryList = 'cp_placescategorylist';
    protected $title              = 'cp_title';
    protected $contactCompany     = 'cp_contactcompany';
    protected $badge              = 'cp_badge';
    protected $contactMenuList    = 'cp_contactmenulist';

    protected $similarTableStore = ['cp_market', 'cp_contacttype', 'cp_menucust', 'cp_menucolcust', 'cp_menurowcust', 'cp_menuurlcust', 'cp_peoplerole', 'cp_opptype', 'cp_oppstage', 'cp_followuptype'];

    /* ===============================CREATE=============================== */
    /* Cfield Table */
    public function doCfieldStore($companyId)
    {
        $masterDefaultTemplateId   = Helper::masterDefaultCompanySetting();


        $customFieldData    = Helper::getDefaultCompanyDetails($this->customField, $masterDefaultTemplateId);

        if (!empty($customFieldData)) {

            foreach ($customFieldData as $key => $customFieldValue) {

                /* cp_cfield Insertion */
                $customFieldValue['company_id'] = $companyId;
                unset($customFieldValue['id']);

                $customFieldResult = Cfield::create($customFieldValue);
                $customFieldInsertedId = $customFieldResult->id;

                /* cp_cfieldoption Insertion */
                $customFieldOptionValue['cfield_id'] = $customFieldInsertedId;
                $customFieldOptionValue['position']  = $customFieldValue['position'];
                $customFieldOptionValue['name']      = $customFieldValue['name'];
                $customFieldOptionValue['points']    = $customFieldValue['points'];

                $customFieldOptionResult      = CfieldOption::create($customFieldOptionValue);
                $customFieldOptionInsertedId  = $customFieldOptionResult->id;
            }
        }

        $result['status'] = true;
        return $result;
    }
    /* Cfield Table END */

    /* eventform Table */
    public function doEventFormStore($companyId)
    {
        $masterDefaultTemplateId    = Helper::masterDefaultCompanySetting();
        $eventFormData              = Helper::getDefaultCompanyDetails($this->eventForm, $masterDefaultTemplateId);

        if (!empty($eventFormData)) {

            /* Get Cp_contact Type */
            $contacttype = ContactType::where('company_id', $masterDefaultTemplateId)->get();

            /* Cfield */
            $customField = Cfield::where('company_id', $companyId)->get();

            $eventId = [];
            foreach ($eventFormData as $key => $value) {
                array_push($eventId, $value['id']);
            }

            /* Email */
            $emailData = EventformEmail::whereIn('event_form_id', $eventId)->get();

            foreach ($eventFormData as $key => $value) {

                /* cp_cfield Insertion */
                $value['company_id'] = $companyId;
                unset($value['id']);

                $eventformResult = Eventform::create($value);
                $eventformInsertedId = $eventformResult->id;

                /* cp_cfieldoption Insertion */
                foreach ($contacttype as $key => $contactTypeValue) {

                    $eventFormContactTypeValue['event_form_id']        = $eventformInsertedId;
                    $eventFormContactTypeValue['company_id']           = $companyId;
                    $eventFormContactTypeValue['contact_type_id']      = $contactTypeValue->id;

                    $eventFormContactTypeResult       = EventformContactType::create($eventFormContactTypeValue);
                    $eventFormContactTypeInsertedId   = $eventFormContactTypeResult->id;
                }

                foreach ($emailData as $key => $emailValue) {
                    $eventFormEmailValue['event_form_id']      = $eventformInsertedId;
                    $eventFormEmailValue['email']              = $emailValue->email;

                    $eventFormEmailResult     = EventformEmail::create($eventFormEmailValue);
                    $eventFormEmailInsertedId = $eventFormEmailResult->id;
                }

                foreach ($customField as $key => $customFieldValue) {
                    $customFieldDataValue['event_form_id']      = $eventformInsertedId;
                    $customFieldDataValue['cfield_id']          = $customFieldValue->id;
                    $customFieldDataValue['position']           = $value['position'];

                    $eventFormCustomFieldResult  = EventformCfield::create($customFieldDataValue);
                    $eventformcfieldInsertedId   = $eventFormCustomFieldResult->id;
                }
            }
        }

        $result['status'] = true;
        return $result;
    }
    /* eventform Table END */

    /* contactMenu Table */
    public function doContactMenuStore($companyId)
    {
        $masterDefaultTemplateId   = Helper::masterDefaultCompanySetting();
        $contactMenu                = Helper::getDefaultCompanyDetails($this->contactMenuList, $masterDefaultTemplateId);

        if (!empty($contactMenu)) {

            $contactMenuDistinct  = ContactMenu::distinct()->where('company_id', $masterDefaultTemplateId)->get()->toArray();

            foreach ($contactMenu as $key => $contactMenuValue) {

                $contactMenuValue['company_id'] = $companyId;
                unset($contactMenuValue['id']);

                $contactMenuListResult       = ContactMenulist::create($contactMenuValue);
                $contactMenuListInsertedId = $contactMenuListResult->id;

                foreach ($contactMenuDistinct as $key => $contactMenuDistinctValue) {

                    $eventFormMatch  = Eventform::where('company_id', $masterDefaultTemplateId)
                        ->where('name', $contactMenuDistinctValue['name'])->get()->toArray();

                    if (!empty($eventFormMatch)) {
                        $eventId = $eventFormMatch[0]['id'];
                    } else {
                        $eventId = null;
                    }

                    $contactMenuData['company_id']         = $companyId;
                    $contactMenuData['name']               = $contactMenuDistinctValue['name'];
                    $contactMenuData['type_id']            = $contactMenuDistinctValue['type_id'];
                    $contactMenuData['position']           = $contactMenuDistinctValue['position'];
                    $contactMenuData['is_hide']            = $contactMenuDistinctValue['is_hide'];
                    $contactMenuData['event_form_id']      = $eventId;
                    $contactMenuData['location']           = $contactMenuDistinctValue['location'];
                    $contactMenuData['contactmenulist_id'] = $contactMenuListInsertedId;

                    $contactMenuResult       = ContactMenu::create($contactMenuData);
                    $contactmenuInsertedId   = $contactMenuResult->id;
                }
            }
        }
        $result['status'] = true;
        return $result;
    }
    /* contactMenu Table END */

    /* placesCategoryList Table */
    public function doPlacesCategoryListStore($companyId)
    {
        $masterDefaultTemplateId = Helper::masterDefaultCompanySetting();
        $placesCategoryList       = Helper::getDefaultCompanyDetails($this->placesCategoryList, $masterDefaultTemplateId);
        $placesCategory           = Helper::getDefaultCompanyDetails($this->placesCategory, $masterDefaultTemplateId);

        if (!empty($placesCategoryList)) {

            foreach ($placesCategoryList as $key => $placesCategoryListValue) {

                $placesCategoryListValue['company_id'] = $companyId;
                unset($placesCategoryListValue['id']);

                $placesCategoryListResult = PlacesCategoryList::create($placesCategoryListValue);
                $placescategorylistInsertedId = $placesCategoryListResult->id;

                foreach ($placesCategoryList as $key => $placesCategoryValue) {
                    $placesCategoryData['name']        = $placesCategoryValue['name'];
                    $placesCategoryData['company_id']  = $placesCategoryValue['company_id'];
                    $placesCategoryData['cat_list_id'] = $placescategorylistInsertedId;
                    $placesCategoryData['hide']        = $placesCategoryValue['hide'];
                    $placesCategoryData['position']    = $placesCategoryValue['position'];

                    $placesCategoryResult = PlacesCategory::create($placesCategoryData);
                    $placesCategoryDataInsertedId = $placesCategoryResult->id;

                    $categorySic           = CategorySic::where('category_id', $placesCategoryValue['id'])->get();

                    foreach ($categorySic as $key => $categorySicvalue) {
                        $categorySicData['sic']         = $categorySicvalue->sic;
                        $categorySicData['category_id'] = $placesCategoryDataInsertedId;

                        $categorySicResult       = CategorySic::create($categorySicData);
                        $categorySicInsertedId = $categorySicResult->id;
                    }
                }

                $placesCategoryResult       = PlacesCategory::create($placesCategoryData);
                $placescategoryInsertedId   = $placesCategoryResult->id;
            }
        }

        $result['status'] = true;
        return $result;
    }
    /* placesCategoryList Table END */

    /* title Table */
    public function doTitleStore($companyId)
    {
        $masterDefaultTemplateId   = Helper::masterDefaultCompanySetting();
        $titleTable                = Helper::getDefaultCompanyDetails($this->title, $masterDefaultTemplateId);

        if (!empty($titleTable)) {

            foreach ($titleTable as $key => $titleTableValue) {

                $query = DB::table($this->title . ' as title');
                $query->select('contactmenulist.name');
                $query->leftJoin('cp_contactmenulist as contactmenulist', 'contactmenulist.id', '=', 'title.contactmenulist_id');
                $query->where('title.company_id', $masterDefaultTemplateId);
                $query->where('title.id', $titleTableValue['id']);
                $results = $query->first();

                $contactMenuListId         = ContactMenulist::where('name', $results->name)->where('company_id', $companyId)->first();
                $placesCategoryList         = Helper::getDefaultCompanyDetails('cp_placescategorylist', $companyId);

                if (!empty($placesCategoryList)) {
                    $placeCategoryListId = $placesCategoryList[0]['id'];
                } else {
                    $placeCategoryListId = null;
                }
                $titleData['company_id']         = $companyId;
                $titleData['name']               = $titleTableValue['name'];
                $titleData['image']              = $titleTableValue['image'];
                $titleData['contactmenulist_id'] = $contactMenuListId->id;
                $titleData['places_cat_list_id'] = $placeCategoryListId;

                $titleResult     = Title::create($titleData);
                $titleInsertedId = $titleResult->id;

                /* UserProfile Update contactMenuListId */
                if ($titleTableValue['name'] == config('constants.Title.otherReplaceName')) {
                    UserProfile::where('company_id', $companyId)
                        ->update(['contactmenulist_id' => $contactMenuListId->id, 'title_id' => $titleInsertedId]);
                }
            }
        }
        $result['status'] = true;
        return $result;
    }
    /* title Table END */

    /* contactCompany Table */
    public function doContactCompanyStore($companyId, $userId)
    {
        $masterDefaultTemplateId    = Helper::masterDefaultCompanySetting();
        $contactCompany             = Helper::getDefaultCompanyDetails($this->contactCompany, $masterDefaultTemplateId);

        if (!empty($contactCompany)) {

            foreach ($contactCompany as $key => $contactCompanyValue) {
                $contactCompanyData['company_id']  = $companyId;
                $contactCompanyData['name']        = $contactCompanyValue['name'];

                $contactCompanyResult   = ContactCompany::create($contactCompanyData);
                $contactCompanyResultId = $contactCompanyResult->id;
            }

            $contactTable      = Helper::getDefaultCompanyDetails('cp_contact', $masterDefaultTemplateId);

            if (!empty($contactTable)) {

                foreach ($contactTable as $key => $contactTypeValue) {

                    /* GET SIMILAR ContactTypeid ON MAIN TEMPLATE CLONE TO NEW COMPANY ID */
                    $contactTypeTemplateData       = ContactType::where('id', $contactTypeValue['contact_type_id'])->first();

                    $contactCompanyTemplateData    = ContactCompany::where('id', $contactTypeValue['contact_company_id'])->first();

                    $newContactCompanyData         = ContactCompany::where('name', $contactCompanyTemplateData->name)
                        ->where('company_id', $companyId)
                        ->orderBy('id', 'ASC')
                        ->first();

                    $contactTypeMasterId = $contactTypeValue['id'];
                    unset($contactTypeValue['id']);

                    $contactTypeValue['contact_type_id']    = $contactTypeTemplateData->id;
                    $contactTypeValue['contact_company_id'] = $newContactCompanyData->id;
                    $contactTypeValue['company_id']         = $companyId;

                    $contactResult = Contact::create($contactTypeValue);
                    $contactInsertedId = $contactResult->id;

                    /*---------------------------------cp_contact-----------------------------------------*/

                    /* Contact Id Based cp_contactrep Insertion */
                    $contactRepData['contact_id'] = $contactInsertedId;
                    $contactRepData['user_id']    = $userId;
                    $contactRepData['created']    = now();

                    $contactRepResult     = ContactRep::create($contactRepData);
                    $contactRepInsertedId = $contactRepResult->id;

                    /*-----------------------------------cp_contactrep---------------------------------------*/

                    $customFieldValueTemplateData    = cfieldvalue::where('entity_id', $contactTypeMasterId)->get();

                    foreach ($customFieldValueTemplateData as $key => $customFieldValue) {

                        $customFieldData      = cfield::where('id', $customFieldValue['cfield_id'])->first();



                        $customfieldDataNewId  = cfield::where('name', $customFieldData->name)
                            ->where('company_id', $companyId)
                            ->first();



                        /* Contact Id Based cp_contactrep Insertion */
                        $customFieldValueData['cfield_id'] = $customfieldDataNewId->id;
                        $customFieldValueData['entity_id'] = $contactInsertedId;
                        $customFieldValueData['cf_value']  = $customFieldValue['cf_value'];
                        $customFieldValueData['updated']   = now();

                        $customFieldValueResult = cfieldvalue::create($customFieldValueData);
                        $customFieldValueInsertedId = $customFieldValueResult->id;
                    }

                    /*------------------------------------cp_cfieldvalue--------------------------------------*/


                    /* Contact Id And People Role Has Been Inserted in New Company Id */

                    $contactPersonnel   = ContactPersonnel::where('contact_id', $contactTypeMasterId)
                        ->where('company_id', $masterDefaultTemplateId)
                        ->get();

                    foreach ($contactPersonnel as $key => $contactPersonnelValue) {

                        $peopleRoleTemplateData    = PeopleRole::where('id', $contactPersonnelValue['peoplerole_id'])
                            ->where('company_id', $companyId)
                            ->first();

                        if (!empty($peopleRoleTemplateData)) {
                            $peopleRoleId = $peopleRoleTemplateData->id;
                        } else {
                            $peopleRoleId = null;
                        }

                        unset($contactPersonnelValue['id']);

                        $contactPersonnelValues["company_id"]      = $companyId;
                        $contactPersonnelValues["contact_id"]      = $contactInsertedId;
                        $contactPersonnelValues["first_name"]      = $contactPersonnelValue['first_name'];
                        $contactPersonnelValues["last_name"]       = $contactPersonnelValue['last_name'];
                        $contactPersonnelValues["title"]           = $contactPersonnelValue['title'];
                        $contactPersonnelValues["last_contacted"]  = $contactPersonnelValue['last_contacted'];
                        $contactPersonnelValues["personnel_notes"] = $contactPersonnelValue['personnel_notes'];
                        $contactPersonnelValues["created"]         = $contactPersonnelValue['created'];
                        $contactPersonnelValues["updated"]         = $contactPersonnelValue['updated'];
                        $contactPersonnelValues["cell_phone"]      = $contactPersonnelValue['cell_phone'];
                        $contactPersonnelValues["email"]           = $contactPersonnelValue['email'];
                        $contactPersonnelValues["peoplerole_id"]   = $peopleRoleId;
                        $contactPersonnelValues["image"]           = $contactPersonnelValue['image'];
                        $contactPersonnelValues["is_disabled"]     = $contactPersonnelValue['is_disabled'];
                        $contactPersonnelValues["moved_to"]        = $contactPersonnelValue['moved_to'];

                        $contactPersonnelResult = ContactPersonnel::create($contactPersonnelValues);
                        $contactPersonnelValueInsertedId = $contactPersonnelResult->id;
                    }

                    /*-----------------------------------cp_contactpersonnel---------------------------------------*/

                    /* Contact Id And ContactPhone Role Has Been Inserted in New Company Id */
                    $peopleRoleTemplateData    = ContactPhone::where('contact_id', $contactTypeMasterId)
                        ->where('company_id', $masterDefaultTemplateId)
                        ->get()->toArray();

                    foreach ($peopleRoleTemplateData as $key => $peopleRolevalue) {
                        unset($peopleRolevalue['id']);
                        $peopleRolevalue['company_id']    = $companyId;
                        $peopleRolevalue['contact_id']    = $contactInsertedId;

                        //
                        $contactPhoneResult       = ContactPhone::create($peopleRolevalue);
                        $contactPhoneInsertedId   = $contactPhoneResult->id;
                    }


                    /*-----------------------------------cp_contactphone---------------------------------------*/

                    if (!empty($contactPersonnelValueInsertedId)) {
                        $insertedPersonalId = $contactPersonnelValueInsertedId;
                    } else {
                        $insertedPersonalId = null;
                    }

                    $contactPhonePersonnelData['contactphone_id']  = $contactInsertedId;
                    $contactPhonePersonnelData['personnel_id']     = $insertedPersonalId;

                    $contactPhonepersonnelResult       = ContactPhonepersonnel::create($contactPhonePersonnelData);
                    $contactphonepersonnelInsertedId   = $contactPhonepersonnelResult->id;

                    /*-----------------------------------cp_contactphonepersonnel-------------------------------*/

                    $contactNoteData    = ContactNote::where('contact_id', $contactTypeMasterId)
                        ->where('company_id', $masterDefaultTemplateId)
                        ->get()->toArray();

                    foreach ($contactNoteData as $key => $contactNoteValue) {
                        unset($contactNoteValue['id']);

                        $contactNoteValue['contact_id'] = $contactInsertedId;
                        $contactNoteValue['company_id'] = $companyId;

                        $contactNoteResult       = ContactNote::create($contactNoteValue);
                        $contactNoteInsertedId   = $contactNoteResult->id;
                    }

                    /*-----------------------------------cp_contactnote-------------------------------*/

                    $followupData     = Followup::where('contact_id', $contactTypeMasterId)
                        ->get()->toArray();

                    foreach ($followupData as $key => $followupValue) {
                        $followupTypeData     = FollowupType::where('id', $followupValue['followup_type_id'])
                            ->where('company_id', $companyId)
                            ->first();

                        unset($followupValue['id']);

                        $followupValue['start']      = now();
                        $followupValue['contact_id'] = $contactInsertedId;
                        $followupValue['user_id']    = $userId;
                        $followupValue['created']    = now();
                        $followupValue['updated']    = now();

                        $followupResult       = Followup::create($followupValue);
                        $followupInsertedId   = $followupResult->id;
                    }

                    /*-----------------------------------cp_followup-------------------------------*/

                    $followupPersonalValue['followup_id']  = $followupInsertedId;
                    $followupPersonalValue['personnel_id'] = $contactPersonnelValueInsertedId;

                    $followupPersonnelResult     = FollowupPersonnel::create($followupPersonalValue);
                    $followupPersonalInsertedId  = $followupPersonnelResult->id;


                    /*-----------------------------------cp_followuppersonnel-------------------------------*/

                    $contactEventFormData     = ContactEventform::where('contact_id', $contactTypeMasterId)
                        ->where('company_id', $masterDefaultTemplateId)
                        ->get()->toArray();

                    foreach ($contactEventFormData as $key => $contactEventFormValue) {
                        $eventFormData         = Eventform::where('id', $contactEventFormValue['event_form_id'])
                            ->where('company_id', $masterDefaultTemplateId)
                            ->first();
                        $newEventFormData     = Eventform::where('name', $eventFormData->name)
                            ->where('company_id', $companyId)
                            ->first();

                        unset($contactEventFormValue['id']);
                        $contactEventFormValue['company_id']    = $companyId;
                        $contactEventFormValue['user_id']       = $userId;
                        $contactEventFormValue['contact_id']    = $contactInsertedId;
                        $contactEventFormValue['event_form_id'] = $newEventFormData->id;
                        $contactEventFormValue['created']       = now();

                        $contactEventFormResult        = ContactEventform::create($contactEventFormValue);
                        $contacteventFormInsertedId    = $contactEventFormResult->id;
                    }

                    /*-----------------------------------cp_contacteventform-------------------------------*/

                    $contactEventFormPersonnelValue['contacteventform_id']  = $contacteventFormInsertedId;
                    $contactEventFormPersonnelValue['personnel_id']         = $contactPersonnelValueInsertedId;

                    $contactEventFormPersonnelResult = ContactEventformpersonnel::create($contactEventFormPersonnelValue);
                    $contactEventFormPersonnelInsertedId = $contactEventFormPersonnelResult->id;

                    /*-----------------------------------cp_contacteventform-------------------------------*/
                }
            }
        }

        $result['status'] = true;
        return $result;
    }
    /* contactCompany Table END */

    /* badge Table */
    public function doBadgeStore($companyId)
    {
        $masterDefaultTemplateId   = Helper::masterDefaultCompanySetting();
        $badgeTable                = Helper::getDefaultCompanyDetails($this->badge, $masterDefaultTemplateId);

        if (!empty($badgeTable)) {

            foreach ($badgeTable as $key => $badgeValue) {
                $badgeValue['company_id'] = $companyId;
                unset($badgeValue['id']);
                $badgeResult    = Badge::create($badgeValue);
                $insertedBadeId = $badgeResult->id;
            }
        }

        /*---------------------------------cp_badge-----------------------------------------*/

        $pointTable        = Helper::getDefaultCompanyDetails('cp_point', $masterDefaultTemplateId);
        $pointTableId     = [];

        if (!empty($pointTable)) {

            foreach ($pointTable as $key => $pointValue) {
                $pointValue['company_id'] = $companyId;
                unset($pointValue['id']);

                $pointResult = Point::create($pointValue);
                $pointInsertedId = $pointResult->id;
                $pointTableId[$key] = $pointInsertedId;
            }
        }

        /*---------------------------------cp_point-----------------------------------------*/

        $eventTypeTable        = Helper::getDefaultCompanyDetails('cp_eventtype', $masterDefaultTemplateId);

        if (!empty($eventTypeTable)) {

            foreach ($eventTypeTable as $key => $eventTypeValue) {
                $eventTypeValue['company_id'] = $companyId;
                unset($eventTypeValue['id']);

                $eventTypeResult        = Eventtype::create($eventTypeValue);
                $eventTypeInsertedId  = $eventTypeResult->id;

                /*---------------------------------cp_eventtype-----------------------------------------*/

                if (!empty($pointTableId[$key])) {
                    $pointId = $pointTableId[$key];
                    $eventTypePointData['event_type_id'] = $eventTypeInsertedId;
                    $eventTypePointData['point_id']      = $pointId;

                    $eventTypePointResult       = EventtypePoint::create($eventTypePointData);
                    $eventTypePointInsertedId   = $eventTypePointResult->id;

                    /*---------------------------------cp_eventtypepoint-----------------------------------------*/
                }
            }
        }

        $result['status'] = true;
        return $result;
    }
    /* badge Table END */

    /* eventCreate Table */
    /* eventCreate Table END */

    /* AppleSignintos Table */
    public function doAppleSignintos($companyId, $userId, $applesSigninTos)
    {
        $appleSigninTos['company_id']    = $companyId;
        $appleSigninTos['user_id']       = $userId;
        $appleSigninTos['email']         = $applesSigninTos['email'];
        $appleSigninTos['private_email'] = $applesSigninTos['private_email'];
        $appleSigninTos['version']       = $applesSigninTos['version'];
        $appleSigninTos['date']          = now();
        $appleSigninTos['is_accepted']   = True;

        $user = AppleSigninTos::create($appleSigninTos);

        return true;
    }
    /* AppleSignintos Table END */


    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
