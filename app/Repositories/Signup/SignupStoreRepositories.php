<?php

namespace App\Repositories\Signup;

use App\Helpers\Helper;

/* Models */
use App\Models\Company\Company;
use App\Models\Users\UserSetting;
use App\Models\Users\UserPhone;
use App\Models\Users\UserProfile;
use App\Models\Auth\AuthUser;

class SignupStoreRepositories
{
    /* ===============================CREATE=============================== */

    /* AuthUser */
    public function doAuthUserStore($request)
    {
        $splitData               = explode(' ', $request['contact']);
        $authUser['first_name']   = $splitData[0];
        $authUser['last_name']    = $splitData[1];
        $authUser['username']     = $request['email'];
        $authUser['email']        = $request['email'];
        $authUser['password']     = Helper::make_password($request['password']);

        try {
            $user = AuthUser::create($authUser);
            $insertedId = $user->id;
        } catch (\Illuminate\Database\QueryException $e) {
            $authUserResult['status'] = false;
            $authUserResult['id']     = '';
            return $authUserResult;
        }

        $authUserResult['status'] = true;
        $authUserResult['id']     = $insertedId;
        return $authUserResult;
    }
    /* End AuthUser */

    /* Company Table */
    public function doCompanyStore($request)
    {
        $timeStampActivity                     = now();
        $company['name']                       = $request['company'];
        $company['last_activity']              = $timeStampActivity;
        $company['activity_30']                = config('constants.Company.activity_30');
        $company['activity_90']                = config('constants.Company.activity_90');
        $company['balance']                    = config('constants.Company.balance');
        $company['twilio']                     = config('constants.Company.twilio');
        $company['dealer']                     = config('constants.Company.dealer');
        $company['referer_url_id']             = config('constants.Company.referer_url_id');
        $company['new_events']                 = config('constants.Company.new_events');
        $company['is_number_of_stats_shown']   = config('constants.Company.is_number_of_stats_shown');

        try {
            $user = Company::create($company);
            $insertedId = $user->id;
        } catch (\Illuminate\Database\QueryException $e) {
            return false;
        }
        $companyResult['status'] = true;
        $companyResult['id']     = $insertedId;
        return $companyResult;
    }
    /* Company Table END */

    /* UserSetting Table */
    public function doUsersettingStore($request, $userId)
    {

        $userSettingGps['user_id']                                 = $userId;
        $userSettingGps['name']                                    = config('constants.Usersetting.setting_name_1');
        $userSettingGps['value']                                   = 1;
        $userSettingGps['updated']                                 = now();
        try {
            $usersetting = UserSetting::create($userSettingGps);
        } catch (\Illuminate\Database\QueryException $e) {
            return false;
        }

        $userSettingLeads['user_id']                                 = $userId;
        $userSettingLeads['name']                                    = config('constants.Usersetting.setting_name_2');
        $userSettingLeads['value']                                   = 1;
        $userSettingLeads['updated']                                 = now();

        try {
            $userSetting = UserSetting::create($userSettingLeads);
            $insertedId = $userSetting->id;
        } catch (\Illuminate\Database\QueryException $e) {
            return false;
        }

        $userSettingResult['status'] = true;
        $userSettingResult['id']     = $insertedId;
        return $userSettingResult;
    }
    /* UserSetting Table END */

    /* UserPhone Table */
    public function doPhoneStore($request, $userId, $companyId, $phoneType)
    {
        $userPhone['user_id']       = $userId;
        $userPhone['phone_type_id'] = $phoneType;
        $userPhone['phone_number']  = $request['phone'];
        $userPhone['company_id']    = $companyId;
        $userPhone['country_code']  = "+1";

        try {
            $userPhoneResult = UserPhone::create($userPhone);
            $insertedId = $userPhoneResult->id;
        } catch (\Illuminate\Database\QueryException $e) {
            $userPhoneResult['status'] = false;
            $userPhoneResult['id']     = '';
            return $userPhoneResult;
        }

        $userPhoneResult['status'] = true;
        $userPhoneResult['id']     = $insertedId;
        return $userPhoneResult;
    }
    /* UserPhone Table End */

    /* UserProfile Table */
    public function doUserProfileStore($request, $userId, $CompanyId, $defaultPhoneId, $ipDetails)
    {
        $userprofile['user_id']                                  = $userId;
        $userprofile['company_id']                               = $CompanyId;
        $userprofile['default_phone_id']                         = $defaultPhoneId;
        $userprofile['manager']                                  = config('constants.Userprofile.manager');
        $userprofile['temppass']                                 = config('constants.Userprofile.temppass');
        $userprofile['secret']                                   = config('constants.Userprofile.secret'); /* Secret ID Generation */
        $userprofile['updated']                                  = now();
        $userprofile['contact_radius']                           = config('constants.Userprofile.contact_radius');
        $userprofile['delete_contacts']                          = config('constants.Userprofile.delete_contacts');
        $userprofile['user_timezone']                            = $ipDetails->timezone;
        $userprofile['no_auto_timezone']                         = config('constants.Userprofile.no_auto_timezone');
        $userprofile['hide_rep_assignment']                      = config('constants.Userprofile.hide_rep_assignment');
        $userprofile['market_manager']                           = config('constants.Userprofile.market_manager');
        $userprofile['is_export']                                = config('constants.Userprofile.is_export');
        $userprofile['is_start_appointment']                     = config('constants.Userprofile.is_start_appointment');
        $userprofile['google_cal_sync']                          = config('constants.Userprofile.google_cal_sync');
        $userprofile['hide_mobile_appt_popup']                   = config('constants.Userprofile.hide_mobile_appt_popup');
        $userprofile['is_transcription_settings_permission']     = config('constants.Userprofile.is_transcription_permission');
        $userprofile['is_transcription_permission']              = config('constants.Userprofile.is_transcription_permission');
        $userprofile['can_send_mms']                             = config('constants.Userprofile.can_send_mms');
        $userprofile['call_review_automation_permission']        = config('constants.Userprofile.call_review_automation_permission');

        try {
            $userProfileResult = UserProfile::create($userprofile);
            $insertedId       = $userProfileResult->id;
        } catch (\Illuminate\Database\QueryException $e) {
            $userprofileResult['status'] = false;
            $userprofileResult['id']     = '';
            return $userprofileResult;
        }

        $userprofileResult['status'] = true;
        $userprofileResult['id']     = $insertedId;
        return $userprofileResult;
    }
    /* UserProfile Table END */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
