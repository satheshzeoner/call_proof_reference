<?php

namespace App\Repositories\CustomField;

use Exception;

/* Models */

use App\Models\Contact\ContactEventformpersonnel;
use App\Models\Cfield\CfieldValue;

use Illuminate\Support\Facades\DB;

class CustomFieldRepositories
{
    /* ===============================CREATE=============================== */


    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* custom Field Value Update */
    public function updateCustomFieldValue($customFieldValue)
    {
        $customFieldDetails = CfieldValue::whereEntity_id($customFieldValue['entity_id'])
            ->whereCfield_id($customFieldValue['cfield_id'])
            ->update($customFieldValue);

        return $customFieldDetails;
    }
    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
