<?php

namespace App\Repositories\Setting;

/* Models */

use App\Models\Auth\AuthUser;
use App\Models\Eventform\Eventform;
use App\Models\Users\UserSetting;
use App\Models\Company\Company;


use App\Models\TaskNotificationMinuteChoice\TaskNotificationMinuteChoice;
use App\Models\Users\UserProfile;
use Exception;

class SettingRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */
    public function getAllBySelectedId($selectedValue)
    {
        $choices = TaskNotificationMinuteChoice::get()->map(function ($choice) use ($selectedValue) {
            $selected = false;
            if ($choice['id'] == $selectedValue) {
                $selected = true;
            }
            $choicesDetails['task_notification_choice_id'] = $choice['id'];
            $choicesDetails['name']      = $choice['name'];
            $choicesDetails['value']     = $choice['value'];
            $choicesDetails['selected']  = $selected;
            return $choicesDetails;
        });
        return $choices;
    }

    /* Get Settings Info */
    public function getSettingsInfoDetails($userId, $companyId)
    {
        return UserProfile::whereUser_id($userId)
            ->whereCompany_id($companyId)
            ->get()
            ->toArray();
    }

    /* Get EventFormId For SettingsInfo */
    public function getEventFormId($userId, $companyId)
    {
        return Eventform::whereCompany_id($companyId)
            ->whereIs_after_call_form('1')
            ->get()
            ->first();
    }

    /* Get Company For SettingsInfo */
    public function getCompanyInfo($companyId)
    {
        return Company::whereId($companyId)
            ->get()
            ->first();
    }

    /* Get UserSetting For Gps SettingsInfo */
    public function getUserSettingGpsInfo($userId)
    {
        return UserSetting::whereUser_id($userId)
            ->whereName(config('constants.Usersetting.setting_name_1'))
            ->whereValue(config('constants.Usersetting.value'))
            ->get()
            ->first();
    }

    /* Get UserSetting For sync_calls SettingsInfo */
    public function getUserSettingSyncCallInfo($userId)
    {
        return UserSetting::whereUser_id($userId)
            ->whereName(config('constants.Usersetting.setting_name_3'))
            ->get()
            ->first();
    }

    /* Get UserSetting For sync_gps SettingsInfo */
    public function getUserSettingSyncGpsInfo($userId)
    {
        return UserSetting::whereUser_id($userId)
            ->whereName(config('constants.Usersetting.setting_name_4'))
            ->get()
            ->first();
    }

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */
    public function updateSettings($userId, $companyId, $syncTaskOnDeviceCalender, $hideMobileAppointmentPopup, $taskPopUpSetting, $isContactPageAsHomepage, $isLogsEnabled, $isStartAppointment, $taskNotificationTimeId)
    {
        try {
            $user = AuthUser::findorFail($userId);
            $user->userProfile()
                ->where('company_id', $companyId)
                ->update([
                    'google_cal_sync' => $syncTaskOnDeviceCalender,
                    'hide_mobile_appt_popup' => $hideMobileAppointmentPopup,
                    'task_popup_disabled' => $taskPopUpSetting,
                    'is_contact_page_as_homepage' => $isContactPageAsHomepage,
                    'task_notification_time_id' => $taskNotificationTimeId,
                ]);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
