<?php

namespace App\Repositories\Event;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Exception;

/* Models */

use App\Models\Event\Event;
use App\Models\Eventtype\Eventtype;

class EventAddRepositories
{
    /* ===============================CREATE=============================== */
    /* Create New Events */
    public function doStoreEvent($Event)
    {
        try {
            $EventResult        = Event::create($Event);

            /* Log Event */
            $result['status'] = true;
            $result['message'] = $EventResult->id;
            return $result;
        } catch (Exception $e) {
            $result['status'] = false;
            $result['message'] = $e->getMessage();
            return $result;
        }
    }
    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* Get EventType Details */
    public function getEventTypeDetails($companyId, $eventTypeName)
    {
        return Eventtype::where('name', $eventTypeName)
            ->where('company_id', $companyId)
            ->first();
    }

    /* GetPoints */
    public function getEventPoints($companyId, $eventTypeId)
    {
        $query = DB::table('cp_eventtype as eventtype');
        $query->select(DB::raw('coalesce( SUM( point.value ), 0 ) AS points'));
        $query->leftJoin('cp_eventtypepoint as eventtypepoint', 'eventtypepoint.event_type_id', '=', 'eventtype.id');
        $query->leftJoin('cp_point as point', 'eventtypepoint.point_id', '=', 'point.id');
        $query->where('eventtype.company_id', $companyId);
        $query->where('eventtype.id', $eventTypeId);
        $query->groupBy('eventtype.id');
        $results = $query->first();

        return $results;
    }



    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
