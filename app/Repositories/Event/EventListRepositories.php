<?php

namespace App\Repositories\Event;


/* Models */

use App\Models\Event\Event;
use App\Models\Call\Call;
use App\Models\Contact\ContactEventform;
use App\Models\Contact\ContactPersonnel;
use App\Models\Users\UserProfile;
use App\Models\Users\UserMarket;
use App\Models\Contact\ContactRep;
use App\Models\Users\UserSetting;

class EventListRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* Get Event Details */
    public function getEventList($userId, $companyId, $request)
    {
        /* paramDetails */
        $paramDetails = $request->all();

        $eventDetails = Event::whereCompany_id($companyId);
        $eventDetails = $eventDetails->whereUser_id($userId);
        $eventDetails = $eventDetails->with('eventType');
        $eventDetails = $eventDetails->with('eventCompany');
        $eventDetails = $eventDetails->with('eventContact');
        $eventDetails = $eventDetails->with('eventOtheruser');
        $eventDetails = $eventDetails->with('eventBadge');
        $eventDetails = $eventDetails->with('eventGoogleplace');
        $eventDetails = $eventDetails->with('eventAppointment');
        $eventDetails = $eventDetails->with('eventAppointment');
        $eventDetails = $eventDetails->with('eventFollow');
        $eventDetails = $eventDetails->with('eventTwilioCall');

        /* Filteration */
        if (isset($paramDetails['event_type_id'])) {
            $eventDetails = $eventDetails->whereEvent_type_id($paramDetails['event_type_id']);
        }

        if (isset($paramDetails['sales_rep_id'])) {
            $eventDetails = $eventDetails->whereUser_id($paramDetails['sales_rep_id']);
        }

        $eventDetails = $eventDetails->orderBy('id', 'DESC');
        $eventDetails = $eventDetails->paginate(config('constants.Pagination.eventItemsPerPage'));
        $eventDetails = $eventDetails->toArray();

        return $eventDetails;
    }

    /* Get Call Details */
    public function getCallDetails($callId)
    {
        $callDetails =  Call::whereId($callId)
            ->with('callType')
            ->get()
            ->toArray();
        return collect($callDetails)->first();
    }

    /* Get ContactEventForm Details */
    public function getContactEventFormDetails($contactEventFormId)
    {
        $contactEventForm = ContactEventform::whereId($contactEventFormId)
            ->with('eventForm')
            ->get()
            ->toArray();
        return collect($contactEventForm)->first();
    }


    /* Get Contact Personal Details */
    public function getContactPersonalDetails($contactId)
    {
        return ContactPersonnel::whereContact_id($contactId)
            ->get()
            ->toArray();
    }

    public function getUserProfileDetails($userId)
    {
        return UserProfile::whereUser_id($userId)
            ->first();
    }

    /* Get User Market Details */
    public function getUserMarketDetails($userId, $companyId)
    {
        return UserMarket::whereCompany_id($companyId)
            ->get()
            ->toArray();
    }

    /* Contact Rep Details */
    public function getContactRepDetails($marketUserId, $contactId)
    {
        return ContactRep::whereIn('user_id', $marketUserId)
            ->whereContact_id($contactId)
            ->exists();
    }

    /* Contact Rep Details */
    public function hideContactRepDetails($marketUserId, $contactId)
    {
        return ContactRep::whereNotIn('user_id', $marketUserId)
            ->whereContact_id($contactId)
            ->exists();
    }

    /* Get User Settings Details */
    public function getUserSettingAssignedStatus($userId)
    {
        return UserSetting::whereUser_id($userId)
            ->where('name', 'ilike', '%' . config('constants.ContactRadius.unassigned_leads') . '%')
            ->first();
    }

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
