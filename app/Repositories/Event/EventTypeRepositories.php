<?php

namespace App\Repositories\Event;


/* Models */

use App\Models\Eventtype\Eventtype;

class EventTypeRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    public function getEventType($userId, $companyId)
    {
        return Eventtype::whereCompany_id($companyId)
            ->get()
            ->map(function ($eventType) {
                $eventTypeDetails['event_id']          = $eventType['id'];
                $eventTypeDetails['event_name']        = $eventType['name'];
                $eventTypeDetails['event_description'] = $eventType['desc'];
                $eventTypeDetails['event_image']       = $eventType['image'];
                return $eventTypeDetails;
            })
            ->toArray();
    }

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
