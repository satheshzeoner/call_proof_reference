<?php

namespace App\Repositories\EventForm;

use Exception;

/* Models */

use App\Models\Contact\ContactEventformpersonnel;
use App\Models\Cfield\CfieldValue;
use App\Models\Contact\ContactImage;
use App\Models\Cfield\CfieldMultiValue;

use Illuminate\Support\Facades\DB;

class EventFormEditRepositories
{
    /* ===============================CREATE=============================== */

    /* New Updated MultiValue Options */
    public function updateNewContactEventFormMultiValueId($multiValueDetails)
    {
        $multiValueId = CfieldMultiValue::create($multiValueDetails);
        return $multiValueId->id;
    }

    /* Get Selected People Options */
    public function getSelectedPeopleOptions($contactEventFormId)
    {
        return ContactEventformpersonnel::whereContacteventform_id($contactEventFormId)
            ->get()
            ->pluck('personnel_id')
            ->toArray();
    }

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */
    /* Get Specific ContactEventForm Details */
    public function getSpecificCustomFieldValues($contactEventFormId, $customFieldId)
    {
        $customFieldValue = CfieldValue::select('cf_value')->whereEntity_id($contactEventFormId)->whereCfield_id($customFieldId)->get()->toArray();
        if (!empty($customFieldValue)) {
            return (collect($customFieldValue)->pluck('cf_value')->first());
        } else {
            return '';
        }
    }
    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* custom Field Value Update */
    public function updateCustomFieldValue($customFieldValue)
    {
        $customFieldDetails = CfieldValue::whereEntity_id($customFieldValue['entity_id'])
            ->whereCfield_id($customFieldValue['cfield_id'])
            ->update($customFieldValue);

        return $customFieldDetails;
    }

    /* Update EventForm Personnel */
    public function storeContactEventFormPersonnel($contactEventFormPersonelDetails)
    {
        $contactEventFormPersonel = ContactEventformpersonnel::create($contactEventFormPersonelDetails);
        return $contactEventFormPersonel->id;
    }

    /* Update ContactImage url PUT */
    public function updateContactImageDetails($contactImageDetails)
    {
        $contactImage = ContactImage::whereCompany_id($contactImageDetails['company_id'])
            ->whereUser_id($contactImageDetails['user_id'])
            ->whereContact_id($contactImageDetails['contact_id'])
            ->whereCfield_id($contactImageDetails['cfield_id'])
            ->whereContact_event_form_id($contactImageDetails['contact_event_form_id'])
            ->update($contactImageDetails);
        return true;
    }



    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */
    public function removeSpecificContactEventFormId($contactEventFormId)
    {
        $contactEventFormDetails = ContactEventformpersonnel::where('contacteventform_id', $contactEventFormId)
            ->delete();
        return $contactEventFormDetails;
    }

    public function removeSpecificContactEventFormMultiValueId($contactEventFormId, $customFieldId)
    {
        $multiValueDetails = CfieldMultiValue::where('entity_id', $contactEventFormId)
            ->where('cfield_id', $customFieldId)
            ->delete();
        return $multiValueDetails;
    }
    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
