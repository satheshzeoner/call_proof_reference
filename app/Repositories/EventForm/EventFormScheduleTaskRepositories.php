<?php

namespace App\Repositories\EventForm;

/* Models */

use App\Models\Followup\FollowupType;
use App\Models\Users\UserProfile;

class EventFormScheduleTaskRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    public function getTaskAssignment($companyId)
    {
        return UserProfile::select('id', 'address', 'user_id')
            ->with(['userAuth' => function ($taskJoin) {
                $taskJoin->select('id', 'first_name', 'last_name');
            }])
            ->whereCompany_id($companyId)
            ->get()
            ->toArray();
    }

    public function getTaskActionDetails($companyId)
    {
        $followUpType = FollowupType::select("id as follow_type_id", "name", "position", "is_default")
            ->whereCompany_id($companyId)
            ->orderBy('position', 'ASC')
            ->get()
            ->toArray();

        return $followUpType;
    }

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
