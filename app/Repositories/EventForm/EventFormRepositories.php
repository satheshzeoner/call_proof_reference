<?php

namespace App\Repositories\EventForm;

use Illuminate\Support\Facades\DB;

/* Models */
use App\Models\Eventform\Eventform;
use App\Models\Contact\ContactEventform;
use App\Models\Contact\ContactPersonnel;

class EventFormRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */
    public function getSelectEventDetails($userId, $companyId, $contactId, $eventFormId)
    {
        if ($eventFormId) {
            $eventForm = Eventform::select('id', 'name', 'event_form_treat_as_id', 'is_schedule_followup', 'send_email_to_rep', 'post_back_method')
                ->where('id', '!=', $eventFormId)
                ->whereCompany_id($companyId)
                ->orderBy('position', 'ASC');
        } else {
            $eventForm = Eventform::select('id', 'name', 'event_form_treat_as_id', 'is_schedule_followup', 'send_email_to_rep', 'post_back_method')
                ->whereCompany_id($companyId)
                ->orderBy('position', 'ASC');
        }
        $eventForm = $eventForm->get();
        $eventForm = $eventForm->map(function ($eventForm) {
            if ($eventForm->event_form_treat_as_id == null) {
                $eventForm->event_form_treat_as_appointment = false;
            } else {
                $eventForm->event_form_treat_as_appointment = true;
            }
            unset($eventForm->event_form_treat_as_id);
            return $eventForm;
        });

        return $eventForm->toArray();
    }

    /* Get Specific EventForm Details */
    public function getSpecificEventDetails($userId, $companyId, $contactId, $eventFormId)
    {
        $eventForm = Eventform::select('id', 'name', 'event_form_treat_as_id', 'is_schedule_followup', 'send_email_to_rep', 'post_back_method')
            ->whereCompany_id($companyId)
            ->whereId($eventFormId);

        return $eventForm->first();
    }

    /* Get Specific ContactEventForm Details */
    public function getSpecificContactEventDetails($contactEventFormId)
    {
        $eventForm = ContactEventform::select('created')
            ->whereId($contactEventFormId);

        return $eventForm->first();
    }

    public function getPeopleforSpecificContact($userId, $companyId, $contactId)
    {
        return ContactPersonnel::select(DB::raw("CONCAT(first_name,' ', last_name) AS full_name"), 'id', 'first_name', 'last_name', 'title')
            ->whereContact_id($contactId)
            ->orderBy('first_name', 'ASC')
            ->get()
            ->toArray();
    }
    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
