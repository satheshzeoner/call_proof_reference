<?php

namespace App\Repositories\EventForm;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Exception;

/* Models */

use App\Models\Contact\ContactEventform;
use App\Models\Contact\ContactEventformpersonnel;
use App\Models\Contact\ContactImage;

class EventFormDestroyRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */
    public function destroyContactEventForm($contatEventFormId)
    {
        try {
            DB::beginTransaction();

            $contactEventFormPersonnel = ContactImage::whereContact_event_form_id($contatEventFormId)
                ->delete();

            $contactEventFormPersonnel = ContactEventformpersonnel::whereContacteventform_id($contatEventFormId)
                ->delete();

            $contactEventForm = ContactEventform::whereId($contatEventFormId)
                ->delete();

            DB::commit();

            return true;
        } catch (Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
            return false;
        }
    }
    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
