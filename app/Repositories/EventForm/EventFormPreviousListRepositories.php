<?php

namespace App\Repositories\EventForm;

/* Models */

use App\Models\Contact\ContactEventform;
use App\Models\Contact\ContactEventformpersonnel;
use App\Models\Contact\ContactRep;
use App\Models\Company\Company;
use Illuminate\Support\Carbon;

class EventFormPreviousListRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */
    public function getPreviousEventDetails($userId, $companyId, $contactId, $searchEvent)
    {
        $eventDetailsId              = $this->getContactEventFormId($companyId, $userId, $contactId);

        $contactEventformpersonnelId = $this->getContactEventPersonalId($eventDetailsId);

        $eventDetails = ContactEventform::whereUser_id($userId);

        $eventDetails = $eventDetails->with(
            [
                'eventForm' => function ($eventFormJoin) use ($searchEvent) {
                    $eventFormJoin->select('id', 'name', 'position', 'points');
                    if ($searchEvent != '') {
                        $eventFormJoin->where('name', 'ilike', '%' . $searchEvent . '%');
                    }
                },
                'contactPersonnal' => function ($contactPersonnalJoin) use ($contactEventformpersonnelId) {
                    $contactPersonnalJoin->whereIn('id', $contactEventformpersonnelId);
                },
                'contactCompany' => function ($eventFormJoin) {
                    $eventFormJoin->select('id', 'name as company_name');
                }
            ]
        );

        $eventDetails = $eventDetails->whereContact_id($contactId);
        $eventDetails = $eventDetails->orderBy('id', 'DESC');
        $eventDetails = $eventDetails->paginate(config('constants.Pagination.eventItemsPerPage'));
        $eventDetails = $eventDetails->toArray();

        return $eventDetails;
    }

    public function getContactEventFormId($companyId, $userId, $contactId)
    {
        return ContactEventform::whereCompany_id($companyId)
            ->whereContact_id($contactId)
            ->whereUser_id($userId)
            ->select('id')
            ->get()->pluck('id')->toArray();
    }

    public function getContactEventPersonalId($eventDetailsId)
    {
        return ContactEventformpersonnel::whereIn('contacteventform_id', $eventDetailsId)->get()->pluck('personnel_id')->toArray();
    }

    public function getSubmittedByDetails($contactId)
    {

        $contactId =  ContactRep::whereContact_id($contactId)
            ->with(['userDetails' => function ($contactId) {
                $contactId->select('id', 'username', 'first_name', 'last_name');
            }])
            ->get()->toArray();
        $firstName = collect($contactId)->pluck('user_details')->pluck('first_name')->first();
        $lastName = collect($contactId)->pluck('user_details')->pluck('last_name')->first();
        $fullName = $firstName . ' ' . $lastName;
        return $fullName;
    }

    /* Get Company Details */
    public function getCompanyDetails($companyId)
    {
        return Company::whereId($companyId)->get()->first();
    }
    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
