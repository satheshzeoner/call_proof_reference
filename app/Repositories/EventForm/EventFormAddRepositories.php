<?php

namespace App\Repositories\EventForm;

use Exception;

/* Models */

use App\Models\Eventform\EventformCfield;
use App\Models\Cfield\Cfield;
use App\Models\Contact\Contact;
use App\Models\Contact\ContactEventform;
use App\Models\Contact\ContactEventformpersonnel;
use App\Models\Contact\ContactImage;
use App\Models\Cfield\CfieldValue;
use App\Models\Cfield\CfieldMultiValue;
use App\Models\Followup\Followup;
use App\Models\Followup\FollowupPersonnel;
use Illuminate\Support\Facades\DB;

class EventFormAddRepositories
{
    /* ===============================CREATE=============================== */

    /* Contact EventForm Store */
    public function storeContactEventForm($contactEventFormDetails)
    {
        $contactEventForm = ContactEventform::create($contactEventFormDetails);
        return $contactEventForm->id;
    }

    /* contact event form personnel Store */
    public function storeContactEventFormPersonnel($contactEventFormPersonelDetails)
    {
        $contactEventFormPersonel = ContactEventformpersonnel::create($contactEventFormPersonelDetails);
        return $contactEventFormPersonel->id;
    }

    /* custom Field Value Store */
    public function storeCustomFieldValue($customFieldValue)
    {
        $customFieldDetails = CfieldValue::create($customFieldValue);
        return $customFieldDetails->id;
    }

    /* followup ScheduleTask */
    public function storeFollowupValue($followupFieldValue)
    {
        $followUpDetails = Followup::create($followupFieldValue);
        return $followUpDetails->id;
    }

    /* followup personal ScheduleTask */
    public function storeFollowupPersonalValue($followupPersonnalValue)
    {
        $followUpPersonalDetails = FollowupPersonnel::create($followupPersonnalValue);
        return $followUpPersonalDetails->id;
    }

    /* Image Store Path File */
    public function storeCustomFieldFilePath($contactImageDetails)
    {
        $contactImage = ContactImage::create($contactImageDetails);
        return $contactImage->id;
    }

    /* MultiValue Options */
    public function storeMultiValueOption($multiValueDetails)
    {
        $customFieldValue = CfieldMultiValue::create($multiValueDetails);
        return $customFieldValue->id;
    }
    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */
    public function getEventFormAddDetails($userId, $companyId, $contactId, $eventFormId)
    {

        $eventFormField = $this->getEventFormIdList($eventFormId);

        $eventFormCfieldDetails = Cfield::select('id', 'name', 'cfield_type_id', 'rfield_id', 'cfield_option_default_id', 'position', 'points', 'cfield_id', 'required')
            ->with(['customFieldType'])
            ->with(['customFieldOption' => function ($cFieldOptionJoin) {
                $cFieldOptionJoin->orderBy('position', 'ASC');
            }])
            ->whereCompany_id($companyId)
            ->whereIn('id', $eventFormField['EventFormId'])
            ->where('name', 'not like', "%-----%")
            ->get()

            ->map(function ($cfieldDetailsQuery) {
                /* InCase of Empty with CustomerField Id  */
                if ($cfieldDetailsQuery['name'] == '') {
                    return $this->fillEmptyNameCfield($cfieldDetailsQuery);
                }

                return $cfieldDetailsQuery;
            })
            ->toArray();

        return $eventFormCfieldDetails;
    }

    public function getEventFormIdList($eventFormId)
    {
        $eventFormFields =  EventformCfield::whereEvent_form_id($eventFormId)
            ->orderBy('position', 'ASC')
            ->get()
            ->toArray();

        $eventResult['EventFormId']  = collect($eventFormFields)->pluck('cfield_id');
        $eventResult['Position']     = collect($eventFormFields)->pluck('position');

        return $eventResult;
    }

    public function fillEmptyNameCfield($cFieldEventDetails)
    {
        $cfieldBlankDetails = collect($cFieldEventDetails)->toArray();

        $eventFormCfieldDetails = Cfield::with(['customFieldType'])
            ->with(['customFieldOption' => function ($cFieldOptionJoin) {
                $cFieldOptionJoin->orderBy('position', 'ASC');
            }])
            ->whereId($cfieldBlankDetails['cfield_id'])
            ->get()
            ->toArray();
        return $eventFormCfieldDetails;
    }

    /* Get Coordination Details */
    public function getContactsCoordination($contactId)
    {
        return Contact::whereId($contactId)->first();
    }

    /* EDIT-Get ContactEventForm Details */
    public function getContactEventFormEditDetails($userId, $companyId, $contactId, $eventFormId, $contactEventFormId)
    {
        $eventFormField = $this->getEventFormIdList($eventFormId);

        $eventFormCfieldDetails = Cfield::select('id', 'name', 'cfield_type_id', 'rfield_id', 'cfield_option_default_id', 'position', 'points', 'cfield_id', 'required')
            ->with(['customFieldType'])
            ->with(['customFieldOption' => function ($cFieldOptionJoin) {
                $cFieldOptionJoin->orderBy('position', 'ASC');
            }])
            ->whereCompany_id($companyId)
            ->whereIn('id', $eventFormField['EventFormId'])
            ->where('name', 'not like', "%-----%")
            ->get()

            ->map(function ($cfieldDetailsQuery) use ($contactEventFormId) {
                /* InCase of Empty with CustomerField Id  */
                if ($cfieldDetailsQuery['name'] == '') {
                    return $this->fillEmptyNameCfield($cfieldDetailsQuery);
                }

                return $cfieldDetailsQuery;
            })
            ->toArray();

        return $eventFormCfieldDetails;
    }

    /* Get Specific ContactEventForm Details */
    public function getSpecificCustomFieldValues($contactEventFormId, $customFieldId)
    {
        $customFieldValue = CfieldValue::select('cf_value')->whereEntity_id($contactEventFormId)->whereCfield_id($customFieldId)->get()->toArray();
        if (!empty($customFieldValue)) {
            return (collect($customFieldValue)->pluck('cf_value')->first());
        } else {
            return '';
        }
    }

    /* Get Specific ContactEventForm Edit Details */
    public function getSpecificCustomFieldEditValues($contactEventFormId, $customFieldId, $typeId)
    {
        if ($typeId == 14) {
            $multiValue = CfieldMultiValue::whereCfield_id($customFieldId)
                ->whereEntity_id($contactEventFormId)
                ->get()
                ->pluck('option_id')->toArray();

            return implode(',', $multiValue);
        }

        $customFieldValue = CfieldValue::select('cf_value')->whereEntity_id($contactEventFormId)->whereCfield_id($customFieldId)->get()->toArray();
        if (!empty($customFieldValue)) {
            return (collect($customFieldValue)->pluck('cf_value')->first());
        } else {
            return '';
        }
    }


    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
