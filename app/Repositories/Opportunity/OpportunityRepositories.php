<?php

namespace App\Repositories\Opportunity;

use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;
use Exception;
/* Models */

use App\Models\Auth\AuthUser;
use App\Models\Users\UserMarket;
use App\Models\Opportunity\Opportunity;
use App\Models\Opportunity\OppType;
use App\Models\Opportunity\OppStage;
use App\Models\Opportunity\OppContact;
use App\Models\Opportunity\OppPersonnel;
use App\Models\Opportunity\OppHistory;


class OpportunityRepositories
{
    /* ===============================CREATE=============================== */

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */
    public function getOpportunityDetails($userId, $isMarketManager, $isManager, $userCompanyId, $oppUserId, $request)
    {
        $users = [];
        $marketIdList = UserMarket::where('user_id', $userId)->pluck('market_id');
        if ($isMarketManager) {
            if (count($marketIdList) > 0) {
                $marketUserIdList = UserMarket::where('company_id', $userCompanyId)->whereIn('market_id', $marketIdList)->distinct()->pluck('user_id');
                $users = AuthUser::WhereIn('id', $marketUserIdList)->pluck('id');
            } else {
                $users = AuthUser::where('id', $userId)->pluck('id');
            }
        } elseif ($isManager) {
            if (count($marketIdList) > 0) {
                $marketUserIdList = UserMarket::where('company_id', $userCompanyId)->whereIn('market_id', $marketIdList)->distinct()->pluck('user_id');
                $users = AuthUser::WhereIn('id', $marketUserIdList)->pluck('id');
            } else {
                $users = UserMarket::where('company_id', $userCompanyId)->pluck('user_id');
            }
        }

        if (count($users) > 0) {
            $user = AuthUser::where('id', $oppUserId)->first();
            if ($user) {
                $dataList = Opportunity::where('user_id', $user->id)->orderBy('created', 'DESC')->get();
            } else {
                $dataList = Opportunity::whereIn('user_id', $users)->orderBy('created', 'DESC')->get();
            }
        } else {
            $dataList = Opportunity::where('user_id', $userId)->orderBy('created', 'DESC')->get();
        }
        return $dataList;
    }
    public function getOpportunityTypeDetails($request, $userId, $userCompanyId)
    {
        $opportunityType = OppType::select('id', 'name')->where('company_id', $userCompanyId)->orderBy('position', 'ASC')
            ->get();
        return $opportunityType;
    }
    public function getopportunitiesStageDetails($request, $userId, $userCompanyId)
    {
        $opportunityStage = OppStage::select('id', 'name')->where('company_id', $userCompanyId)->orderBy('position', 'ASC')
            ->get();
        return $opportunityStage;
    }
    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */
    public function updateOpportunitiesDetails($request, $userCompanyId, $opportunityId)
    {
        try {
            $linkedAccounts = ($request->has('linked_accounts')) ? collect(json_decode($request->linked_accounts)) : [];
            $linkedContacts = ($request->has('linked_contacts')) ? collect(json_decode($request->linked_contacts))  : [];
            DB::beginTransaction();
            $opportunity = Opportunity::find($opportunityId);
            $opportunity->opp_name = $request->opportunity_name;
            $opportunity->opp_stage_id = $request->opportunity_stage_id;
            $opportunity->opp_type_id = $request->opportunity_type_id;
            $opportunity->user_id = $request->owner_id;
            $opportunity->fill($request->all());
            $opportunity->save();

            if ($opportunity) {
                $existingOpportunityContact = OppContact::where('company_id', $userCompanyId)->where('opp_id', $opportunity->id)->delete();
                foreach ($linkedAccounts as $key => $linkedAccount) {
                    $opportunityContact = new OppContact();
                    $opportunityContact->company_id = $userCompanyId;
                    $opportunityContact->contact_id = $linkedAccount;
                    $opportunityContact->opp_id = $opportunity->id;
                    $opportunityContact->created = now();
                    $opportunityContact->save();
                }
                foreach ($linkedContacts as $key => $linkedContact) {
                    $opportunityPersonnel = OppPersonnel::where('opp_id', $opportunity->id)->first();
                    if ($opportunityPersonnel) {
                        $opportunityPersonnel->opp_id = $opportunity->id;
                        $opportunityPersonnel->personnel_id = $linkedContact;
                        $opportunityPersonnel->save();
                    }
                }
                $opportunityContact = new OppContact();
                $opportunityContact->company_id = $userCompanyId;
                $opportunityContact->contact_id = $opportunity->contact_id;
                $opportunityContact->opp_id = $opportunity->id;
                $opportunityContact->created = now();
                $opportunityContact->save();
            }
            DB::commit();
            return $opportunity;
        } catch (Exception $e) {
            DB::rollBack();
            Helper::customError(500, $e->getMessage(), $request->all(), config('constants.ServiceName.Opportunity'));
            return false;
        }
    }
    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */
    /* Remove Specific Opportunity */
    public function removeOpportunity($userId, $companyId, $opportunityId)
    {
        return Opportunity::whereId($opportunityId)
            ->delete();
    }
    public function removeOpportunityContact($userId, $companyId, $opportunityId)
    {
        return OppContact::whereOpp_id($opportunityId)
            ->delete();
    }
    public function removeOpportunityHistory($userId, $companyId, $opportunityId)
    {
        return OppHistory::whereOpp_id($opportunityId)
            ->delete();
    }
    public function removeOpportunityPersonnel($userId, $companyId, $opportunityId)
    {
        return OppPersonnel::whereOpp_id($opportunityId)
            ->delete();
    }

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
