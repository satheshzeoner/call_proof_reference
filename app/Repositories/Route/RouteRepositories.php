<?php

namespace App\Repositories\Route;

/* Models */

use App\Models\Route\Route;
use App\Models\Route\RouteContact;
use App\Models\Contact\Contact;
use App\Models\Users\User;

use Illuminate\Support\Facades\DB;

use Exception;

class RouteRepositories
{
    /* ===============================CREATE=============================== */

    /* Store Route Details */
    public function storeRouteDetails($routeDetails)
    {
        $routeResult = Route::create($routeDetails);
        return $routeResult->id;
    }

    /* Store Route Details */
    public function storeRouteContactDetails($routeContactDetails)
    {
        $routeContactResult = RouteContact::create($routeContactDetails);
        return $routeContactResult->id;
    }
    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* Get CurrentRoute List with Contacts */
    public function getCurrentRouteList($userId, $companyId, $routeId)
    {
        return Route::whereUser_id($userId)
            ->whereCompany_id($companyId)
            ->whereId($routeId)
            ->with('routeContact')
            ->get()
            ->toArray();
    }

    /* RouteContactDetails */
    public function getRouteContactDetails($contactIdList)
    {
        return Contact::select('id as contact_id', DB::raw('CONCAT(first_name, \' \',last_name) as full_name'), 'first_name', 'last_name', 'address', 'latitude', 'longitude')
            ->whereIn('id', $contactIdList)
            ->where('first_name', '<>', '')
            ->whereNotNull('latitude')
            ->whereNotNull('longitude')
            ->get()
            ->toArray();
    }

    /* Get MyRoutes Details */
    public function getMyRoutes($userId, $companyId)
    {
        return Route::whereUser_id($userId)
            ->whereCompany_id($companyId)
            ->with('routeContact')
            ->get()
            ->toArray();
    }

    /* Get AllRoutes Details */
    public function getAllRoutes($companyId)
    {
        return Route::whereCompany_id($companyId)
            ->with('routeContact')
            ->get()
            ->toArray();
    }

    /* AssignedBy Details */
    public function assignedByDetails($assignedByUserId)
    {
        $userDetails = User::select(DB::raw('CONCAT(first_name, \' \',last_name) as assigned_by'))->whereId($assignedByUserId)
            ->first();
        return $userDetails->assigned_by;
    }

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */
    /* Update Route Details */
    public function updateRouteDetails($routeDetails, $routeId)
    {
        $routeResult = Route::whereId($routeId)
            ->update($routeDetails);
        return true;
    }

    /* Update Route Details */
    public function updateRouteContactDetails($routeContactDetails, $routeId)
    {
        $routeContactResult = RouteContact::whereRoute_id($routeId)
            ->update($routeContactDetails);
        return true;
    }


    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* Remove Specific Routes */
    public function removeRoutes($userId, $companyId, $routeId)
    {
        return Route::whereCompany_id($companyId)
            ->whereUser_id($userId)
            ->whereId($routeId)
            ->delete();
    }

    /* Remove Specific Routes Contact */
    public function removeRoutesContact($userId, $companyId, $routeId)
    {
        return RouteContact::whereCompany_id($companyId)
            ->whereUser_id($userId)
            ->whereRoute_id($routeId)
            ->delete();
    }
    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
