<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Users\User;
use Laravel\Passport\Passport;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
    }

    public function test_access_token()
    {
        $response = $this->get('api/access-token');

        $response->assertStatus(200);
    }

    public function test_login()
    {
        $response = $this->post('api/login');

        $response->assertStatus(200);
    }

    public function test_login_param()
    {
        // $response = $this->post('api/login', [
        //     'email' => 'rricks@fastsigns.com',
        //     'password' => '12345678',
        // ]);

        // $response->dump();

        // $response
        //     ->assertStatus(200);

        // Session::start();
        // $response = $this->call('POST', 'api/login', array(
        //     '_token' => csrf_token(),
        //     'email' => 'rricks@fastsigns.com',
        //     'password' => '12345678',
        // ));

        // $response->dumpHeaders();

        // $response->dumpSession();

        // $response->dump();

        // $response->assertSee($response);

        // $response->assertStatus(200);
        // $this->assertRedirectedTo('questions');

        $userData = [
            "company"   => "test03",
            "contact"   => "John Doe",
            "email"     => "doe1@example.com",
            "phone"     => "8754874511",
            "password"  => "123456"
        ];

        $this->json('POST', 'http://127.0.0.1:8000/api/register', $userData, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                'clientStatusCode',
            ]);
    }

    /* Fail Case */
    public function test_dashboard()
    {
        $response = $this->post('api/dashboard-detail');

        $response->assertStatus(401);
    }

    public function test_registration()
    {
        $response = $this->json('POST', 'api/register', ['Accept' => 'application/json'])
            ->assertStatus(200);
    }
}
