<?php

namespace Tests\Unit\API\Dashboard;

use Tests\TestCase;

class dashboardTest extends TestCase
{
    public function testWidgets()
    {
        $response = $this->withHeader('Authorization', $this->token)
            ->json('GET', '/api/widgets', []);

        $response->assertStatus($this->assertStatusSuccessCode);
        $response->assertJsonCount($this->assertJsonCountActualCode);
        $response->assertJsonStructure($this->assertJsonStructureCode);
    }

    public function testDashboardDetails()
    {
        $response = $this->withHeader('Authorization', $this->token)
            ->json('POST', '/api/dashboard-details', ['timeframe' => '1w']);

        $response->assertStatus($this->assertStatusSuccessCode);
        $response->assertJsonCount($this->assertJsonCountActualCode);
        $response->assertJsonStructure($this->assertJsonStructureCode);
    }

    public function testWidgetsUpdate()
    {
        $response = $this->withHeader('Authorization', $this->token)
            ->json('PUT', '/api/widgets', [
                'widgets' => '[{"name": "Appointments","id": 1,"position": 1},{"name": "Calls","id": 2,"position": 2}]',
                'widgets_available' => '[{"name": "Emails","id": 3,"position": 0}]'
            ]);

        $response->assertStatus($this->assertStatusSuccessCode);
        $response->assertJsonCount($this->assertJsonCountActualCode);
        $response->assertJsonStructure($this->assertMessageJsonStructureCode);
    }
}
