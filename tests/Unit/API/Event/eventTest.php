<?php

namespace Tests\Unit\API\Event;

use Tests\TestCase;

class eventTest extends TestCase
{
    public function testEventType()
    {

        // define your $token here
        $response = $this->withHeader('Authorization', $this->token)
            ->json('GET', '/api/event-type', []);

        // $response->assertUnauthorized();
        $response->assertStatus(200);
        $response->assertJsonCount(5);
        $response->assertJsonStructure([
            'clientStatusCode',
            'user_id',
            'company_id',
            'data',
            'uri'
        ]);
    }

    public function testEvents()
    {
        // define your $token here
        $response = $this->withHeader('Authorization', $this->token)
            ->json('GET', '/api/events', []);

        $response->assertStatus(200);
        $response->assertJsonCount(19);
        $response->assertJsonStructure([
            'clientStatusCode',
            'user_id',
            'company_id',
            'data',
            'uri'
        ]);
    }
}
