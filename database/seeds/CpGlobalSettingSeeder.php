<?php

use Illuminate\Database\Seeder;

class CpGlobalSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('users')->get()->count() == 0) {

            DB::table('users')->insert([
                'name' => 'Default Template Account',
                'key' => 'default_template_account',
                'value' => 10900,
                'created_at' => now()
            ]);

            DB::table('users')->insert([
                'name' => 'User1',
                'email' => 'user1@email.com',
                'password' => bcrypt('password'),
            ]);
        } else {
            echo "Table is not empty";
        }
    }
}
