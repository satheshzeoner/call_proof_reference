<?php

use Illuminate\Database\Seeder;

class CpWidgetlistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // check if table users is empty
        if (DB::table('cp_widgetlist')->get()->count() == 0) {

            DB::table('cp_widgetlist')->insert([

                [
                    'widget_type' => 'appointments',
                    'name' => 'Appointments',
                    'created_at' => now(),
                ],
                [
                    'widget_type' => 'calls',
                    'name' => 'Calls',
                    'created_at' => now(),
                ],
                [
                    'widget_type' => 'emails',
                    'name' => 'Emails',
                    'created_at' => now(),
                ],

            ]);
        } else {
            echo "Table is not empty";
        }
    }
}
