<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CalculatingDistanceExtensions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //delete if exists, support default point function for calculating distance in contact list 
        DB::statement('
        drop extension if exists cube CASCADE;
       ');
        //create query for support default point function for calculating distance in contact list
        DB::statement('
        create extension cube;
       ');
        DB::statement('
        create extension earthdistance;
       ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
